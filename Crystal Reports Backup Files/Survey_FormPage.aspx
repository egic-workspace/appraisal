﻿<%@ Page Language="C#" MasterPageFile="~/Main_Master_Page.master" AutoEventWireup="true" CodeFile="Survey_FormPage.aspx.cs" Inherits="Survey_Survey_FormPage" Title="Survey Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <table style="width: 100%;" align="center">
 <tr>
    <td style="background-color:#e6f1f8; height: 2px; width: 70%;" >
    <span  style="font-size:11pt; font-weight:bold; color:#000080; font-family:Courier New;">
        &nbsp; Managers Survey </span>
    </td></tr>
 </table>
 
    <table align="left" dir="ltr" style="width: 100%; height: 99%; float: left">
        <tr>
            <td style="width: 623px">
                <asp:Label ID="Label21" runat="server" Font-Bold="True" Font-Size="11pt" 
                    ForeColor="Red" Text="Employee Code :-" Width="130px"></asp:Label>
                <asp:Label ID="lbl_emp_code" runat="server" Font-Bold="True" Font-Size="11pt" 
                    ForeColor="Black" Text="--------------------"></asp:Label>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            </td>
            <td align="right" dir="ltr">
                <asp:Label ID="Label27" runat="server" Font-Bold="True" Font-Size="11pt" 
                    ForeColor="Blue" Text="Open Year :-" Width="120px"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="txt_year" runat="server" Font-Bold="True" ReadOnly="True" 
                    Width="100px" Height="20px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td style="width: 623px">
                <asp:Label ID="Label22" runat="server" Font-Bold="True" Font-Size="11pt" 
                    ForeColor="Red" Text="Employee Name :-" Width="130px"></asp:Label>
                <asp:Label ID="lbl_emp_name" runat="server" Font-Bold="True" Font-Size="11pt" 
                    ForeColor="Black" Text="--------------------"></asp:Label>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            </td>
            <td align="right" dir="ltr">
                <asp:Label ID="Label24" runat="server" Font-Bold="True" Font-Size="11pt" 
                    ForeColor="Blue" Height="18px" Text="Total Results :-" Width="120px" 
                    Visible="False"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="txt_top_result" runat="server" Font-Bold="True" 
                    ReadOnly="True" Width="100px" Visible="False"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td style="width: 623px">
                <asp:Label ID="Label23" runat="server" Font-Bold="True" Font-Size="11pt" 
                    ForeColor="Red" Text="Job Name :-" Width="130px"></asp:Label>
                <asp:Label ID="lbl_job_name" runat="server" Font-Bold="True" Font-Size="11pt" 
                    ForeColor="Black" Text="--------------------"></asp:Label>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            </td>
            <td align="right" dir="ltr">
                <asp:Label ID="Label26" runat="server" Font-Bold="True" Font-Size="11pt" 
                    ForeColor="Blue" Height="18px" Text="Manager :-" Width="120px"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="txt_manager" runat="server" Font-Bold="True" ReadOnly="True" 
                    Width="200px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <asp:Image ID="Image12" runat="server" ImageUrl="~/Images/line3.png" />
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:Label ID="Label8" runat="server" Font-Bold="True" Font-Size="11pt" 
                    ForeColor="#333399" 
                    Text="1. Could the employee perform at a higher level, in a different position or take on increased responsibilities within the next year (consider the person’s ability only, not whether there is a position available to support this growth)?"></asp:Label>
            </td>
            <td align="center" rowspan="2">
                <asp:DropDownList ID="ddl_q1" runat="server" Font-Bold="True" Font-Size="15pt" 
                    ForeColor="#FF0066">
                    <asp:ListItem Value="S">Select</asp:ListItem>
                    <asp:ListItem Value="1">Yes</asp:ListItem>
                    <asp:ListItem Value="0">No</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td colspan="2" dir="rtl">
                <asp:Label ID="Label28" runat="server" Font-Bold="True" Font-Size="11pt" 
                    ForeColor="#333399" 
                    
                    Text="هل يستطيع الموظف  أن يؤدي أداء أعلى في وظيفة مختلفة أو أن يتحمل مسؤوليات أكثر خلال العام المقبل؟"></asp:Label>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <asp:Image ID="Image1" runat="server" ImageUrl="~/Images/line3.png" />
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:Label ID="Label9" runat="server" Font-Bold="True" Font-Size="11pt" 
                    ForeColor="#333399" 
                    Text="2. Could the employee perform at a higher level, in a different position, or take on increased responsibilities within the next three years (consider the person’s ability only, not whether there is a position available to support this growth)?"></asp:Label>
            </td>
            <td align="center" rowspan="2">
                <asp:DropDownList ID="ddl_q2" runat="server" Font-Bold="True" Font-Size="15pt" 
                    ForeColor="#FF0066">
                    <asp:ListItem Value="S">Select</asp:ListItem>
                    <asp:ListItem Value="1">Yes</asp:ListItem>
                    <asp:ListItem Value="0">No</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td colspan="2" dir="rtl">
                <asp:Label ID="Label29" runat="server" Font-Bold="True" Font-Size="11pt" 
                    ForeColor="#333399" 
                    
                    Text="هل يستطيع الموظف  أن يؤدي أداء أعلى في وظيفة مختلفة أو أن يتحمل مسؤوليات أكثر خلال ال3 اعوام المقبلة؟"></asp:Label>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <asp:Image ID="Image2" runat="server" ImageUrl="~/Images/line3.png" />
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:Label ID="Label10" runat="server" Font-Bold="True" Font-Size="11pt" 
                    ForeColor="#333399" 
                    Text="3. Can you envision this employee performing two levels above his or her current position in the next five to six years?"></asp:Label>
            </td>
            <td align="center" rowspan="2">
                <asp:DropDownList ID="ddl_q3" runat="server" Font-Bold="True" Font-Size="15pt" 
                    ForeColor="#FF0066">
                    <asp:ListItem Value="S">Select</asp:ListItem>
                    <asp:ListItem Value="1">Yes</asp:ListItem>
                    <asp:ListItem Value="0">No</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td colspan="2" dir="rtl">
                <asp:Label ID="Label30" runat="server" Font-Bold="True" Font-Size="11pt" 
                    ForeColor="#333399" 
                    
                    Text="هل يمكن أن تتوقع  لهذا الموظف أن يؤدى وظيفه اعلى بدرجتين من وظيفته الحاليه  في السنوات الخمس إلى الست المقبلة؟"></asp:Label>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <asp:Image ID="Image3" runat="server" ImageUrl="~/Images/line3.png" />
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:Label ID="Label11" runat="server" Font-Bold="True" Font-Size="11pt" 
                    ForeColor="#333399" 
                    Text="4. Is the organization likely to value growth of the skills and competencies of this employee over the next several years?"></asp:Label>
            </td>
            <td align="center" rowspan="2">
                <asp:DropDownList ID="ddl_q4" runat="server" Font-Bold="True" Font-Size="15pt" 
                    ForeColor="#FF0066">
                    <asp:ListItem Value="S">Select</asp:ListItem>
                    <asp:ListItem Value="1">Yes</asp:ListItem>
                    <asp:ListItem Value="0">No</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td colspan="2" dir="rtl">
                <asp:Label ID="Label31" runat="server" Font-Bold="True" Font-Size="11pt" 
                    ForeColor="#333399" 
                    
                    Text="هل تركز الشركة على تنميه مهارات وكفاءات هذا الموظف على مدى السنوات القادمة؟"></asp:Label>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <asp:Image ID="Image4" runat="server" ImageUrl="~/Images/line3.png" />
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:Label ID="Label12" runat="server" Font-Bold="True" Font-Size="11pt" 
                    ForeColor="#333399" 
                    Text="5. Could the employee learn the additional skills and competencies he or she needs to be able to perform at a higher or different level?"></asp:Label>
            </td>
            <td align="center" rowspan="2">
                <asp:DropDownList ID="ddl_q5" runat="server" Font-Bold="True" Font-Size="15pt" 
                    ForeColor="#FF0066">
                    <asp:ListItem Value="S">Select</asp:ListItem>
                    <asp:ListItem Value="1">Yes</asp:ListItem>
                    <asp:ListItem Value="0">No</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td colspan="2" dir="rtl">
                <asp:Label ID="Label32" runat="server" Font-Bold="True" Font-Size="11pt" 
                    ForeColor="#333399" 
                    
                    Text="هل يستطيع الموظف أن يتعلم المهارات والكفاءات الإضافية التي يحتاجها ليكون قادرا على الأداء فى وظيفة أعلى؟"></asp:Label>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <asp:Image ID="Image5" runat="server" ImageUrl="~/Images/line3.png" />
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:Label ID="Label14" runat="server" Font-Bold="True" Font-Size="11pt" 
                    ForeColor="#333399" 
                    
                    Text="6. Does the employee demonstrate leadership ability—by showing initiative and vision, delivering on promised results, communicating effectively, and taking appropriate risks?"></asp:Label>
            </td>
            <td align="center" rowspan="2">
                <asp:DropDownList ID="ddl_q6" runat="server" Font-Bold="True" Font-Size="15pt" 
                    ForeColor="#FF0066">
                    <asp:ListItem Value="S">Select</asp:ListItem>
                    <asp:ListItem Value="1">Yes</asp:ListItem>
                    <asp:ListItem Value="0">No</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td colspan="2" dir="rtl">
                <asp:Label ID="Label34" runat="server" Font-Bold="True" Font-Size="11pt" 
                    ForeColor="#333399" 
                    
                    Text="ھل الموظف لديه القدرة القیادیة من خلال عرض المبادرة والرؤیة، وتوفیر النتائج المرجوه والتواصل الفعال؟"></asp:Label>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <asp:Image ID="Image7" runat="server" ImageUrl="~/Images/line3.png" />
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:Label ID="Label15" runat="server" Font-Bold="True" Font-Size="11pt" 
                    ForeColor="#333399" 
                    
                    Text="7. Does the employee demonstrate an ability to comfortably interact with people at a higher level or in different areas?"></asp:Label>
            </td>
            <td align="center" rowspan="2">
                <asp:DropDownList ID="ddl_q7" runat="server" Font-Bold="True" Font-Size="15pt" 
                    ForeColor="#FF0066">
                    <asp:ListItem Value="S">Select</asp:ListItem>
                    <asp:ListItem Value="1">Yes</asp:ListItem>
                    <asp:ListItem Value="0">No</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td colspan="2" dir="rtl">
                <asp:Label ID="Label35" runat="server" Font-Bold="True" Font-Size="11pt" 
                    ForeColor="#333399" 
                    
                    Text="هل الموظف لديه القدرة على التفاعل بشكل فعال مع مختلف انواع الموظفين والمديرين؟"></asp:Label>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <asp:Image ID="Image8" runat="server" ImageUrl="~/Images/line3.png" />
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:Label ID="Label16" runat="server" Font-Bold="True" Font-Size="11pt" 
                    ForeColor="#333399" 
                    
                    Text="8. Does the employee demonstrate comfort with a broader company perspective than his or her job currently requires?"></asp:Label>
            </td>
            <td align="center" rowspan="2">
                <asp:DropDownList ID="ddl_q8" runat="server" Font-Bold="True" Font-Size="15pt" 
                    ForeColor="#FF0066">
                    <asp:ListItem Value="S">Select</asp:ListItem>
                    <asp:ListItem Value="1">Yes</asp:ListItem>
                    <asp:ListItem Value="0">No</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td colspan="2" dir="rtl">
                <asp:Label ID="Label36" runat="server" Font-Bold="True" Font-Size="11pt" 
                    ForeColor="#333399" 
                    
                    Text=" ھل یتمتع الموظف بمنظور واسع  للشرکة أکثر مما تحتاجه وظيفته الحالیة؟"></asp:Label>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <asp:Image ID="Image9" runat="server" ImageUrl="~/Images/line3.png" />
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:Label ID="Label17" runat="server" Font-Bold="True" Font-Size="11pt" 
                    ForeColor="#333399" 
                    
                    Text="9. Does the employee demonstrate flexibility and motivation to move into a job that might be different than any that currently exist?"></asp:Label>
            </td>
            <td align="center" rowspan="2">
                <asp:DropDownList ID="ddl_q9" runat="server" Font-Bold="True" Font-Size="15pt" 
                    ForeColor="#FF0066">
                    <asp:ListItem Value="S">Select</asp:ListItem>
                    <asp:ListItem Value="1">Yes</asp:ListItem>
                    <asp:ListItem Value="0">No</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td colspan="2" dir="rtl">
                <asp:Label ID="Label37" runat="server" Font-Bold="True" Font-Size="11pt" 
                    ForeColor="#333399" 
                    
                    Text="هل الموظف لديه المرونة والحافز لاداء وظيفة اخرى قد تكون مختلفة عن وظيفته الحالية؟"></asp:Label>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <asp:Image ID="Image10" runat="server" ImageUrl="~/Images/line3.png" />
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:Label ID="Label18" runat="server" Font-Bold="True" Font-Size="11pt" 
                    ForeColor="#333399" 
                    
                    Text="10. Does the employee welcome opportunities for learning and development?"></asp:Label>
            </td>
            <td align="center" rowspan="2">
                <asp:DropDownList ID="ddl_q10" runat="server" Font-Bold="True" Font-Size="15pt" 
                    ForeColor="#FF0066">
                    <asp:ListItem Value="S">Select</asp:ListItem>
                    <asp:ListItem Value="1">Yes</asp:ListItem>
                    <asp:ListItem Value="0">No</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td colspan="2" dir="rtl">
                <asp:Label ID="Label38" runat="server" Font-Bold="True" Font-Size="11pt" 
                    ForeColor="#333399" 
                    Text="هل يرحب الموظف بفرص التعلم والتطوير؟"></asp:Label>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <asp:Image ID="Image11" runat="server" ImageUrl="~/Images/line3.png" />
            </td>
        </tr>
        <tr>
            <td align="center" colspan="3">
                <asp:Label ID="Label19" runat="server" Font-Bold="True" Font-Size="11pt" 
                    ForeColor="Red" Text="Total Results :-" Visible="False"></asp:Label>
                <asp:TextBox ID="txt_result" runat="server" Font-Bold="True" ReadOnly="True" 
                    Width="150px" Visible="False"></asp:TextBox>
                <asp:Label ID="Label20" runat="server" Font-Bold="True" Font-Size="11pt" 
                    ForeColor="Red" Text="%" Visible="False"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="center" colspan="3">
                <asp:Button ID="btn_check" runat="server" Font-Bold="True" 
                    onclick="btn_check_Click" class="btn btn-primary"  Text="Check Survey" Width="500px" 
                    Visible="False" />
            </td>
        </tr>
        <tr>
            <td align="center" colspan="3">
                <asp:Button ID="btn_save" runat="server" Font-Bold="True" 
                    onclick="btn_save_Click" class="btn btn-primary"  Text="Save Survey" Width="500px" />
            </td>
        </tr>
        <tr>
            <td align="center" colspan="3">
                <asp:Label ID="lbl_rec_date" runat="server" Font-Bold="True" Font-Size="11pt" 
                    ForeColor="Blue" Text="00/00/0000" Width="120px"></asp:Label>
            </td>
        </tr>
    </table>
 
</asp:Content>

