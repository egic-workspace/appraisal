﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="~/FinalVersion/MyControles/NY_Objectives.ascx.cs" Inherits="Final_Version_NY_Objectives" %>
  <link href="../../CSS/StyleSheet.css" rel="stylesheet" type="text/css" />
  <link href="../../CSS/StyleSheet2.css" rel="stylesheet" type="text/css" />
  <link href="../../CSS/bootstrap.min.css" rel="stylesheetxx" />
  <script type="text/javascript">
     function CheckNumeric(e) {
        if (window.event && ((e.keyCode < 48 || e.keyCode > 57) & e.keyCode != 8)) {
            event.returnValue = false;
            return false;
        }
        else if ((e.which < 48 || e.which > 57) & e.which != 8) {
            e.preventDefault();
            return false;
        }
    }
  </script>

<asp:GridView ID="Gridview1" runat="server" ShowFooter="true"  Width="100%" CssClass="mGrid"                    
                      AlternatingRowStyle-CssClass="alt"  OnRowDeleting="OnRowDeleting" 
                      PagerStyle-CssClass="pgr"  AutoGenerateColumns="false"   Font-Names="Arial" >
        <Columns>
        <asp:BoundField DataField="RowNumber" HeaderText="م" Visible="false" />
        
        <asp:TemplateField HeaderStyle-Width="400px"  HeaderText="الهدف">
            <ItemTemplate>
                <div class="flex-container">
            <asp:TextBox ID="txtNYObjective" Width="100%" onload="txtNYObjective_Load" class="form-control fill-width" Font-Size="14pt" TextMode="MultiLine"  Text='<%# Bind("obj_desc") %>' runat="server"></asp:TextBox>
                    </div>
            </ItemTemplate>
        </asp:TemplateField>
        
        <asp:TemplateField HeaderStyle-Width="85px" HeaderText="الوزن" ItemStyle-CssClass="v-align-top">
            <ItemTemplate>
                <asp:TextBox ID="txtNYWeight" Width="100%" class="form-control" onkeypress="CheckNumeric(event);" Font-Size="14pt" Text='<%# Bind("obj_weight") %>' runat="server"></asp:TextBox>
            </ItemTemplate>
        </asp:TemplateField>
     
        <asp:TemplateField HeaderStyle-Width="150px" HeaderText = "موعد التحقق" ItemStyle-CssClass="v-align-top">
            <ItemTemplate>
                <asp:DropDownList ID="ddlDate" Width="150px" runat="server" Font-Bold="True" Font-Size="11pt" Text='<%# Bind("QUART") %>' >
                    <asp:ListItem Value="0">Select</asp:ListItem>
                    <asp:ListItem Value="1">Q1</asp:ListItem>
                    <asp:ListItem Value="2">Q2</asp:ListItem>
                    <asp:ListItem Value="3">Q3</asp:ListItem>
                    <asp:ListItem Value="4">Q4</asp:ListItem>
                </asp:DropDownList>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderStyle-Width="150px" HeaderText="">
            <ItemTemplate>
            <asp:Button ID="Buttondelete" runat="server" class="btn btn-primary"  BackColor="#F79222" Font-Bold="True" 
             Font-Names="Arial" Font-Size="12pt" Width="100%" CommandName="Delete" OnRowDataBound="OnRowDataBound"  Text="حـذف" CausesValidation="False" />
            </ItemTemplate>
             <FooterStyle HorizontalAlign="Right" />
            <FooterTemplate>
             <asp:Button ID="ButtonAdd" runat="server" class="btn btn-primary"  BackColor="#F79222" Font-Bold="True" 
             Font-Names="Arial" Font-Size="12pt" Width="100%" onclick="ButtonAdd_Click" Text="إضافة هدف جديد" CausesValidation="False" />
            </FooterTemplate>
        </asp:TemplateField>
        
        </Columns>
</asp:GridView>
<p>
   <asp:Image ID="Image1" runat="server" ImageUrl="~/Images/line3.png" Width="100%" Height="10px"  />
</p>