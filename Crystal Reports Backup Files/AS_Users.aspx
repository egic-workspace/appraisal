﻿<%@ Page Language="C#" MasterPageFile="~/Main_Master_Page.master" AutoEventWireup="true" CodeFile="AS_Users.aspx.cs" Inherits="Basic_Data_AS_Users" Title="المستخدمين" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <script type = "text/javascript">
        function Confirm() {
            var confirm_value = document.createElement("INPUT");
            confirm_value.type = "hidden";
            confirm_value.name = "confirm_value";
            if (confirm("هل تريد حذف هذه البيانات ?")) {
                confirm_value.value = "Yes";
            } else {
                confirm_value.value = "No";
            }
            document.forms[0].appendChild(confirm_value);
        }
    </script>
      <table style="width: 100%;" align="center">
 <tr>
    <td style="background-color:#71625F; height: 2px; width: 70%;" >
    <span  style="font-size:11pt; font-weight:bold; color:#FFFFFF; font-family:Courier New;">
        &nbsp; المستخدمين </span>
    </td></tr>
 </table><p>
    </p>
    <p>
        <table align="center" 
            
            style="width: 80%; height: 100px; float: right; margin-top: 50px; margin-right: 30px; ">
            <tr>
                <td align="center">
            <asp:Label ID="lbl_MSG" runat="server" Font-Bold="True" ForeColor="Red" 
                        Font-Size="12pt" Font-Names="Arial"></asp:Label>
                </td>
                <td>
                    &nbsp;</td>
                <td rowspan="11">
            <asp:GridView ID="GridView1" runat="server" AllowPaging="True" 
                EmptyDataText="لا توجد اسماء مسجلة" Caption="المستخدمين" AutoGenerateColumns="False" 
                CaptionAlign="Top" onselectedindexchanged="GridView1_SelectedIndexChanged" 
                CssClass="mGrid" PagerStyle-CssClass="pgr" 
                AlternatingRowStyle-CssClass="alt" AutoGenerateSelectButton="True" 
                DataKeyNames="USER_NAME" onpageindexchanging="GridView1_PageIndexChanging" 
                onselectedindexchanging="GridView1_SelectedIndexChanging" Width="500px" 
                        Font-Names="Arial" Font-Size="13pt">
                
                <Columns>
                        <asp:TemplateField HeaderText="اسم المستخدم" SortExpression="Value">
                            <ItemTemplate>
                                <asp:Label ID="lbluser" runat="server" Text='<%# Bind("USER_NAME") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        
                    </Columns>
                
                <PagerStyle />
                <AlternatingRowStyle />
            </asp:GridView>
                </td>
            </tr>
            <tr>
                <td>
            <asp:Label ID="Label8" runat="server" Font-Bold="True" Text="اسم المستخدم" 
                Width="76px" Font-Size="12pt" Font-Names="Arial" ForeColor="#231F20"></asp:Label>
                    <asp:TextBox ID="txt_user_name" runat="server" Font-Bold="True" Font-Size="13pt" 
                        Width="250px"></asp:TextBox>
                </td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td align="center">
                    <asp:Label ID="Label9" runat="server" Font-Bold="True" Font-Size="10pt" 
                        Text="برجاء كتابة اسم المستخدم كما هو بقاعدة بيانات Active Directory" 
                        Width="300px"></asp:Label>
                </td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td align="center">
            <asp:Button ID="btn_new" runat="server" class="btn btn-primary" BackColor="#F79222"  Font-Bold="True" 
                Font-Size="15pt" ForeColor="White" Text="جديد" Width="100px" 
                onclick="btn_new_Click" />
            <asp:Button ID="btn_save" runat="server" class="btn btn-primary" BackColor="#F79222" Font-Bold="True" 
                Font-Size="15pt" ForeColor="White" Text="تسجيل" Width="100px" 
                onclick="btn_save_Click" />
            <asp:Button ID="btn_delete" runat="server" class="btn btn-primary" BackColor="#F79222"  Font-Bold="True" 
                Font-Size="15pt" ForeColor="White" Text="إلغاء" Width="100px" 
                onclick="btn_delete_Click" onclientclick="Confirm()" />
                </td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
        </table>
        <br />
    </p>
</asp:Content>

