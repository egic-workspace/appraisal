﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="~/MyControles/EmpObjectivesControl.ascx.cs" Inherits="MyControles_EmpObjectivesControl" %>
  <link href="../CSS/StyleSheet.css" rel="stylesheet" type="text/css" />
  <link href="../CSS/StyleSheet2.css" rel="stylesheet" type="text/css" />
  <link rel="stylesheetxx" href="../CSS/bootstrap.min.css"/>

<asp:GridView 
    ID="Gridview1" 
    runat="server" 
    ShowFooter="true" 
    Width="100%" 
    CssClass="mGrid" 
    AlternatingRowStyle-CssClass="alt" 
    PagerStyle-CssClass="pgr" 
    AutoGenerateColumns="false" 
    Font-Names="Arial" >
        <Columns>

        <asp:TemplateField HeaderStyle-Width="350px"  HeaderText="الهدف">
            <ItemTemplate>
            <asp:TextBox ID="txt_obj_desc" Enabled="false" class="form-control" Width="100%" Font-Size="13pt" TextMode="MultiLine" Text='<%# Bind("obj_desc") %>' runat="server"></asp:TextBox>
            </ItemTemplate>
        </asp:TemplateField>
        
        <asp:TemplateField HeaderStyle-Width="30px" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" HeaderText="الوزن">
            <ItemTemplate>
                <asp:TextBox ID="txt_obj_weight" Enabled="false" class="form-control text-center" Font-Size="13pt" Text='<%# Bind("obj_weight") %>' runat="server"></asp:TextBox>
            </ItemTemplate>
        </asp:TemplateField>

        <asp:TemplateField HeaderStyle-Width="350px"  HeaderText="سبب التغيير" >
            <ItemTemplate>
                <asp:TextBox ID="txt_obj_notes" Enabled="false" TextMode="MultiLine" Width="100%" class="form-control" Font-Size="13pt" Text='<%# Bind("obj_notes") %>' runat="server"></asp:TextBox>
            </ItemTemplate>
        </asp:TemplateField>

        </Columns>
</asp:GridView>
 
<p>
  <asp:Image ID="Image1" runat="server" ImageUrl="~/Images/line3.png" Width="100%" Height="10px" />
</p>