﻿<%@ Page Language="C#" MasterPageFile="~/Main_Master_Page.master" AutoEventWireup="true" CodeFile="Nass_Enter.aspx.cs" Inherits="NewAssesment_Nass_Enter" Title="  ادخال تقييمات الموظفين" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="../CSS/bootstrap.min.css">
  <script src="../JavaScript/jquery.min.js"></script>
  <script src="../JavaScript/bootstrap.min.js"></script>
  <script type="text/javascript" src="JavaScript/loader.js"></script>
 <%-- https://www.gstatic.com/charts/loader.js--%>
    <script type="text/javascript">
       //html { overflow-y: hidden; }
      google.charts.load("current", {packages:["corechart"]});
      google.charts.setOnLoadCallback(drawChart);
      google.charts.setOnLoadCallback( draw_Standard_Chart);
     
         
       function drawChart() {
       
      var exll = document.getElementById('<%=lbl_exelent.ClientID%>').innerHTML.replace("%", "") ;
      var good = document.getElementById('<%=lbl_good.ClientID%>').innerHTML.replace("%", "") ;
      var vgood = document.getElementById('<%=lbl_very_good.ClientID%>').innerHTML.replace("%", "") ;
      var bad = document.getElementById('<%=lbl_bad.ClientID%>').innerHTML.replace("%", "") ;
      var data = google.visualization.arrayToDataTable([
          ['Rank', 'Percentage'],
          ['إستثنائي',   Math.ceil(exll)],
          ['يقوق التوقعات',  Math.ceil(vgood)],
           ['يحقق التوقعات',     Math.ceil(good) ],
          ['متوسط – غير مرضي', Math.ceil(bad)]
        ]);

        var options = {
          title: 'محقق التقييم',
          is3D: true,
        };

       // var chart = new google.visualization.PieChart(document.getElementById('piechart_3d'));
         var chart = new google.visualization.LineChart(document.getElementById('piechart_3d'));
        chart.draw(data, options);
      }
      
    function draw_Standard_Chart() {
       
      var data = google.visualization.arrayToDataTable([
          ['Rank', 'Percentage'],
          ['إستثنائي',   5],
          ['يفوق التوقعات',  15],
          ['يحقق التوقعات',    70 ],
          ['متوسط – غير مرضي', 10]
        ]);

        var options = {
          title: 'هدف التقييم',
          is3D: true,
        };

       // var chart = new google.visualization.PieChart(document.getElementById('piechart_3d2'));
       var chart = new google.visualization.LineChart(document.getElementById('piechart_3d2'));
        chart.draw(data, options);
      }
      
          function ZoomDiv1() {
           var panel = document.getElementById('piechart_3d');

        var printWindow = window.open('', '', 'scrollbars=yes,  resizable=yes, width=900, height=500');
        
        printWindow.document.write('<html><head><title> My Charts </title>');
        printWindow.document.write('');
        printWindow.document.write('</head><body dir="rtl" style="zoom: 2" >');
        printWindow.document.write('');
        printWindow.document.write(panel.innerHTML);
        printWindow.document.write('</body></html>');
        printWindow.document.close();

          return false;
    }
    
      function ZoomDiv2() {

        var panel2 = document.getElementById('piechart_3d2');

        var printWindow = window.open('', '', 'scrollbars=yes,  resizable=yes, width=900, height=500');
        	
        printWindow.document.write('<html><head><title> My Charts </title>');
        printWindow.document.write('');
        printWindow.document.write('</head><body dir="rtl" style="zoom: 2">');
        printWindow.document.write('');
        printWindow.document.write(panel2.innerHTML);
        printWindow.document.write('</body></html>');
        printWindow.document.close();

          return false;
    }
    
     function showModal() {
            $("#myModal").modal('show');
        }

        $(function () {
            $("#btnShow").click(function () {
                showModal();
            });
        });
        
       function ModalDiv() {

        var chartss = document.getElementById('piechart_3d2');
         $('.modal-body').empty(); 
         $('.modal-body').append(chartss.innerHTML);
        showModal();
    }
        function ModalDiv2() {

        var chartss = document.getElementById('piechart_3d');
         $('.modal-body').empty(); 
         $('.modal-body').append(chartss.innerHTML);
        showModal();
    }
    </script>
<table style="width: 100%;" align="center">
 <tr>
    <td style="background-color:#71625F; height: 2px; width: 70%; " >
    <span  style="font-size:11pt; font-weight:bold; color:#ffffff; font-family:Courier New;">
        &nbsp; ادخال التقييم للموظفين </span>
    </td></tr>
 </table>
    <p>
    </p>
    <p>
         <table align="center" dir="rtl" style="width: 98%; height: 98%; float: right">
        <tr>
            <td align="center" colspan="5" dir="rtl">
            <asp:GridView ID="GridView1" runat="server" 
                EmptyDataText="ليس لديك بيانات موظفين مسجلة" 
                    Caption="الموظفين المطلوب تقييمهم" AutoGenerateColumns="False" 
                CaptionAlign="Top" 
                CssClass="mGrid" PagerStyle-CssClass="pgr" 
                AlternatingRowStyle-CssClass="alt" 
                DataKeyNames="EMP_NO"  Width="800px" 
                        Font-Names="Arial" Font-Size="14pt" 
                    onrowdatabound="GridView1_RowDataBound1">
                
                <Columns>
                        
                        <asp:TemplateField HeaderText="كود الموظف" SortExpression="Value">
                            <ItemTemplate>
                                <asp:LinkButton ID="lblemp_no" runat="server" class="label label-warning" CommandName='<%# Bind("JOB_NO") %>' Width="90%" onclick="EMP_no_Click" ToolTip= '<%# Bind("EMP_NAME") %>' Text='<%# Bind("EMP_NO") %>'></asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                     
                          <asp:TemplateField HeaderText="اسم الموظف">
                          <ItemTemplate>
                                <asp:Label ID="lblemp_name" runat="server" Text='<%# Bind("EMP_NAME") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                       
                           <asp:TemplateField HeaderText="كود الوظيفة" Visible="false" >
                         <ItemTemplate>
                                <asp:Label ID="lbljob_code" runat="server" Text='<%# Bind("JOB_NO") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                       
                         
                        <asp:TemplateField HeaderText="الوظيفة">
                         <ItemTemplate>
                                <asp:Label ID="lbljob_name" runat="server" Text='<%# Bind("JOB_NAME") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="emp_company" HeaderText="الشركة" />
                        
                          <asp:TemplateField HeaderText="التقييم" Visible="False">
                         <ItemTemplate>
                                <asp:Label ID="lblstatus" runat="server" Text='<%# Bind("STATUS") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        
                           <asp:TemplateField HeaderText="التقييم">
                         <ItemTemplate>
                                <asp:Label ID="lblfstatus" runat="server" Text='<%# Bind("FSTATUS") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        
                           <asp:TemplateField HeaderText="موافقة الموظف">
                         <ItemTemplate>
                                <asp:Label ID="lblEmpApprove" runat="server" Text='<%# Bind("EMP_ACCEPT") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>

                          <asp:TemplateField HeaderText="المستوي" Visible="false">
                         <ItemTemplate>
                                <asp:Label ID="lbllevel" runat="server" Text='<%# Bind("LEVEL") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                          <asp:BoundField DataField="RSLT" HeaderText="النتيجة" Visible="false" />
                          <asp:BoundField DataField="GRAD" HeaderText="وصف النتيجة"  Visible="false"/>
                       
                        
                    </Columns>
                
                <PagerStyle />
                <AlternatingRowStyle />
            </asp:GridView>
                </td>
        </tr>
        <tr style='<%=MidYearVersionHelper.IsMidYearVersion(Session["OPEN_YEAR"].ToString(), Session["OPEN_VERSION"].ToString())?"display:none":""%>'>
            <td style="height: 15px">
                &nbsp;</td>
            <td style="height: 15px" align="center">
                &nbsp;</td>
            <td style="height: 15px" align="center" colspan="2">
                <asp:Button ID="btn_all_reslt" runat="server" BackColor="#F79222" 
                                    class="btn btn-primary" Font-Bold="True" 
                    Font-Names="Arial" Font-Size="12pt" 
                                     Text="نتائج الكل" Width="150px" 
                    onclick="btn_all_reslt_Click" />
                <asp:Button ID="btn_egic_reslt" runat="server" BackColor="#F79222" 
                                    class="btn btn-primary" Font-Bold="True" 
                    Font-Names="Arial" Font-Size="12pt" 
                                     Text="نتائج EGIC" Width="150px" 
                    onclick="btn_egic_reslt_Click" />
                <asp:Button ID="btn_os_reslt" runat="server" BackColor="#F79222" 
                                    class="btn btn-primary" Font-Bold="True" 
                    Font-Names="Arial" Font-Size="12pt" 
                                     Text="نتائج Outsourse" Width="150px" 
                    onclick="btn_os_reslt_Click" />
                                    </td>
            <td style="height: 15px" align="center">
                &nbsp;</td>
        </tr>
        <tr>
            <td style="height: 15px">
                &nbsp;</td>
            <td style="height: 15px" align="center">
                &nbsp;</td>
            <td style="height: 15px" align="center" colspan="2">
                &nbsp;</td>
            <td style="height: 15px" align="center">
                &nbsp;</td>
        </tr>
        <tr style='<%=MidYearVersionHelper.IsMidYearVersion(Session["OPEN_YEAR"].ToString(), Session["OPEN_VERSION"].ToString())?"display:none":""%>'>
            <td style="height: 15px">
                &nbsp;</td>
            <td style="height: 15px" align="center">
                &nbsp;</td>
            <td style="height: 15px" align="center" colspan="2">
                <table align="center" dir="rtl" onload="drawChart()" 
    
                    style="padding: inherit; margin: inherit; border: medium solid #F79222; background-color: #FFFFFF; " 
                    border="1" frame="border" width="800" >
        <tr bgcolor="#71625F">
            <td align="center">
                <span  style="font-size:11pt; font-weight:bold; color:#71625F; font-family:Courier New;">
            <asp:Label ID="Label10" runat="server" Font-Bold="True" Text="التقييم" 
                    Font-Size="15pt" Font-Names="Arial" ForeColor="White"></asp:Label>
        </span>
            </td>
            <td align="center">
    <span  style="font-size:11pt; font-weight:bold; color:#71625F; font-family:Courier New;">
            <asp:Label ID="Label9" runat="server" Font-Bold="True" Text="المخطط" 
                    Font-Size="15pt" Font-Names="Arial" ForeColor="White" Width="60px"></asp:Label>
        </span>
            </td>
            <td align="center">
    <span  style="font-size:11pt; font-weight:bold; color:#71625F; font-family:Courier New;">
            <asp:Label ID="Label12" runat="server" Font-Bold="True" Text="الفعلى" 
                    Font-Size="15pt" Font-Names="Arial" ForeColor="White"></asp:Label>
        </span>
            </td>
        </tr>
        <tr>
            <td>
                <span  style="font-size:11pt; font-weight:bold; color:#000000; font-family:Courier New;">
            <asp:Label ID="Label23" runat="server" Font-Bold="True" Text="إستثنائي - Exceptional" 
                    Font-Size="15pt" Font-Names="Arial" ForeColor="Black"></asp:Label>
        </span>
            </td>
            <td>
    <span  style="font-size:11pt; font-weight:bold; color:#000000; font-family:Courier New;">
            <asp:Label ID="Label32" runat="server" Font-Bold="True" Text="5%" 
                    Font-Size="15pt" Font-Names="Arial" ForeColor="Black"></asp:Label>
        </span>
            </td>
            <td>
    <span  style="font-size:11pt; font-weight:bold; color:#000000; font-family:Courier New;">
            <asp:Label ID="lbl_exelent" runat="server" Font-Bold="True" Text="0%" Font-Size="15pt" 
                    Font-Names="Arial" ForeColor="Black"></asp:Label>
        </span>
            </td>
        </tr>
        <tr>
            <td>
    <span  style="font-size:11pt; font-weight:bold; color:#000000; font-family:Courier New;" 
                    dir="rtl">
            <asp:Label ID="Label22" runat="server" Font-Bold="True" Text="يفوق التوقعات - Exceed Expectations" 
                    Font-Size="15pt" Font-Names="Arial" ForeColor="Black"></asp:Label>
        </span>
            </td>
            <td>
    <span  style="font-size:11pt; font-weight:bold; color:#000000; font-family:Courier New;">
            <asp:Label ID="Label31" runat="server" Font-Bold="True" Text="15%" 
                    Font-Size="15pt" Font-Names="Arial" ForeColor="Black"></asp:Label>
        </span>
            </td>
            <td>
    <span  style="font-size:11pt; font-weight:bold; color:#000000; font-family:Courier New;">
            <asp:Label ID="lbl_very_good" runat="server" Font-Bold="True" Text="0%" Font-Size="15pt" 
                    Font-Names="Arial" ForeColor="Black"></asp:Label>
        </span>
            </td>
        </tr>
        <tr>
            <td>
                <span  style="font-size:11pt; font-weight:bold; color:#000000; font-family:Courier New;">
            <asp:Label ID="Label21" runat="server" Font-Bold="True" 
                    Text="يحقق التوقعات - Meet Expectations" Font-Size="15pt" 
                    Font-Names="Arial" ForeColor="Black"></asp:Label>
        </span>
            </td>
            <td>
    <span  style="font-size:11pt; font-weight:bold; color:#000000; font-family:Courier New;">
            <asp:Label ID="Label65" runat="server" Font-Bold="True" Text="70%" 
                    Font-Size="15pt" Font-Names="Arial" ForeColor="Black"></asp:Label>
        </span>
            </td>
            <td>
    <span  style="font-size:11pt; font-weight:bold; color:#000000; font-family:Courier New;">
            <asp:Label ID="lbl_good" runat="server" Font-Bold="True" Text="0%" Font-Size="15pt" 
                    Font-Names="Arial" ForeColor="Black"></asp:Label>
        </span>
            </td>
        </tr>
        <tr>
            <td>
                <span  style="font-size:11pt; font-weight:bold; color:#000000; font-family:Courier New;">
            <asp:Label ID="Label14" runat="server" Font-Bold="True" Text="متوسط – غير مرضي  Moderate - Unsatisfactory" 
                    Font-Size="15pt" Font-Names="Arial" ForeColor="Black"></asp:Label>
        </span>
            </td>
            <td>
    <span  style="font-size:11pt; font-weight:bold; color:#000000; font-family:Courier New;">
            <asp:Label ID="Label64" runat="server" Font-Bold="True" Text="10%" 
                    Font-Size="15pt" Font-Names="Arial" ForeColor="Black"></asp:Label>
        </span>
            </td>
            <td>
    <span  style="font-size:11pt; font-weight:bold; color:#000000; font-family:Courier New;">
            <asp:Label ID="lbl_bad" runat="server" Font-Bold="True" Text="0%" Font-Size="15pt" 
                    Font-Names="Arial" ForeColor="Black"></asp:Label>
        </span>
            </td>
        </tr>
        <tr>
            <td>
             <div id="piechart_3d2" onclick="ModalDiv()"
                    style="height:100%; width:400px; overflow-x:hidden; overflow-y: hidden; padding-bottom:10px;" align="center" 
                    dir="rtl"></div>
            </td>
            <td>
                &nbsp;</td>
            <td>
                    <div id="piechart_3d" onclick="ModalDiv2()"
                    style="height:100%; width:400px;   overflow-x:hidden; overflow-y: hidden; padding-bottom:10px;" align="center" 
                    dir="rtl" ></div>
            </td>
        </tr>
        </table>
                                    </td>
            <td style="height: 15px" align="center">
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
    </table><br />
    </p>
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog modal-sm">
      <div class="modal-content" style="width:650px" >
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">نظام تقييم الموظفين</h4>
        </div>
        <div class="modal-body" style="zoom: 1.5" >
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>

</asp:Content>

