﻿<%@ Page Language="C#" MasterPageFile="~/Main_Master_Page.master" AutoEventWireup="true" CodeFile="AS_Enter.aspx.cs" Inherits="Trans_AS_Enter" Title=" ادخال تقييمات الموظفين" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <table style="width: 100%;" align="center">
 <tr>
    <td style="background-color:#e6f1f8; height: 2px; width: 70%;" >
    <span  style="font-size:11pt; font-weight:bold; color:#000080; font-family:Courier New;">
        &nbsp; ادخال التقييم للموظفين </span>
    </td></tr>
 </table>
    <p>
    </p>
    <p>
         <table align="center" dir="rtl" style="width: 98%; height: 98%; float: right">
        <tr>
            <td>
                &nbsp;</td>
            <td colspan="5">
    <span  style="font-size:11pt; font-weight:bold; color:#000000; font-family:Courier New;">
            <asp:Label ID="Label63" runat="server" Font-Bold="True" Text="السنة المفتوحة للتقييم" 
                    Font-Size="16pt" Font-Names="Arial" ForeColor="Black" Width="180px"></asp:Label>
                <asp:TextBox ID="TXT_OPEN_YEAR" runat="server" BackColor="#FF9933" 
                    BorderStyle="Solid" Font-Bold="True" Font-Size="15pt" Width="150px" 
                    Enabled="False"></asp:TextBox>
        </span>
            </td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td align="center">
            <asp:Button ID="btn_all_levels" runat="server" BackColor="#3366FF" Font-Bold="True" 
                Font-Size="15pt" ForeColor="White" Text="جميع المستويات " Width="300px" 
                onclick="btn_all_levels_Click" />
            </td>
            <td align="center" colspan="4">
            <asp:Button ID="btn_frst_level" runat="server" BackColor="#3366FF" Font-Bold="True" 
                Font-Size="15pt" ForeColor="White" Text="المستوي الأول" Width="300px" 
                onclick="btn_frst_level_Click" />
            </td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td align="center" colspan="7">
            <asp:GridView ID="GridView1" runat="server" 
                EmptyDataText="ليس لديك بيانات موظفين مسجلة" 
                    Caption="الموظفين المطلوب تقييمهم" AutoGenerateColumns="False" 
                CaptionAlign="Top" onselectedindexchanged="GridView1_SelectedIndexChanged" 
                CssClass="mGrid" PagerStyle-CssClass="pgr" 
                AlternatingRowStyle-CssClass="alt" AutoGenerateSelectButton="True" 
                DataKeyNames="EMP_NO" onpageindexchanging="GridView1_PageIndexChanging" Width="800px" 
                        Font-Names="Arial" Font-Size="14pt" 
                    onselectedindexchanging="GridView1_SelectedIndexChanging" 
                    onrowdatabound="GridView1_RowDataBound">
                
                <Columns>
                        
                        <asp:TemplateField HeaderText="كود الموظف" SortExpression="Value">
                            <ItemTemplate>
                                <asp:Label ID="lblemp_no" runat="server" Text='<%# Bind("EMP_NO") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                     
                             <asp:TemplateField HeaderText="اسم الموظف">
                          <ItemTemplate>
                                <asp:Label ID="lblemp_name" runat="server" Text='<%# Bind("EMP_NAME") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        
                        <asp:TemplateField HeaderText="الوظيفة">
                         <ItemTemplate>
                                <asp:Label ID="lbljob_name" runat="server" Text='<%# Bind("JOB_NAME") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        
                          <asp:TemplateField HeaderText="نموذج">
                         <ItemTemplate>
                                <asp:Label ID="lblform_id" runat="server" Text='<%# Bind("FORM_ID") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        
                          <asp:TemplateField HeaderText="التقييم" Visible="false">
                         <ItemTemplate>
                                <asp:Label ID="lblstatus" runat="server" Text='<%# Bind("STATUS") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        
                           <asp:TemplateField HeaderText="التقييم">
                         <ItemTemplate>
                                <asp:Label ID="lblfstatus" runat="server" Text='<%# Bind("FSTATUS") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        
                          <asp:TemplateField HeaderText="المستوي" Visible="false">
                         <ItemTemplate>
                                <asp:Label ID="lbllevel" runat="server" Text='<%# Bind("LEVEL") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="emp_company" HeaderText="الشركة" />
                    </Columns>
                
                <PagerStyle />
                <AlternatingRowStyle />
            </asp:GridView>
                </td>
        </tr>
        <tr>
            <td style="height: 15px">
                &nbsp;</td>
            <td style="height: 15px" align="center" colspan="5">
                <asp:Label ID="lblResult" runat="server" Font-Bold="True" 
                    Font-Names="Arial" Font-Size="15pt">عرض النتائج</asp:Label>
            </td>
            <td style="height: 15px" align="center">
                &nbsp;</td>
        </tr>
        <tr>
            <td style="height: 15px">
                </td>
            <td style="height: 15px" align="center">
            <asp:Button ID="btn_calc" runat="server" BackColor="#3366FF" Font-Bold="True" 
                Font-Size="15pt" ForeColor="White" Text="EGIC مستوى اول" Width="200px" 
                onclick="btn_calc_Click" />
            </td>
            <td style="height: 15px" align="center">
            <asp:Button ID="btn_calcAll" runat="server" BackColor="#3366FF" Font-Bold="True" 
                Font-Size="15pt" ForeColor="White" Text="EGIC جميع المستويات" Width="250px" 
                onclick="btn_calcAll_Click" />
            </td>
            <td style="height: 15px" align="center">
            <asp:Button ID="btn_calcOS" runat="server" BackColor="#3366FF" Font-Bold="True" 
                Font-Size="15pt" ForeColor="White" Text="OutSource مستوى اول" Width="250px" 
                onclick="btn_calcOS_Click" />
            </td>
            <td style="height: 15px" align="center">
            <asp:Button ID="btn_calcOSAll" runat="server" BackColor="#3366FF" Font-Bold="True" 
                Font-Size="15pt" ForeColor="White" Text="OutSource جميع المستويات" Width="300px" 
                onclick="btn_calcOSAll_Click" />
            </td>
            <td style="height: 15px" align="center">
                &nbsp;</td>
            <td style="height: 15px" align="center">
                &nbsp;</td>
        </tr>
        <tr>
            <td style="height: 15px">
                &nbsp;</td>
            <td style="height: 15px" align="center" colspan="5">
                <asp:Label ID="lblResultComp" runat="server" Font-Bold="True" 
                    Font-Names="Arial" Font-Size="15pt"></asp:Label>
            </td>
            <td style="height: 15px" align="center">
                &nbsp;</td>
        </tr>
        <tr>
            <td align="center" colspan="6" rowspan="2">
                <table align="center" dir="rtl" 
    
                    style="padding: inherit; margin: inherit; border: thin solid #FF0000; background-color: #66CCFF;" 
                    border="1" frame="border" width="800px">
        <tr bgcolor="Black">
            <td align="center">
    <span  style="font-size:11pt; font-weight:bold; color:#000000; font-family:Courier New;">
            <asp:Label ID="Label9" runat="server" Font-Bold="True" Text="الهدف" 
                    Font-Size="15pt" Font-Names="Arial" ForeColor="White"></asp:Label>
        </span>
            </td>
            <td align="center">
    <span  style="font-size:11pt; font-weight:bold; color:#000000; font-family:Courier New;">
            <asp:Label ID="Label10" runat="server" Font-Bold="True" Text="التقييم" 
                    Font-Size="15pt" Font-Names="Arial" ForeColor="White"></asp:Label>
        </span>
            </td>
            <td align="center">
    <span  style="font-size:11pt; font-weight:bold; color:#000000; font-family:Courier New;">
            <asp:Label ID="Label12" runat="server" Font-Bold="True" Text="المحقق" 
                    Font-Size="15pt" Font-Names="Arial" ForeColor="White"></asp:Label>
        </span>
            </td>
        </tr>
        <tr>
            <td>
    <span  style="font-size:11pt; font-weight:bold; color:#000000; font-family:Courier New;">
            <asp:Label ID="Label64" runat="server" Font-Bold="True" Text="10%" 
                    Font-Size="15pt" Font-Names="Arial" ForeColor="Black"></asp:Label>
        </span>
            </td>
            <td>
    <span  style="font-size:11pt; font-weight:bold; color:#000000; font-family:Courier New;">
            <asp:Label ID="Label14" runat="server" Font-Bold="True" Text="متوسط - غير مرضي" 
                    Font-Size="15pt" Font-Names="Arial" ForeColor="Black"></asp:Label>
        </span>
            </td>
            <td>
    <span  style="font-size:11pt; font-weight:bold; color:#000000; font-family:Courier New;">
            <asp:Label ID="lbl_bad" runat="server" Font-Bold="True" Text="0%" Font-Size="15pt" 
                    Font-Names="Arial" ForeColor="Black"></asp:Label>
        </span>
            </td>
        </tr>
        <tr>
            <td>
    <span  style="font-size:11pt; font-weight:bold; color:#000000; font-family:Courier New;">
            <asp:Label ID="Label65" runat="server" Font-Bold="True" Text="65%" 
                    Font-Size="15pt" Font-Names="Arial" ForeColor="Black"></asp:Label>
        </span>
            </td>
            <td>
    <span  style="font-size:11pt; font-weight:bold; color:#000000; font-family:Courier New;">
            <asp:Label ID="Label21" runat="server" Font-Bold="True" Text="جيد" Font-Size="15pt" 
                    Font-Names="Arial" ForeColor="Black"></asp:Label>
        </span>
            </td>
            <td>
    <span  style="font-size:11pt; font-weight:bold; color:#000000; font-family:Courier New;">
            <asp:Label ID="lbl_good" runat="server" Font-Bold="True" Text="0%" Font-Size="15pt" 
                    Font-Names="Arial" ForeColor="Black"></asp:Label>
        </span>
            </td>
        </tr>
        <tr>
            <td>
    <span  style="font-size:11pt; font-weight:bold; color:#000000; font-family:Courier New;">
            <asp:Label ID="Label31" runat="server" Font-Bold="True" Text="20%" 
                    Font-Size="15pt" Font-Names="Arial" ForeColor="Black"></asp:Label>
        </span>
            </td>
            <td>
    <span  style="font-size:11pt; font-weight:bold; color:#000000; font-family:Courier New;" 
                    dir="rtl">
            <asp:Label ID="Label22" runat="server" Font-Bold="True" Text="جيد جداً" 
                    Font-Size="15pt" Font-Names="Arial" ForeColor="Black"></asp:Label>
        </span>
            </td>
            <td>
    <span  style="font-size:11pt; font-weight:bold; color:#000000; font-family:Courier New;">
            <asp:Label ID="lbl_very_good" runat="server" Font-Bold="True" Text="0%" Font-Size="15pt" 
                    Font-Names="Arial" ForeColor="Black"></asp:Label>
        </span>
            </td>
        </tr>
        <tr>
            <td>
    <span  style="font-size:11pt; font-weight:bold; color:#000000; font-family:Courier New;">
            <asp:Label ID="Label32" runat="server" Font-Bold="True" Text="5%" 
                    Font-Size="15pt" Font-Names="Arial" ForeColor="Black"></asp:Label>
        </span>
            </td>
            <td>
    <span  style="font-size:11pt; font-weight:bold; color:#000000; font-family:Courier New;">
            <asp:Label ID="Label23" runat="server" Font-Bold="True" Text="ممتاز" 
                    Font-Size="15pt" Font-Names="Arial" ForeColor="Black"></asp:Label>
        </span>
            </td>
            <td>
    <span  style="font-size:11pt; font-weight:bold; color:#000000; font-family:Courier New;">
            <asp:Label ID="lbl_exelent" runat="server" Font-Bold="True" Text="0%" Font-Size="15pt" 
                    Font-Names="Arial" ForeColor="Black"></asp:Label>
        </span>
            </td>
        </tr>
</table>
&nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td colspan="4">
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td colspan="4">
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td colspan="4">
                <asp:TextBox ID="TextBox1" runat="server" BackColor="#FFCCFF" Font-Size="14pt"></asp:TextBox>
            </td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td colspan="4">
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td colspan="4">
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
    </table><br />
    </p>

   

</asp:Content>

