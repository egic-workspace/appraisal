﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="MessagePopup.aspx.cs" Inherits="MessagePopup" %>
<%@ Register TagPrefix="ajaxToolkit"   Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">

<head id="Head1" runat="server">
    <title></title>
    <style type="text/css">
        .MessageBoxPopupBackground
        {

            -moz-opacity: 0.4;
            width: 100%;
            height: 100%;
            background-color: #999999;
            position: absolute;
            z-index: 500001;
            top: 0px;
            left: 0px;
        }
        .popupHeader
        {
            float: left;
            padding: 5px 0px 0px 0px;
            width: 420px;
            font-family: tahoma;
            font-weight: bold;
            height: 25px;
            text-decoration: none;
            background-image: url("images/body_tab_top.jpg");
            background-repeat: repeat-x;
            color: #FFFFFF;
        }
        .popupHeader span
        {
            color: #fff;
            text-decoration: none;
            line-height: 15px;
            text-decoration: none;
            float: left;
            margin-left: 10px;
        }
        .popupHeader a
        {
            color: #fff !important;
            text-decoration: none !important;
            line-height: 15px;
            text-decoration: none;
            float: right;
            margin-right: 10px;
        }
        .popup_button
        {
            color: #fff !important;
            font-family: arial, Geneva, sans-serif;
            font-size: 12px;
            font-weight: normal;
            text-decoration: none !important;
            width: auto;
            background-image: url('images/button_bg.jpg');
            background-repeat: repeat-x; /*height: 24px;*/
            line-height: 22px;
            padding: 3px 15px 3px 15px;
            float: left;
            margin: 0px 0px 0px 5px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <%--<ajaxToolkit:ToolkitScriptManager runat="server" ID="sm1" />--%>
   
    
     <ajaxToolkit:ToolkitScriptManager  ID="sm1" runat="server">
    </ajaxToolkit:ToolkitScriptManager>
    <asp:UpdatePanel runat="server" ID="upMain">
        <ContentTemplate>
            <asp:Button runat="server" ID="btnShow" 

              OnClick="btnShowSuccess_Click" Text="Success" />
            <asp:Button runat="server" ID="Button1" 

              OnClick="btnShowError_Click" Text="Error" />
            <asp:Button runat="server" ID="Button2" 

              OnClick="btnShowWarning_Click" Text="Warning" />
            <asp:Button runat="server" ID="Button3" 

              OnClick="btnShowMessage_Click" Text="Message" />
            <%--Message popup area start--%>
            <asp:Button runat="server" ID="btnMessagePopupTargetButton" Style="display: none;" />
            
            <ajax:modalpopupextender ID="mpeMessagePopup" runat="server" PopupControlID="pnlMessageBox"

                TargetControlID="btnMessagePopupTargetButton" OkControlID="btnOk" CancelControlID="btnCancel"

                BackgroundCssClass="MessageBoxPopupBackground">
            </ajax:modalpopupextender>
            <asp:Panel runat="server" ID="pnlMessageBox" 

                   BackColor="White" Width="420" 

                   Style="display: none; border: 2px solid #780606;">
                <div class="popupHeader" style="width: 420px;">
                    <asp:Label ID="lblMessagePopupHeading" Text="Information" 

                        runat="server"></asp:Label><asp:LinkButton

                        ID="btnCancel" runat="server" 

                         Style="float: right; margin-right: 5px;">X</asp:LinkButton>
                </div>
                <div style="max-height: 500px; width: 420px; overflow: hidden;">
                    <div style="float:left; width:380px; margin:20px;">
                        <table style="padding: 0; border-spacing: 0; border-collapse: collapse; width: 100%;">
                            <tr>
                                <td style="text-align: left; vertical-align: top; width: 11%;">
                                    <asp:Literal runat="server" ID="ltrMessagePopupImage"></asp:Literal>
                                </td>
                                <td style="width: 2%;">
                                </td>
                                <td style="text-align: left; vertical-align: top; width: 87%;">
                                    <p style="margin: 0px; padding: 0px; color: #5F0202;">
                                        
                                            <asp:Label runat="server" ID="lblMessagePopupText"></asp:Label>
                                    </p>
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align: right; vertical-align: top;" colspan="3">
                                    <div style="margin-right: 0px; float: right; width: auto;">
                                        <asp:LinkButton ID="btnOk" runat="server" 

                                            CssClass="popup_button">Ok</asp:LinkButton>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </asp:Panel>
            <%--Message popup area end--%>
           
        </ContentTemplate>
    </asp:UpdatePanel>
   
    </form>
</body>
</html>
