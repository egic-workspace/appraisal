﻿<%@ Page Language="C#" MasterPageFile="~/Main_Master_Page.master" AutoEventWireup="true" CodeFile="Form_A.aspx.cs" Inherits="Trans_Form_A" Title=" Form (1)" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
   <script type = "text/javascript">
        function Confirm() {
            var confirm_value = document.createElement("INPUT");
            confirm_value.type = "hidden";
            confirm_value.name = "confirm_value";
            if (confirm("هل تريد حذف التقييم ?")) {
                confirm_value.value = "Yes";
            } else {
                confirm_value.value = "No";
            }
            document.forms[0].appendChild(confirm_value);
        }
    </script>
 <table style="width: 100%;" align="center">
 <tr>
    <td style="background-color:#e6f1f8; height: 2px; width: 70%;" >
    <span  style="font-size:11pt; font-weight:bold; color:#000080; font-family:Courier New;">
        &nbsp; نموذج تقييم رقم (1) </span>
    </td></tr>
 </table>
 <table style="width: 100%;" align="center">
 <tr>
    <td style="background-color:#FF9933; height: 2px; width: 70%;" >
    <span  style="font-size:11pt; font-weight:bold; color:#000000; font-family:Courier New;">
        &nbsp;  
            <asp:Label ID="Label8" runat="server" Font-Bold="True" Text="اسم الموظف" 
                Width="110px" Font-Size="16pt" Font-Names="Arial" ForeColor="Black"></asp:Label>
            <asp:Label ID="lbl_emp_name" runat="server" Font-Bold="True" Text="EMP_NAME" 
                Width="300px" Font-Size="16pt" Font-Names="Arial" ForeColor="#0000CC"></asp:Label>
    <span  style="font-size:11pt; font-weight:bold; color:#000000; font-family:Courier New;">
            <asp:Label ID="Label65" runat="server" Font-Bold="True" Text="نتيجة التقييم" 
                Width="110px" Font-Size="16pt" Font-Names="Arial" ForeColor="Black"></asp:Label>
        </span>
        <asp:TextBox ID="txt_top_result" runat="server" BackColor="#33CCFF" 
            BorderStyle="Solid" Enabled="False" Font-Bold="True" Font-Size="13pt"></asp:TextBox>
        </span>
        <asp:Button ID="btn_print" runat="server" BackColor="#FF33CC" Font-Bold="True" 
            Font-Names="Arial" ForeColor="White" onclick="btn_print_Click" Text="طباعة" 
            Width="100px" Font-Size="11pt" />
    </td></tr>
 </table>

    <table align="center" dir="rtl" 
    style="border-width: thin; border-style: solid; width: 98%; height: 98px; float: right; background-color: #66CCFF;">
        <tr bgcolor="Black">
            <td align="center">
    <span  style="font-size:11pt; font-weight:bold; color:#000000; font-family:Courier New;">
            <asp:Label ID="Label9" runat="server" Font-Bold="True" Text="النسبة" 
                    Font-Size="11pt" Font-Names="Arial" ForeColor="White"></asp:Label>
        </span>
            </td>
            <td align="center">
    <span  style="font-size:11pt; font-weight:bold; color:#000000; font-family:Courier New;">
            <asp:Label ID="Label10" runat="server" Font-Bold="True" Text="التقييم" 
                    Font-Size="11pt" Font-Names="Arial" ForeColor="White"></asp:Label>
        </span>
            </td>
            <td align="center">
    <span  style="font-size:11pt; font-weight:bold; color:#000000; font-family:Courier New;">
            <asp:Label ID="Label11" runat="server" Font-Bold="True" Text="الشرح" 
                    Font-Size="11pt" Font-Names="Arial" ForeColor="White"></asp:Label>
        </span>
            </td>
            <td align="center">
    <span  style="font-size:11pt; font-weight:bold; color:#000000; font-family:Courier New;">
            <asp:Label ID="Label12" runat="server" Font-Bold="True" Text="الدرجة" 
                    Font-Size="11pt" Font-Names="Arial" ForeColor="White"></asp:Label>
        </span>
            </td>
        </tr>
        <tr>
            <td>
    <span  style="font-size:11pt; font-weight:bold; color:#000000; font-family:Courier New;">
            <asp:Label ID="Label13" runat="server" Font-Bold="True" Text="أقل من 60%" 
                    Font-Size="11pt" Font-Names="Arial" ForeColor="Black"></asp:Label>
        </span>
            </td>
            <td>
    <span  style="font-size:11pt; font-weight:bold; color:#000000; font-family:Courier New;">
            <asp:Label ID="Label14" runat="server" Font-Bold="True" Text="غير مرضي" 
                    Font-Size="11pt" Font-Names="Arial" ForeColor="Black"></asp:Label>
        </span>
            </td>
            <td>
    <span  style="font-size:11pt; font-weight:bold; color:#000000; font-family:Courier New;">
            <asp:Label ID="Label15" runat="server" Font-Bold="True" 
                    Text="أداء الموظف دون التوقعات بشكل مستمر ولا يتحسن , ويجب اتخاذ الإجراءات التصحيحية في الحال" 
                    Font-Size="11pt" Font-Names="Arial" ForeColor="Black"></asp:Label>
        </span>
            </td>
            <td>
    <span  style="font-size:11pt; font-weight:bold; color:#000000; font-family:Courier New;">
            <asp:Label ID="Label24" runat="server" Font-Bold="True" Text="1" Font-Size="11pt" 
                    Font-Names="Arial" ForeColor="Black"></asp:Label>
        </span>
            </td>
        </tr>
        <tr>
            <td>
    <span  style="font-size:11pt; font-weight:bold; color:#000000; font-family:Courier New;">
            <asp:Label ID="Label29" runat="server" Font-Bold="True" Text="من 61% إلي 70%" 
                    Font-Size="11pt" Font-Names="Arial" ForeColor="Black"></asp:Label>
        </span>
            </td>
            <td>
    <span  style="font-size:11pt; font-weight:bold; color:#000000; font-family:Courier New;">
            <asp:Label ID="Label20" runat="server" Font-Bold="True" Text="متوسط" 
                    Font-Size="11pt" Font-Names="Arial" ForeColor="Black"></asp:Label>
        </span>
            </td>
            <td>
    <span  style="font-size:11pt; font-weight:bold; color:#000000; font-family:Courier New;">
            <asp:Label ID="Label16" runat="server" Font-Bold="True" 
                    Text="الموظف يعمل علي تطوير الجدارات والمهارات ويرتقي في بعض الأحيان الي مستوي التوقعات وما زال يحتاج الي إشراف مستمر" 
                    Font-Size="11pt" Font-Names="Arial" ForeColor="Black"></asp:Label>
        </span>
            </td>
            <td>
    <span  style="font-size:11pt; font-weight:bold; color:#000000; font-family:Courier New;">
            <asp:Label ID="Label25" runat="server" Font-Bold="True" Text="2" Font-Size="11pt" 
                    Font-Names="Arial" ForeColor="Black"></asp:Label>
        </span>
            </td>
        </tr>
        <tr>
            <td>
    <span  style="font-size:11pt; font-weight:bold; color:#000000; font-family:Courier New;">
            <asp:Label ID="Label30" runat="server" Font-Bold="True" Text="من 71% إلي 85%" 
                    Font-Size="11pt" Font-Names="Arial" ForeColor="Black"></asp:Label>
        </span>
            </td>
            <td>
    <span  style="font-size:11pt; font-weight:bold; color:#000000; font-family:Courier New;">
            <asp:Label ID="Label21" runat="server" Font-Bold="True" Text="جيد" Font-Size="11pt" 
                    Font-Names="Arial" ForeColor="Black"></asp:Label>
        </span>
            </td>
            <td>
    <span  style="font-size:11pt; font-weight:bold; color:#000000; font-family:Courier New;">
            <asp:Label ID="Label17" runat="server" Font-Bold="True" 
                    Text="الموظف يمارس الجدارات المطلوبة ويرتقي في أغلب الأحيان الي مستوي التوقعات ويحتاج الي متابعة واشراف من آن الي آخر" 
                    Font-Size="11pt" Font-Names="Arial" ForeColor="Black"></asp:Label>
        </span>
            </td>
            <td>
    <span  style="font-size:11pt; font-weight:bold; color:#000000; font-family:Courier New;">
            <asp:Label ID="Label26" runat="server" Font-Bold="True" Text="3" Font-Size="11pt" 
                    Font-Names="Arial" ForeColor="Black"></asp:Label>
        </span>
            </td>
        </tr>
        <tr>
            <td>
    <span  style="font-size:11pt; font-weight:bold; color:#000000; font-family:Courier New;">
            <asp:Label ID="Label31" runat="server" Font-Bold="True" Text="من 86% إلي 95% " 
                    Font-Size="11pt" Font-Names="Arial" ForeColor="Black"></asp:Label>
        </span>
            </td>
            <td>
    <span  style="font-size:11pt; font-weight:bold; color:#000000; font-family:Courier New;">
            <asp:Label ID="Label22" runat="server" Font-Bold="True" Text="جيد جداً" 
                    Font-Size="11pt" Font-Names="Arial" ForeColor="Black"></asp:Label>
        </span>
            </td>
            <td>
    <span  style="font-size:11pt; font-weight:bold; color:#000000; font-family:Courier New;">
            <asp:Label ID="Label18" runat="server" Font-Bold="True" 
                    Text="الموظف يمارس الجدارات المطلوبة ويرتقي بشكل ثابت الي مستوي التوقعات دون الحاجة الي إشراف" 
                    Font-Size="11pt" Font-Names="Arial" ForeColor="Black"></asp:Label>
        </span>
            </td>
            <td>
    <span  style="font-size:11pt; font-weight:bold; color:#000000; font-family:Courier New;">
            <asp:Label ID="Label27" runat="server" Font-Bold="True" Text="4" Font-Size="11pt" 
                    Font-Names="Arial" ForeColor="Black"></asp:Label>
        </span>
            </td>
        </tr>
        <tr>
            <td>
    <span  style="font-size:11pt; font-weight:bold; color:#000000; font-family:Courier New;">
            <asp:Label ID="Label32" runat="server" Font-Bold="True" Text="أكثر من 95%" 
                    Font-Size="11pt" Font-Names="Arial" ForeColor="Black"></asp:Label>
        </span>
            </td>
            <td>
    <span  style="font-size:11pt; font-weight:bold; color:#000000; font-family:Courier New;">
            <asp:Label ID="Label23" runat="server" Font-Bold="True" Text="ممتاز" 
                    Font-Size="11pt" Font-Names="Arial" ForeColor="Black"></asp:Label>
        </span>
            </td>
            <td>
    <span  style="font-size:11pt; font-weight:bold; color:#000000; font-family:Courier New;">
            <asp:Label ID="Label19" runat="server" Font-Bold="True" 
                    Text="الموظف يمارس الجدارات المطلوبة ويفوق التوقعات في أغلب الأحيان , دون الحاجة إلي إشراف" 
                    Font-Size="11pt" Font-Names="Arial" ForeColor="Black"></asp:Label>
        </span>
            </td>
            <td>
    <span  style="font-size:11pt; font-weight:bold; color:#000000; font-family:Courier New;">
            <asp:Label ID="Label28" runat="server" Font-Bold="True" Text="5" Font-Size="11pt" 
                    Font-Names="Arial" ForeColor="Black"></asp:Label>
        </span>
            </td>
        </tr>
</table>

    <p>
        <br />
    </p>
    <table align="center" dir="rtl" style="width: 98%; height: 90px; float: right">
        <tr>
            <td align="center" colspan="3">
                &nbsp;</td>
        </tr>
        <tr>
            <td>
    <span  style="font-size:11pt; font-weight:bold; color:#000000; font-family:Courier New;">
            <asp:Label ID="lbl_1" runat="server" Font-Bold="True" Text="تقييم الجدارات" 
                    Font-Size="20pt" Font-Names="Arial" ForeColor="#0000CC"></asp:Label>
        </span>
            </td>
            <td>
    <span  style="font-size:11pt; font-weight:bold; color:#000000; font-family:Courier New;">
            <asp:Label ID="lbl_2" runat="server" Font-Bold="True" Text="التقييم" 
                    Font-Size="20pt" Font-Names="Arial" ForeColor="#0000CC"></asp:Label>
        </span>
            </td>
            <td>
    <span  style="font-size:11pt; font-weight:bold; color:#000000; font-family:Courier New;">
            <asp:Label ID="lbl_3" runat="server" Font-Bold="True" Text="التبرير" 
                    Font-Size="20pt" Font-Names="Arial" ForeColor="#0000CC"></asp:Label>
        </span>
            </td>
        </tr>
        <tr>
            <td>
    <span  style="font-size:11pt; font-weight:bold; color:#000000; font-family:Courier New;">
            <asp:Label ID="Label33" runat="server" Font-Bold="True" Text="كم المهام المنجزة" 
                    Font-Size="16pt" Font-Names="Arial" ForeColor="Black"></asp:Label>
                <br />
            <asp:Label ID="Label34" runat="server" Font-Bold="True" 
                    Text="Quantity of output work" Font-Size="16pt" Font-Names="Arial" 
                    ForeColor="Black"></asp:Label>
        </span>
            </td>
            <td>
                <asp:DropDownList ID="evaL_out_qty" runat="server" Font-Bold="True" 
                    Font-Size="14pt" Width="80px">
                    <asp:ListItem Value="0">اختر</asp:ListItem>
                    <asp:ListItem>1</asp:ListItem>
                    <asp:ListItem>2</asp:ListItem>
                    <asp:ListItem>3</asp:ListItem>
                    <asp:ListItem>4</asp:ListItem>
                    <asp:ListItem>5</asp:ListItem>
                </asp:DropDownList>
            </td>
            <td>
                <asp:TextBox ID="just_out_qty" runat="server" Height="70px" 
                    TextMode="MultiLine" Width="280px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td colspan="3">
    <span  style="font-size:11pt; font-weight:bold; color:#000000; font-family:Courier New;">
            <asp:Label ID="Label35" runat="server" Font-Bold="True" 
                    Text="كمية العمل الذي تم انجازه بواسطة فرد أو مجموعة في مشروع محدد أو مهمات معينة بغض النظر عن مستوي الجودة والوقت المستخدم" 
                    Font-Size="11pt" Font-Names="Arial" ForeColor="Gray"></asp:Label>
        </span>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <asp:Image ID="Image1" runat="server" ImageUrl="~/Images/line3.png" />
            </td>
        </tr>
        <tr>
            <td>
    <span  style="font-size:11pt; font-weight:bold; color:#000000; font-family:Courier New;">
            <asp:Label ID="Label36" runat="server" Font-Bold="True" 
                    Text="توقيت تسليم مهمات العمل" Font-Size="16pt" Font-Names="Arial" 
                    ForeColor="Black"></asp:Label>
                <br />
            <asp:Label ID="Label37" runat="server" Font-Bold="True" 
                    Text="Timeliness of Delivery Output" Font-Size="16pt" Font-Names="Arial" 
                    ForeColor="Black"></asp:Label>
        </span>
            </td>
            <td>
                <asp:DropDownList ID="eval_deliv_time" runat="server" Font-Bold="True" 
                    Font-Size="14pt" Width="80px">
                    <asp:ListItem Value="0">اختر</asp:ListItem>
                    <asp:ListItem>1</asp:ListItem>
                    <asp:ListItem>2</asp:ListItem>
                    <asp:ListItem>3</asp:ListItem>
                    <asp:ListItem>4</asp:ListItem>
                    <asp:ListItem>5</asp:ListItem>
                </asp:DropDownList>
            </td>
            <td>
                <asp:TextBox ID="just_deliv_time" runat="server" Height="70px" 
                    TextMode="MultiLine" Width="280px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td colspan="3">
    <span  style="font-size:11pt; font-weight:bold; color:#000000; font-family:Courier New;">
            <asp:Label ID="Label38" runat="server" Font-Bold="True" 
                    Text="الإلتزام بتسليم الأعمال وتحقيق الأهداف في الأوقات المحددة،  بغض النظر عن مستوي الجودة وكفاءة استخدام الموارد المتاحة" 
                    Font-Size="11pt" Font-Names="Arial" ForeColor="Gray"></asp:Label>
        </span>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <asp:Image ID="Image2" runat="server" ImageUrl="~/Images/line3.png" />
            </td>
        </tr>
        <tr>
            <td>
    <span  style="font-size:11pt; font-weight:bold; color:#000000; font-family:Courier New;">
            <asp:Label ID="Label39" runat="server" Font-Bold="True" Text="جودة المهام المنجزة" 
                    Font-Size="16pt" Font-Names="Arial" ForeColor="Black"></asp:Label>
                <br />
            <asp:Label ID="Label40" runat="server" Font-Bold="True" 
                    Text="Quality of Work Output" Font-Size="16pt" Font-Names="Arial" 
                    ForeColor="Black"></asp:Label>
        </span>
            </td>
            <td>
                <asp:DropDownList ID="eval_work_qlity" runat="server" Font-Bold="True" 
                    Font-Size="14pt" Width="80px">
                    <asp:ListItem Value="0">اختر</asp:ListItem>
                    <asp:ListItem>1</asp:ListItem>
                    <asp:ListItem>2</asp:ListItem>
                    <asp:ListItem>3</asp:ListItem>
                    <asp:ListItem>4</asp:ListItem>
                    <asp:ListItem>5</asp:ListItem>
                </asp:DropDownList>
            </td>
            <td>
                <asp:TextBox ID="just_work_qlity" runat="server" Height="70px" 
                    TextMode="MultiLine" Width="280px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td colspan="3">
    <span  style="font-size:11pt; font-weight:bold; color:#000000; font-family:Courier New;">
            <asp:Label ID="Label41" runat="server" Font-Bold="True" 
                    Text=" جودة تنفيذ الأعمال فيما يتعلق بالأخطاء والفاقد ةاعادة العمل مرة أخري بغض النظر عن كمية العمل والوقت المحدد لتنفيذه" 
                    Font-Size="11pt" Font-Names="Arial" ForeColor="Gray"></asp:Label>
        </span>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <asp:Image ID="Image3" runat="server" ImageUrl="~/Images/line3.png" />
            </td>
        </tr>
        <tr>
            <td>
    <span  style="font-size:11pt; font-weight:bold; color:#000000; font-family:Courier New;">
            <asp:Label ID="Label42" runat="server" Font-Bold="True" Text="الأمان و الصحة" 
                    Font-Size="16pt" Font-Names="Arial" ForeColor="Black"></asp:Label>
                <br />
            <asp:Label ID="Label43" runat="server" Font-Bold="True" Text="Health and Safety" 
                    Font-Size="16pt" Font-Names="Arial" ForeColor="Black"></asp:Label>
        </span>
            </td>
            <td>
                <asp:DropDownList ID="eval_safty" runat="server" Font-Bold="True" 
                    Font-Size="14pt" Width="80px">
                    <asp:ListItem Value="0">اختر</asp:ListItem>
                    <asp:ListItem>1</asp:ListItem>
                    <asp:ListItem>2</asp:ListItem>
                    <asp:ListItem>3</asp:ListItem>
                    <asp:ListItem>4</asp:ListItem>
                    <asp:ListItem>5</asp:ListItem>
                </asp:DropDownList>
            </td>
            <td>
                <asp:TextBox ID="just_safty" runat="server" Height="70px" TextMode="MultiLine" 
                    Width="280px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td colspan="3">
    <span  style="font-size:11pt; font-weight:bold; color:#000000; font-family:Courier New;">
            <asp:Label ID="Label44" runat="server" Font-Bold="True" 
                    Text="الالتزام بتعليمات الامن و السلامة و الصحة المهنية" Font-Size="11pt" 
                    Font-Names="Arial" ForeColor="Gray"></asp:Label>
        </span>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <asp:Image ID="Image4" runat="server" ImageUrl="~/Images/line3.png" />
            </td>
        </tr>
        <tr>
            <td>
    <span  style="font-size:11pt; font-weight:bold; color:#000000; font-family:Courier New;">
            <asp:Label ID="Label45" runat="server" Font-Bold="True" Text="روح الفريق" 
                    Font-Size="16pt" Font-Names="Arial" ForeColor="Black"></asp:Label>
                <br />
            <asp:Label ID="Label46" runat="server" Font-Bold="True" Text="Team Work" 
                    Font-Size="16pt" Font-Names="Arial" ForeColor="Black"></asp:Label>
        </span>
            </td>
            <td>
                <asp:DropDownList ID="eval_team_work" runat="server" Font-Bold="True" 
                    Font-Size="14pt" Width="80px">
                    <asp:ListItem Value="0">اختر</asp:ListItem>
                    <asp:ListItem>1</asp:ListItem>
                    <asp:ListItem>2</asp:ListItem>
                    <asp:ListItem>3</asp:ListItem>
                    <asp:ListItem>4</asp:ListItem>
                    <asp:ListItem>5</asp:ListItem>
                </asp:DropDownList>
            </td>
            <td>
                <asp:TextBox ID="just_team_work" runat="server" Height="70px" 
                    TextMode="MultiLine" Width="280px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td colspan="3">
    <span  style="font-size:11pt; font-weight:bold; color:#000000; font-family:Courier New;">
            <asp:Label ID="Label47" runat="server" Font-Bold="True" 
                    Text=" مدى التعاون فى تواصل المعلومات والأفكار فى بيئة العمل ومدى اللإستعداد للعمل مع الأخرين " 
                    Font-Size="11pt" Font-Names="Arial" ForeColor="Gray"></asp:Label>
        </span>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <asp:Image ID="Image5" runat="server" ImageUrl="~/Images/line3.png" />
            </td>
        </tr>
        <tr>
            <td>
    <span  style="font-size:11pt; font-weight:bold; color:#000000; font-family:Courier New;">
            <asp:Label ID="Label48" runat="server" Font-Bold="True" Text="الألتزام" 
                    Font-Size="16pt" Font-Names="Arial" ForeColor="Black"></asp:Label>
                <br />
            <asp:Label ID="Label49" runat="server" Font-Bold="True" Text="
Commitment" Font-Size="16pt" Font-Names="Arial" ForeColor="Black"></asp:Label>
        </span>
            </td>
            <td>
                <asp:DropDownList ID="eval_commit" runat="server" Font-Bold="True" 
                    Font-Size="14pt" Width="80px">
                    <asp:ListItem Value="0">اختر</asp:ListItem>
                    <asp:ListItem>1</asp:ListItem>
                    <asp:ListItem>2</asp:ListItem>
                    <asp:ListItem>3</asp:ListItem>
                    <asp:ListItem>4</asp:ListItem>
                    <asp:ListItem>5</asp:ListItem>
                </asp:DropDownList>
            </td>
            <td>
                <asp:TextBox ID="just_commit" runat="server" Height="70px" TextMode="MultiLine" 
                    Width="280px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td colspan="3">
    <span  style="font-size:11pt; font-weight:bold; color:#000000; font-family:Courier New;">
            <asp:Label ID="Label56" runat="server" Font-Bold="True" 
                    Text=" المواظبة ومدى الإستجابة للإعمال التى تتطلب حجم أعمال مطلوب إنجازة فى موعد محدد وفى ظروف صعبة" 
                    Font-Size="11pt" Font-Names="Arial" ForeColor="Gray"></asp:Label>
        </span>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <asp:Image ID="Image6" runat="server" ImageUrl="~/Images/line3.png" />
            </td>
        </tr>
        <tr>
            <td>
    <span  style="font-size:11pt; font-weight:bold; color:#000000; font-family:Courier New;">
            <asp:Label ID="Label50" runat="server" Font-Bold="True" Text="المرونة" 
                    Font-Size="16pt" Font-Names="Arial" ForeColor="Black"></asp:Label>
                <br />
            <asp:Label ID="Label51" runat="server" Font-Bold="True" Text="
Flexibility" Font-Size="16pt" Font-Names="Arial" ForeColor="Black"></asp:Label>
        </span>
            </td>
            <td>
                <asp:DropDownList ID="eval_flexib" runat="server" Font-Bold="True" 
                    Font-Size="14pt" Width="80px">
                    <asp:ListItem Value="0">اختر</asp:ListItem>
                    <asp:ListItem>1</asp:ListItem>
                    <asp:ListItem>2</asp:ListItem>
                    <asp:ListItem>3</asp:ListItem>
                    <asp:ListItem>4</asp:ListItem>
                    <asp:ListItem>5</asp:ListItem>
                </asp:DropDownList>
            </td>
            <td>
                <asp:TextBox ID="just_flexib" runat="server" Height="70px" TextMode="MultiLine" 
                    Width="280px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td colspan="3">
    <span  style="font-size:11pt; font-weight:bold; color:#000000; font-family:Courier New;">
            <asp:Label ID="Label57" runat="server" Font-Bold="True" 
                    Text="مدى السرعة فى تعلم و تطبيق الإجراءات و المهام الجديدة ومدى الإستعداد لتحمل مهام أوأعباء جديدة" 
                    Font-Size="11pt" Font-Names="Arial" ForeColor="Gray"></asp:Label>
        </span>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <asp:Image ID="Image7" runat="server" ImageUrl="~/Images/line3.png" />
            </td>
        </tr>
        <tr>
            <td>
    <span  style="font-size:11pt; font-weight:bold; color:#000000; font-family:Courier New;">
            <asp:Label ID="Label52" runat="server" Font-Bold="True" 
                    Text="الحضور والقوة الذهنية" Font-Size="16pt" Font-Names="Arial" 
                    ForeColor="Black"></asp:Label>
                <br />
            <asp:Label ID="Label53" runat="server" Font-Bold="True" 
                    Text="Intellectual Horsepower" Font-Size="16pt" Font-Names="Arial" 
                    ForeColor="Black"></asp:Label>
        </span>
            </td>
            <td>
                <asp:DropDownList ID="eval_horsepower" runat="server" Font-Bold="True" 
                    Font-Size="14pt" Width="80px">
                    <asp:ListItem Value="0">اختر</asp:ListItem>
                    <asp:ListItem>1</asp:ListItem>
                    <asp:ListItem>2</asp:ListItem>
                    <asp:ListItem>3</asp:ListItem>
                    <asp:ListItem>4</asp:ListItem>
                    <asp:ListItem>5</asp:ListItem>
                </asp:DropDownList>
            </td>
            <td>
                <asp:TextBox ID="just_horsepower" runat="server" Height="70px" 
                    TextMode="MultiLine" Width="280px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td colspan="3">
    <span  style="font-size:11pt; font-weight:bold; color:#000000; font-family:Courier New;">
            <asp:Label ID="Label58" runat="server" Font-Bold="True" 
                    Text="القدرة الذهنية علي ربط وفهم الأحداث والمفاهيم الصعبة" Font-Size="11pt" 
                    Font-Names="Arial" ForeColor="Gray"></asp:Label>
        </span>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <asp:Image ID="Image8" runat="server" ImageUrl="~/Images/line3.png" />
            </td>
        </tr>
        <tr>
            <td>
    <span  style="font-size:11pt; font-weight:bold; color:#000000; font-family:Courier New;">
            <asp:Label ID="Label54" runat="server" Font-Bold="True" Text="حل المشكلات" 
                    Font-Size="16pt" Font-Names="Arial" ForeColor="Black"></asp:Label>
                <br />
            <asp:Label ID="Label55" runat="server" Font-Bold="True" Text="Problem Solving" 
                    Font-Size="16pt" Font-Names="Arial" ForeColor="Black"></asp:Label>
        </span>
            </td>
            <td>
                <asp:DropDownList ID="eval_problem_solve" runat="server" Font-Bold="True" 
                    Font-Size="14pt" Width="80px">
                    <asp:ListItem Value="0">اختر</asp:ListItem>
                    <asp:ListItem>1</asp:ListItem>
                    <asp:ListItem>2</asp:ListItem>
                    <asp:ListItem>3</asp:ListItem>
                    <asp:ListItem>4</asp:ListItem>
                    <asp:ListItem>5</asp:ListItem>
                </asp:DropDownList>
            </td>
            <td>
                <asp:TextBox ID="just_problem_solve" runat="server" Height="70px" 
                    TextMode="MultiLine" Width="280px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td colspan="3">
    <span  style="font-size:11pt; font-weight:bold; color:#000000; font-family:Courier New;">
            <asp:Label ID="Label59" runat="server" Font-Bold="True" 
                    Text="القدرة علي تحديد المشكلات و ايجاد الحلول المناسبة" Font-Size="11pt" 
                    Font-Names="Arial" ForeColor="Gray"></asp:Label>
        </span>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <asp:Image ID="Image9" runat="server" ImageUrl="~/Images/line3.png" />
            </td>
        </tr>
        <tr>
            <td>
    <span  style="font-size:11pt; font-weight:bold; color:#000000; font-family:Courier New;">
            <asp:Label ID="Label62" runat="server" Font-Bold="True" Text="السلوك الايجابي" 
                    Font-Size="16pt" Font-Names="Arial" ForeColor="Black"></asp:Label>
                <br />
            <asp:Label ID="Label63" runat="server" Font-Bold="True" Text="Positive Attitude " 
                    Font-Size="16pt" Font-Names="Arial" ForeColor="Black"></asp:Label>
        </span>
            </td>
            <td>
                <asp:DropDownList ID="eval_pos_attitude" runat="server" Font-Bold="True" 
                    Font-Size="14pt" Width="80px">
                    <asp:ListItem Value="0">اختر</asp:ListItem>
                    <asp:ListItem>1</asp:ListItem>
                    <asp:ListItem>2</asp:ListItem>
                    <asp:ListItem>3</asp:ListItem>
                    <asp:ListItem>4</asp:ListItem>
                    <asp:ListItem>5</asp:ListItem>
                </asp:DropDownList>
            </td>
            <td>
                <asp:TextBox ID="just_pos_attitude" runat="server" Height="70px" TextMode="MultiLine" 
                    Width="280px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td colspan="3">
    <span  style="font-size:11pt; font-weight:bold; color:#000000; font-family:Courier New;">
            <asp:Label ID="Label64" runat="server" Font-Bold="True" 
                    Text="يمتلك سلوك ايجابي" Font-Size="11pt" 
                    Font-Names="Arial" ForeColor="Gray"></asp:Label>
        </span>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <asp:Image ID="Image10" runat="server" ImageUrl="~/Images/line3.png" />
            </td>
        </tr>
        <tr>
            <td align="center">
                &nbsp;</td>
            <td align="center">
        <asp:Button ID="btn_print0" runat="server" BackColor="#FF33CC" Font-Bold="True" 
            Font-Names="Arial" ForeColor="White" onclick="btn_print_Click" Text="طباعة" 
            Width="250px" Font-Size="15pt" />
            </td>
            <td align="center">
                &nbsp;</td>
        </tr>
        <tr>
            <td align="center">
    <span  style="font-size:11pt; font-weight:bold; color:#000000; font-family:Courier New;">
            <asp:Label ID="Label60" runat="server" Font-Bold="True" Text="النتيجة النهائية" 
                    Font-Size="16pt" Font-Names="Arial" ForeColor="#0000CC"></asp:Label>
        </span>
            </td>
            <td align="center">
            <asp:Button ID="btn_save" runat="server" BackColor="#3366FF" Font-Bold="True" 
                Font-Size="15pt" ForeColor="White" Text="حفظ التقييم" Width="250px" 
                    onclick="btn_save_Click" />
            </td>
            <td align="center">
    <span  style="font-size:11pt; font-weight:bold; color:#000000; font-family:Courier New;">
            <asp:Label ID="Label61" runat="server" Font-Bold="True" Text="المدير المباشر" 
                    Font-Size="16pt" Font-Names="Arial" ForeColor="#0000CC"></asp:Label>
        </span>
            </td>
        </tr>
        <tr>
            <td align="center">
                <asp:TextBox ID="txt_down_result" runat="server" BackColor="#FF9933" 
                    BorderStyle="Solid" Font-Bold="True" Font-Size="15pt" Width="100px" 
                    Enabled="False"></asp:TextBox>
            </td>
            <td align="center">
            <asp:Button ID="btn_delete" runat="server" BackColor="Black" Font-Bold="True" 
                Font-Size="15pt" ForeColor="White" Text="إلغاء التقييم" Width="250px" 
                    onclick="btn_delete_Click" onclientclick="Confirm()" />
            </td>
            <td align="center">
                <asp:TextBox ID="txt_manager" runat="server" BackColor="#FF9933" 
                    BorderStyle="Solid" Enabled="False" Font-Bold="True" Font-Size="15pt" 
                    Width="250px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td align="center">
            <asp:Button ID="btn_calculate" runat="server" BackColor="#009999" Font-Bold="True" 
                Font-Size="15pt" ForeColor="White" Text="احتساب النتيجة" Width="150px" 
                    onclick="btn_calculate_Click" />
            </td>
            <td align="center">
            <asp:Button ID="btn_approve" runat="server" BackColor="#FF0066" Font-Bold="True" 
                Font-Size="15pt" ForeColor="White" Text="موافقة المدير" Width="250px" 
                    onclick="btn_approve_Click" />
            </td>
            <td align="center">
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
    </table>



</asp:Content>

