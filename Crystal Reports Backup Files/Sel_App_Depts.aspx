﻿<%@ Page Language="C#" MasterPageFile="~/Main_Master_Page.master" AutoEventWireup="true" CodeFile="Sel_App_Depts.aspx.cs" Inherits="FinalVersion_NewAssesment_SelDepts" Title="إختيار الإدارة" %>

<%@ Register assembly="Telerik.Web.UI" namespace="Telerik.Web.UI" tagprefix="telerik" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
 <meta name="viewport" content="width=device-width, initial-scale=1">
     <link href="../CSS/StyleSheet2.css" rel="stylesheet" type="text/css" />
   <link rel="Stylesheet"   href="../CSS/bootstrap.min.css"/>
  
  <script src="../JavaScript/jquery.min.js"></script>
  <script src="../JavaScript/bootstrap.min.js"></script>
   <script type="text/javascript">
        function showModal() {
            $("#myModal").modal('show');
        }

        $(function () {
            $("#btnShow").click(function () {
                showModal();
            });
        });
        
         function showConfModal() {
            $("#confirm-delete").modal('show');
        }

        $(function () {
            $("#btnShow").click(function () {
                showConfModal();
            });
        });
        
   
    </script>
<table style="width: 100%;" align="center">
 <tr>
    <td style="background-color:#71625F; height: 2px; width: 70%;" >
    <span  style="font-size:11pt; font-weight:bold; color:#FFFFFF; font-family:Arial;">
        &nbsp; إختيار الإدارة التابعة </span>
    </td></tr>
 </table>
    <p>
        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>
    </p>
    <p>
         <table align="center" dir="rtl" style="width: 98%; height: 98%; float: right">
        <tr>
            <td align="center" colspan="4">
                <asp:DropDownList ID="ddl_departments" runat="server" Font-Bold="True" 
                    Font-Size="25pt" Width="400px" ForeColor="#FF6600">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td style="height: 15px">
                &nbsp;</td>
            <td style="height: 15px" align="center" colspan="2">
                &nbsp;</td>
            <td style="height: 15px" align="center">
                &nbsp;</td>
        </tr>
        <tr>
            <td style="height: 15px">
                &nbsp;</td>
            <td style="height: 15px" align="center" colspan="2">
                <span style="font-size:11pt; font-weight:bold; color:#000000; font-family:Courier New;">
                                <asp:Button ID="btn_approve" runat="server"  Font-Bold="True" 
                                    Font-Names="Arial" Font-Size="15pt" 
                    onclick="btn_approve_Click"  class="btn btn-success" Text="دخــــــــــــــــول" 
                                    Width="250px" ForeColor="White" />
                                </span>
            </td>
            <td style="height: 15px" align="center">
                &nbsp;</td>
        </tr>
        <tr>
            <td align="center" colspan="3">
                
                <telerik:RadNotification ID="RadNotification1" runat="server" Position="Center" 
                    Skin="Hay" Width="300px">
<NotificationMenu ID="TitleMenu"></NotificationMenu>
                </telerik:RadNotification>
            </td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td align="center" colspan="3">
                
                <asp:Image ID="Image1" runat="server" ImageUrl="~/Images/deptt.png" 
                    Width="269px" />
            </td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        </table><br />
    </p>
 <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog modal-sm">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">نظام تقييم الموظفين</h4>
        </div>
        <div class="modal-body">
          <asp:Label ID="lblMessage" runat="server" Font-Bold="True" Font-Size="12pt" 
                Font-Names="Arial" ForeColor="Red"></asp:Label>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>
  <div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
             نظام تقييم الموظفين  
            </div>
            <div class="modal-body">
               <asp:Label ID="lblConf_Message" runat="server" Font-Bold="True" Font-Size="12pt"  Font-Names="Arial" ForeColor="Red"></asp:Label>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">خروج</button>
                <asp:Button ID="Button2" class="btn btn-danger btn-ok" runat="server" onclick="btnapp_Click" Text="موافق" />
            </div>
        </div>
    </div>
</div>
</asp:Content>

