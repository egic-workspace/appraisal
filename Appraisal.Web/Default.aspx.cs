using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.DirectoryServices;
using System.Runtime.InteropServices;
using System.Web.UI;

public partial class Default : Page
{
    AppService _appService = new AppService();
    private bool isTestEnvironment = false;

    //[DllImport("ADVAPI32.dll", EntryPoint = "LogonUserW", SetLastError = true, CharSet = CharSet.Auto)]
    //public static extern bool LogonUser(string lpszUsername, string lpszDomain, string lpszPassword, int dwLogonType, int dwLogonProvider, ref IntPtr phToken);

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (!isTestEnvironment)
            {
                lbluser_code.Visible = false;
                txt_user_code.Visible = false;
            }
        }
    }

    protected void Page_PreInit(object sender, EventArgs e)
    {
        MidYearVersionHelper.ResetIsMidYearVersion();
        Session.Clear();
        string connStr = ConfigurationManager.ConnectionStrings["ASS_ConnectionString"].ConnectionString;
        isTestEnvironment = !string.IsNullOrWhiteSpace(connStr) && connStr.Contains("HOST=10.110.10.70");
    }

    private void Get_Active_Data()
    {
        string connection = ConfigurationManager.ConnectionStrings["ADConnection"].ToString();
        DirectorySearcher dssearch = new DirectorySearcher(connection);
        dssearch.Filter = "(sAMAccountName=" + txt_usr_name.Text + ")";
        SearchResult sresult = dssearch.FindOne();
        DirectoryEntry dsresult = sresult.GetDirectoryEntry();


        string f_name = "";
        if (dsresult.Properties["givenName"] != null && dsresult.Properties["givenName"].Count > 0)
            f_name = dsresult.Properties["givenName"][0].ToString();

        string l_name = "";
        if (dsresult.Properties["sn"] != null && dsresult.Properties["sn"].Count > 0)
            l_name = dsresult.Properties["sn"][0].ToString();

        Session["NAME"] = f_name + " " + l_name;
        if (dsresult.Properties["title"] != null && dsresult.Properties["title"].Count > 0)
            Session["TITLE"] = dsresult.Properties["title"][0].ToString();

        if (dsresult.Properties["department"] != null && dsresult.Properties["department"].Count > 0)
            Session["DEPT"] = dsresult.Properties["department"][0].ToString();

        if (dsresult.Properties["mail"] != null && dsresult.Properties["mail"].Count > 0)
            Session["MAIL"] = dsresult.Properties["mail"][0].ToString();

        if (dsresult.Properties["description"] != null && dsresult.Properties["description"].Count > 0)
            Session["EMP_NO"] = dsresult.Properties["description"][0].ToString();
        string xx = Session["EMP_NO"].ToString();
    }

    public static string GetDomainName(string usernameDomain)
    {
        if (string.IsNullOrEmpty(usernameDomain))
        {
            throw (new ArgumentException("Argument can't be null.", "usernameDomain"));
        }
        if (usernameDomain.Contains("\\"))
        {
            int index = usernameDomain.IndexOf("\\");
            return usernameDomain.Substring(0, index);
        }
        else if (usernameDomain.Contains("@"))
        {
            int index = usernameDomain.IndexOf("@");
            return usernameDomain.Substring(index + 1);
        }
        else
        {
            return "";
        }
    }

    protected void btn_enter_Click(object sender, EventArgs e)
    {
        string domainName = dd_domain.SelectedValue.ToString();
        string userName = GetUsername(txt_usr_name.Text);
        IntPtr token = IntPtr.Zero;

        //bool result = LogonUser(userName, domainName, txt_password.Text, 2, 0, ref token);
        bool result = false;
        DirectoryEntry entry = new DirectoryEntry("LDAP://Egic.com.eg", userName, txt_password.Text);
        DirectorySearcher searcher = new DirectorySearcher(entry);
        try
        {
            SearchResult adsSearchResult = searcher.FindOne();
            result = true;
        }
        catch (Exception ex)
        {
            result = false;
        }
        if (result)
        {
            Get_Active_Data();
            int Lock = _appService.Check_Locked_Department(Session["EMP_NO"].ToString());

            if (Lock == 1)
            {
                if (isTestEnvironment)
                    Session["EMP_NO"] = txt_user_code.Text;

                Session["USER"] = userName;
                Session["DOMAIN"] = domainName;
                Session["Logged_User"] = userName;

                int ISdirector = _appService.Check_Ass_Departments(Session["EMP_NO"].ToString());
                if (ISdirector == 0 || ISdirector == 1)
                {
                    Session["SEL_DEPT"] = "ALL";
                    Response.Redirect("~/Home_Page.aspx");
                }
                else
                {
                    // menuItems[i].NavigateUrl = string.Format("~/{0}NewAssesment/SelDepts.aspx", finalVersionUrl);
                    string year = _appService.Get_Ass_Open_Year().ToString();
                    string version = _appService.Get_Ass_Open_Version().ToString();
                    string finalVersionUrl = MidYearVersionHelper.IsMidYearVersion(year, version) ? "" : "FinalVersion/";
                    if (finalVersionUrl == "FinalVersion/")
                    {
                        Response.Redirect("~/FinalVersion/NewAssesment/SelDepts.aspx");
                    }
                    else
                    {
                        Response.Redirect("~/Home_Page.aspx");
                    }

                }

            }
            else
            {
                lbl_message.Text = " ���� �� ����� ������ ��� ������� �� ��� ������� ";
                return;
            }
        }
        else
        {
            lbl_message.Text = "��� �� ���� ������ �� ��������";
            return;
        }
    }

    public static string GetUsername(string usernameDomain)
    {
        if (string.IsNullOrEmpty(usernameDomain))
        {
            throw (new ArgumentException("Argument can't be null.", "usernameDomain"));
        }
        if (usernameDomain.Contains("\\"))
        {
            int index = usernameDomain.IndexOf("\\");
            return usernameDomain.Substring(index + 1);
        }
        else if (usernameDomain.Contains("@"))
        {
            int index = usernameDomain.IndexOf("@");
            return usernameDomain.Substring(0, index);
        }
        else
        {
            return usernameDomain;
        }
    }

    protected void btn_cancel_Click(object sender, EventArgs e)
    {
        txt_usr_name.Text = "";
        txt_password.Text = "";
    }
}