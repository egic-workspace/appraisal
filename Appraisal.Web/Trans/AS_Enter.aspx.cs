﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;


public partial class Trans_AS_Enter : System.Web.UI.Page
{
    AppService ser = new AppService();
    
    DataTable dt = new DataTable();

    protected void Page_Load(object sender, EventArgs e)
    {
       
        if (!IsPostBack)
        {
            if (Session["Logged_User"] == null)
            {
                //Response.Redirect("~/Default.aspx");
            }
            //****************************
            TXT_OPEN_YEAR.Text = ser.Get_Open_Year().ToString();
            //****************************
            try
            {
                  Fill_Grid_First_Level();
                //Fill_Grid_ALL();
                //int Emp_count = ser.Get_First_Emp_Level_Count(Session["EMP_NO"].ToString());
                //double bad = ser.Get_Bad_Results(Session["Logged_User"].ToString(), "2", Session["EMP_NO"].ToString());
                //double mid = ser.Get_Midum_Results(Session["Logged_User"].ToString(), "2", Session["EMP_NO"].ToString());
                //double god = ser.Get_Good_Results(Session["Logged_User"].ToString(), "2", Session["EMP_NO"].ToString());
                //double vgd = ser.Get_Very_Good_Results(Session["Logged_User"].ToString(), "2", Session["EMP_NO"].ToString());
                //double ext = ser.Get_Exellent_Results(Session["Logged_User"].ToString(), "2", Session["EMP_NO"].ToString());

                lbl_bad.Text = "0" + "%";
                //lbl_midum.Text = "0" + "%";
                lbl_good.Text = "0" + "%";
                lbl_very_good.Text = "0" + "%";
                lbl_exelent.Text = "0" + "%";
            }
            catch
            {

            }
            // Fill_Grid_ALL();
            //**************************
           // Session["EMP_NO"] = "1395";
        }
    }
    private void Fill_Grid_ALL()
    {
        try
        {
            //Session["EMP_NO"] = "1395";
            Clear_Grids();
            
            string EMP_NO = Session["EMP_NO"].ToString();
           // dt = ser.Bind_ENTER_EMP_GRID(EMP_NO);
            dt = ser.Bind_ENTER_EMP_GRID(EMP_NO, TXT_OPEN_YEAR.Text);
            //GridView1.DataSource = null;
            GridView1.DataSource = dt;
            GridView1.DataBind();
        }
        catch
        {

        }
    }
    private void Fill_Grid_First_Level()
    {
        try
        {
           // Session["EMP_NO"] = "1395";
            Clear_Grids();
            
            string EMP_NO = Session["EMP_NO"].ToString();
            // dt = ser.Bind_ENTER_EMP_GRID(EMP_NO);
            dt = ser.Bind_ENTER_EMP_FRST_LEVEL_GRID(EMP_NO, TXT_OPEN_YEAR.Text);
            //GridView1.DataSource = null;
            GridView1.DataSource = dt;
            GridView1.DataBind();
        }
        catch
        {

        }
    }
    private void Clear_Grids()
    {
        GridView1.DataSource = null;
        GridView1.DataBind();
    }
    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {

    }
    protected void GridView1_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            Session["SFORMID"] = ((Label)GridView1.SelectedRow.FindControl("lblform_id")).Text;
            Session["S_EMP_NAME"] = ((Label)GridView1.SelectedRow.FindControl("lblemp_name")).Text;
            Session["S_EMP_NO"] = ((Label)GridView1.SelectedRow.FindControl("lblemp_no")).Text;
            Session["S_YEAR"] = TXT_OPEN_YEAR.Text;
            Session["ASS_STATUS"] = ((Label)GridView1.SelectedRow.FindControl("lblstatus")).Text;
            Session["S_LEVEL"] = ((Label)GridView1.SelectedRow.FindControl("lbllevel")).Text;
            string MyPage = ser.Get_Page_Name(Session["SFORMID"].ToString());
            Session["DISPLAY"] = "NO";
            Response.Redirect("~/Trans/" + MyPage);
            //Response.Redirect("~/Trans/AS_Enter.aspx");
        }
        catch
        {
            
        }
    }
    protected void GridView1_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
    {

    }
    protected void btn_frst_level_Click(object sender, EventArgs e)
    {
        try
        {
            Fill_Grid_First_Level();
        }
        catch
        {
 
        }
    }
    protected void btn_all_levels_Click(object sender, EventArgs e)
    {
       
        try
        {
            Fill_Grid_ALL();
            //int Emp_count = ser.Get_All_Emp_Level_Count(Session["EMP_NO"].ToString());
            //double bad = ser.Get_Bad_Results(Session["Logged_User"].ToString(), "00", Session["EMP_NO"].ToString());
            //double mid = ser.Get_Midum_Results(Session["Logged_User"].ToString(), "00", Session["EMP_NO"].ToString());
            //double god = ser.Get_Good_Results(Session["Logged_User"].ToString(), "00", Session["EMP_NO"].ToString());
            //double vgd = ser.Get_Very_Good_Results(Session["Logged_User"].ToString(), "00", Session["EMP_NO"].ToString());
            //double ext = ser.Get_Exellent_Results(Session["Logged_User"].ToString(), "00", Session["EMP_NO"].ToString());
            //lblResultComp.Text = "نتائج موظفي EGIC";
            //lbl_bad.Text = Math.Round((bad / Emp_count) * 100, 2) + "%";
            //lbl_midum.Text = Math.Round((mid / Emp_count) * 100, 2) + "%";
            //lbl_good.Text = Math.Round((god / Emp_count) * 100, 2) + "%";
            //lbl_very_good.Text = Math.Round((vgd / Emp_count) * 100, 2) + "%";
            //lbl_exelent.Text = Math.Round((ext / Emp_count) * 100, 2) + "%";
        }
        catch
        {

        }
    }
    protected void btn_calc_Click(object sender, EventArgs e)
    {
        try
        {
            int Emp_count = ser.Get_First_Emp_Level_Count(Session["EMP_NO"].ToString(), "EGIC", "ALL");
            double bad = ser.Get_Bad_Results(Session["Logged_User"].ToString(), "2", Session["EMP_NO"].ToString());
            double mid = ser.Get_Midum_Results(Session["Logged_User"].ToString(), "2", Session["EMP_NO"].ToString());
            double god = ser.Get_Good_Results(Session["Logged_User"].ToString(), "2", Session["EMP_NO"].ToString());
            double vgd = ser.Get_Very_Good_Results(Session["Logged_User"].ToString(), "2", Session["EMP_NO"].ToString());
            double ext = ser.Get_Exellent_Results(Session["Logged_User"].ToString(), "2", Session["EMP_NO"].ToString());
            lblResultComp.Text = "نتائج موظفي EGIC مستوى اول";
            bad = bad + mid;
            lbl_bad.Text = Math.Round((bad / Emp_count) * 100, 2) + "%";
            //lbl_midum.Text = Math.Round((mid / Emp_count) * 100, 2) + "%";
            lbl_good.Text = Math.Round((god / Emp_count) * 100, 2) + "%";
            lbl_very_good.Text = Math.Round((vgd / Emp_count) * 100, 2) + "%";
            lbl_exelent.Text = Math.Round((ext / Emp_count) * 100, 2) + "%";
        }
        catch
        {
            lbl_bad.Text = "0" + "%";
            //lbl_midum.Text = "0" + "%";
            lbl_good.Text = "0" + "%";
            lbl_very_good.Text = "0" + "%";
            lbl_exelent.Text = "0" + "%";
        }
    }
    protected void btn_calcOS_Click(object sender, EventArgs e)
    {
        int Emp_count = ser.Get_First_Emp_Level_Count_OS(Session["EMP_NO"].ToString(), "ALL");
        double bad = ser.Get_Bad_Results_OS(Session["Logged_User"].ToString(), "2", Session["EMP_NO"].ToString());
        double mid = ser.Get_Midum_Results_OS(Session["Logged_User"].ToString(), "2", Session["EMP_NO"].ToString());
        double god = ser.Get_Good_Results_OS(Session["Logged_User"].ToString(), "2", Session["EMP_NO"].ToString());
        double vgd = ser.Get_Very_Good_Results_OS(Session["Logged_User"].ToString(), "2", Session["EMP_NO"].ToString());
        double ext = ser.Get_Exellent_Results_OS(Session["Logged_User"].ToString(), "2", Session["EMP_NO"].ToString());
        lblResultComp.Text = "نتائج موظفي OutSource مستوى اول";
        bad = bad + mid;
        lbl_bad.Text = Math.Round((bad / Emp_count) * 100, 2) + "%";
        //lbl_midum.Text = Math.Round((mid / Emp_count) * 100, 2) + "%";
        lbl_good.Text = Math.Round((god / Emp_count) * 100, 2) + "%";
        lbl_very_good.Text = Math.Round((vgd / Emp_count) * 100, 2) + "%";
        lbl_exelent.Text = Math.Round((ext / Emp_count) * 100, 2) + "%";
    }
    protected void btn_calcAll_Click(object sender, EventArgs e)
    {
        int Emp_count = ser.Get_All_Emp_Level_Count(Session["EMP_NO"].ToString(), "EGIC", "ALL");
        double bad = ser.Get_Bad_Results(Session["Logged_User"].ToString(), "00", Session["EMP_NO"].ToString());
        double mid = ser.Get_Midum_Results(Session["Logged_User"].ToString(), "00", Session["EMP_NO"].ToString());
        double god = ser.Get_Good_Results(Session["Logged_User"].ToString(), "00", Session["EMP_NO"].ToString());
        double vgd = ser.Get_Very_Good_Results(Session["Logged_User"].ToString(), "00", Session["EMP_NO"].ToString());
        double ext = ser.Get_Exellent_Results(Session["Logged_User"].ToString(), "00", Session["EMP_NO"].ToString());
        lblResultComp.Text = "نتائج موظفي EGIC جميع المستويات";
        bad = bad + mid;
        lbl_bad.Text = Math.Round((bad / Emp_count) * 100, 2) + "%";
        //lbl_midum.Text = Math.Round((mid / Emp_count) * 100, 2) + "%";
        lbl_good.Text = Math.Round((god / Emp_count) * 100, 2) + "%";
        lbl_very_good.Text = Math.Round((vgd / Emp_count) * 100, 2) + "%";
        lbl_exelent.Text = Math.Round((ext / Emp_count) * 100, 2) + "%";
    }
    protected void btn_calcOSAll_Click(object sender, EventArgs e)
    {
        int Emp_count = ser.Get_All_Emp_Level_Count_OS(Session["EMP_NO"].ToString(), "ALL");
        double bad = ser.Get_Bad_Results_OS(Session["Logged_User"].ToString(), "00", Session["EMP_NO"].ToString());
        double mid = ser.Get_Midum_Results_OS(Session["Logged_User"].ToString(), "00", Session["EMP_NO"].ToString());
        double god = ser.Get_Good_Results_OS(Session["Logged_User"].ToString(), "00", Session["EMP_NO"].ToString());
        double vgd = ser.Get_Very_Good_Results_OS(Session["Logged_User"].ToString(), "00", Session["EMP_NO"].ToString());
        double ext = ser.Get_Exellent_Results_OS(Session["Logged_User"].ToString(), "00", Session["EMP_NO"].ToString());
        lblResultComp.Text = "نتائج موظفي OutSource جميع المستويات";
        bad = bad + mid;
        lbl_bad.Text = Math.Round((bad / Emp_count) * 100, 2) + "%";
        //lbl_midum.Text = Math.Round((mid / Emp_count) * 100, 2) + "%";
        lbl_good.Text = Math.Round((god / Emp_count) * 100, 2) + "%";
        lbl_very_good.Text = Math.Round((vgd / Emp_count) * 100, 2) + "%";
        lbl_exelent.Text = Math.Round((ext / Emp_count) * 100, 2) + "%";
    }
    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {

    }
}
