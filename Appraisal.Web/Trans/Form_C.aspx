﻿<%@ Page Language="C#" MasterPageFile="~/Main_Master_Page.master" AutoEventWireup="true" CodeFile="Form_C.aspx.cs" Inherits="Trans_Form_C" Title=" Form (3)" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
 <script type = "text/javascript">
        function Confirm() {
            var confirm_value = document.createElement("INPUT");
            confirm_value.type = "hidden";
            confirm_value.name = "confirm_value";
            if (confirm("هل تريد حذف التقييم ?")) {
                confirm_value.value = "Yes";
            } else {
                confirm_value.value = "No";
            }
            document.forms[0].appendChild(confirm_value);
        }
    </script>
    <table style="width: 100%;" align="center">
 <tr>
    <td style="background-color:#e6f1f8; height: 2px; width: 70%;" >
    <span  style="font-size:11pt; font-weight:bold; color:#000080; font-family:Courier New;">
        &nbsp; نموذج تقييم رقم (3) </span>
    </td></tr>
 </table>
 <table style="width: 100%;" align="center">
 <tr>
    <td style="background-color:#FF9933; height: 2px; width: 70%;" >
    <span  style="font-size:11pt; font-weight:bold; color:#000000; font-family:Courier New;">
        &nbsp;  
            <asp:Label ID="Label8" runat="server" Font-Bold="True" Text="اسم الموظف" 
                Width="110px" Font-Size="16pt" Font-Names="Arial" ForeColor="Black"></asp:Label>
            <asp:Label ID="lbl_emp_name" runat="server" Font-Bold="True" Text="EMP_NAME" 
                Width="300px" Font-Size="16pt" Font-Names="Arial" ForeColor="#0000CC"></asp:Label>
    <span  style="font-size:11pt; font-weight:bold; color:#000000; font-family:Courier New;">
            <asp:Label ID="Label80" runat="server" Font-Bold="True" Text="نتيجة التقييم" 
                Width="110px" Font-Size="16pt" Font-Names="Arial" ForeColor="Black"></asp:Label>
        <asp:TextBox ID="txt_top_result" runat="server" BackColor="#33CCFF" 
            BorderStyle="Solid" Enabled="False" Font-Bold="True" Font-Size="13pt"></asp:TextBox>
        <asp:Button ID="btn_print" runat="server" BackColor="#FF33CC" Font-Bold="True" 
            Font-Names="Arial" ForeColor="White" onclick="btn_print_Click" Text="طباعة" 
            Width="100px" Font-Size="11pt" />
        </span>
        </span>
    </td></tr>
 </table>

    <table dir="rtl" style="width: 98%; height: 98%; float: right">
        <tr align="center" style="background-color: #C0C0C0">
            <td>
    <span  style="font-size:11pt; font-weight:bold; color:#000000; font-family:Courier New;">
            <asp:Label ID="lbl_4" runat="server" Font-Bold="True" Text="تقييم الأهداف" 
                    Font-Size="20pt" Font-Names="Arial" ForeColor="#0000CC"></asp:Label>
        </span>
            </td>
            <td align="center">
    <span  style="font-size:11pt; font-weight:bold; color:#000000; font-family:Courier New;">
            <asp:Label ID="lbl_5" runat="server" Font-Bold="True" Text="الوزن النسبي" 
                    Font-Size="13pt" Font-Names="Arial" ForeColor="#0000CC"></asp:Label>
        </span>
            </td>
            <td align="center">
    <span  style="font-size:11pt; font-weight:bold; color:#000000; font-family:Courier New;">
            <asp:Label ID="lbl_7" runat="server" Font-Bold="True" Text="النسبة المحققة" 
                    Font-Size="13pt" Font-Names="Arial" ForeColor="#0000CC"></asp:Label>
        </span>
            </td>
            <td align="center">
    <span  style="font-size:11pt; font-weight:bold; color:#000000; font-family:Courier New;">
            <asp:Label ID="lbl_6" runat="server" Font-Bold="True" Text="التبرير" 
                    Font-Size="20pt" Font-Names="Arial" ForeColor="#0000CC"></asp:Label>
        </span>
            </td>
        </tr>
        <tr>
            <td>
    <span  style="font-size:11pt; font-weight:bold; color:#000000; font-family:Courier New;">
            <asp:Label ID="lbl_8" runat="server" Font-Bold="True" Text="هدف 1" 
                    Font-Size="13pt" Font-Names="Arial" ForeColor="#FF3300"></asp:Label>
        </span>
                <asp:TextBox ID="g1_name" runat="server" Font-Bold="True" Font-Size="13pt" 
                    Width="330px" TextMode="MultiLine"></asp:TextBox>
            </td>
            <td align="center">
                <asp:TextBox ID="g1_weight" runat="server" Font-Bold="True" Font-Size="13pt" 
                    Width="80px">0</asp:TextBox>
            </td>
            <td align="center">
                <asp:TextBox ID="g1_actual" runat="server" Font-Bold="True" Font-Size="13pt" 
                    Width="80px">0</asp:TextBox>
            </td>
            <td>
                <asp:TextBox ID="g1_just" runat="server" Font-Bold="True" Font-Size="13pt" 
                    Width="300px" TextMode="MultiLine"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
    <span  style="font-size:11pt; font-weight:bold; color:#000000; font-family:Courier New;">
            <asp:Label ID="lbl_9" runat="server" Font-Bold="True" Text="هدف 2" 
                    Font-Size="13pt" Font-Names="Arial" ForeColor="#FF3300"></asp:Label>
                <asp:TextBox ID="g2_name" runat="server" Font-Bold="True" Font-Size="13pt" 
                    Width="330px" TextMode="MultiLine"></asp:TextBox>
        </span>
            </td>
            <td align="center">
                <asp:TextBox ID="g2_weight" runat="server" Font-Bold="True" Font-Size="13pt" 
                    Width="80px">0</asp:TextBox>
            </td>
            <td align="center">
                <asp:TextBox ID="g2_actual" runat="server" Font-Bold="True" Font-Size="13pt" 
                    Width="80px">0</asp:TextBox>
            </td>
            <td>
                <asp:TextBox ID="g2_just" runat="server" Font-Bold="True" Font-Size="13pt" 
                    Width="300px" TextMode="MultiLine"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
    <span  style="font-size:11pt; font-weight:bold; color:#000000; font-family:Courier New;">
            <asp:Label ID="lbl_10" runat="server" Font-Bold="True" Text="هدف 3" 
                    Font-Size="13pt" Font-Names="Arial" ForeColor="#FF3300"></asp:Label>
                <asp:TextBox ID="g3_name" runat="server" Font-Bold="True" Font-Size="13pt" 
                    Width="330px" TextMode="MultiLine"></asp:TextBox>
        </span>
            </td>
            <td align="center">
                <asp:TextBox ID="g3_weight" runat="server" Font-Bold="True" Font-Size="13pt" 
                    Width="80px">0</asp:TextBox>
            </td>
            <td align="center">
                <asp:TextBox ID="g3_actual" runat="server" Font-Bold="True" Font-Size="13pt" 
                    Width="80px">0</asp:TextBox>
            </td>
            <td>
                <asp:TextBox ID="g3_just" runat="server" Font-Bold="True" Font-Size="13pt" 
                    Width="300px" TextMode="MultiLine"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
    <span  style="font-size:11pt; font-weight:bold; color:#000000; font-family:Courier New;">
            <asp:Label ID="lbl_11" runat="server" Font-Bold="True" Text="هدف 4" 
                    Font-Size="13pt" Font-Names="Arial" ForeColor="#FF3300"></asp:Label>
                <asp:TextBox ID="g4_name" runat="server" Font-Bold="True" Font-Size="13pt" 
                    Width="330px" TextMode="MultiLine"></asp:TextBox>
        </span>
            </td>
            <td align="center">
                <asp:TextBox ID="g4_weight" runat="server" Font-Bold="True" Font-Size="13pt" 
                    Width="80px">0</asp:TextBox>
            </td>
            <td align="center">
                <asp:TextBox ID="g4_actual" runat="server" Font-Bold="True" Font-Size="13pt" 
                    Width="80px">0</asp:TextBox>
            </td>
            <td>
                <asp:TextBox ID="g4_just" runat="server" Font-Bold="True" Font-Size="13pt" 
                    Width="300px" TextMode="MultiLine"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
    <span  style="font-size:11pt; font-weight:bold; color:#000000; font-family:Courier New;">
            <asp:Label ID="lbl_12" runat="server" Font-Bold="True" Text="هدف 5" 
                    Font-Size="13pt" Font-Names="Arial" ForeColor="#FF3300"></asp:Label>
                <asp:TextBox ID="g5_name" runat="server" Font-Bold="True" Font-Size="13pt" 
                    Width="330px" TextMode="MultiLine"></asp:TextBox>
        </span>
            </td>
            <td align="center">
                <asp:TextBox ID="g5_weight" runat="server" Font-Bold="True" Font-Size="13pt" 
                    Width="80px">0</asp:TextBox>
            </td>
            <td align="center">
                <asp:TextBox ID="g5_actual" runat="server" Font-Bold="True" Font-Size="13pt" 
                    Width="80px">0</asp:TextBox>
            </td>
            <td>
                <asp:TextBox ID="g5_just" runat="server" Font-Bold="True" Font-Size="13pt" 
                    Width="300px" TextMode="MultiLine"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
    <span  style="font-size:11pt; font-weight:bold; color:#000000; font-family:Courier New;">
            <asp:Label ID="lbl_13" runat="server" Font-Bold="True" Text="هدف 6" 
                    Font-Size="13pt" Font-Names="Arial" ForeColor="#FF3300"></asp:Label>
                <asp:TextBox ID="g6_name" runat="server" Font-Bold="True" Font-Size="13pt" 
                    Width="330px" TextMode="MultiLine"></asp:TextBox>
        </span>
            </td>
            <td align="center">
                <asp:TextBox ID="g6_weight" runat="server" Font-Bold="True" Font-Size="13pt" 
                    Width="80px">0</asp:TextBox>
            </td>
            <td align="center">
                <asp:TextBox ID="g6_actual" runat="server" Font-Bold="True" Font-Size="13pt" 
                    Width="80px">0</asp:TextBox>
            </td>
            <td>
                <asp:TextBox ID="g6_just" runat="server" Font-Bold="True" Font-Size="13pt" 
                    Width="300px" TextMode="MultiLine"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
    </table>

    <table align="center" dir="rtl" 
    style="border-width: thin; border-style: solid; width: 98%; height: 98px; float: right; background-color: #66CCFF;">
        <tr bgcolor="Black">
            <td align="center">
    <span  style="font-size:11pt; font-weight:bold; color:#000000; font-family:Courier New;">
            <asp:Label ID="Label9" runat="server" Font-Bold="True" Text="النسبة" 
                    Font-Size="11pt" Font-Names="Arial" ForeColor="White"></asp:Label>
        </span>
            </td>
            <td align="center">
    <span  style="font-size:11pt; font-weight:bold; color:#000000; font-family:Courier New;">
            <asp:Label ID="Label10" runat="server" Font-Bold="True" Text="التقييم" 
                    Font-Size="11pt" Font-Names="Arial" ForeColor="White"></asp:Label>
        </span>
            </td>
            <td align="center">
    <span  style="font-size:11pt; font-weight:bold; color:#000000; font-family:Courier New;">
            <asp:Label ID="Label11" runat="server" Font-Bold="True" Text="الشرح" 
                    Font-Size="11pt" Font-Names="Arial" ForeColor="White"></asp:Label>
        </span>
            </td>
            <td align="center">
    <span  style="font-size:11pt; font-weight:bold; color:#000000; font-family:Courier New;">
            <asp:Label ID="Label12" runat="server" Font-Bold="True" Text="الدرجة" 
                    Font-Size="11pt" Font-Names="Arial" ForeColor="White"></asp:Label>
        </span>
            </td>
        </tr>
        <tr>
            <td>
    <span  style="font-size:11pt; font-weight:bold; color:#000000; font-family:Courier New;">
            <asp:Label ID="Label13" runat="server" Font-Bold="True" Text="أقل من 60%" 
                    Font-Size="11pt" Font-Names="Arial" ForeColor="Black"></asp:Label>
        </span>
            </td>
            <td>
    <span  style="font-size:11pt; font-weight:bold; color:#000000; font-family:Courier New;">
            <asp:Label ID="Label14" runat="server" Font-Bold="True" Text="غير مرضي" 
                    Font-Size="11pt" Font-Names="Arial" ForeColor="Black"></asp:Label>
        </span>
            </td>
            <td>
    <span  style="font-size:11pt; font-weight:bold; color:#000000; font-family:Courier New;">
            <asp:Label ID="Label15" runat="server" Font-Bold="True" 
                    Text="أداء الموظف دون التوقعات بشكل مستمر ولا يتحسن , ويجب اتخاذ الإجراءات التصحيحية في الحال" 
                    Font-Size="11pt" Font-Names="Arial" ForeColor="Black"></asp:Label>
        </span>
            </td>
            <td>
    <span  style="font-size:11pt; font-weight:bold; color:#000000; font-family:Courier New;">
            <asp:Label ID="Label24" runat="server" Font-Bold="True" Text="1" Font-Size="11pt" 
                    Font-Names="Arial" ForeColor="Black"></asp:Label>
        </span>
            </td>
        </tr>
        <tr>
            <td>
    <span  style="font-size:11pt; font-weight:bold; color:#000000; font-family:Courier New;">
            <asp:Label ID="Label29" runat="server" Font-Bold="True" Text="من 61% إلي 70%" 
                    Font-Size="11pt" Font-Names="Arial" ForeColor="Black"></asp:Label>
        </span>
            </td>
            <td>
    <span  style="font-size:11pt; font-weight:bold; color:#000000; font-family:Courier New;">
            <asp:Label ID="Label20" runat="server" Font-Bold="True" Text="متوسط" 
                    Font-Size="11pt" Font-Names="Arial" ForeColor="Black"></asp:Label>
        </span>
            </td>
            <td>
    <span  style="font-size:11pt; font-weight:bold; color:#000000; font-family:Courier New;">
            <asp:Label ID="Label16" runat="server" Font-Bold="True" 
                    Text="الموظف يعمل علي تطوير الجدارات والمهارات ويرتقي في بعض الأحيان الي مستوي التوقعات وما زال يحتاج الي إشراف مستمر" 
                    Font-Size="11pt" Font-Names="Arial" ForeColor="Black"></asp:Label>
        </span>
            </td>
            <td>
    <span  style="font-size:11pt; font-weight:bold; color:#000000; font-family:Courier New;">
            <asp:Label ID="Label25" runat="server" Font-Bold="True" Text="2" Font-Size="11pt" 
                    Font-Names="Arial" ForeColor="Black"></asp:Label>
        </span>
            </td>
        </tr>
        <tr>
            <td>
    <span  style="font-size:11pt; font-weight:bold; color:#000000; font-family:Courier New;">
            <asp:Label ID="Label30" runat="server" Font-Bold="True" Text="من 71% إلي 85%" 
                    Font-Size="11pt" Font-Names="Arial" ForeColor="Black"></asp:Label>
        </span>
            </td>
            <td>
    <span  style="font-size:11pt; font-weight:bold; color:#000000; font-family:Courier New;">
            <asp:Label ID="Label21" runat="server" Font-Bold="True" Text="جيد" Font-Size="11pt" 
                    Font-Names="Arial" ForeColor="Black"></asp:Label>
        </span>
            </td>
            <td>
    <span  style="font-size:11pt; font-weight:bold; color:#000000; font-family:Courier New;">
            <asp:Label ID="Label17" runat="server" Font-Bold="True" 
                    Text="الموظف يمارس الجدارات المطلوبة ويرتقي في أغلب الأحيان الي مستوي التوقعات ويحتاج الي متابعة واشراف من آن الي آخر" 
                    Font-Size="11pt" Font-Names="Arial" ForeColor="Black"></asp:Label>
        </span>
            </td>
            <td>
    <span  style="font-size:11pt; font-weight:bold; color:#000000; font-family:Courier New;">
            <asp:Label ID="Label26" runat="server" Font-Bold="True" Text="3" Font-Size="11pt" 
                    Font-Names="Arial" ForeColor="Black"></asp:Label>
        </span>
            </td>
        </tr>
        <tr>
            <td>
    <span  style="font-size:11pt; font-weight:bold; color:#000000; font-family:Courier New;">
            <asp:Label ID="Label31" runat="server" Font-Bold="True" Text="من 86% إلي 95% " 
                    Font-Size="11pt" Font-Names="Arial" ForeColor="Black"></asp:Label>
        </span>
            </td>
            <td>
    <span  style="font-size:11pt; font-weight:bold; color:#000000; font-family:Courier New;">
            <asp:Label ID="Label22" runat="server" Font-Bold="True" Text="جيد جداً" 
                    Font-Size="11pt" Font-Names="Arial" ForeColor="Black"></asp:Label>
        </span>
            </td>
            <td>
    <span  style="font-size:11pt; font-weight:bold; color:#000000; font-family:Courier New;">
            <asp:Label ID="Label18" runat="server" Font-Bold="True" 
                    Text="الموظف يمارس الجدارات المطلوبة ويرتقي بشكل ثابت الي مستوي التوقعات دون الحاجة الي إشراف" 
                    Font-Size="11pt" Font-Names="Arial" ForeColor="Black"></asp:Label>
        </span>
            </td>
            <td>
    <span  style="font-size:11pt; font-weight:bold; color:#000000; font-family:Courier New;">
            <asp:Label ID="Label27" runat="server" Font-Bold="True" Text="4" Font-Size="11pt" 
                    Font-Names="Arial" ForeColor="Black"></asp:Label>
        </span>
            </td>
        </tr>
        <tr>
            <td>
    <span  style="font-size:11pt; font-weight:bold; color:#000000; font-family:Courier New;">
            <asp:Label ID="Label32" runat="server" Font-Bold="True" Text="أكثر من 95%" 
                    Font-Size="11pt" Font-Names="Arial" ForeColor="Black"></asp:Label>
        </span>
            </td>
            <td>
    <span  style="font-size:11pt; font-weight:bold; color:#000000; font-family:Courier New;">
            <asp:Label ID="Label23" runat="server" Font-Bold="True" Text="ممتاز" 
                    Font-Size="11pt" Font-Names="Arial" ForeColor="Black"></asp:Label>
        </span>
            </td>
            <td>
    <span  style="font-size:11pt; font-weight:bold; color:#000000; font-family:Courier New;">
            <asp:Label ID="Label19" runat="server" Font-Bold="True" 
                    Text="الموظف يمارس الجدارات المطلوبة ويفوق التوقعات في أغلب الأحيان , دون الحاجة إلي إشراف" 
                    Font-Size="11pt" Font-Names="Arial" ForeColor="Black"></asp:Label>
        </span>
            </td>
            <td>
    <span  style="font-size:11pt; font-weight:bold; color:#000000; font-family:Courier New;">
            <asp:Label ID="Label28" runat="server" Font-Bold="True" Text="5" Font-Size="11pt" 
                    Font-Names="Arial" ForeColor="Black"></asp:Label>
        </span>
            </td>
        </tr>
</table>

    <p>
        <br />
    </p>
    <table align="center" dir="rtl" style="width: 98%; height: 90px; float: right">
        <tr>
            <td align="center" colspan="3">
                &nbsp;</td>
        </tr>
        <tr>
            <td>
    <span  style="font-size:11pt; font-weight:bold; color:#000000; font-family:Courier New;">
            <asp:Label ID="lbl_1" runat="server" Font-Bold="True" Text="تقييم الجدارات" 
                    Font-Size="20pt" Font-Names="Arial" ForeColor="#0000CC"></asp:Label>
        </span>
            </td>
            <td>
    <span  style="font-size:11pt; font-weight:bold; color:#000000; font-family:Courier New;">
            <asp:Label ID="lbl_2" runat="server" Font-Bold="True" Text="التقييم" 
                    Font-Size="20pt" Font-Names="Arial" ForeColor="#0000CC"></asp:Label>
        </span>
            </td>
            <td>
    <span  style="font-size:11pt; font-weight:bold; color:#000000; font-family:Courier New;">
            <asp:Label ID="lbl_3" runat="server" Font-Bold="True" Text="التبرير" 
                    Font-Size="20pt" Font-Names="Arial" ForeColor="#0000CC"></asp:Label>
        </span>
            </td>
        </tr>
        <tr>
            <td>
    <span  style="font-size:11pt; font-weight:bold; color:#000000; font-family:Courier New;">
            <asp:Label ID="Label52" runat="server" Font-Bold="True" 
                    Text="الحضور والقوة الذهنية" Font-Size="16pt" Font-Names="Arial" 
                    ForeColor="Black"></asp:Label>
                <br />
            <asp:Label ID="Label53" runat="server" Font-Bold="True" 
                    Text="Intellectual Horsepower" Font-Size="16pt" Font-Names="Arial" 
                    ForeColor="Black"></asp:Label>
        </span>
            </td>
            <td>
                <asp:DropDownList ID="eval_horsepower" runat="server" Font-Bold="True" 
                    Font-Size="14pt" Width="80px">
                    <asp:ListItem Value="0">اختر</asp:ListItem>
                    <asp:ListItem>1</asp:ListItem>
                    <asp:ListItem>2</asp:ListItem>
                    <asp:ListItem>3</asp:ListItem>
                    <asp:ListItem>4</asp:ListItem>
                    <asp:ListItem>5</asp:ListItem>
                </asp:DropDownList>
            </td>
            <td>
                <asp:TextBox ID="just_horsepower" runat="server" Height="70px" 
                    TextMode="MultiLine" Width="280px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td colspan="3">
    <span  style="font-size:11pt; font-weight:bold; color:#000000; font-family:Courier New;">
            <asp:Label ID="Label58" runat="server" Font-Bold="True" 
                    Text="القدرة الذهنية علي ربط وفهم الأحداث والمفاهيم الصعبة" Font-Size="11pt" 
                    Font-Names="Arial" ForeColor="Gray"></asp:Label>
        </span>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <asp:Image ID="Image1" runat="server" ImageUrl="~/Images/line3.png" />
            </td>
        </tr>
        <tr>
            <td>
    <span  style="font-size:11pt; font-weight:bold; color:#000000; font-family:Courier New;">
            <asp:Label ID="Label54" runat="server" Font-Bold="True" Text="حل المشكلات" 
                    Font-Size="16pt" Font-Names="Arial" ForeColor="Black"></asp:Label>
                <br />
            <asp:Label ID="Label55" runat="server" Font-Bold="True" Text="Problem Solving" 
                    Font-Size="16pt" Font-Names="Arial" ForeColor="Black"></asp:Label>
        </span>
            </td>
            <td>
                <asp:DropDownList ID="eval_problem_solve" runat="server" Font-Bold="True" 
                    Font-Size="14pt" Width="80px">
                    <asp:ListItem Value="0">اختر</asp:ListItem>
                    <asp:ListItem>1</asp:ListItem>
                    <asp:ListItem>2</asp:ListItem>
                    <asp:ListItem>3</asp:ListItem>
                    <asp:ListItem>4</asp:ListItem>
                    <asp:ListItem>5</asp:ListItem>
                </asp:DropDownList>
            </td>
            <td>
                <asp:TextBox ID="just_problem_solve" runat="server" Height="70px" 
                    TextMode="MultiLine" Width="280px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td colspan="3">
    <span  style="font-size:11pt; font-weight:bold; color:#000000; font-family:Courier New;">
            <asp:Label ID="Label59" runat="server" Font-Bold="True" 
                    Text="القدرة علي تحديد المشكلات و ايجاد الحلول المناسبة" Font-Size="11pt" 
                    Font-Names="Arial" ForeColor="Gray"></asp:Label>
        </span>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <asp:Image ID="Image2" runat="server" ImageUrl="~/Images/line3.png" />
            </td>
        </tr>
        <tr>
            <td>
    <span  style="font-size:11pt; font-weight:bold; color:#000000; font-family:Courier New;">
            <asp:Label ID="Label62" runat="server" Font-Bold="True" Text="جودة أتخاذ القرار" 
                    Font-Size="16pt" Font-Names="Arial" ForeColor="Black"></asp:Label>
                <br />
            <asp:Label ID="Label63" runat="server" Font-Bold="True" Text="Decision Quality" 
                    Font-Size="16pt" Font-Names="Arial" ForeColor="Black"></asp:Label>
        </span>
            </td>
            <td>
                <asp:DropDownList ID="eval_d_qlty" runat="server" Font-Bold="True" 
                    Font-Size="14pt" Width="80px">
                    <asp:ListItem Value="0">اختر</asp:ListItem>
                    <asp:ListItem>1</asp:ListItem>
                    <asp:ListItem>2</asp:ListItem>
                    <asp:ListItem>3</asp:ListItem>
                    <asp:ListItem>4</asp:ListItem>
                    <asp:ListItem>5</asp:ListItem>
                </asp:DropDownList>
            </td>
            <td>
                <asp:TextBox ID="just_d_qlty" runat="server" Height="70px" 
                    TextMode="MultiLine" Width="280px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td colspan="3">
    <span  style="font-size:11pt; font-weight:bold; color:#000000; font-family:Courier New;">
            <asp:Label ID="Label64" runat="server" Font-Bold="True" 
                    Text="القدرة علي اتخاذ القرارات الصحيحة في التوقيت المناسب" Font-Size="11pt" 
                    Font-Names="Arial" ForeColor="Gray"></asp:Label>
        </span>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <asp:Image ID="Image3" runat="server" ImageUrl="~/Images/line3.png" />
            </td>
        </tr>
        <tr>
            <td>
    <span  style="font-size:11pt; font-weight:bold; color:#000000; font-family:Courier New;">
            <asp:Label ID="Label65" runat="server" Font-Bold="True" Text="بناء فريق عمل فعال" 
                    Font-Size="16pt" Font-Names="Arial" ForeColor="Black"></asp:Label>
                <br />
            <asp:Label ID="Label66" runat="server" Font-Bold="True" Text="Building Effective Team" 
                    Font-Size="16pt" Font-Names="Arial" ForeColor="Black"></asp:Label>
        </span>
            </td>
            <td>
                <asp:DropDownList ID="eval_B_team" runat="server" Font-Bold="True" 
                    Font-Size="14pt" Width="80px">
                    <asp:ListItem Value="0">اختر</asp:ListItem>
                    <asp:ListItem>1</asp:ListItem>
                    <asp:ListItem>2</asp:ListItem>
                    <asp:ListItem>3</asp:ListItem>
                    <asp:ListItem>4</asp:ListItem>
                    <asp:ListItem>5</asp:ListItem>
                </asp:DropDownList>
            </td>
            <td>
                <asp:TextBox ID="just_B_team" runat="server" Height="70px" 
                    TextMode="MultiLine" Width="280px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td colspan="3">
    <span  style="font-size:11pt; font-weight:bold; color:#000000; font-family:Courier New;">
            <asp:Label ID="Label75" runat="server" Font-Bold="True" 
                    
                    Text="• يكون فرق العمل المطلوبة عند الحاجة اليها• يرفع الروح المعنوية لفريقه ويخلق روح الإنتماء• يشجع الحوارات المفتوحة والصريحة• يتحمل نتائج العمل مع اعطاء الحرية للفريق بالعمل• يوضح معايير النجاح ويحتفل مع الفريق بالنتائج المتميزة" Font-Size="11pt" 
                    Font-Names="Arial" ForeColor="Gray"></asp:Label>
        </span>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <asp:Image ID="Image4" runat="server" ImageUrl="~/Images/line3.png" />
            </td>
        </tr>
        <tr>
            <td>
    <span  style="font-size:11pt; font-weight:bold; color:#000000; font-family:Courier New;">
            <asp:Label ID="Label67" runat="server" Font-Bold="True" Text="عملي" 
                    Font-Size="16pt" Font-Names="Arial" ForeColor="Black"></asp:Label>
                <br />
            <asp:Label ID="Label68" runat="server" Font-Bold="True" Text="Action Oriented" 
                    Font-Size="16pt" Font-Names="Arial" ForeColor="Black"></asp:Label>
        </span>
            </td>
            <td>
                <asp:DropDownList ID="eval_orianted" runat="server" Font-Bold="True" 
                    Font-Size="14pt" Width="80px">
                    <asp:ListItem Value="0">اختر</asp:ListItem>
                    <asp:ListItem>1</asp:ListItem>
                    <asp:ListItem>2</asp:ListItem>
                    <asp:ListItem>3</asp:ListItem>
                    <asp:ListItem>4</asp:ListItem>
                    <asp:ListItem>5</asp:ListItem>
                </asp:DropDownList>
            </td>
            <td>
                <asp:TextBox ID="just_orianted" runat="server" Height="70px" 
                    TextMode="MultiLine" Width="280px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td colspan="3" style="height: 15px">
    <span  style="font-size:11pt; font-weight:bold; color:#000000; font-family:Courier New;">
            <asp:Label ID="Label76" runat="server" Font-Bold="True" 
                    
                    Text="• يحسن التصرف في ظل عدم الوضوح وقلة التخطيط• يستغل الفرص أكثر من الآخرين• عملي في تنفيذ الأعمالالمعقدة جداً• يستمتع بالعمل المستمر" Font-Size="11pt" 
                    Font-Names="Arial" ForeColor="Gray"></asp:Label>
        </span>
            </td>
        </tr>
        <tr>
            <td colspan="3" style="height: 15px">
                <asp:Image ID="Image5" runat="server" ImageUrl="~/Images/line3.png" />
            </td>
        </tr>
        <tr>
            <td>
    <span  style="font-size:11pt; font-weight:bold; color:#000000; font-family:Courier New;">
            <asp:Label ID="Label69" runat="server" Font-Bold="True" Text="عدم الحاجة الى التوجيه" 
                    Font-Size="16pt" Font-Names="Arial" ForeColor="Black"></asp:Label>
                <br />
            <asp:Label ID="Label70" runat="server" Font-Bold="True" Text="No Need To Guidance" 
                    Font-Size="16pt" Font-Names="Arial" ForeColor="Black"></asp:Label>
        </span>
            </td>
            <td>
                <asp:DropDownList ID="eval_no_support" runat="server" Font-Bold="True" 
                    Font-Size="14pt" Width="80px">
                    <asp:ListItem Value="0">اختر</asp:ListItem>
                    <asp:ListItem>1</asp:ListItem>
                    <asp:ListItem>2</asp:ListItem>
                    <asp:ListItem>3</asp:ListItem>
                    <asp:ListItem>4</asp:ListItem>
                    <asp:ListItem>5</asp:ListItem>
                </asp:DropDownList>
            </td>
            <td>
                <asp:TextBox ID="just_no_support" runat="server" Height="70px" 
                    TextMode="MultiLine" Width="280px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td colspan="3">
    <span  style="font-size:11pt; font-weight:bold; color:#000000; font-family:Courier New;">
            <asp:Label ID="Label77" runat="server" Font-Bold="True" 
                    Text="هي درجة الإشراف المطلوبة لتحقيق مستويات الأداء المخططة." Font-Size="11pt" 
                    Font-Names="Arial" ForeColor="Gray"></asp:Label>
        </span>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <asp:Image ID="Image6" runat="server" ImageUrl="~/Images/line3.png" />
            </td>
        </tr>
        <tr>
            <td>
    <span  style="font-size:11pt; font-weight:bold; color:#000000; font-family:Courier New;">
            <asp:Label ID="Label71" runat="server" Font-Bold="True" Text="إستخدام الموارد المتاحة" 
                    Font-Size="16pt" Font-Names="Arial" ForeColor="Black"></asp:Label>
                <br />
            <asp:Label ID="Label72" runat="server" Font-Bold="True" Text="Use of Resources" 
                    Font-Size="16pt" Font-Names="Arial" ForeColor="Black"></asp:Label>
        </span>
            </td>
            <td>
                <asp:DropDownList ID="eval_resources" runat="server" Font-Bold="True" 
                    Font-Size="14pt" Width="80px">
                    <asp:ListItem Value="0">اختر</asp:ListItem>
                    <asp:ListItem>1</asp:ListItem>
                    <asp:ListItem>2</asp:ListItem>
                    <asp:ListItem>3</asp:ListItem>
                    <asp:ListItem>4</asp:ListItem>
                    <asp:ListItem>5</asp:ListItem>
                </asp:DropDownList>
            </td>
            <td>
                <asp:TextBox ID="just_resources" runat="server" Height="70px" 
                    TextMode="MultiLine" Width="280px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td colspan="3">
    <span  style="font-size:11pt; font-weight:bold; color:#000000; font-family:Courier New;">
            <asp:Label ID="Label78" runat="server" Font-Bold="True" 
                    
                    Text="مدي كفاءة استخدام الموارد المتاحة من حيث الوقت، والمواد والمال و الأفراد لإنجاز الأعمال المطلوبة بغض النظر عن الجودة والالتزام بالوقت المحدد." Font-Size="11pt" 
                    Font-Names="Arial" ForeColor="Gray"></asp:Label>
        </span>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <asp:Image ID="Image7" runat="server" ImageUrl="~/Images/line3.png" />
            </td>
        </tr>
        <tr>
            <td>
    <span  style="font-size:11pt; font-weight:bold; color:#000000; font-family:Courier New;">
            <asp:Label ID="Label73" runat="server" Font-Bold="True" Text="القيمة المضافة للعملاء داخل وخارج الشركة" 
                    Font-Size="16pt" Font-Names="Arial" ForeColor="Black"></asp:Label>
                <br />
            <asp:Label ID="Label74" runat="server" Font-Bold="True" Text="Customer Impact/Value Added" 
                    Font-Size="16pt" Font-Names="Arial" ForeColor="Black"></asp:Label>
        </span>
            </td>
            <td>
                <asp:DropDownList ID="eval_value_added" runat="server" Font-Bold="True" 
                    Font-Size="14pt" Width="80px">
                    <asp:ListItem Value="0">اختر</asp:ListItem>
                    <asp:ListItem>1</asp:ListItem>
                    <asp:ListItem>2</asp:ListItem>
                    <asp:ListItem>3</asp:ListItem>
                    <asp:ListItem>4</asp:ListItem>
                    <asp:ListItem>5</asp:ListItem>
                </asp:DropDownList>
            </td>
            <td>
                <asp:TextBox ID="just_value_added" runat="server" Height="70px" 
                    TextMode="MultiLine" Width="280px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td colspan="3">
    <span  style="font-size:11pt; font-weight:bold; color:#000000; font-family:Courier New;">
            <asp:Label ID="Label79" runat="server" Font-Bold="True" 
                    Text="مدي ملائمة نتائج العمل مع توقعات العملاء الداخليين والخارجيين" Font-Size="11pt" 
                    Font-Names="Arial" ForeColor="Gray"></asp:Label>
        </span>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <asp:Image ID="Image8" runat="server" ImageUrl="~/Images/line3.png" />
            </td>
        </tr>
        <tr>
            <td align="center" colspan="3">
                <table dir="rtl" style="width: 98%; height: 98%; float: right">
                    <tr align="center" style="background-color: #C0C0C0">
                        <td>
                            <span style="font-size:11pt; font-weight:bold; color:#000000; font-family:Courier New;">
                            <asp:Label ID="lbl_14" runat="server" Font-Bold="True" Font-Names="Arial" 
                                Font-Size="20pt" ForeColor="#0000CC" Text="تقييم أهداف العام المقبل"></asp:Label>
                            </span>
                        </td>
                        <td align="center">
                            <span style="font-size:11pt; font-weight:bold; color:#000000; font-family:Courier New;">
                            <asp:Label ID="lbl_15" runat="server" Font-Bold="True" Font-Names="Arial" 
                                Font-Size="13pt" ForeColor="#0000CC" Text="الوزن النسبي"></asp:Label>
                            </span>
                        </td>
                        <td align="center">
                            <span style="font-size:11pt; font-weight:bold; color:#000000; font-family:Courier New;">
                            <asp:Label ID="lbl_16" runat="server" Font-Bold="True" Font-Names="Arial" 
                                Font-Size="13pt" ForeColor="#0000CC" Text="النسبة المحققة" Visible="False"></asp:Label>
                            </span>
                        </td>
                        <td align="center">
                            <span style="font-size:11pt; font-weight:bold; color:#000000; font-family:Courier New;">
                            <asp:Label ID="lbl_17" runat="server" Font-Bold="True" Font-Names="Arial" 
                                Font-Size="20pt" ForeColor="#0000CC" Text="التبرير" Visible="False"></asp:Label>
                            </span>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <span style="font-size:11pt; font-weight:bold; color:#000000; font-family:Courier New;">
                            <asp:Label ID="lbl_18" runat="server" Font-Bold="True" Font-Names="Arial" 
                                Font-Size="13pt" ForeColor="#FF3300" Text="هدف 1"></asp:Label>
                            </span>
                            <asp:TextBox ID="g1_name0" runat="server" Font-Bold="True" Font-Size="13pt" 
                                Width="530px" TextMode="MultiLine"></asp:TextBox>
                        </td>
                        <td align="center">
                            <asp:TextBox ID="g1_weight0" runat="server" Font-Bold="True" Font-Size="13pt" 
                                Width="60px">0</asp:TextBox>
                        </td>
                        <td align="center">
                            <asp:TextBox ID="g1_actual0" runat="server" Font-Bold="True" Font-Size="13pt" 
                                Width="60px" Visible="False">0</asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="g1_just0" runat="server" Font-Bold="True" Font-Size="13pt" 
                                Width="60px" Visible="False"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <span style="font-size:11pt; font-weight:bold; color:#000000; font-family:Courier New;">
                            <asp:Label ID="lbl_19" runat="server" Font-Bold="True" Font-Names="Arial" 
                                Font-Size="13pt" ForeColor="#FF3300" Text="هدف 2"></asp:Label>
                            <asp:TextBox ID="g2_name0" runat="server" Font-Bold="True" Font-Size="13pt" 
                                Width="530px" TextMode="MultiLine"></asp:TextBox>
                            </span>
                        </td>
                        <td align="center">
                            <asp:TextBox ID="g2_weight0" runat="server" Font-Bold="True" Font-Size="13pt" 
                                Width="60px">0</asp:TextBox>
                        </td>
                        <td align="center">
                            <asp:TextBox ID="g2_actual0" runat="server" Font-Bold="True" Font-Size="13pt" 
                                Width="60px" Visible="False">0</asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="g2_just0" runat="server" Font-Bold="True" Font-Size="13pt" 
                                Width="60px" Visible="False"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <span style="font-size:11pt; font-weight:bold; color:#000000; font-family:Courier New;">
                            <asp:Label ID="lbl_20" runat="server" Font-Bold="True" Font-Names="Arial" 
                                Font-Size="13pt" ForeColor="#FF3300" Text="هدف 3"></asp:Label>
                            <asp:TextBox ID="g3_name0" runat="server" Font-Bold="True" Font-Size="13pt" 
                                Width="530px" TextMode="MultiLine"></asp:TextBox>
                            </span>
                        </td>
                        <td align="center">
                            <asp:TextBox ID="g3_weight0" runat="server" Font-Bold="True" Font-Size="13pt" 
                                Width="60px">0</asp:TextBox>
                        </td>
                        <td align="center">
                            <asp:TextBox ID="g3_actual0" runat="server" Font-Bold="True" Font-Size="13pt" 
                                Width="60px" Visible="False">0</asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="g3_just0" runat="server" Font-Bold="True" Font-Size="13pt" 
                                Width="60px" Visible="False"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <span style="font-size:11pt; font-weight:bold; color:#000000; font-family:Courier New;">
                            <asp:Label ID="lbl_21" runat="server" Font-Bold="True" Font-Names="Arial" 
                                Font-Size="13pt" ForeColor="#FF3300" Text="هدف 4"></asp:Label>
                            <asp:TextBox ID="g4_name0" runat="server" Font-Bold="True" Font-Size="13pt" 
                                Width="530px" TextMode="MultiLine"></asp:TextBox>
                            </span>
                        </td>
                        <td align="center">
                            <asp:TextBox ID="g4_weight0" runat="server" Font-Bold="True" Font-Size="13pt" 
                                Width="60px">0</asp:TextBox>
                        </td>
                        <td align="center">
                            <asp:TextBox ID="g4_actual0" runat="server" Font-Bold="True" Font-Size="13pt" 
                                Width="60px" Visible="False">0</asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="g4_just0" runat="server" Font-Bold="True" Font-Size="13pt" 
                                Width="60px" Visible="False"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <span style="font-size:11pt; font-weight:bold; color:#000000; font-family:Courier New;">
                            <asp:Label ID="lbl_22" runat="server" Font-Bold="True" Font-Names="Arial" 
                                Font-Size="13pt" ForeColor="#FF3300" Text="هدف 5"></asp:Label>
                            <asp:TextBox ID="g5_name0" runat="server" Font-Bold="True" Font-Size="13pt" 
                                Width="530px" TextMode="MultiLine"></asp:TextBox>
                            </span>
                        </td>
                        <td align="center">
                            <asp:TextBox ID="g5_weight0" runat="server" Font-Bold="True" Font-Size="13pt" 
                                Width="60px">0</asp:TextBox>
                        </td>
                        <td align="center">
                            <asp:TextBox ID="g5_actual0" runat="server" Font-Bold="True" Font-Size="13pt" 
                                Width="60px" Visible="False">0</asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="g5_just0" runat="server" Font-Bold="True" Font-Size="13pt" 
                                Width="60px" Visible="False"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <span style="font-size:11pt; font-weight:bold; color:#000000; font-family:Courier New;">
                            <asp:Label ID="lbl_23" runat="server" Font-Bold="True" Font-Names="Arial" 
                                Font-Size="13pt" ForeColor="#FF3300" Text="هدف 6"></asp:Label>
                            <asp:TextBox ID="g6_name0" runat="server" Font-Bold="True" Font-Size="13pt" 
                                Width="530px" TextMode="MultiLine"></asp:TextBox>
                            </span>
                        </td>
                        <td align="center">
                            <asp:TextBox ID="g6_weight0" runat="server" Font-Bold="True" Font-Size="13pt" 
                                Width="60px">0</asp:TextBox>
                        </td>
                        <td align="center">
                            <asp:TextBox ID="g6_actual0" runat="server" Font-Bold="True" Font-Size="13pt" 
                                Width="60px" Visible="False">0</asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox ID="g6_just0" runat="server" Font-Bold="True" Font-Size="13pt" 
                                Width="60px" Visible="False"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;</td>
                        <td>
                            &nbsp;</td>
                        <td>
                            &nbsp;</td>
                        <td>
                            &nbsp;</td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td align="center">
                &nbsp;</td>
            <td align="center">
    <span  style="font-size:11pt; font-weight:bold; color:#000000; font-family:Courier New;">
    <span  style="font-size:11pt; font-weight:bold; color:#000000; font-family:Courier New;">
        <asp:Button ID="btn_print0" runat="server" BackColor="#FF33CC" Font-Bold="True" 
            Font-Names="Arial" ForeColor="White" onclick="btn_print_Click" Text="طباعة" 
            Width="250px" Font-Size="15pt" />
        </span>
        </span>
            </td>
            <td align="center">
                &nbsp;</td>
        </tr>
        <tr>
            <td align="center">
    <span  style="font-size:11pt; font-weight:bold; color:#000000; font-family:Courier New;">
            <asp:Label ID="Label60" runat="server" Font-Bold="True" Text="النتيجة النهائية" 
                    Font-Size="16pt" Font-Names="Arial" ForeColor="#0000CC"></asp:Label>
        </span>
            </td>
            <td align="center">
            <asp:Button ID="btn_save" runat="server" BackColor="#3366FF" Font-Bold="True" 
                Font-Size="15pt" ForeColor="White" Text="حفظ التقييم" Width="250px" 
                    onclick="btn_save_Click" />
            </td>
            <td align="center">
    <span  style="font-size:11pt; font-weight:bold; color:#000000; font-family:Courier New;">
            <asp:Label ID="Label61" runat="server" Font-Bold="True" Text="المدير المسئول" 
                    Font-Size="16pt" Font-Names="Arial" ForeColor="#0000CC"></asp:Label>
        </span>
            </td>
        </tr>
        <tr>
            <td align="center">
                <asp:TextBox ID="txt_down_result" runat="server" BackColor="#FF9933" 
                    BorderStyle="Solid" Font-Bold="True" Font-Size="15pt" Width="100px" 
                    Enabled="False"></asp:TextBox>
            </td>
            <td align="center">
            <asp:Button ID="btn_delete" runat="server" BackColor="Black" Font-Bold="True" 
                Font-Size="15pt" ForeColor="White" Text="إلغاء التقييم" Width="250px" 
                    onclick="btn_delete_Click" onclientclick="Confirm()" />
            </td>
            <td align="center">
                <asp:TextBox ID="txt_manager" runat="server" BackColor="#FF9933" 
                    BorderStyle="Solid" Enabled="False" Font-Bold="True" Font-Size="15pt" 
                    Width="250px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td align="center">
            <asp:Button ID="btn_calculate" runat="server" BackColor="#009999" Font-Bold="True" 
                Font-Size="15pt" ForeColor="White" Text="احتساب النتيجة" Width="150px" 
                    onclick="btn_calculate_Click" />
            </td>
            <td align="center">
            <asp:Button ID="btn_approve" runat="server" BackColor="#FF0066" Font-Bold="True" 
                Font-Size="15pt" ForeColor="White" Text="موافقة المدير" Width="250px" 
                    onclick="btn_approve_Click" />
            </td>
            <td align="center">
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
    </table>
</asp:Content>

