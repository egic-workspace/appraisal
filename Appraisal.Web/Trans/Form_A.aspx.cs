﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;


public partial class Trans_Form_A : System.Web.UI.Page
{
    AppService ser = new AppService();
    
    DataTable DsT = new DataTable();
    DataTable dt = new DataTable();
    DataTable FormDT = new DataTable();

    protected void Page_PreInit(object sender, EventArgs e)
    {
        if (Session["DISPLAY"] == null || Session["Logged_User"] == null)
        {
            Response.Redirect("~/Trans/AS_Enter.aspx");
        }
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["DISPLAY"].ToString().Trim() == "YES")
        {
            Display_Only();
        }
        if (!IsPostBack)
        {
            if (Session["Logged_User"] == null || Session["S_EMP_NAME"] == null )
            {
                Response.Redirect("~/Trans/AS_Enter.aspx");
            }
            btn_approve.Visible = false;
            btn_delete.Visible = false;  

            lbl_emp_name.Text = Session["S_EMP_NAME"].ToString();
            txt_manager.Text = Session["Logged_User"].ToString();
            string Level = Session["S_LEVEL"].ToString();  // check levels
            string ASST = Session["ASS_STATUS"].ToString().Trim();
            int APPROVER = ser.Check_Approver(Session["EMP_NO"].ToString());
            if (Level.Trim() == "2")
            {
                if (ASST == "0") // when it not saved and not approved
                {
                    int XX = ser.Check_Redandancy(Session["S_EMP_NO"].ToString(), Session["S_YEAR"].ToString());
                    if (XX != 0)
                    {
                        Alert.Show(" هذا الموظف تم ادخال تقييم له في هذا العام ولكن علي نموذج مختلف ");
                        return;
                    }
                    btn_save.Visible = true;
                    btn_approve.Visible = false;
                }
                else
                    if (ASST == "1") // when it saved only
                    {
                        Get_Data_FormA();
                        btn_save.Visible = true;
                        btn_delete.Visible = true;
                        if (APPROVER != 0)
                        {
                            btn_approve.Visible = true;
                        }
                    }
                    else
                        if (ASST == "2")  // when it approved
                        {
                            Get_Data_FormA();
                            btn_save.Visible = false;
                            btn_delete.Visible = false;

                            if (APPROVER != 0)
                            {
                                btn_approve.Visible = false;
                            }
                            Display_Only();
                        }
            }
            else
            {
                Get_Data_FormA();
                btn_save.Visible = false;
                btn_delete.Visible = false;
                Display_Only();
                if (ASST == "1")
                {
                    if (APPROVER != 0)
                    {
                        btn_approve.Visible = true;
                    }
                }
            }
        }
      
    }
    protected void btn_calculate_Click(object sender, EventArgs e)
    {
        if (eval_work_qlity.SelectedIndex == 0 ||
          eval_commit.SelectedIndex == 0 ||
          eval_deliv_time.SelectedIndex == 0 ||
          eval_flexib.SelectedIndex == 0 ||
          eval_horsepower.SelectedIndex == 0 ||
          evaL_out_qty.SelectedIndex == 0 ||
          eval_pos_attitude.SelectedIndex == 0 ||
          eval_problem_solve.SelectedIndex == 0 ||
          eval_safty.SelectedIndex == 0 ||
          eval_team_work.SelectedIndex == 0)
        {
            Alert.Show(" برجاء اختيار جميع الجدارات ");
            return;
        }
        else
        {
            Calculate_Form_A();
        }
    }
    protected void btn_save_Click(object sender, EventArgs e)
    {
        try
        {
            if (eval_work_qlity.SelectedIndex == 0 ||
          eval_commit.SelectedIndex == 0 ||
          eval_deliv_time.SelectedIndex == 0 ||
          eval_flexib.SelectedIndex == 0 ||
          eval_horsepower.SelectedIndex == 0 ||
          evaL_out_qty.SelectedIndex == 0 ||
          eval_pos_attitude.SelectedIndex == 0 ||
          eval_problem_solve.SelectedIndex == 0 ||
          eval_safty.SelectedIndex == 0 ||
          eval_team_work.SelectedIndex == 0)
            {
                Alert.Show(" برجاء اختيار جميع الجدارات ");
                return;
            }
            else
            {
                string YSTS = ser.Check_Year(Session["S_YEAR"].ToString());
                if (YSTS.Trim() == "Y")  // 1- check locked year or not
                {
                    int RDSTS = ser.Check_Redandancy(Session["S_EMP_NO"].ToString(), Session["S_YEAR"].ToString());
                    if (RDSTS == 0)      // 2- check redandancy data 
                    {
                        double FinalR = Calculate_Form_A();
                        int SVHDR = ser.Save_Form_HDR(Session["S_EMP_NO"].ToString(), Session["S_EMP_NAME"].ToString(), Session["SFORMID"].ToString(), "0", Session["S_YEAR"].ToString(), "0", Session["Logged_User"].ToString());
                        int RESLT = ser.Save_Form_A_DTL(SVHDR.ToString(), Session["S_EMP_NAME"].ToString(), evaL_out_qty.SelectedValue.ToString(), just_out_qty.Text, 
                            eval_deliv_time.SelectedValue.ToString(), just_deliv_time.Text, 
                            eval_work_qlity.SelectedValue.ToString(), just_work_qlity.Text, 
                            eval_safty.SelectedValue.ToString(), just_safty.Text, eval_team_work.SelectedValue.ToString(),
                            just_team_work.Text, eval_commit.SelectedValue.ToString(),
                            just_commit.Text, eval_flexib.SelectedValue.ToString(), just_flexib.Text,
                            eval_horsepower.SelectedValue.ToString(), just_horsepower.Text,
                            eval_problem_solve.SelectedValue.ToString(), just_problem_solve.Text,
                            eval_pos_attitude.SelectedValue.ToString(), just_pos_attitude.Text, FinalR.ToString());

                        if (RESLT != 0 && SVHDR != 0 )
                        {

                            Alert.Show("تم حفظ التقييم بنجاح");
                            Clear();
                            //Response.Redirect("~/Trans/AS_Enter.aspx");
                        }
                        else
                        {
                            Alert.Show("يوجد مشكله في عملية الادخال");
                            return;
                        }
                    }
                    else
                    {
                        double UP_FinalR = Calculate_Form_A();
                        int UPDTL = ser.Update_Form_A_DTL(Session["HDR_ID"].ToString(), Session["S_EMP_NAME"].ToString(), evaL_out_qty.SelectedValue.ToString(), just_out_qty.Text,
                            eval_deliv_time.SelectedValue.ToString(), just_deliv_time.Text,
                            eval_work_qlity.SelectedValue.ToString(), just_work_qlity.Text,
                            eval_safty.SelectedValue.ToString(), just_safty.Text, eval_team_work.SelectedValue.ToString(),
                            just_team_work.Text, eval_commit.SelectedValue.ToString(),
                            just_commit.Text, eval_flexib.SelectedValue.ToString(), just_flexib.Text,
                            eval_horsepower.SelectedValue.ToString(), just_horsepower.Text,
                            eval_problem_solve.SelectedValue.ToString(), just_problem_solve.Text,
                            eval_pos_attitude.SelectedValue.ToString(), just_pos_attitude.Text, UP_FinalR.ToString());

                        if (UPDTL != 0)
                        {
                            Alert.Show(" تم حفظ بيانات التقييم بنجاج  ");
                            Clear();
                            //Response.Redirect("~/Trans/AS_Enter.aspx");
                        }
                        else
                        {
                            Alert.Show("  برجاء اعادة اختيار الموظف المطلوب تقييمة أو تقليل عدد احرف كتابة التبريرات  ");
                            return;
                        }
                       
                    }
                }
                else
                {
                    Alert.Show(" السنة الحالية تم اغلاقها ");
                    return;
                }
            }
        }
        catch
        {
            Alert.Show("يوجد مشكله في عملية الادخال");
        }
    }
    private void Get_Data_FormA()
    {
        try
        {

            DsT = ser.Get_Form_A_Data(Session["S_EMP_NO"].ToString(), Session["S_YEAR"].ToString());
          FormDT = DsT;
          foreach (DataRow var in FormDT.Rows)
           {
                Session["HDR_ID"] = var[0].ToString();
                string emp_code = var[1].ToString();
                string emp_name = var[2].ToString();
                string form_id  = var[3].ToString();
                if (form_id.Trim() != "1") { Response.Redirect("~/Trans/AS_Enter.aspx"); }
                evaL_out_qty.ClearSelection();
                evaL_out_qty.Items.FindByValue(var[4].ToString()).Selected = true;
                just_out_qty.Text = var[5].ToString();

                eval_deliv_time.ClearSelection();
                eval_deliv_time.Items.FindByValue(var[6].ToString()).Selected = true;
                just_deliv_time.Text = var[7].ToString();

                eval_work_qlity.ClearSelection();
                eval_work_qlity.Items.FindByValue(var[8].ToString()).Selected = true;
                just_work_qlity.Text = var[9].ToString();

                eval_safty.ClearSelection();
                eval_safty.Items.FindByValue(var[10].ToString()).Selected = true;
                just_safty.Text = var[11].ToString();

                eval_team_work.ClearSelection();
                eval_team_work.Items.FindByValue(var[12].ToString()).Selected = true;
                just_team_work.Text = var[13].ToString();

                eval_commit.ClearSelection();
                eval_commit.Items.FindByValue(var[14].ToString()).Selected = true;
                just_commit.Text = var[15].ToString();

                eval_flexib.ClearSelection();
                eval_flexib.Items.FindByValue(var[16].ToString()).Selected = true;
                just_flexib.Text = var[17].ToString();

                eval_horsepower.ClearSelection();
                eval_horsepower.Items.FindByValue(var[18].ToString()).Selected = true;
                just_horsepower.Text = var[19].ToString();

                eval_problem_solve.ClearSelection();
                eval_problem_solve.Items.FindByValue(var[20].ToString()).Selected = true;
                just_problem_solve.Text = var[21].ToString();

                eval_pos_attitude.ClearSelection();
                eval_pos_attitude.Items.FindByValue(var[22].ToString()).Selected = true;
                just_pos_attitude.Text = var[23].ToString();

                txt_down_result.Text = " % "+ var[24].ToString();
                txt_top_result.Text = " % " + var[24].ToString();

                txt_manager.Text = var[25].ToString(); 

           }

        }
        catch
        {
 
        }
    }
    private double Calculate_Form_A()
    {
        double F_RSLT = 0;
        try
        {
            double A = double.Parse(eval_work_qlity.SelectedValue.ToString());
            double B = double.Parse(eval_commit.SelectedValue.ToString());
            double C = double.Parse(eval_deliv_time.SelectedValue.ToString());
            double D = double.Parse(eval_flexib.SelectedValue.ToString());
            double E = double.Parse(eval_horsepower.SelectedValue.ToString());
            double F = double.Parse(evaL_out_qty.SelectedValue.ToString());
            double G = double.Parse(eval_pos_attitude.SelectedValue.ToString());
            double H = double.Parse(eval_problem_solve.SelectedValue.ToString());
            double I = double.Parse(eval_safty.SelectedValue.ToString());
            double J = double.Parse(eval_team_work.SelectedValue.ToString());

            double Eval_Sum = A + B + C + D + E + F + G + H + I + J;

            double Final_Result = (Eval_Sum / 50) * 100 ;
            F_RSLT = Final_Result;
            txt_top_result.Text = " % " +  Final_Result.ToString().Substring(0,6);
            txt_down_result.Text = " % " + Final_Result.ToString().Substring(0, 6);
        }
        catch
        {
            txt_top_result.Text = "000";
            txt_down_result.Text = "000";
        }

        return F_RSLT ;
    }
    private void Display_Only()
    {
        eval_work_qlity.Enabled = false;
        eval_commit.Enabled = false;
        eval_deliv_time.Enabled = false;
        eval_flexib.Enabled = false;
        eval_horsepower.Enabled = false;
        evaL_out_qty.Enabled = false;
        eval_pos_attitude.Enabled = false;
        eval_problem_solve.Enabled = false;
        eval_safty.Enabled = false;
        eval_team_work.Enabled = false;

        just_commit.Enabled = false;
        just_deliv_time.Enabled = false;
        just_flexib.Enabled = false;
        just_horsepower.Enabled = false;
        just_out_qty.Enabled = false;
        just_pos_attitude.Enabled = false;
        just_problem_solve.Enabled = false;
        just_safty.Enabled = false;
        just_team_work.Enabled = false;
        just_work_qlity.Enabled = false;

        btn_approve.Enabled = false;
        btn_save.Enabled = false;
        btn_calculate.Enabled = false;
        btn_delete.Enabled = false;
    }
    private void Clear()
    {
        //Session["HDR_ID"] = "";
        eval_work_qlity.SelectedIndex = 0;
        eval_commit.SelectedIndex = 0;
        eval_deliv_time.SelectedIndex = 0;
        eval_flexib.SelectedIndex = 0;
        eval_horsepower.SelectedIndex = 0;
        evaL_out_qty.SelectedIndex = 0;
        eval_pos_attitude.SelectedIndex = 0;
        eval_problem_solve.SelectedIndex = 0;
        eval_safty.SelectedIndex = 0;
        eval_team_work.SelectedIndex = 0;

        just_commit.Text = "";
        just_deliv_time.Text = "";
        just_flexib.Text = "";
        just_horsepower.Text = "";
        just_out_qty.Text = "";
        just_pos_attitude.Text = "";
        just_problem_solve.Text = "";
        just_safty.Text = "";
        just_team_work.Text = "";
        just_work_qlity.Text = "";

        txt_down_result.Text = "";
        txt_top_result.Text = "";
    }
    protected void btn_approve_Click(object sender, EventArgs e)
    {
        try
        {
            int APRV = ser.Make_Approve(Session["HDR_ID"].ToString(), Session["S_EMP_NO"].ToString(), Session["Logged_User"].ToString(), Session["S_YEAR"].ToString());
            if (APRV == 1)
            {
                Alert.Show(" تمت الموافقة بنجاح ");
            }

        }
        catch
        {
            Alert.Show(" يوجد مشكلة في عملية الموافقة  ");
            return;
        }
    }
    protected void btn_delete_Click(object sender, EventArgs e)
    {
        try
        {
             string confirmValue = Request.Form["confirm_value"];
             if (confirmValue == "No")
             {
                
             }
             else
             {
                 int DEL = ser.Delete_Form1(Session["HDR_ID"].ToString());
                 if (DEL == 1)
                 {
                     Alert.Show(" تم حذف التقييم بنجاح ");
                     Response.Redirect("~/Trans/AS_Enter.aspx");
                 }
                 else
                 {
                     Alert.Show(" يوجد مشكلة في عملية الحذف ");
                     return;
                 }
             }
        }
        catch
        {

        }
    }
    protected void btn_print_Click(object sender, EventArgs e)
    {
      
        Response.Redirect("~/Reports/print_a.aspx");
    }
}
