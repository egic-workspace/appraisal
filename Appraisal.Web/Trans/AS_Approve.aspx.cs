﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;


public partial class Trans_AS_Approve : System.Web.UI.Page
{
    AppService ser = new AppService();
    
    DataTable Dst = new DataTable();
    DataTable dt = new DataTable();

    //  int APPROVER = ser.Check_Approver(Session["EMP_NO"].ToString());
    protected void Page_PreInit(object sender, EventArgs e)
    {
        if (Session["Logged_User"] == null)
        {
            Response.Redirect("~/Default.aspx");
        }
        int APPROVER = ser.Check_Approver(Session["EMP_NO"].ToString());
        if (APPROVER == 0) { Response.Redirect("~/Setting/Unauthorized_Pg.aspx"); }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Session["Logged_User"] == null)
            {
                Response.Redirect("~/Default.aspx");
            }
            //****************************
            TXT_OPEN_YEAR.Text = ser.Get_Open_Year().ToString();
            Fill_Grid_ALL();
            Fill_Grid_NOT_APRV_ALL();
            //**************************
        }
    }
    private void Fill_Grid_ALL()
    {
        try
        {
            Clear_Grids();
            
            string EMP_NO = Session["EMP_NO"].ToString();
            // dt = ser.Bind_ENTER_EMP_GRID(EMP_NO);
            dt = ser.Bind_APPROVE_EMP_GRID(EMP_NO, TXT_OPEN_YEAR.Text);
            GridView1.DataSource = dt;
            GridView1.DataBind();
        }
        catch
        {

        }
    }
    private void Fill_Grid_First_Level()
    {
        try
        {
            Clear_Grids();
            
            string EMP_NO = Session["EMP_NO"].ToString();
            // dt = ser.Bind_ENTER_EMP_GRID(EMP_NO);
            dt = ser.Bind_APPROVE_EMP_FRST_LEVEL_GRID(EMP_NO, TXT_OPEN_YEAR.Text);
            GridView1.DataSource = dt;
            GridView1.DataBind();
        }
        catch
        {

        }
    }
    private void Clear_Grids()
    {
        GridView1.DataSource = null;
        GridView1.DataBind();
    }
    private void Clear_Grids_Two()
    {
        GridView2.DataSource = null;
        GridView2.DataBind();
    }

    protected void btn_all_levels_Click(object sender, EventArgs e)
    {
        Fill_Grid_ALL();
        Fill_Grid_NOT_APRV_ALL();
    }
    protected void btn_frst_level_Click(object sender, EventArgs e)
    {
        Fill_Grid_First_Level();
        Fill_Grid_NOT_APRV_First_Level();
    }
    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {

    }
    protected void GridView1_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            Session["SFORMID"] = ((Label)GridView1.SelectedRow.FindControl("lblform_id")).Text;
            Session["S_EMP_NAME"] = ((Label)GridView1.SelectedRow.FindControl("lblemp_name")).Text;
            Session["S_EMP_NO"] = ((Label)GridView1.SelectedRow.FindControl("lblemp_no")).Text;
            Session["S_YEAR"] = TXT_OPEN_YEAR.Text;
           
            Session["S_LEVEL"] = ((Label)GridView1.SelectedRow.FindControl("lbllevel")).Text;
            Session["ASS_STATUS"] = ((Label)GridView1.SelectedRow.FindControl("lblstatus")).Text;
            string MyPage = ser.Get_Page_Name(Session["SFORMID"].ToString());
            Session["DISPLAY"] = "YES";
            Response.Redirect("~/Trans/" + MyPage,false);
        }
        catch(Exception exx)
        {
            string xx = exx.Message.ToString();
        }

    }
    protected void GridView1_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
    {

    }
    protected void btn_approve_Click(object sender, EventArgs e)
    {
        int APRV = 0;
        string confirmValue = Request.Form["confirm_value"];
        if (confirmValue == "No")
        {

        }
        else
        {
            foreach (GridViewRow row in GridView1.Rows)
            {
                CheckBox chk = row.Cells[0].FindControl("chk_data") as CheckBox;

                if (chk != null && chk.Checked)
                {
                    string emp_no = (row.Cells[2].FindControl("lblemp_no") as Label).Text;
                    string form_id = (row.Cells[2].FindControl("lblform_id") as Label).Text;
                    APRV = ser.Make_Approve_by_Form(emp_no, form_id, Session["Logged_User"].ToString(), TXT_OPEN_YEAR.Text);
                }
            }

            if (APRV == 1)
            {
                Alert.Show(" تمت الموافقة بنجاح ");
                Fill_Grid_ALL();
                Fill_Grid_NOT_APRV_ALL();
            }
            else
            {
                Alert.Show(" لم يتم إختيار موظف للتقييم   ");
                return;
            }
        }
    }

    private void Fill_Grid_NOT_APRV_ALL()
    {
        try
        {
            Clear_Grids_Two();
            
            string EMP_NO = Session["EMP_NO"].ToString();
            // dt = ser.Bind_ENTER_EMP_GRID(EMP_NO);
            Dst = ser.Bind_NOT_APPROVE_EMP_GRID(EMP_NO, TXT_OPEN_YEAR.Text);
            GridView2.DataSource = Dst;
            GridView2.DataBind();
        }
        catch
        {

        }
    }
    private void Fill_Grid_NOT_APRV_First_Level()
    {
        try
        {
            Clear_Grids_Two();
            
            string EMP_NO = Session["EMP_NO"].ToString();
            // dt = ser.Bind_ENTER_EMP_GRID(EMP_NO);
            Dst = ser.Bind_NOT_APPROVE_EMP_FRST_LEVEL_GRID(EMP_NO, TXT_OPEN_YEAR.Text);
            GridView2.DataSource = Dst;
            GridView2.DataBind();
        }
        catch
        {

        }
    }
}
