﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;


public partial class Trans_Form_D : System.Web.UI.Page
{
    AppService ser = new AppService();
    
    DataTable DsT = new DataTable();
    DataTable dt = new DataTable();
    DataTable FormDT = new DataTable();

    //protected void Page_PreInit(object sender, EventArgs e)
    //{
    //    if (Session["DISPLAY"].ToString().Trim() == "YES")
    //    {
    //        Display_Only();
    //    }
    //}
    protected void Page_Load(object sender, EventArgs e)
    {
        // check the static target employees
        string st = ser.Emp_Static_Target(Session["S_EMP_NO"].ToString());
        if (st == "1")
        {
            Static_Target();
        }
        if (Session["DISPLAY"].ToString().Trim() == "YES")
        {
            Display_Only();
        }
        if (!IsPostBack)
        {
            if (Session["Logged_User"] == null || Session["S_EMP_NAME"] == null)
            {
                Response.Redirect("~/Trans/AS_Enter.aspx");
            }
            btn_approve.Visible = false;
            btn_delete.Visible = false;
            lbl_emp_name.Text = Session["S_EMP_NAME"].ToString();
            txt_manager.Text = Session["Logged_User"].ToString();
            string Level = Session["S_LEVEL"].ToString();  // check levels
            string ASST = Session["ASS_STATUS"].ToString().Trim();
            int APPROVER = ser.Check_Approver(Session["EMP_NO"].ToString());
            if (Level.Trim() == "2")
            {
                if (ASST == "0") // when it not saved and not approved
                {
                    int XX = ser.Check_Redandancy(Session["S_EMP_NO"].ToString(), Session["S_YEAR"].ToString());
                    if (XX != 0)
                    {
                        Alert.Show(" هذا الموظف تم ادخال تقييم له في هذا العام ولكن علي نموذج مختلف ");
                        return;
                    }
                    btn_save.Visible = true;
                    btn_approve.Visible = false;
                }
                else
                    if (ASST == "1") // when it saved only
                    {
                        Get_Data_FormD();
                        btn_save.Visible = true;
                        btn_delete.Visible = true;
                        if (APPROVER != 0)
                        {
                            btn_approve.Visible = true;
                        }
                    }
                    else
                        if (ASST == "2")  // when it approved
                        {
                            Get_Data_FormD();
                            btn_save.Visible = false;
                            btn_delete.Visible = false;
                            if (APPROVER != 0)
                            {
                                btn_approve.Visible = false;
                            }
                            Display_Only();
                        }
            }
            else
            {
                Get_Data_FormD();
                btn_save.Visible = false;
                btn_delete.Visible = false;
                Display_Only();
                if (ASST == "1")
                {
                    if (APPROVER != 0)
                    {
                        btn_approve.Visible = true;
                    }
                }
            }
        }
    }
    private void Get_Data_FormD()
    {
        try
        {

            DsT = ser.Get_Form_D_Data(Session["S_EMP_NO"].ToString(), Session["S_YEAR"].ToString());
            FormDT = DsT;
            foreach (DataRow var in FormDT.Rows)
            {
                Session["HDR_ID_D"] = var[0].ToString();
                string emp_code = var[1].ToString();
                string emp_name = var[2].ToString();
                string form_id = var[3].ToString();
                if (form_id.Trim() != "4") { Response.Redirect("~/Trans/AS_Enter.aspx"); }

                g1_name.Text = var[4].ToString();
                g1_weight.Text = var[5].ToString();
                g1_actual.Text = var[6].ToString();
                g1_just.Text = var[7].ToString();

                g2_name.Text = var[8].ToString();
                g2_weight.Text = var[9].ToString();
                g2_actual.Text = var[10].ToString();
                g2_just.Text = var[11].ToString();

                g3_name.Text = var[12].ToString();
                g3_weight.Text = var[13].ToString();
                g3_actual.Text = var[14].ToString();
                g3_just.Text = var[15].ToString();

                g4_name.Text = var[16].ToString();
                g4_weight.Text = var[17].ToString();
                g4_actual.Text = var[18].ToString();
                g4_just.Text = var[19].ToString();

                g5_name.Text = var[20].ToString();
                g5_weight.Text = var[21].ToString();
                g5_actual.Text = var[22].ToString();
                g5_just.Text = var[23].ToString();

                g6_name.Text = var[24].ToString();
                g6_weight.Text = var[25].ToString();
                g6_actual.Text = var[26].ToString();
                g6_just.Text = var[27].ToString();

                g1_name0.Text = var[42].ToString();
                g1_weight0.Text = var[43].ToString();

                //***********************************************
                eval_horsepower.ClearSelection();
                eval_horsepower.Items.FindByValue(var[28].ToString()).Selected = true;
                just_horsepower.Text = var[29].ToString();

                eval_problem_solve.ClearSelection();
                eval_problem_solve.Items.FindByValue(var[30].ToString()).Selected = true;
                just_problem_solve.Text = var[31].ToString();

                eval_measur_work.ClearSelection();
                eval_measur_work.Items.FindByValue(var[32].ToString()).Selected = true;
                just_measur_work.Text = var[33].ToString();

                eval_motivating.ClearSelection();
                eval_motivating.Items.FindByValue(var[34].ToString()).Selected = true;
                just_motivating.Text = var[35].ToString();

                eval_planning.ClearSelection();
                eval_planning.Items.FindByValue(var[36].ToString()).Selected = true;
                just_planning.Text = var[37].ToString();

                eval_team_contrb.ClearSelection();
                eval_team_contrb.Items.FindByValue(var[38].ToString()).Selected = true;
                just_team_contrb.Text = var[39].ToString();

                eval_work_habits.ClearSelection();
                eval_work_habits.Items.FindByValue(var[40].ToString()).Selected = true;
                just_work_habits.Text = var[41].ToString();


                //********************************************************

                g1_name0.Text = var[42].ToString();
                g1_weight0.Text = var[43].ToString();
                g1_actual0.Text = var[44].ToString();
                g1_just0.Text = var[45].ToString();

                g2_name0.Text = var[46].ToString();
                g2_weight0.Text = var[47].ToString();
                g2_actual0.Text = var[48].ToString();
                g2_just0.Text = var[49].ToString();

                g3_name0.Text = var[50].ToString();
                g3_weight0.Text = var[51].ToString();
                g3_actual0.Text = var[52].ToString();
                g3_just0.Text = var[53].ToString();

                g4_name0.Text = var[54].ToString();
                g4_weight0.Text = var[55].ToString();
                g4_actual0.Text = var[56].ToString();
                g4_just0.Text = var[57].ToString();

                g5_name0.Text = var[58].ToString();
                g5_weight0.Text = var[59].ToString();
                g5_actual0.Text = var[60].ToString();
                g5_just0.Text = var[61].ToString();

                g6_name0.Text = var[62].ToString();
                g6_weight0.Text = var[63].ToString();
                g6_actual0.Text = var[64].ToString();
                g6_just0.Text = var[65].ToString();

                //**************************************************

                txt_down_result.Text = " % " + var[66].ToString();
                txt_top_result.Text = " % " + var[66].ToString();

                txt_manager.Text = var[67].ToString();

            }

        }
        catch
        {

        }
    }
    protected void btn_calculate_Click(object sender, EventArgs e)
    {
        if (eval_horsepower.SelectedIndex == 0 ||
        eval_problem_solve.SelectedIndex == 0 ||
        eval_measur_work.SelectedIndex == 0 ||
        eval_motivating.SelectedIndex == 0 ||
        eval_planning.SelectedIndex == 0 ||
        eval_team_contrb.SelectedIndex == 0 ||
        eval_work_habits.SelectedIndex == 0 || Check_Objectives() > 0 || Check_Objectives_Next_Year() > 0)
        {
            Alert.Show(" برجاء اختيار جميع الجدارات أو فحص نسب الاهداف");
            return;
        }
        else
        {
            Check_Objectives();


        }
    }
    protected void btn_approve_Click(object sender, EventArgs e)
    {
        try
        {
            int APRV = ser.Make_Approve(Session["HDR_ID_D"].ToString(), Session["S_EMP_NO"].ToString(), Session["Logged_User"].ToString(), Session["S_YEAR"].ToString());
            if (APRV == 1)
            {
                Alert.Show(" تمت الموافقة بنجاح ");
                Response.Redirect("~/Trans/AS_Enter.aspx");
            }

        }
        catch
        {
            Alert.Show(" يوجد مشكلة في عملية الموافقة  ");
            return;
        }
    }
    protected void btn_save_Click(object sender, EventArgs e)
    {
        try
        {
            if (eval_horsepower.SelectedIndex == 0 ||
        eval_problem_solve.SelectedIndex == 0 ||
        eval_measur_work.SelectedIndex == 0 ||
        eval_motivating.SelectedIndex == 0 ||
        eval_planning.SelectedIndex == 0 ||
        eval_team_contrb.SelectedIndex == 0 ||
        eval_work_habits.SelectedIndex == 0 || Check_Objectives() > 0 || Check_Objectives_Next_Year() > 0)
            {
                Alert.Show(" برجاء اختيار جميع الجدارات أو فحص الاهداف");
                return;
            }
            else
            {
                string YSTS = ser.Check_Year(Session["S_YEAR"].ToString());
                if (YSTS.Trim() == "Y")  // 1- check locked year or not
                {
                    int RDSTS = ser.Check_Redandancy(Session["S_EMP_NO"].ToString(), Session["S_YEAR"].ToString());
                    if (RDSTS == 0)      // 2- check redandancy data 
                    {
                        double FinalR = Calculate_Form_D();
                        int SVHDR = ser.Save_Form_HDR(Session["S_EMP_NO"].ToString(), Session["S_EMP_NAME"].ToString(), Session["SFORMID"].ToString(), "0", Session["S_YEAR"].ToString(), "0", Session["Logged_User"].ToString());
                        int RESLT = ser.Save_Form_D_DTL(SVHDR.ToString(), Session["S_EMP_NAME"].ToString(), g1_name.Text, g1_weight.Text, g1_actual.Text, g1_just.Text, g2_name.Text, g2_weight.Text,
                            g2_actual.Text, g2_just.Text, g3_name.Text, g3_weight.Text, g3_actual.Text, g3_just.Text, g4_name.Text, g4_weight.Text,
                            g4_actual.Text, g4_just.Text, g5_name.Text, g5_weight.Text, g5_actual.Text, g5_just.Text, g6_name.Text, g6_weight.Text, g6_actual.Text, g6_just.Text, eval_horsepower.SelectedValue.ToString(), just_horsepower.Text, eval_problem_solve.SelectedValue.ToString(), just_problem_solve.Text, eval_measur_work.SelectedValue.ToString(), just_measur_work.Text,
                            eval_motivating.SelectedValue.ToString(), just_motivating.Text, eval_planning.SelectedValue.ToString(), just_planning.Text, eval_team_contrb.SelectedValue.ToString(), just_team_contrb.Text, eval_work_habits.SelectedValue.ToString(), just_work_habits.Text, g1_name0.Text, g1_weight0.Text, g1_actual0.Text, g1_just0.Text, g2_name0.Text, g2_weight0.Text,
                            g2_actual0.Text, g2_just0.Text, g3_name0.Text, g3_weight0.Text, g3_actual0.Text, g3_just0.Text, g4_name0.Text, g4_weight0.Text,
                            g4_actual0.Text, g4_just0.Text, g5_name0.Text, g5_weight0.Text, g5_actual0.Text, g5_just0.Text, g6_name0.Text, g6_weight0.Text, g6_actual0.Text, g6_just0.Text, FinalR.ToString());


                        if (RESLT != 0 && SVHDR != 0)
                        {

                            Alert.Show("تم حفظ التقييم بنجاح");
                            Clear();
                           // Response.Redirect("~/Trans/AS_Enter.aspx");
                        }
                        else
                        {
                            Alert.Show("يوجد مشكله في عملية الادخال");
                            return;
                        }
                    }
                    else
                    {
                        double UP_FinalR = Calculate_Form_D();
                        int UPDTL = ser.Update_Form_D_DTL(Session["HDR_ID_D"].ToString(), Session["S_EMP_NAME"].ToString(), g1_name.Text, g1_weight.Text, g1_actual.Text, g1_just.Text, g2_name.Text, g2_weight.Text,
                            g2_actual.Text, g2_just.Text, g3_name.Text, g3_weight.Text, g3_actual.Text, g3_just.Text, g4_name.Text, g4_weight.Text,
                            g4_actual.Text, g4_just.Text, g5_name.Text, g5_weight.Text, g5_actual.Text, g5_just.Text, g6_name.Text, g6_weight.Text, g6_actual.Text, g6_just.Text, eval_horsepower.SelectedValue.ToString(), just_horsepower.Text, eval_problem_solve.SelectedValue.ToString(), just_problem_solve.Text, eval_measur_work.SelectedValue.ToString(), just_measur_work.Text,
                            eval_motivating.SelectedValue.ToString(), just_motivating.Text, eval_planning.SelectedValue.ToString(), just_planning.Text, eval_team_contrb.SelectedValue.ToString(), just_team_contrb.Text, eval_work_habits.SelectedValue.ToString(), just_work_habits.Text, g1_name0.Text, g1_weight0.Text, g1_actual0.Text, g1_just0.Text, g2_name0.Text, g2_weight0.Text,
                            g2_actual0.Text, g2_just0.Text, g3_name0.Text, g3_weight0.Text, g3_actual0.Text, g3_just0.Text, g4_name0.Text, g4_weight0.Text,
                            g4_actual0.Text, g4_just0.Text, g5_name0.Text, g5_weight0.Text, g5_actual0.Text, g5_just0.Text, g6_name0.Text, g6_weight0.Text, g6_actual0.Text, g6_just0.Text, UP_FinalR.ToString());
                        if (UPDTL != 0)
                        {
                            Alert.Show(" تم حفظ بيانات التقييم بنجاج  ");
                            Clear();
                           // Response.Redirect("~/Trans/AS_Enter.aspx");
                        }
                        else
                        {
                            Alert.Show("  برجاء اعادة اختيار الموظف المطلوب تقييمة أو تقليل عدد احرف كتابة الاهداف او التبريرات  ");
                            return;
                        }

                    }
                }
                else
                {
                    Alert.Show(" السنة الحالية تم اغلاقها ");
                    return;
                }
            }
        }
        catch
        {
            Alert.Show("يوجد مشكله في عملية الادخال");
        }
    }
    private double Calculate_Form_D()
    {
        double F_RSLT = 0;
        try
        {

            double A = double.Parse(eval_measur_work.SelectedValue.ToString());
            double B = double.Parse(eval_motivating.SelectedValue.ToString());
            double C = double.Parse(eval_horsepower.SelectedValue.ToString());
            double D = double.Parse(eval_planning.SelectedValue.ToString());
            double E = double.Parse(eval_problem_solve.SelectedValue.ToString());
            double L = double.Parse(eval_team_contrb.SelectedValue.ToString());
            double M = double.Parse(eval_work_habits.SelectedValue.ToString());

            if (g1_actual.Text == "") { g1_actual.Text = "0"; }
            double F = double.Parse(g1_actual.Text);
            if (g2_actual.Text == "") { g2_actual.Text = "0"; }
            double G = double.Parse(g2_actual.Text);
            if (g3_actual.Text == "") { g3_actual.Text = "0"; }
            double H = double.Parse(g3_actual.Text);
            if (g4_actual.Text == "") { g4_actual.Text = "0"; }
            double I = double.Parse(g4_actual.Text);
            if (g5_actual.Text == "") { g5_actual.Text = "0"; }
            double J = double.Parse(g5_actual.Text);
            if (g6_actual.Text == "") { g6_actual.Text = "0"; }
            double K = double.Parse(g6_actual.Text);

            double Eval_Sum = A + B + C + D + E + L + M ;
            //double Competencies_Result = ((Eval_Sum / 35 ) * 100) * 0.3;
            double Competencies_Result = ((Eval_Sum / 35) * 100) * 0.2;

            double Eval_Actual = F + G + H + I + J + K;
            //double Objectives_Result = Eval_Actual * 0.7;
            double Objectives_Result = Eval_Actual * 0.8;

            double Final_Result =  Competencies_Result + Objectives_Result  ;

            F_RSLT = Final_Result;
            txt_top_result.Text = " % " + Final_Result.ToString().Substring(0, 6);
            txt_down_result.Text = " % " + Final_Result.ToString().Substring(0, 6);
        }
        catch
        {
            txt_top_result.Text = "000";
            txt_down_result.Text = "000";
        }

        return F_RSLT;
    }

    private void Static_Target()
    {
        g1_name.Enabled = false;
        g1_weight.Enabled = false;
        g1_actual.Enabled = false;
        g1_just.Enabled = false;

        g2_name.Enabled = false;
        g2_weight.Enabled = false;
        g2_actual.Enabled = false;
        g2_just.Enabled = false;

        g3_name.Enabled = false;
        g3_weight.Enabled = false;
        g3_actual.Enabled = false;
        g3_just.Enabled = false;

        g4_name.Enabled = false;
        g4_weight.Enabled = false;
        g4_actual.Enabled = false;
        g4_just.Enabled = false;

        g5_name.Enabled = false;
        g5_weight.Enabled = false;
        g5_actual.Enabled = false;
        g5_just.Enabled = false;

        g6_name.Enabled = false;
        g6_weight.Enabled = false;
        g6_actual.Enabled = false;
        g6_just.Enabled = false;

        g1_name0.Enabled = false;
        g1_weight0.Enabled = false;
        g1_actual0.Enabled = false;
        g1_just0.Enabled = false;

        g2_name0.Enabled = false;
        g2_weight0.Enabled = false;
        g2_actual0.Enabled = false;
        g2_just0.Enabled = false;

        g3_name0.Enabled = false;
        g3_weight0.Enabled = false;
        g3_actual0.Enabled = false;
        g3_just0.Enabled = false;

        g4_name0.Enabled = false;
        g4_weight0.Enabled = false;
        g4_actual0.Enabled = false;
        g4_just0.Enabled = false;

        g5_name0.Enabled = false;
        g5_weight0.Enabled = false;
        g5_actual0.Enabled = false;
        g5_just0.Enabled = false;

        g6_name0.Enabled = false;
        g6_weight0.Enabled = false;
        g6_actual0.Enabled = false;
        g6_just0.Enabled = false;

    }
    private void Display_Only()
    {
        btn_approve.Enabled = false;
        btn_save.Enabled = false;
        btn_calculate.Enabled = false;
        btn_delete.Enabled = false;

        g1_name.Enabled = false;
        g1_weight.Enabled = false;
        g1_actual.Enabled = false;
        g1_just.Enabled = false;

        g2_name.Enabled = false;
        g2_weight.Enabled = false;
        g2_actual.Enabled = false;
        g2_just.Enabled = false;

        g3_name.Enabled = false;
        g3_weight.Enabled = false;
        g3_actual.Enabled = false;
        g3_just.Enabled = false;

        g4_name.Enabled = false;
        g4_weight.Enabled = false;
        g4_actual.Enabled = false;
        g4_just.Enabled = false;

        g5_name.Enabled = false;
        g5_weight.Enabled = false;
        g5_actual.Enabled = false;
        g5_just.Enabled = false;

        g6_name.Enabled = false;
        g6_weight.Enabled = false;
        g6_actual.Enabled = false;
        g6_just.Enabled = false;

        eval_horsepower.Enabled = false;
        eval_problem_solve.Enabled = false;
        eval_measur_work.Enabled = false;
        eval_motivating.Enabled = false;
        eval_planning.Enabled = false;
        eval_team_contrb.Enabled = false;
        eval_work_habits.Enabled = false;

        just_horsepower.Enabled = false;
        just_problem_solve.Enabled = false;
        just_measur_work.Enabled = false;
        just_motivating.Enabled = false;
        just_planning.Enabled = false;
        just_team_contrb.Enabled = false;
        just_work_habits.Enabled = false;

        g1_name0.Enabled = false;
        g1_weight0.Enabled = false;
        g1_actual0.Enabled = false;
        g1_just0.Enabled = false;

        g2_name0.Enabled = false;
        g2_weight0.Enabled = false;
        g2_actual0.Enabled = false;
        g2_just0.Enabled = false;

        g3_name0.Enabled = false;
        g3_weight0.Enabled = false;
        g3_actual0.Enabled = false;
        g3_just0.Enabled = false;

        g4_name0.Enabled = false;
        g4_weight0.Enabled = false;
        g4_actual0.Enabled = false;
        g4_just0.Enabled = false;

        g5_name0.Enabled = false;
        g5_weight0.Enabled = false;
        g5_actual0.Enabled = false;
        g5_just0.Enabled = false;

        g6_name0.Enabled = false;
        g6_weight0.Enabled = false;
        g6_actual0.Enabled = false;
        g6_just0.Enabled = false;
    }

    private void Clear()
    {
        Session["HDR_ID_D"] = "";
        g1_name.Text = "";
        g1_weight.Text = "0";
        g1_actual.Text = "0";
        g1_just.Text = "";

        g2_name.Text = "";
        g2_weight.Text = "0";
        g2_actual.Text = "0";
        g2_just.Text = "";

        g3_name.Text = "";
        g3_weight.Text = "0";
        g3_actual.Text = "0";
        g3_just.Text = "";

        g4_name.Text = "";
        g4_weight.Text = "0";
        g4_actual.Text = "0";
        g4_just.Text = "";

        g5_name.Text = "";
        g5_weight.Text = "0";
        g5_actual.Text = "0";
        g5_just.Text = "";

        g6_name.Text = "";
        g6_weight.Text = "0";
        g6_actual.Text = "0";
        g6_just.Text = "";

        eval_horsepower.SelectedIndex = 0;
        eval_problem_solve.SelectedIndex = 0;
        eval_measur_work.SelectedIndex = 0;
        eval_motivating.SelectedIndex = 0;
        eval_planning.SelectedIndex = 0;
        eval_team_contrb.SelectedIndex = 0;
        eval_work_habits.SelectedIndex = 0;

        just_horsepower.Text = "";
        just_problem_solve.Text = "";
        just_measur_work.Text = "";
        just_motivating.Text = "";
        just_planning.Text = "";
        just_team_contrb.Text = "";
        just_work_habits.Text = "";

        g1_name0.Text = "";
        g1_weight0.Text = "0";
        g1_actual0.Text = "0";
        g1_just0.Text = "";

        g2_name0.Text = "";
        g2_weight0.Text = "0";
        g2_actual0.Text = "0";
        g2_just0.Text = "";

        g3_name0.Text = "";
        g3_weight0.Text = "0";
        g3_actual0.Text = "0";
        g3_just0.Text = "";

        g4_name0.Text = "";
        g4_weight0.Text = "0";
        g4_actual0.Text = "0";
        g4_just0.Text = "";

        g5_name0.Text = "";
        g5_weight0.Text = "0";
        g5_actual0.Text = "0";
        g5_just0.Text = "";

        g6_name0.Text = "";
        g6_weight0.Text = "0";
        g6_actual0.Text = "0";
        g6_just0.Text = "";

        txt_down_result.Text = "0";
        txt_top_result.Text = "0";
    }

    private int Check_Objectives_Next_Year()
    {
        int ret_val = 0;
        if (g1_weight0.Text == "") { g1_weight0.Text = "0"; }
        double AA = double.Parse(g1_weight0.Text);
        if (g2_weight0.Text == "") { g2_weight0.Text = "0"; }
        double BB = double.Parse(g2_weight0.Text);
        if (g3_weight0.Text == "") { g3_weight0.Text = "0"; }
        double CC = double.Parse(g3_weight0.Text);
        if (g4_weight0.Text == "") { g4_weight0.Text = "0"; }
        double DD = double.Parse(g4_weight0.Text);
        if (g5_weight0.Text == "") { g5_weight0.Text = "0"; }
        double EE = double.Parse(g5_weight0.Text);
        if (g6_weight0.Text == "") { g6_weight0.Text = "0"; }
        double LL = double.Parse(g6_weight0.Text);

        //if (g1_actual0.Text == "") { g1_actual0.Text = "0"; }
        //double FF = double.Parse(g1_actual0.Text);
        //if (g2_actual0.Text == "") { g2_actual0.Text = "0"; }
        //double GG = double.Parse(g2_actual0.Text);
        //if (g3_actual0.Text == "") { g3_actual0.Text = "0"; }
        //double HH = double.Parse(g3_actual0.Text);
        //if (g4_actual0.Text == "") { g4_actual0.Text = "0"; }
        //double II = double.Parse(g4_actual0.Text);
        //if (g5_actual0.Text == "") { g5_actual0.Text = "0"; }
        //double JJ = double.Parse(g5_actual0.Text);
        //if (g6_actual0.Text == "") { g6_actual0.Text = "0"; }
        //double KK = double.Parse(g6_actual0.Text);

        double Wgh = AA + BB + CC + DD + EE + LL;
        //double Act = FF + GG + HH + II + JJ + KK;

        //***********************************************************************
        if (AA > 0)  //|| FF > 0
        {
            if (g1_name0.Text == "")
            { Alert.Show(" برجاء ادخال اسم الهدف  المقبل الاول "); ret_val = ret_val + 1; }
            //if (FF > AA)
            //{
            //    Alert.Show("يجب أن يكون النسبة المحققه أقل من  أو يساوى الوزن النسبى للهدف  المقبل الاول "); ret_val = ret_val + 1;
            //}
        }
        if (BB > 0)  //|| GG > 0
        {
            if (g2_name0.Text == "")
            { Alert.Show(" برجاء ادخال اسم الهدف المقبل الثاني "); ret_val = ret_val + 1; }
            //if (GG > BB)
            //{
            //    Alert.Show("يجب أن يكون النسبة المحققه أقل من  أو يساوى الوزن النسبى للهدف  المقبل الثاني "); ret_val = ret_val + 1;
            //}
        }
        if (CC > 0)  // || HH > 0
        {
            if (g3_name0.Text == "")
            { Alert.Show(" برجاء ادخال اسم الهدف  المقبل الثالث "); ret_val = ret_val + 1; }
            //if (HH > CC)
            //{
            //    Alert.Show("يجب أن يكون النسبة المحققه أقل من  أو يساوى الوزن النسبى للهدف المقبل الثالث "); ret_val = ret_val + 1;
            //}
        }
        if (DD > 0) //|| II > 0
        {
            if (g4_name0.Text == "")
            { Alert.Show(" برجاء ادخال اسم الهدف  المقبل الرابع "); ret_val = ret_val + 1; }
            //if (II > DD)
            //{
            //    Alert.Show("يجب أن يكون النسبة المحققه أقل من  أو يساوى الوزن النسبى للهدف  المقبل الرابع "); ret_val = ret_val + 1;
            //}
        }
        if (EE > 0) //|| JJ > 0
        {
            if (g5_name0.Text == "")
            { Alert.Show(" برجاء ادخال اسم الهدف  المقبل الخامس "); ret_val = ret_val + 1; }
            //if (JJ > EE)
            //{
            //    Alert.Show("يجب أن يكون النسبة المحققه أقل من  أو يساوى الوزن النسبى للهدف  المقبل الخامس "); ret_val = ret_val + 1;
            //}
        }
        if (LL > 0 )  //|| KK > 0
        {
            if (g6_name0.Text == "")
            { Alert.Show(" برجاء ادخال اسم الهدف المقبل السادس "); ret_val = ret_val + 1; }
            //if (KK > LL)
            //{
            //    Alert.Show("يجب أن يكون النسبة المحققه أقل من  أو يساوى الوزن النسبى للهدف المقبل السادس "); ret_val = ret_val + 1;
            //}
        }
        //**************************************************************************
        //if (FF > AA) { Alert.Show(" برجاء التحقق من نسبةالهدف الاول في اهداف العام المقبل"); ret_val = ret_val + 1; }
        //if (GG > BB) { Alert.Show("برجاء التحقق من نسبةالهدف الثاني في اهداف العام المقبل"); ret_val = ret_val + 1; }
        //if (HH > CC) { Alert.Show("برجاء التحقق من نسبةالهدف الثالث في اهداف العام المقبل"); ret_val = ret_val + 1; }
        //if (II > DD) { Alert.Show("برجاء التحقق من نسبةالهدف الرابع في اهداف العام المقبل"); ret_val = ret_val + 1; }
        //if (JJ > EE) { Alert.Show(" برجاء التحقق من نسبةالهدف الخامس في اهداف العام المقبل"); ret_val = ret_val + 1; }
        //if (KK > LL) { Alert.Show(" برجاء التحقق من نسبةالهدف السادس في اهداف العام المقبل"); ret_val = ret_val + 1; }
        if (Wgh > 0 )
        {
            if (Wgh != 100)
            {
                Alert.Show("برجاء التحقق من الوزن النسبي لإهداف العام المقبل لابد الا يتعدي 100%");
                ret_val = ret_val + 1;
            }
        }

        return ret_val;
    }
    private int Check_Objectives()
    {
        int ret_val = 0;
        if (g1_weight.Text == "") { g1_weight.Text = "0"; }
        double AA = double.Parse(g1_weight.Text);
        if (g2_weight.Text == "") { g2_weight.Text = "0"; }
        double BB = double.Parse(g2_weight.Text);
        if (g3_weight.Text == "") { g3_weight.Text = "0"; }
        double CC = double.Parse(g3_weight.Text);
        if (g4_weight.Text == "") { g4_weight.Text = "0"; }
        double DD = double.Parse(g4_weight.Text);
        if (g5_weight.Text == "") { g5_weight.Text = "0"; }
        double EE = double.Parse(g5_weight.Text);
        if (g6_weight.Text == "") { g6_weight.Text = "0"; }
        double LL = double.Parse(g6_weight.Text);

        if (g1_actual.Text == "") { g1_actual.Text = "0"; }
        double FF = double.Parse(g1_actual.Text);
        if (g2_actual.Text == "") { g2_actual.Text = "0"; }
        double GG = double.Parse(g2_actual.Text);
        if (g3_actual.Text == "") { g3_actual.Text = "0"; }
        double HH = double.Parse(g3_actual.Text);
        if (g4_actual.Text == "") { g4_actual.Text = "0"; }
        double II = double.Parse(g4_actual.Text);
        if (g5_actual.Text == "") { g5_actual.Text = "0"; }
        double JJ = double.Parse(g5_actual.Text);
        if (g6_actual.Text == "") { g6_actual.Text = "0"; }
        double KK = double.Parse(g6_actual.Text);

        double Wgh = AA + BB + CC + DD + EE + LL;
        double Act = FF + GG + HH + II + JJ + KK;

        //***********************************************************************
        if (AA > 0 || FF > 0)
        {
            if (g1_name.Text == "")
            { Alert.Show(" برجاء ادخال اسم الهدف الاول "); ret_val = ret_val + 1; }
            if (FF > AA)
            {
                Alert.Show("يجب أن يكون النسبة المحققه أقل من  أو يساوى الوزن النسبى للهدف الاول "); ret_val = ret_val + 1;
            }
        }
        if (BB > 0 || GG > 0)
        {
            if (g2_name.Text == "")
            { Alert.Show(" برجاء ادخال اسم الهدف الثاني "); ret_val = ret_val + 1; }
            if (GG > BB)
            {
                Alert.Show("يجب أن يكون النسبة المحققه أقل من  أو يساوى الوزن النسبى للهدف الثاني "); ret_val = ret_val + 1;
            }
        }
        if (CC > 0 || HH > 0)
        {
            if (g3_name.Text == "")
            { Alert.Show(" برجاء ادخال اسم الهدف الثالث "); ret_val = ret_val + 1; }
            if (HH > CC)
            {
                Alert.Show("يجب أن يكون النسبة المحققه أقل من  أو يساوى الوزن النسبى للهدف الثالث "); ret_val = ret_val + 1;
            }
        }
        if (DD > 0 || II > 0)
        {
            if (g4_name.Text == "")
            { Alert.Show(" برجاء ادخال اسم الهدف الرابع "); ret_val = ret_val + 1; }
            if (II > DD)
            {
                Alert.Show("يجب أن يكون النسبة المحققه أقل من  أو يساوى الوزن النسبى للهدف الرابع "); ret_val = ret_val + 1;
            }
        }
        if (EE > 0 || JJ > 0)
        {
            if (g5_name.Text == "")
            { Alert.Show(" برجاء ادخال اسم الهدف الخامس "); ret_val = ret_val + 1; }
            if (JJ > EE)
            {
                Alert.Show("يجب أن يكون النسبة المحققه أقل من  أو يساوى الوزن النسبى للهدف الخامس "); ret_val = ret_val + 1;
            }
        }
        if (LL > 0 || KK > 0)
        {
            if (g6_name.Text == "")
            { Alert.Show(" برجاء ادخال اسم الهدف السادس "); ret_val = ret_val + 1; }
            if (KK > LL)
            {
                Alert.Show("يجب أن يكون النسبة المحققه أقل من  أو يساوى الوزن النسبى للهدف السادس "); ret_val = ret_val + 1;
            }
        }
        //**************************************************************************

        //if (FF > AA) { Alert.Show("برجاء التحقق من نسبةالهدف الاول"); ret_val = ret_val + 1; }
        //if (GG > BB) { Alert.Show("برجاء التحقق من نسبةالهدف الثاني"); ret_val = ret_val + 1; }
        //if (HH > CC) { Alert.Show("برجاء التحقق من نسبةالهدف الثالث"); ret_val = ret_val + 1; }
        //if (II > DD) { Alert.Show("برجاء التحقق من نسبةالهدف الرابع"); ret_val = ret_val + 1; }
        //if (JJ > EE) { Alert.Show("برجاء التحقق من نسبةالهدف الخامس"); ret_val = ret_val + 1; }
        //if (KK > LL) { Alert.Show("برجاء التحقق من نسبةالهدف السادس"); ret_val = ret_val + 1; }

        if (Wgh != 100)
        {
            Alert.Show("برجاء التحقق من الوزن النسبي لابد الا يتعدي 100%");
            ret_val = ret_val + 1;
        }
        else
        {
            txt_down_result.Text = " % " + Calculate_Form_D();
        }

        return ret_val;
    }
    protected void btn_delete_Click(object sender, EventArgs e)
    {
        try
        {
            string confirmValue = Request.Form["confirm_value"];
            if (confirmValue == "No")
            {

            }
            else
            {
                string st = ser.Emp_Static_Target(Session["S_EMP_NO"].ToString());
                if (st == "1")
                {
                    Alert.Show(" لا يمكن حذف التقييم حيث ان الاهداف مثبتة .. يرجى عمل التعديلات اللازمة على الجدادرات بدون حذف");
                }
                else
                {
                    int DEL = ser.Delete_Form4(Session["HDR_ID_D"].ToString());
                    if (DEL == 1)
                    {
                        Alert.Show(" تم حذف التقييم بنجاح ");
                        Response.Redirect("~/Trans/AS_Enter.aspx");
                    }
                    else
                    {
                        Alert.Show(" يوجد مشكلة في عملية الحذف ");
                        return;
                    }
                }
            }
        }
        catch
        {

        }
    }
    protected void btn_print_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/Reports/Print_d.aspx");
    }
}
