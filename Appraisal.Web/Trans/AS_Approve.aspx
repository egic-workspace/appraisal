﻿<%@ Page Language="C#" MasterPageFile="~/Main_Master_Page.master" AutoEventWireup="true" CodeFile="AS_Approve.aspx.cs" Inherits="Trans_AS_Approve" Title="الموافقات" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<script type = "text/javascript">
        function Confirm() {
            var confirm_value = document.createElement("INPUT");
            confirm_value.type = "hidden";
            confirm_value.name = "confirm_value";
            if (confirm("هل تريد الموافقة علي التقييمات ?")) {
                confirm_value.value = "Yes";
            } else {
                confirm_value.value = "No";
            }
            document.forms[0].appendChild(confirm_value);
        }
    </script>
    <table style="width: 100%;" align="center">
 <tr>
    <td style="background-color:#e6f1f8; height: 2px; width: 70%;" >
    <span  style="font-size:11pt; font-weight:bold; color:#000080; font-family:Courier New;">
        &nbsp; موافقة التقييم </span>
    </td></tr>
 </table>
    <p>
    </p>
    <p>
         <table align="center" dir="rtl" style="width: 98%; height: 98%; float: right">
        <tr>
            <td>
                &nbsp;</td>
            <td colspan="2">
    <span  style="font-size:11pt; font-weight:bold; color:#000000; font-family:Courier New;">
            <asp:Label ID="Label63" runat="server" Font-Bold="True" Text="السنة المفتوحة للتقييم" 
                    Font-Size="16pt" Font-Names="Arial" ForeColor="Black" Width="180px"></asp:Label>
                <asp:TextBox ID="TXT_OPEN_YEAR" runat="server" BackColor="#FF9933" 
                    BorderStyle="Solid" Font-Bold="True" Font-Size="15pt" Width="150px" 
                    Enabled="False"></asp:TextBox>
        </span>
            </td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td align="center">
            <asp:Button ID="btn_all_levels" runat="server" BackColor="#3366FF" Font-Bold="True" 
                Font-Size="15pt" ForeColor="White" Text="جميع المستويات " Width="300px" 
                onclick="btn_all_levels_Click" />
            </td>
            <td align="center">
            <asp:Button ID="btn_frst_level" runat="server" BackColor="#3366FF" Font-Bold="True" 
                Font-Size="15pt" ForeColor="White" Text="المستوي الأول" Width="300px" 
                onclick="btn_frst_level_Click" />
            </td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td align="center" colspan="4">
            <asp:GridView ID="GridView1" runat="server" 
                EmptyDataText="ليس لديك بيانات موظفين مسجلة" 
                    Caption="الموظفين المطلوب الموافقة علي تقييمهم" AutoGenerateColumns="False" 
                CaptionAlign="Top" onselectedindexchanged="GridView1_SelectedIndexChanged" 
                CssClass="mGrid" PagerStyle-CssClass="pgr" 
                AlternatingRowStyle-CssClass="alt" 
                DataKeyNames="EMP_NO" onpageindexchanging="GridView1_PageIndexChanging" Width="800px" 
                        Font-Names="Arial" Font-Size="14pt" 
                    onselectedindexchanging="GridView1_SelectedIndexChanging" 
                    AutoGenerateSelectButton="True">
                
                <Columns>
   
                         <asp:TemplateField HeaderText="**" >
                            <ItemTemplate>
                               <asp:CheckBox ID="chk_data" runat="server" ></asp:CheckBox>
                            </ItemTemplate>
                        </asp:TemplateField>
                        
                        <asp:TemplateField HeaderText="كود الموظف" SortExpression="Value">
                            <ItemTemplate>
                                <asp:Label ID="lblemp_no" runat="server" Text='<%# Bind("EMP_NO") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                     
                             <asp:TemplateField HeaderText="اسم الموظف">
                          <ItemTemplate>
                                <asp:Label ID="lblemp_name" runat="server" Text='<%# Bind("EMP_NAME") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        
                        <asp:TemplateField HeaderText="الوظيفة">
                         <ItemTemplate>
                                <asp:Label ID="lbljob_name" runat="server" Text='<%# Bind("JOB_NAME") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        
                          <asp:TemplateField HeaderText="نموذج">
                         <ItemTemplate>
                                <asp:Label ID="lblform_id" runat="server" Text='<%# Bind("FORM_ID") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        
                          <asp:TemplateField HeaderText="التقييم" Visible="false">
                         <ItemTemplate>
                                <asp:Label ID="lblstatus" runat="server" Text='<%# Bind("STATUS") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        
                           <asp:TemplateField HeaderText="التقييم">
                         <ItemTemplate>
                                <asp:Label ID="lblfstatus" runat="server" Text='<%# Bind("FSTATUS") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        
                           <asp:TemplateField HeaderText="النتيجة">
                         <ItemTemplate>
                                <asp:Label ID="lblrslt" runat="server" Text='<%# Bind("rslt") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        
                         <asp:TemplateField HeaderText="المستوي">
                         <ItemTemplate>
                                <asp:Label ID="lbllevel" runat="server" Text='<%# Bind("LEVEL") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        
                         <asp:TemplateField HeaderText="الشركة" Visible="false">
                         <ItemTemplate>
                                <asp:Label ID="lblcompany" runat="server" Text='<%# Bind("emp_company") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        
                    </Columns>
                
                <PagerStyle />
                <AlternatingRowStyle />
            </asp:GridView>
                </td>
        </tr>
        <tr>
            <td style="height: 15px">
                </td>
            <td style="height: 15px" align="center" colspan="3">
            <asp:Button ID="btn_approve" runat="server" BackColor="#FF0066" Font-Bold="True" 
                Font-Size="15pt" ForeColor="White" Text="موافقة المدير" Width="250px" 
                    onclick="btn_approve_Click" onclientclick="Confirm()" />
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td align="center" colspan="4">
            <asp:GridView ID="GridView2" runat="server" 
                EmptyDataText="ليس لديك بيانات موظفين مسجلة" 
                    Caption="باقي الموظفين " AutoGenerateColumns="False" 
                CaptionAlign="Top" onselectedindexchanged="GridView1_SelectedIndexChanged" 
                CssClass="mGrid" PagerStyle-CssClass="pgr" 
                AlternatingRowStyle-CssClass="alt" 
                DataKeyNames="EMP_NO" onpageindexchanging="GridView1_PageIndexChanging" Width="800px" 
                        Font-Names="Arial" Font-Size="14pt" 
                    onselectedindexchanging="GridView1_SelectedIndexChanging">
                
                <Columns>
   
                        <asp:TemplateField HeaderText="كود الموظف" SortExpression="Value">
                            <ItemTemplate>
                                <asp:Label ID="lblemp_no0" runat="server" Text='<%# Bind("EMP_NO") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                     
                             <asp:TemplateField HeaderText="اسم الموظف">
                          <ItemTemplate>
                                <asp:Label ID="lblemp_name0" runat="server" Text='<%# Bind("EMP_NAME") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        
                        <asp:TemplateField HeaderText="الوظيفة">
                         <ItemTemplate>
                                <asp:Label ID="lbljob_name0" runat="server" Text='<%# Bind("JOB_NAME") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        
                          <asp:TemplateField HeaderText="نموذج">
                         <ItemTemplate>
                                <asp:Label ID="lblform_id0" runat="server" Text='<%# Bind("FORM_ID") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        
                          <asp:TemplateField HeaderText="التقييم" Visible="false">
                         <ItemTemplate>
                                <asp:Label ID="lblstatus0" runat="server" Text='<%# Bind("STATUS") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        
                           <asp:TemplateField HeaderText="التقييم">
                         <ItemTemplate>
                                <asp:Label ID="lblfstatus0" runat="server" Text='<%# Bind("FSTATUS") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        
                           <asp:TemplateField HeaderText="النتيجة">
                         <ItemTemplate>
                                <asp:Label ID="lblrslt0" runat="server" Text='<%# Bind("rslt") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        
                    </Columns>
                
                <PagerStyle />
                <AlternatingRowStyle />
            </asp:GridView>
                </td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
    </table><br />
    </p>
</asp:Content>

