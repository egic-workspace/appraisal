﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class MessagePopup : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }


    protected void btnShowMessage_Click(object sender, EventArgs e)
    {
        ShowPopupMessage("We have created a message box popup for our site.",
          PopupMessageType.Message);
    }

    protected void btnShowSuccess_Click(object sender, EventArgs e)
    {
        ShowPopupMessage("Custom success box created successfully for our site!",
          PopupMessageType.Success);
    }

    protected void btnShowWarning_Click(object sender, EventArgs e)
    {
        ShowPopupMessage("This is a warning message box!",
          PopupMessageType.Warning);
    }

    protected void btnShowError_Click(object sender, EventArgs e)
    {
        try
        {
            throw new Exception("Some error in code! Kindly contact your " +
              "administrator for more details.<br/> Thanks");
        }
        catch (Exception ex)
        {
            ShowPopupMessage(ex.Message, PopupMessageType.Error);
        }
    }

    /// <summary>
    /// Details: Change modal popup image according to PopupMessageType
    /// </summary>
    /// <param name="message"></param>
    /// <param name="messageType"></param>
    private void ShowPopupMessage(string message, PopupMessageType messageType)
    {
        switch (messageType)
        {
            case PopupMessageType.Error:
                lblMessagePopupHeading.Text = "Error";
                //Render image in literal control
                ltrMessagePopupImage.Text = "<img src='" +
                  Page.ResolveUrl("~/images/imgError.png") + "' alt='' />";
                break;
            case PopupMessageType.Message:
                lblMessagePopupHeading.Text = "Information";
                ltrMessagePopupImage.Text = "<img src='" +
                  Page.ResolveUrl("~/images/imgInformation.png") + "' alt='' />";
                break;
            case PopupMessageType.Warning:
                lblMessagePopupHeading.Text = "Warning";
                ltrMessagePopupImage.Text = "<img src='" +
                  Page.ResolveUrl("~/images/imgWarning.png") + "' alt='' />";
                break;
            case PopupMessageType.Success:
                lblMessagePopupHeading.Text = "Success";
                ltrMessagePopupImage.Text = "<img src='" +
                  Page.ResolveUrl("~/images/imgSuccess.png") + "' alt='' />";
                break;
            default:
                lblMessagePopupHeading.Text = "Information";
                ltrMessagePopupImage.Text = "<img src='" +
                  Page.ResolveUrl("~/images/imgInformation.png") + "' alt='' />";
                break;
        }

        lblMessagePopupText.Text = message;
        mpeMessagePopup.Show();
    }

    /// <summary>
    /// Message type enum
    /// </summary>
    public enum PopupMessageType
    {
        Error,
        Message,
        Warning,
        Success
    }
}
