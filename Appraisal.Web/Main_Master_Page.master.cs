using System;
using System.Web.UI.WebControls;


public partial class Main_Master_Page : System.Web.UI.MasterPage
{
    AppService _appService = new AppService();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Logged_User"] == null)
        {
            Response.Redirect("~/Default.aspx");
            return;
        }

        bool isHRUser = _appService.Check_HR_User(Session["Logged_User"].ToString()) == 1;
        if (!isHRUser)
            Hide_Master_Data();

        string emp_no = Session["EMP_NO"].ToString();
        int APPROVER = _appService.Check_Approver(emp_no);
        if (APPROVER == 0)
        {
            Hide_Master_Data_Approver();
        }
        if (!isHRUser && APPROVER == 0)
        {
            Hide_Menu("0");
        }

        string
            year = "",
            version = "";

        if (Session["OPEN_YEAR"] != null)
            year = Session["OPEN_YEAR"].ToString();
        else
        {
            year = _appService.Get_Ass_Open_Year().ToString();
            Session["OPEN_YEAR"] = year;
        }

        if (Session["OPEN_VERSION"] != null)
            version = Session["OPEN_VERSION"].ToString();
        else
        {
            version = _appService.Get_Ass_Open_Version().ToString();
            Session["OPEN_VERSION"] = version;
        }

        string finalVersionUrl = MidYearVersionHelper.IsMidYearVersion(year, version) ? "" : "FinalVersion/";

        int isDirector = _appService.Check_Ass_Departments(emp_no);
        if (isDirector == 0 || isDirector == 1 || string.IsNullOrWhiteSpace(finalVersionUrl))
            Hide_Menu("6");

        for (int i = 0; i < Menu1.Items.Count; i++)
            if (Menu1.Items[i].Value == "4-1")
                Menu1.Items[i].NavigateUrl = string.Format("~/{0}NewAssesment/Nass_Enter.aspx", finalVersionUrl);
            else if (Menu1.Items[i].Value == "4-2")
                Menu1.Items[i].NavigateUrl = string.Format("~/{0}NewAssesment/Ass_approve.aspx", finalVersionUrl);
            else if (Menu1.Items[i].Value == "5")
            {
                //Menu1.Items[i].NavigateUrl = string.Format("~/{0}NewAssesment/EmpAssessmentAgreement.aspx", finalVersionUrl);
                Menu1.Items.RemoveAt(i);
            }
    }

    private void Hide_Master_Data_Approver()
    {
        try
        {
            MenuItem parent = Menu1.FindItem("0");
            MenuItem s1 = Menu1.FindItem("0/0-4");
            parent.ChildItems.Remove(s1);
        }
        catch
        { }
    }

    private void Hide_Master_Data()
    {
        try
        {
            MenuItem parent = Menu1.FindItem("0");
            MenuItem s1 = Menu1.FindItem("0/0-1");
            parent.ChildItems.Remove(s1);
            MenuItem s2 = Menu1.FindItem("0/0-2");
            parent.ChildItems.Remove(s2);
            MenuItem s3 = Menu1.FindItem("0/0-3");
            parent.ChildItems.Remove(s3);
            MenuItem s5 = Menu1.FindItem("0/0-5");
            parent.ChildItems.Remove(s5);
        }
        catch
        { }
    }

    private void Hide_Menu(string val)
    {
        MenuItemCollection menuItems = Menu1.Items;

        MenuItem Item1 = new MenuItem();
        MenuItem SItem = new MenuItem();

        foreach (MenuItem menuItem in menuItems)
        {
            if (menuItem.Value == val.Trim())
            {
                Item1 = menuItem;
            }
        }

        menuItems.Remove(Item1);
    }

    protected void Page_PreInit(object sender, EventArgs e)
    {
        if (Request.UserAgent.Contains("AppleWebKit"))
            Request.Browser.Adapters.Clear();
    }
}