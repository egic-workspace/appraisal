﻿using System;
using System.Configuration;
using System.Data;
using System.Web.UI.WebControls;


public partial class Final_Version_Group_questions : System.Web.UI.UserControl
{
    //*************************************************************
    AppService ser = new AppService();

    // public string Emp_Code = "";
    public string jobCode = "";
    public string _year = "";
    public string _version = "";
    public string displayMode = "";
    public string haveData = "";
    //public string Comp_Result = "";
    //*************************************************************
    public string compID = "";
    public string message = "";
    public bool messageStatus = false;
    public string compName = "";
    public DataTable comptencesDT = new DataTable();
    public DataTable compDescsDT = new DataTable();
    public DataTable compIndicatoresDT = new DataTable();
    public DataTable compAnswersDT = new DataTable();
    //*************************************************************

    public GridView Romko_GV;

    protected void Page_Init(object sender, EventArgs e)
    {
        lbl_com_ID.Text = compID;
        bool disableApp;
        string disableAppStr = ConfigurationManager.AppSettings["DisableApp"];
        if (!bool.TryParse(disableAppStr, out disableApp))
            disableApp = false;

        if (displayMode.Trim() == "Y" || disableApp)
        {
            GV_Indicatores.Enabled = false;
        }

        if (haveData.Trim() == "Y")
        {

        }
        Get_All_Data();
    }

    private DataTable GetDataTable(GridView dtg)
    {
        DataTable dt = new DataTable();

        // add the columns to the datatable            
        if (dtg.HeaderRow != null)
        {

            for (int i = 0; i < dtg.HeaderRow.Cells.Count; i++)
            {
                dt.Columns.Add(dtg.HeaderRow.Cells[i].Text);
            }
        }

        //  add each of the data rows to the table
        foreach (GridViewRow row in dtg.Rows)
        {
            DataRow dr;
            dr = dt.NewRow();

            for (int i = 0; i < row.Cells.Count; i++)
            {
                dr[i] = row.Cells[i].Text.Replace(" ", "");
            }
            dt.Rows.Add(dr);
        }
        return dt;
    }

    private void Get_All_Data()
    {
        try
        {
            Clear_Grids();
            GV_Indicatores.DataSource = compIndicatoresDT;
            GV_Indicatores.DataBind();
            Romko_GV = GV_Indicatores;
            compAnswersDT = GetDataTable(GV_Indicatores);
            AddRowSpanToGridView();
        }
        catch (Exception ex)
        {
            string error = ex.Message;
        }
    }

    public void AddRowSpanToGridView()
    {
        for (int rowIndex = compIndicatoresDT.Rows.Count - 2; rowIndex >= 0; rowIndex--)
        {
            GridViewRow currentRow = GV_Indicatores.Rows[rowIndex];
            GridViewRow previousRow = GV_Indicatores.Rows[rowIndex + 1];
            string currentRowParentCode = compIndicatoresDT.Rows[rowIndex]["PARENT_CODE"].ToString();
            string previousRowParentCode = compIndicatoresDT.Rows[rowIndex + 1]["PARENT_CODE"].ToString();

            if (currentRowParentCode == previousRowParentCode)
            {
                if (previousRow.Cells[0].RowSpan < 2)
                {
                    currentRow.Cells[0].RowSpan = 2;
                }
                else
                    currentRow.Cells[0].RowSpan = previousRow.Cells[0].RowSpan + 1;
                previousRow.Cells[0].Visible = false;
            }
        }
    }

    private void Clear_Grids()
    {
        GV_Indicatores.DataSource = null;
        GV_Indicatores.DataBind();
    }

    public static Table DataTableToHTMLTable(DataTable dt, bool includeHeaders)
    {
        Table tbl = new Table();
        TableRow tr = null;
        TableCell cell = null;

        int rows = dt.Rows.Count;
        int cols = dt.Columns.Count;

        if (includeHeaders)
        {
            TableHeaderRow htr = new TableHeaderRow();
            TableHeaderCell hcell = null;
            for (int i = 0; i < cols; i++)
            {
                hcell = new TableHeaderCell();
                hcell.Text = dt.Columns[i].ColumnName.ToString();
                htr.Cells.Add(hcell);
            }
            tbl.Rows.Add(htr);
        }

        for (int j = 0; j < rows; j++)
        {
            tr = new TableRow();
            for (int k = 0; k < cols; k++)
            {
                cell = new TableCell();
                if (k == 1)
                {
                    cell.Text = dt.Rows[j][k].ToString() + "*";
                    cell.HorizontalAlign = HorizontalAlign.Left;
                }
                else
                {
                    cell.Text = " *  " + dt.Rows[j][k].ToString() + " <td>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp;</td> ";
                    cell.HorizontalAlign = HorizontalAlign.Right;
                }
                tr.Cells.Add(cell);
            }
            tbl.Rows.Add(tr);
        }
        return tbl;

    }

    public static string ConvertDataTableToHTML(DataTable dt)
    {
        string html = "<table>";
        //add header row
        html += "<tr>";
        for (int i = 0; i < dt.Columns.Count; i++)
            html += "<td>" + dt.Columns[i].ColumnName + "</td>";
        html += "</tr>";
        //add rows
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            html += "<tr>";
            for (int j = 0; j < dt.Columns.Count; j++)
                html += "<td>" + dt.Rows[i][j].ToString() + "</td>";
            html += "</tr>";
        }
        html += "</table>";
        return html;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        //lbl_avg.Text = Comp_Result;
    }

    protected void GridView1_SelectedIndexChanged(object sender, EventArgs e)
    {

    }

    protected void OnRowDataBound(object sender, GridViewRowEventArgs e)
    {
    }

    protected void GV_Indicatores_DataBound(object sender, EventArgs e)
    {

    }
}