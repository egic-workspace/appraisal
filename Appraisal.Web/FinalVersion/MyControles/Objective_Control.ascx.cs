﻿using System;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Web.UI;
using System.Web.UI.WebControls;


public partial class Final_Version_Objective_Control : System.Web.UI.UserControl
{
    AppService _appService = new AppService();
    public DataTable dtCurrentObjectivesTable = new DataTable();
    public GridView gvObjectives;
    public string displayMode = "";
    public string staticMode = "";
    public string nyMode = "";
    public string empNO = "";
    public string _ver = "";
    public string _year = "";
    public string _sequance = "";

    protected void Page_Init(object sender, EventArgs e)
    {

    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            bool disableApp;
            string disableAppStr = ConfigurationManager.AppSettings["DisableApp"];
            if (!bool.TryParse(disableAppStr, out disableApp))
                disableApp = false;
            if (staticMode.Trim() == "Y" || disableApp)
            {
                Gridview1.Enabled = false;
            }
            if (displayMode.Trim() == "Y")
            {
                Bind_GridView();
            }
            else
            {
                SetInitialRow();
            }
        }
        gvObjectives = Gridview1;
    }

    protected void ButtonAdd_Click(object sender, EventArgs e)
    {
        if (staticMode.Trim() == "Y")
        {
            Alert.Show("لا يمكنك اضافة اهداف أخري");
        }
        else
        {
            //Save_Objectives();
            AddNewRowToGrid();

            Page.MaintainScrollPositionOnPostBack = true;
            Page.SetFocus(Gridview1.ClientID);
        }
    }

    private void Save_Objectives()
    {
        DataTable OBJDT = new DataTable();
        OBJDT.Columns.Add("SEQ");
        OBJDT.Columns.Add("OBJ");
        OBJDT.Columns.Add("WGT");
        OBJDT.Columns.Add("DGR");
        OBJDT.Columns.Add("NOT");
        OBJDT.Columns.Add("DEV");
        OBJDT.Columns.Add("TRN");
        OBJDT.Rows.Clear();
        OBJDT.Rows.Clear();
        try
        {
            dtCurrentObjectivesTable = (DataTable)ViewState["CurrentObjectivesTable"];
            int count = dtCurrentObjectivesTable.Rows.Count;
            int i = 0;
            foreach (GridViewRow row in Gridview1.Rows)
            {
                i++;
                string seqc = i.ToString();
                string objc = ((TextBox)row.FindControl("txtObjective")).Text;
                string weight = ((TextBox)row.FindControl("txtWeight")).Text;
                //string degree = ((TextBox)row.FindControl("txtFinalEvaluation")).Text;
                string degree = ((DropDownList)row.FindControl("txtFinalEvaluation")).Text;
                string note = ((TextBox)row.FindControl("txtResult")).Text;
                string develop_points = ((TextBox)row.FindControl("txtDevelopmentPoints")).Text;
                string train_prog = ((TextBox)row.FindControl("txtTrainingCourses")).Text;

                OBJDT.NewRow();
                OBJDT.Rows.Add(seqc, objc, weight, degree, note, develop_points, train_prog);
            }
            DataRow drCurrentRow = null;
            drCurrentRow = dtCurrentObjectivesTable.NewRow();
            drCurrentRow["RowNumber"] = i + 1;
            drCurrentRow["obj_actual"] = "0";
            drCurrentRow["obj_weight"] = "0";
            dtCurrentObjectivesTable.Rows.Add(drCurrentRow);
            ViewState["CurrentObjectivesTable"] = dtCurrentObjectivesTable;
            dtCurrentObjectivesTable.AcceptChanges();
            Gridview1.DataSource = dtCurrentObjectivesTable;
            Gridview1.DataBind();

            if (OBJDT != null)
            {
                //Obj_Status = ser.Save_ASS_NY_OBJ_ByType_DTTL(Session["ASS_EMP_ID"].ToString(), Session["OPEN_YEAR"].ToString(), Session["OPEN_VERSION"].ToString(), Control_Type , OBJDT);
            }
            else
            {
                //Obj_Status = 0; 
            }
        }
        catch
        {
            if (OBJDT != null)
            {
                //Obj_Status = ser.Save_ASS_NY_OBJ_ByType_DTTL(Session["ASS_EMP_ID"].ToString(), Session["OPEN_YEAR"].ToString(), Session["OPEN_VERSION"].ToString(), Control_Type, OBJDT);
            }
        }

    }

    private void AddNewRowToGrid()
    {
        int rowIndex = 0;

        if (ViewState["CurrentObjectivesTable"] != null)
        {
            dtCurrentObjectivesTable = (DataTable)ViewState["CurrentObjectivesTable"];
            DataRow drCurrentRow = null;
            if (dtCurrentObjectivesTable.Rows.Count > 0)
            {
                try
                {
                    for (int i = 1; i <= dtCurrentObjectivesTable.Rows.Count; i++)
                    {
                        TextBox txtObjective = (TextBox)Gridview1.Rows[rowIndex].Cells[1].FindControl("txtObjective");
                        TextBox txtWeight = (TextBox)Gridview1.Rows[rowIndex].Cells[2].FindControl("txtWeight");
                        DropDownList txtFinalEvaluation = (DropDownList)Gridview1.Rows[rowIndex].Cells[3].FindControl("txtFinalEvaluation");
                        TextBox txtResult = (TextBox)Gridview1.Rows[rowIndex].Cells[4].FindControl("txtResult");
                        TextBox txtDevelopmentPoints = (TextBox)Gridview1.Rows[rowIndex].Cells[5].FindControl("txtDevelopmentPoints");
                        TextBox txtTrainingCourses = (TextBox)Gridview1.Rows[rowIndex].Cells[6].FindControl("txtTrainingCourses");

                        drCurrentRow = dtCurrentObjectivesTable.NewRow();
                        drCurrentRow["RowNumber"] = i + 1;
                        drCurrentRow["obj_actual"] = "0";
                        drCurrentRow["obj_weight"] = "0";

                        dtCurrentObjectivesTable.Rows[i - 1]["obj_desc"] = txtObjective.Text;
                        dtCurrentObjectivesTable.Rows[i - 1]["obj_weight"] = txtWeight.Text;
                        dtCurrentObjectivesTable.Rows[i - 1]["obj_actual"] = txtFinalEvaluation.SelectedValue;
                        dtCurrentObjectivesTable.Rows[i - 1]["obj_notes"] = txtResult.Text;
                        dtCurrentObjectivesTable.Rows[i - 1]["obj_develop_points"] = txtDevelopmentPoints.Text;
                        dtCurrentObjectivesTable.Rows[i - 1]["obj_train_prog"] = txtTrainingCourses.Text;
                        txtObjective.Focus();
                        rowIndex++;
                    }
                    dtCurrentObjectivesTable.Rows.Add(drCurrentRow);
                    ViewState["CurrentObjectivesTable"] = dtCurrentObjectivesTable;
                    dtCurrentObjectivesTable.AcceptChanges();
                    Gridview1.DataSource = dtCurrentObjectivesTable;
                    Gridview1.DataBind();
                }
                catch(Exception ex)
                {
                    Alert.Show(ex.Message);
                    Alert.Show("برجاء ادخال بيانات الهدف");
                    return;
                }
            }
        }
        else
        {
            Response.Write("ViewState is null");
        }

        //Set Previous Data on Postbacks
        SetPreviousData();
    }

    private void SetInitialRow()
    {
        DataTable dt = new DataTable();
        DataRow dr = null;
        dt.Columns.Add(new DataColumn("RowNumber", typeof(string)));
        dt.Columns.Add(new DataColumn("obj_desc", typeof(string)));
        dt.Columns.Add(new DataColumn("obj_weight", typeof(string)));
        dt.Columns.Add(new DataColumn("obj_actual", typeof(string)));
        dt.Columns.Add(new DataColumn("obj_notes", typeof(string)));
        dt.Columns.Add(new DataColumn("obj_develop_points", typeof(string)));
        dt.Columns.Add(new DataColumn("obj_train_prog", typeof(string)));
        dr = dt.NewRow();

        dr["RowNumber"] = 1;
        dr["obj_desc"] = string.Empty;
        dr["obj_weight"] = "0";
        dr["obj_actual"] = "0";
        dr["obj_notes"] = string.Empty;
        dr["obj_develop_points"] = string.Empty;
        dr["obj_train_prog"] = string.Empty;
        dt.Rows.Add(dr);
        ViewState["CurrentObjectivesTable"] = dt;

        Gridview1.DataSource = dt;
        Gridview1.DataBind();
    }

    private void SetPreviousData()
    {
        int rowIndex = 0;
        if (ViewState["CurrentObjectivesTable"] != null)
        {
            DataTable dt = (DataTable)ViewState["CurrentObjectivesTable"];
            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    TextBox txtObjective = (TextBox)Gridview1.Rows[rowIndex].Cells[1].FindControl("txtObjective");
                    TextBox txtWeight = (TextBox)Gridview1.Rows[rowIndex].Cells[2].FindControl("txtWeight");
                    //TextBox txtFinalEvaluation = (TextBox)Gridview1.Rows[rowIndex].Cells[3].FindControl("txtFinalEvaluation");
                    DropDownList txtFinalEvaluation = (DropDownList)Gridview1.Rows[rowIndex].Cells[3].FindControl("txtFinalEvaluation");
                    TextBox txtResult = (TextBox)Gridview1.Rows[rowIndex].Cells[4].FindControl("txtResult");
                    TextBox txtDevelopmentPoints = (TextBox)Gridview1.Rows[rowIndex].Cells[5].FindControl("txtDevelopmentPoints");
                    TextBox txtTrainingCourses = (TextBox)Gridview1.Rows[rowIndex].Cells[6].FindControl("txtTrainingCourses");

                    txtObjective.Text = dt.Rows[i]["obj_desc"].ToString();
                    txtWeight.Text = dt.Rows[i]["obj_weight"].ToString();
                    //txtFinalEvaluation.Text = dt.Rows[i]["obj_actual"].ToString();

                    var item = txtFinalEvaluation.Items.FindByValue(dt.Rows[i]["obj_actual"].ToString());
                    if (item != null)
                    {
                        txtFinalEvaluation.ClearSelection();
                        item.Selected = true;
                    }

                    txtResult.Text = dt.Rows[i]["obj_notes"].ToString();
                    txtDevelopmentPoints.Text = dt.Rows[i]["obj_develop_points"].ToString();
                    txtTrainingCourses.Text = dt.Rows[i]["obj_train_prog"].ToString();
                    rowIndex++;
                }
            }
        }
    }

    private void Bind_GridView()
    {
        ViewState["CurrentObjectivesTable"] = dtCurrentObjectivesTable;
        Gridview1.DataSource = dtCurrentObjectivesTable;
        Gridview1.DataBind();
        gvObjectives = Gridview1;

        double totalActual = 0;
        foreach (DataRow row in dtCurrentObjectivesTable.Rows)
            if (double.TryParse(row["obj_actual"].ToString(), out double _actual) && _actual > 0 && double.TryParse(row["obj_weight"].ToString(), out double _weight) && _weight > 0)
                totalActual += _actual * (_weight / 100);

        //double total = dtCurrentTable.AsEnumerable().Sum(row => row.Field<double>("obj_actual") * (row.Field<double>("obj_weight") / 100));
        //decimal total = dtCurrentTable.AsEnumerable().Sum(row => row.Field<decimal>("obj_actual"));
        // GridView1.FooterRow.Cells[1].Text = "Total";
        // GridView1.FooterRow.Cells[1].HorizontalAlign = HorizontalAlign.Right;
        Gridview1.FooterRow.Cells[4].Text = Math.Round(totalActual, 1).ToString();

        Gridview1.FooterRow.Cells[4].ForeColor = Color.White;
        Gridview1.FooterRow.Cells[4].BackColor = System.Drawing.ColorTranslator.FromHtml("#71625F");
        Gridview1.FooterRow.Cells[4].Font.Bold = true;
        Gridview1.FooterRow.Cells[4].Font.Size = 16;

        Gridview1.FooterRow.Cells[3].Text = "الإجمالي";
        Gridview1.FooterRow.Cells[3].ForeColor = Color.White;
        Gridview1.FooterRow.Cells[3].BackColor = System.Drawing.ColorTranslator.FromHtml("#71625F");
        Gridview1.FooterRow.Cells[3].Font.Bold = true;
        Gridview1.FooterRow.Cells[3].Font.Size = 16;
    }

    protected void txtObjective_Load(object sender, EventArgs e)
    {
    }

    protected void txtResult_Load(object sender, EventArgs e)
    {

    }

    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
    }

    protected void ButtonDel_Click(object sender, EventArgs e)
    {
    }

    protected void OnRowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        try
        {
            if (Gridview1.Rows.Count == 1)
            {
                Alert.Show(" لا يمكن حذف اخر صف بالجدول ");
                return;
            }

            int index = Convert.ToInt32(e.RowIndex);
            DataTable OBJDT = new DataTable();
            OBJDT.Columns.Add("RowNumber");
            OBJDT.Columns.Add("obj_desc");
            OBJDT.Columns.Add("obj_weight");
            OBJDT.Columns.Add("obj_actual");
            OBJDT.Columns.Add("obj_notes");
            OBJDT.Columns.Add("obj_develop_points");
            OBJDT.Columns.Add("obj_train_prog");
            OBJDT.Rows.Clear();
            int i = 0;
            foreach (GridViewRow row in Gridview1.Rows)
            {
                if (i != index)
                {
                    string objc = ((TextBox)row.FindControl("txtObjective")).Text;
                    string weight = ((TextBox)row.FindControl("txtWeight")).Text;
                    //string degree = ((TextBox)row.FindControl("txtFinalEvaluation")).Text;
                    string degree = ((DropDownList)row.FindControl("txtFinalEvaluation")).Text;
                    string note = ((TextBox)row.FindControl("txtResult")).Text;
                    string developmentPoints = ((TextBox)row.FindControl("txtDevelopmentPoints")).Text;
                    string trainingCourses = ((TextBox)row.FindControl("txtTrainingCourses")).Text;
                    OBJDT.NewRow();
                    OBJDT.Rows.Add(i + 1, objc, weight, degree, note, developmentPoints, trainingCourses);
                }
                i++;
            }
            Gridview1.DataSource = OBJDT;
            Gridview1.DataBind();

            ViewState["CurrentObjectivesTable"] = OBJDT;
        }
        catch
        {

        }
    }

}