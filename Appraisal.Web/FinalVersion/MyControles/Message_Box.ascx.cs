﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class Final_Version_Message_Box : System.Web.UI.UserControl
{
    public delegate void delegate_OkClick();
    public event delegate_OkClick OnOkClick;

    public string Header
    {
        set { lblHeader.Text = value; }
        //set { lblHeader.InnerHtml = value; }
    }

    public string Message
    {
        set { lblMessage.Text = value; }
    }

    public Button CancelButton
    {
        get { return btnCancel; }
    }

    public Button OkButton
    {
        get { return btnOk; }
    }
    public AjaxControlToolkit.ModalPopupExtender MessageBox
    {
        get { return mpeMessageBox; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void btnOk_OnClick(object sender, EventArgs e)
    {
        //raise the event if not null
        if (OnOkClick != null)
            OnOkClick();
    }
    public void displayMessage(string message)
    {
        displayMessage(message, null);
    }

    public void displayMessage(string message, int? type)
    {
        lblHeader.Text = "Alert Message";
        //lblHeader.InnerHtml = title;
        lblMessage.Text = message;
        btnCancel.Attributes["style"] = "display:block";
        btnCancel.Text = "Ok";
        mpeMessageBox.Show();
    }
}
