﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="~/FinalVersion/MyControles/Objective_Control.ascx.cs" Inherits="Final_Version_Objective_Control" %>
<link rel="stylesheet" type="text/css" href="../../CSS/StyleSheet.css" />
<link rel="stylesheet" type="text/css" href="../../CSS/StyleSheet2.css" />
<link rel="Stylesheet" type="text/css" href="../../CSS/bootstrap.min.css"/>
<script type="text/javascript">
function CheckNumeric(e) {
    if (window.event && ((e.keyCode < 48 || e.keyCode > 57) & e.keyCode != 8) && e.keyCode != 110 ) {
        event.returnValue = false;
        return false;
    }
    else if (((e.which < 48 || e.which > 57) & e.which != 8) && e.which != 110) {
        e.preventDefault();
        return false;
    }
}

  function MaskMoney(obj, evt) 
    {
	//alert(evt.keyCode);
    if (!(evt.keyCode == 190 || (evt.keyCode >= 48 && evt.keyCode <= 57))) return false;
    var parts = obj.value.split('.');
    if (parts.length > 2) return false;
    if (evt.keyCode == 46) return (parts.length == 1);
    if (parts[0].length >= 14) return false;
    if (parts.length == 2 && parts[1].length >= 2) return false;
    }
    function addNewObjectiveRow(btn) {
        var firstRow = $('[id$="OBJ_CONTROL_DATA_Gridview1"] tr').eq(1);
        var newRow = firstRow.clone();

        var newRowNum = $('[id$="OBJ_CONTROL_DATA_Gridview1"] tr').length;

        //new Txt Objective
        newRow.find('[id$="txtObjective"]').val('');
        var newTxtObjectiveId = newRow.find('[id$="txtObjective"]').attr('id').replace('ctl02', `ctl0${newRowNum}`);
        var newTxtObjectiveName = newRow.find('[id$="txtObjective"]').attr('name').replace('ctl02', `ctl0${newRowNum}`);
        newRow.find('[id$="txtObjective"]').attr({ 'id': newTxtObjectiveId, 'name': newTxtObjectiveName });

        //new txt Weight
        newRow.find('[id$="txtWeight"]').val('0');
        var newTxtWeightId = newRow.find('[id$="txtWeight"]').attr('id').replace('ctl02', `ctl0${newRowNum}`);
        var newTxtWeightName = newRow.find('[id$="txtWeight"]').attr('name').replace('ctl02', `ctl0${newRowNum}`);
        newRow.find('[id$="txtWeight"]').attr({ 'id': newTxtWeightId, 'name': newTxtWeightName });

        //new txt Final Evaluation
        newRow.find('[id$="txtFinalEvaluation"] option:selected').prop('selected', false);
        newRow.find('[id$="txtFinalEvaluation"] option:selected').removeAttr('selected');
        newRow.find('[id$="txtFinalEvaluation"] option').eq(0).attr('selected', 'selected');
        var newTxtFinalEvaluationId = newRow.find('[id$="txtFinalEvaluation"]').attr('id').replace('ctl02', `ctl0${newRowNum}`);
        var newTxtFinalEvaluationName = newRow.find('[id$="txtFinalEvaluation"]').attr('name').replace('ctl02', `ctl0${newRowNum}`);
        newRow.find('[id$="txtFinalEvaluation"]').attr({ 'id': newTxtFinalEvaluationId, 'name': newTxtFinalEvaluationName });

        //new txt Development Points
        newRow.find('[id$="txtDevelopmentPoints"]').val('');
        var newTxtDevelopmentPointsId = newRow.find('[id$="txtDevelopmentPoints"]').attr('id').replace('ctl02', `ctl0${newRowNum}`);
        var newTxtDevelopmentPointsName = newRow.find('[id$="txtDevelopmentPoints"]').attr('name').replace('ctl02', `ctl0${newRowNum}`);
        newRow.find('[id$="txtDevelopmentPoints"]').attr({ 'id': newTxtDevelopmentPointsId, 'name': newTxtDevelopmentPointsName });

        //new Button delete
        var newButtondeleteId = newRow.find('[id$="Buttondelete"]').attr('id').replace('ctl02', `ctl0${newRowNum}`);
        var newButtondeleteName = newRow.find('[id$="Buttondelete"]').attr('name').replace('ctl02', `ctl0${newRowNum}`);
        newRow.find('[id$="Buttondelete"]').attr({ 'id': newButtondeleteId, 'name': newButtondeleteName });

        $('[id$="OBJ_CONTROL_DATA_Gridview1"]').find('tr:last').prev().after(newRow);

        $(btn).prop('disabled', true);
    }
</script>
<asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
<asp:GridView ID="Gridview1" runat="server" ShowFooter="true"  Width="98%" CssClass="mGrid" AlternatingRowStyle-CssClass="alt" OnRowDeleting="OnRowDeleting" PagerStyle-CssClass="pgr"   AutoGenerateColumns="false"  Font-Names="Arial" onrowdatabound="GridView1_RowDataBound" >
    <Columns>
        <asp:BoundField DataField="RowNumber" HeaderText="م" Visible="false" />

        <asp:TemplateField HeaderStyle-Width="250px"  HeaderText="الهدف">
            <ItemTemplate>
                <div class="flex-container">
                <asp:TextBox ID="txtObjective" CssClass="form-control fill-width" Width="100%" Font-Size="13pt" TextMode="MultiLine" Text='<%# Bind("obj_desc") %>' runat="server"></asp:TextBox>
                    </div>
            </ItemTemplate>
        </asp:TemplateField>

        <asp:TemplateField HeaderStyle-Width="50px" HeaderText="الوزن" ItemStyle-CssClass="v-align-top">
            <ItemTemplate>  
                <asp:TextBox ID="txtWeight"  CssClass="form-control" Width="100%" Font-Size="13pt" MaxLength="3" Text='<%# Bind("obj_weight") %>' runat="server"></asp:TextBox>
                <asp:RegularExpressionValidator ID="Regex1" runat="server" ValidationExpression="((\d+)((\.\d{1,3})?))$" ErrorMessage="برجاء إدخال رقم صحيح" ControlToValidate="txtWeight" />
            </ItemTemplate>
        </asp:TemplateField>

        <asp:TemplateField HeaderStyle-Width="250px"  HeaderText="السبب" Visible="false">
            <ItemTemplate>
                <asp:TextBox ID="txtResult" TextMode="MultiLine" onload="txtResult_Load" Width="100%" CssClass="form-control" Font-Size="13pt" Text='<%# Bind("obj_notes") %>' runat="server"></asp:TextBox>
            </ItemTemplate>
        </asp:TemplateField>

        <asp:TemplateField HeaderStyle-Width="90px" HeaderText="التقييم النهائي" ItemStyle-CssClass="v-align-top">
            <ItemTemplate>

                <asp:DropDownList runat="server" Width="100%" ID="txtFinalEvaluation" Font-Size="13pt" Text='<%# Bind("obj_actual") %>'>
                    <asp:ListItem Value="0" >Select...</asp:ListItem>
                    <asp:ListItem Value="1">1</asp:ListItem>
                    <asp:ListItem Value="2">2</asp:ListItem>
                    <asp:ListItem Value="3">3</asp:ListItem>
                    <asp:ListItem Value="4">4</asp:ListItem>
                    <asp:ListItem Value="5">5</asp:ListItem>
                </asp:DropDownList>

            </ItemTemplate>
        </asp:TemplateField>

        <asp:TemplateField HeaderStyle-Width="250px"  HeaderText="أسباب التقييم والنقاط التطويريه التى يحتاجها الموظف لأداء الأهداف بشكل أفضل">
            <ItemTemplate>
                <div class="flex-container">
                    <asp:TextBox ID="txtDevelopmentPoints" TextMode="MultiLine" Width="100%" CssClass="form-control fill-width" Font-Size="13pt" runat="server"  Text='<%# Bind("obj_develop_points") %>'  ></asp:TextBox>
                </div>
            </ItemTemplate>
            <FooterStyle HorizontalAlign="Right" />
            <FooterTemplate>
                <asp:Button ID="ButtonAdd" runat="server" CssClass="btn btn-primary"  BackColor="#F79222" Font-Bold="True" Font-Names="Arial" Font-Size="12pt" Width="80%" onclick="ButtonAdd_Click" Text="إضافة هدف جديد" CausesValidation="False"  />
                <button id="btnAddNewObjectiveRow" onclick="addNewObjectiveRow(this)" class="btn btn-info hidden" type="button">إضافة هدف جديد</button>
            </FooterTemplate>
        </asp:TemplateField>

        <asp:TemplateField HeaderStyle-Width="250px"  HeaderText="التوصيه ببرنامج تدريبى معين إن وجد" Visible="false">
            <ItemTemplate>
                <asp:TextBox ID="txtTrainingCourses" TextMode="MultiLine" Width="100%" CssClass="form-control" Font-Size="13pt" runat="server"  Text='<%# Bind("obj_train_prog") %>'  ></asp:TextBox>
            </ItemTemplate>
        </asp:TemplateField>

        <asp:TemplateField HeaderStyle-Width="70px" HeaderText="">
            <ItemTemplate>
                <asp:Button ID="Buttondelete" runat="server" CssClass="btn btn-primary"  BackColor="#F79222" Font-Bold="True" Font-Names="Arial" Font-Size="12pt" Width="70px" CommandName="Delete" OnRowDataBound="OnRowDataBound"  Text="حـذف" CausesValidation="False" />
            </ItemTemplate>
        </asp:TemplateField>

    </Columns>
</asp:GridView>
 <p>
  <asp:Image ID="Image1" runat="server" ImageUrl="~/Images/line3.png" Width="100%" Height="10px" />
</p>