﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="~/FinalVersion/MyControles/Group_questions.ascx.cs" Inherits="Final_Version_Group_questions" %>
<style type="text/css">
    .style_gp {
        width: 100%;
        float: right;
    }
    .text-center{
        text-align:center;
    }
    .Grid {
        background-color: #fff;
        margin: 5px 0 10px 0;
        border: solid 1px #525252;
        border-collapse: collapse;
        font-family: Calibri;
        color: #474747;
    }

        .Grid td {
            padding: 2px;
            border: solid 1px #c1c1c1;
        }

        .Grid th {
            padding: 4px 2px;
            color: #fff;
            background: #71625F;
            border-left: solid 1px #525252;
            font-size: 0.9em;
            text-align: center;
        }

        .Grid .alt {
            background: #fcfcfc;
        }

        .Grid .pgr {
            background: #363670;
        }

            .Grid .pgr table {
                margin: 3px 0;
            }

            .Grid .pgr td {
                border-width: 0;
                padding: 0 6px;
                border-left: solid 1px #666;
                font-weight: bold;
                color: #fff;
                line-height: 12px;
            }

            .Grid .pgr a {
                color: Gray;
                text-decoration: none;
            }

                .Grid .pgr a:hover {
                    color: #000;
                    text-decoration: none;
                }

    * {
        padding: 0;
        margin: 0;
    }

    td[rowspan="3"] {
        vertical-align: bottom;
    }
    .auto-style1 {
        color: #000000;
        background-color: #FFFFFF;
    }
</style>
<table align="center" class="style_gp" frame="border" style="border: thin solid #808080; background-color: #FFFFFF;">

    <tr>
        <td align="center" dir="rtl" class="style_hg" colspan="2">

            <asp:Panel ID="Panel2" runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="11pt">
                <asp:GridView ID="GV_Indicatores" runat="server" AutoGenerateColumns="False" CssClass="Grid" AlternatingRowStyle-CssClass="alt" PagerStyle-CssClass="pgr" OnRowDataBound="OnRowDataBound" Width="100%" CellPadding="2" OnDataBound="GV_Indicatores_DataBound">
                    <Columns>
                        <asp:TemplateField HeaderText="الاساسية" HeaderStyle-Width="250px">
                            <ItemTemplate>
                                <p style="width: 100%; text-align: center; white-space: pre-line; font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; font-size: medium; background-color: transparent;">
                                    <%# Eval("DESC_AR") %>
                                </p>
                                <p style="width: 100%; text-align: center; white-space: pre-line; font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; font-size: medium; background-color: transparent;margin-bottom: 110px;">
                                    <%# Eval("DESC_EN") %>
                                </p>
                                <asp:Label ID="lblCompResult" runat="server" Text='<%# Bind("LBL_COMP_RESULT") %>' Style="min-width: 40px; text-align: center; float: left; background: #f79222; padding: 5px; border-radius: 0 10px 0 0; color: white;" />
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="الوصف" ControlStyle-Width="300px">
                            <ItemTemplate>
                                <asp:Label ID="lblArDesc" runat="server" Text='<%# Bind("pos_desc_ar") %>' />
                                <br />
                                <asp:Label ID="lblEnDesc" runat="server" Style="float: left; text-align: left;" Text='<%# Bind("pos_desc_en") %>' />
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="تقييم المدير &nbsp; &nbsp;  Manager Evaluation" ItemStyle-CssClass="text-center">
                            <ItemTemplate>

                                <asp:Label ID="lblParentCode" runat="server" Text='<%# Bind("parent_code") %>' Visible="false" />
                                <asp:Label ID="lblCompcode" runat="server" Text='<%# Bind("CODE") %>' Visible="false" />
                                <asp:DropDownList ID="ddlanswers" Width="150px" runat="server" Style="text-align: center;" Font-Bold="True" Text='<%# Bind("COMP_RESULT") %>' Font-Size="11pt">
                                    <asp:ListItem Value="0">اختر</asp:ListItem>
                                    <asp:ListItem Value="1">1</asp:ListItem>
                                    <asp:ListItem Value="2">2</asp:ListItem>
                                    <asp:ListItem Value="3">3</asp:ListItem>
                                    <asp:ListItem Value="4">4</asp:ListItem>
                                    <asp:ListItem Value="5">5</asp:ListItem>
                                </asp:DropDownList>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderStyle-Width="250px" HeaderText=" أسباب التقييم والنقاط التطويريه التى يحتاجها الموظف لأداء الكفاءات بشكل أفضل">
                            <ItemTemplate>
                                <div class="flex-container">
                                <asp:TextBox ID="txtDevelopmentPoints" TextMode="MultiLine" Width="100%" CssClass="form-control fill-width" Font-Size="13pt" runat="server" Text='<%# Bind("DEVELOP_POINTS") %>'></asp:TextBox>
                                    </div>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderStyle-Width="250px" HeaderText="التوصيه ببرنامج تدريبى معين إن وجد" Visible="false">
                            <ItemTemplate>
                                <div class="flex-container">
                                <asp:TextBox ID="txtTrainingCourses" TextMode="MultiLine" Width="100%" CssClass="form-control fill-width" Font-Size="13pt" runat="server" Text='<%# Bind("TRAIN_PROG") %>'></asp:TextBox>
                                    </div>
                            </ItemTemplate>
                        </asp:TemplateField>

                    </Columns>
                    <PagerStyle CssClass="pgr"></PagerStyle>
                    <AlternatingRowStyle CssClass="alt" />
                    <EditRowStyle CssClass="td-v-align-botm" />
                </asp:GridView>
            </asp:Panel>
        </td>
    </tr>
    <tr>
        <td style="text-align:center;">
            <asp:Label ID="lblTotalCompResultText" runat="server"  Font-Bold="True" Font-Size="Larger" Style="font-size:Larger;font-weight:bold;padding: 5px;border-radius: 0 5px 5px 0;display: inline-block;background-color: #e8e8e8!important;margin-left: -5px;" Text="مجموع نتائج الكفاءات: " CssClass="auto-style1"></asp:Label>
            <asp:Label ID="lblTotalCompResult" runat="server" Font-Bold="True" Font-Size="Larger" Style="                    font-size: Larger;
                    font-weight: bold;
                    min-width: 40px;
                    text-align: center;
                    background: #f79222;
                    padding: 5px;
                    border-radius: 5px 0 0 5px;
                    color: white;
                    display: inline-block;"></asp:Label>
        </td>
    </tr>
    <tr>
        <td>&nbsp;
        </td>
        <td>
            <asp:Label ID="lbl_com_ID" runat="server" Text="00" Visible="False"></asp:Label>
        </td>
    </tr>
</table>
<p>
    <asp:Image ID="Image1" runat="server" ImageUrl="~/Images/line3.png" Width="100%" Height="10px" />
</p>
