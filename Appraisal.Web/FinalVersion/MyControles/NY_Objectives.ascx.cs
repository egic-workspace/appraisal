﻿using System;
using System.Configuration;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Final_Version_NY_Objectives : System.Web.UI.UserControl
{
    public string Display_Mode = "";
    public string Static_Mode = "";
    public string NY_Mode = "";
    public string Grid_Title = "";
    public string Control_Type = "";
    public DataTable dtCurrentNYObjectivesTable = new DataTable();
    public GridView GV_Objectes ;
    AppService _appService = new AppService();

    protected void Page_Init(object sender, EventArgs e)
    {
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {

            bool disableApp;
            string disableAppStr = ConfigurationManager.AppSettings["DisableApp"];
            if (!bool.TryParse(disableAppStr, out disableApp))
                disableApp = false;

            Gridview1.Caption = Grid_Title;
            if (Static_Mode.Trim() == "Y" || disableApp)
            {
                Gridview1.Enabled = false;
            }
            if (Display_Mode.Trim() == "Y")
            {
                ViewState["CurrentNYObjectivesTable"] = dtCurrentNYObjectivesTable;
                Gridview1.DataSource = dtCurrentNYObjectivesTable;
                Gridview1.DataBind();
                GV_Objectes = Gridview1;
            }
            else
            {
                SetInitialRow();
            }
        }

        GV_Objectes = Gridview1;
    }

    protected void txtNYObjective_Load(object sender, EventArgs e)
    {
        
    }

    protected void ButtonAdd_Click(object sender, EventArgs e)
    {
        if (Static_Mode.Trim() == "Y")
        {
            Alert.Show("لا يمكنك اضافة اهداف أخري");
        }
        else
        {
            Save_NY_Objectives();
            AddNewRowToGrid();

            Page.MaintainScrollPositionOnPostBack = true;
            Page.SetFocus(Gridview1.ClientID);
        }
    }

    protected void OnRowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        try
        {
            dtCurrentNYObjectivesTable = ViewState["CurrentNYObjectivesTable"] as DataTable;
            if (dtCurrentNYObjectivesTable.Rows.Count > 1)
            {

                int count = dtCurrentNYObjectivesTable.Rows.Count;
                for (int i = 0; i < count; i++)
                {
                    string seqc = (i + 1).ToString();
                    string objc = ((TextBox)Gridview1.Rows[i].FindControl("txtNYObjective")).Text;
                    string weight = ((TextBox)Gridview1.Rows[i].FindControl("txtNYWeight")).Text;
                    string quarter = ((DropDownList)Gridview1.Rows[i].FindControl("ddlDate")).SelectedValue;

                    dtCurrentNYObjectivesTable.Rows[i]["RowNumber"] = seqc;
                    dtCurrentNYObjectivesTable.Rows[i]["obj_desc"] = objc;
                    dtCurrentNYObjectivesTable.Rows[i]["obj_weight"] = weight;
                    dtCurrentNYObjectivesTable.Rows[i]["quart"] = quarter;

                }


                int selectedIndex = Convert.ToInt32(e.RowIndex);

                dtCurrentNYObjectivesTable.Rows[selectedIndex].Delete();
                dtCurrentNYObjectivesTable.AcceptChanges();
                
                ViewState["CurrentNYObjectivesTable"] = dtCurrentNYObjectivesTable;
                Gridview1.DataSource = dtCurrentNYObjectivesTable;
                Gridview1.DataBind();
            }
            else
            {
                Alert.Show(" لا يمكن حذف اخر صف بالجدول ");
                return;
            }
        }
        catch
        {

        }
    }

    private void Save_NY_Objectives()
    {
        DataTable OBJDT = new DataTable("NYOBJDT");
        OBJDT.Columns.Add("RowNumber");
        OBJDT.Columns.Add("obj_desc");
        OBJDT.Columns.Add("obj_weight");
        OBJDT.Columns.Add("quart");
        OBJDT.Rows.Clear();
        try
        {
            dtCurrentNYObjectivesTable = (DataTable)ViewState["CurrentNYObjectivesTable"];
            int cont = dtCurrentNYObjectivesTable.Rows.Count;
            int i = 0;
            foreach (GridViewRow row in Gridview1.Rows)
            {
                i++;
                string seqc = i.ToString();
                string objc = ((TextBox)row.FindControl("txtNYObjective")).Text;
                string weight = ((TextBox)row.FindControl("txtNYWeight")).Text;
                string quarter = ((DropDownList)row.FindControl("ddlDate")).SelectedValue;

                OBJDT.NewRow();
                OBJDT.Rows.Add(seqc, objc, weight, quarter);
            }
            if (OBJDT != null)
            {
                //Obj_Status = ser.Save_ASS_NY_OBJ_ByType_DTTL(Session["ASS_EMP_ID"].ToString(), Session["OPEN_YEAR"].ToString(), Session["OPEN_VERSION"].ToString(), Control_Type , OBJDT);
            }
            else
            { 
                //Obj_Status = 0; 
            }
        }
        catch
        {
            if (OBJDT != null)
            {
                //Obj_Status = ser.Save_ASS_NY_OBJ_ByType_DTTL(Session["ASS_EMP_ID"].ToString(), Session["OPEN_YEAR"].ToString(), Session["OPEN_VERSION"].ToString(), Control_Type, OBJDT);
            }
        }

    }

    private void AddNewRowToGrid()
    {
        int rowIndex = 0;

        if (ViewState["CurrentNYObjectivesTable"] != null)
        {
            dtCurrentNYObjectivesTable = (DataTable)ViewState["CurrentNYObjectivesTable"];
            DataRow drCurrentRow = null;
            if (dtCurrentNYObjectivesTable.Rows.Count > 0)
            {
                try
                {
                    for (int i = 1; i <= dtCurrentNYObjectivesTable.Rows.Count; i++)
                    {
                        //extract the TextBox values
                        TextBox txtNYObjective = (TextBox)Gridview1.Rows[rowIndex].Cells[1].FindControl("txtNYObjective");
                        TextBox txtNYWeight = (TextBox)Gridview1.Rows[rowIndex].Cells[2].FindControl("txtNYWeight");
                        DropDownList ddlDate = (DropDownList)Gridview1.Rows[rowIndex].Cells[3].FindControl("ddlDate");

                        drCurrentRow = dtCurrentNYObjectivesTable.NewRow();
                        drCurrentRow["RowNumber"] = i + 1;
                        drCurrentRow["obj_desc"] = string.Empty;
                        drCurrentRow["obj_weight"] = "0";
                        drCurrentRow["quart"] = "0";

                        dtCurrentNYObjectivesTable.Rows[i - 1]["obj_desc"] = txtNYObjective.Text;
                        dtCurrentNYObjectivesTable.Rows[i - 1]["obj_weight"] = txtNYWeight.Text;
                        dtCurrentNYObjectivesTable.Rows[i - 1]["quart"] = ddlDate.SelectedValue;
                        txtNYObjective.Focus();
                        rowIndex++;
                    }
                    dtCurrentNYObjectivesTable.Rows.Add(drCurrentRow);
                    ViewState["CurrentNYObjectivesTable"] = dtCurrentNYObjectivesTable;
                    dtCurrentNYObjectivesTable.AcceptChanges();
                    Gridview1.DataSource = dtCurrentNYObjectivesTable;
                    Gridview1.DataBind();
                }
                catch (Exception ex)
                {
                    string error = ex.Message;
                    Alert.Show("برجاء ادخال بيانات الهدف");
                    return;
                }
            }
        }
        else
        {
            Response.Write("ViewState is null");
        }

        //Set Previous Data on Postbacks
        SetPreviousData();
    }

    private void SetInitialRow()
    {
        DataTable dt = new DataTable();
        DataRow dr = null;
        dt.Columns.Add(new DataColumn("RowNumber", typeof(string)));
        dt.Columns.Add(new DataColumn("obj_desc", typeof(string)));
        dt.Columns.Add(new DataColumn("obj_weight", typeof(string)));
        dt.Columns.Add(new DataColumn("quart", typeof(string)));
        dr = dt.NewRow();

        dr["RowNumber"] = 1;
        dr["obj_desc"] = string.Empty;
        dr["obj_weight"] = "0";
        dr["quart"] = "0";
        dt.Rows.Add(dr);
        ViewState["CurrentNYObjectivesTable"] = dt;

        Gridview1.DataSource = dt;
        Gridview1.DataBind();
    }

    private void SetPreviousData()
    {
        int rowIndex = 0;
        if (ViewState["CurrentNYObjectivesTable"] != null)
        {
            DataTable dt = (DataTable)ViewState["CurrentNYObjectivesTable"];
            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    TextBox txtNYObjective = (TextBox)Gridview1.Rows[rowIndex].Cells[1].FindControl("txtNYObjective");
                    TextBox txtNYWeight = (TextBox)Gridview1.Rows[rowIndex].Cells[2].FindControl("txtNYWeight");
                    DropDownList ddlDate = (DropDownList)Gridview1.Rows[rowIndex].Cells[3].FindControl("ddlDate");

                    txtNYObjective.Text = dt.Rows[i]["obj_desc"].ToString();
                    txtNYWeight.Text = dt.Rows[i]["obj_weight"].ToString();
                    ddlDate.SelectedValue = dt.Rows[i]["quart"].ToString();

                    rowIndex++;
                }
            }
        }
    }
}