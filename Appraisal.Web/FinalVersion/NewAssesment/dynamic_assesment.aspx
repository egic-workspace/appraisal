﻿<%@ Page Language="C#" MasterPageFile="~/Main_Master_Page.master" AutoEventWireup="true" CodeFile="~/FinalVersion/NewAssesment/dynamic_assesment.aspx.cs" Inherits="Final_Version_NewAssesment_dynamic_assesment" Title="" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="~/FinalVersion/MyControles/test.ascx" TagName="test" TagPrefix="uc1" %>
<%@ Register Src="~/FinalVersion/MyControles/Message_Box.ascx" TagName="MessageBox" TagPrefix="mb" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <link type="text/css" rel="stylesheet" href="../../CSS/bootstrap.min.css" />
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

    <script type="text/javascript">
        function Hidelabel() {
            $('*[class^="badge"]').hide();
        }
        function AutoExpand(txtbox) {

            txtbox.style.height = "1px";
            txtbox.style.width = "250px";
            txtbox.style.height = (50 + txtbox.scrollHeight) + "px";
            var textLength = txtbox.length;
            if (textLength > 30) { txtbox.style.fontSize = "5px"; }
            else if (textLength > 20) { txtbox.style.fontSize = "10px"; }
            else if (textLength > 10) { txtbox.style.fontSize = "15px"; }
        }
        function AutoExpand_comments(txtbox) {
            txtbox.style.height = "1px";
            txtbox.style.height = (25 + txtbox.scrollHeight) + "px";
        }
        function showModal() {
            $("#myModal").modal('show');
        }
        $(function () {
            $("#btnShow").click(function () {
                showModal();
            });

            $('textarea').each(function () {
                $(this).height($(this).prop('scrollHeight'));
            });
        });
        function PrintPanel() {
            $('*[class^="btn btn-primary"]').hide();
            //badge
            $('*[class^="badge"]').show();
            debugger;
            var type = document.getElementById("<%=lbltype.ClientID %>").innerHTML;
            var panelHeader = document.getElementById("<%=panelHeader.ClientID %>");
            var panelObjectives = document.getElementById("<%=panelObjectives.ClientID %>");
            var panelCompetencies = document.getElementById("<%=panelCompetencies.ClientID %>");
            var panelNote = document.getElementById("<%=panelNote.ClientID %>");
            var panelTrainingProgram = document.getElementById("<%=panelTrainingProgram.ClientID %>");
            var panelNextYearObjectives = document.getElementById("<%=panelNextYearObjectives.ClientID %>");
            var panelSignature = document.getElementById("<%=panelSignature.ClientID %>");
            var printWindow = window.open('', '', 'scrollbars=yes, resizable=yes, width=700, height=700');
            printWindow.document.write('<html><head>');
            printWindow.document.write('');
            printWindow.document.write('');
            printWindow.document.write('\<script type="text/javascript"> <%=jsScript%> ><\/script>');
            printWindow.document.write('<link rel = "stylesheet" href = "https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css" crossorigin = "anonymous" >');
            printWindow.document.write('<style>fieldset{display:contents;}</style>');
            printWindow.document.write('</head><body dir="rtl"  >');
            printWindow.document.write('<h1>نموذج تقييم موظف</h1>');

                printWindow.document.write(panelHeader.innerHTML);
            if (type == "A") {
                printWindow.document.write(panelObjectives.innerHTML);
                printWindow.document.write(panelCompetencies.innerHTML);
                printWindow.document.write(panelNote.innerHTML);
                printWindow.document.write(panelTrainingProgram.innerHTML);
                //printWindow.document.write(panelNextYearObjectives.innerHTML);
                printWindow.document.write(panelSignature.innerHTML);
            }
            else if (type == "O") {
                printWindow.document.write(panelObjectives.innerHTML);
                printWindow.document.write(panelNote.innerHTML);
                printWindow.document.write(panelTrainingProgram.innerHTML);
                //printWindow.document.write(panelNextYearObjectives.innerHTML);
                printWindow.document.write(panelSignature.innerHTML);
            }
            else if (type == "C") {
                printWindow.document.write(panelCompetencies.innerHTML);
                printWindow.document.write(panelNote.innerHTML);
                printWindow.document.write(panelTrainingProgram.innerHTML);
                printWindow.document.write(panelSignature.innerHTML);
            }
            printWindow.document.write('\<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"><\/script>');
            printWindow.document.write(`<script type="text/javascript">
                                            $(function(){
                                                $("textarea").each(function () {
                                                    $(this).height($(this).prop("scrollHeight"));
                                                });
                                                $("select,input:text").each(function () {
                                                    $(this).after('<strong>'+$(this).val()+'</strong>');
                                                    $(this).remove();
                                                });
                                            })
                                        <\/script>`);
            printWindow.document.write(`<style type="text/css">
                                            .flex-container {display: flex;}
                                            .fill-width {flex: 1;width: 400px;min-height: 100px;max-height: 1900px;height: auto;}
                                            textarea {resize: none;}
                                            .v-align-top {vertical-align: top;}
                                            .v-align-top input {text-align: center;}
                                        </style >`);
            
            printWindow.document.write('</body></html>');
            printWindow.document.close();
            $('*[class^="btn btn-primary"]').show();
            $('*[class^="badge"]').hide();

            setTimeout(function () {
                printWindow.print();
            }, 700);
            return false;
        }
    </script>
    <style type="text/css">
        .flex-container {
            display: flex;
        }

        .fill-width {
            flex: 1;
            width: 400px;
            min-height: 100px;
            max-height: 1900px;
            height: auto;
        }

        textarea {
            resize: none;
        }

        .v-align-top {
            vertical-align: top;
        }

        .v-align-top input {
            text-align: center;
        }
    </style>
    <table style="width: 100%;" align="center">
        <tr>
            <td style="background-color: #71625F; height: 2px; width: 70%;">
                <span style="font-size: 11pt; font-weight: bold; color: #FFFFFF; font-family: Arial;">&nbsp; ادخال التقييم للموظفين 
                </span>
            </td>
        </tr>
    </table>
    <p>
        <table align="center" dir="rtl" style="width: 98%; height: 98%; float: right">
            <tr>
                <td align="center" colspan="4">
                    <asp:Panel ID="panelHeader" runat="server" class="panel panel-default" BorderStyle="Ridge" Width="90%" ScrollBars="Vertical">
                        <table align="center" style="width: 100%; float: right">
                            <tr>
                                <td align="right">
                                    <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                        <asp:Label ID="Label18" runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="14pt" ForeColor="Black" Text="كود الموظف" Width="90px"></asp:Label>
                                        <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                            <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                                <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                                    <asp:Label ID="lbl_emp_no" runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="14pt" ForeColor="Blue" Text=""></asp:Label>
                                                </span>
                                            </span>
                                        </span>
                                    </span>
                                </td>
                                <td align="right" dir="rtl" style="padding-right: 33pt;">
                                    <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                        <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                            <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                                <asp:Label ID="Label8" runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="14pt" ForeColor="Black" Text="اسم الموظف" Width="90px"></asp:Label>
                                                <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                                    <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                                        <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                                            <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                                                <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                                                    <asp:Label ID="lbl_emp_name" runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="14pt" ForeColor="Blue" Text="اسم الموظف"></asp:Label>
                                                                </span>
                                                            </span>
                                                        </span>
                                                    </span>
                                                </span>
                                            </span>
                                        </span>
                                    </span>
                                </td>
                                <td align="right">
                                    <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                        <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                            <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                                <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                                    <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                                        <asp:Label ID="Label10" runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="14pt" ForeColor="Black" Text="سنة التقييم" Width="80px"></asp:Label>
                                                        <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                                            <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                                                <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                                                    <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                                                        <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                                                            <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                                                                <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                                                                    <asp:Label ID="lbl_year" runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="14pt" ForeColor="Blue" Text="1999"></asp:Label>
                                                                                    <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                                                                        <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                                                                            <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                                                                                <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                                                                                    <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                                                                                        <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                                                                                            <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                                                                                                <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                                                                                                    <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                                                                                                        <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                                                                                                            <asp:Label ID="lbltype" runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="9pt" ForeColor="White"></asp:Label>
                                                                                                                        </span>

                                                                                                                    </span>

                                                                                                                </span>

                                                                                                            </span>

                                                                                                        </span>

                                                                                                    </span>

                                                                                                </span>

                                                                                            </span>

                                                                                        </span>

                                                                                    </span>

                                                                                </span>
                                                                            </span>

                                                                        </span>

                                                                    </span>

                                                                </span>

                                                            </span>

                                                        </span>

                                                    </span>

                                                </span>

                                            </span>

                                        </span>

                                    </span>
                                </td>
                                <td align="right">
                                    <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                        <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                            <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                                <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                                    <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                                        <asp:Label ID="Label1" runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="14pt" ForeColor="Black" Text="موقع العمل" Width="80px"></asp:Label>
                                                        <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                                            <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                                                <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                                                    <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                                                        <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                                                            <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                                                                <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                                                                    <asp:Label ID="lblLocation" runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="14pt" ForeColor="Blue" Text=""></asp:Label>
                                                                                    <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                                                                        <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                                                                            <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                                                                                <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                                                                                    <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                                                                                        <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                                                                                            <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                                                                                                <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                                                                                                    <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                                                                                                        <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                                                                                                            <asp:Label ID="Label3" runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="9pt" ForeColor="White"></asp:Label>
                                                                                                                        </span>

                                                                                                                    </span>

                                                                                                                </span>

                                                                                                            </span>

                                                                                                        </span>

                                                                                                    </span>

                                                                                                </span>

                                                                                            </span>

                                                                                        </span>

                                                                                    </span>

                                                                                </span>
                                                                            </span>

                                                                        </span>

                                                                    </span>

                                                                </span>

                                                            </span>

                                                        </span>

                                                    </span>

                                                </span>

                                            </span>

                                        </span>

                                    </span>
                                </td>
                            </tr>
                            <tr>
                                <td align="right">
                                    <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                        <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                            <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                                <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                                    <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                                        <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                                            <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                                                <asp:Label ID="lbl_obj_rslt_title" runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="14pt" ForeColor="Black" Text="نسبة الأهداف" Width="90px"></asp:Label>
                                                                <asp:Label ID="lbl_obj_result" runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="14pt" ForeColor="Blue" Text="0"></asp:Label>
                                                            </span>

                                                        </span>

                                                    </span>

                                                </span>

                                            </span>

                                        </span>

                                    </span>
                                </td>
                                <td align="right" style="padding-right: 33pt;">
                                    <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                        <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                            <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                                <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                                    <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                                        <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                                            <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                                                <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                                                    <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                                                        <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                                                            <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                                                                <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                                                                    <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                                                                        <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                                                                            <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                                                                                <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                                                                                    <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                                                                                        <asp:Label ID="lbl_comp_rslt_title" runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="14pt" ForeColor="Black" Text="نسبة الكفاءات" Width="100px"></asp:Label>
                                                                                                    </span>

                                                                                                </span>
                                                                                                <asp:Label ID="lbl_comp_result" runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="14pt" ForeColor="Blue" Text="0"></asp:Label>
                                                                                            </span>

                                                                                        </span>

                                                                                    </span>

                                                                                </span>

                                                                            </span>

                                                                        </span>

                                                                    </span>

                                                                </span>

                                                            </span>

                                                        </span>

                                                    </span>
                                                </span>

                                            </span>

                                        </span>

                                    </span>
                                </td>
                                <td align="right">
                                    <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                        <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                            <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                                <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                                    <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                                        <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                                            <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                                                <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                                                    <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                                                        <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                                                            <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                                                                <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                                                                    <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                                                                        <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                                                                            <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                                                                                <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                                                                                    <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                                                                                        <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                                                                                            <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                                                                                                <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                                                                                                    <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                                                                                                        <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                                                                                                            <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                                                                                                                <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                                                                                                                    <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                                                                                                                        <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                                                                                                                            <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                                                                                                                                <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                                                                                                                                    <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                                                                                                                                        <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                                                                                                                                            <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                                                                                                                                                <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                                                                                                                                                    <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                                                                                                                                                        <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                                                                                                                                                            <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                                                                                                                                                                <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                                                                                                                                                                    <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                                                                                                                                                                        <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                                                                                                                                                                            <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                                                                                                                                                                                <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                                                                                                                                                                                    <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                                                                                                                                                                                        <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                                                                                                                                                                                            <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                                                                                                                                                                                                <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                                                                                                                                                                                                    <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                                                                                                                                                                                                        <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                                                                                                                                                                                                            <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                                                                                                                                                                                                                <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                                                                                                                                                                                                                    <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                                                                                                                                                                                                                        <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                                                                                                                                                                                                                            <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                                                                                                                                                                                                                                <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                                                                                                                                                                                                                                    <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                                                                                                                                                                                                                                        <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                                                                                                                                                                                                                                            <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                                                                                                                                                                                                                                                <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                                                                                                                                                                                                                                                    <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                                                                                                                                                                                                                                                        <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                                                                                                                                                                                                                                                            <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                                                                                                                                                                                                                                                                <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                                                                                                                                                                                                                                                                    <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                                                                                                                                                                                                                                                                        <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                                                                                                                                                                                                                                                                            <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                                                                                                                                                                                                                                                                                <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                                                                                                                                                                                                                                                                                    <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                                                                                                                                                                                                                                                                                        <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                                                                                                                                                                                                                                                                                            <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                                                                                                                                                                                                                                                                                                <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                                                                                                                                                                                                                                                                                                    <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                                                                                                                                                                                                                                                                                                        <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                                                                                                                                                                                                                                                                                                            <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                                                                                                                                                                                                                                                                                                                <asp:Label ID="Label14" runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="14pt" ForeColor="Black" Text="التقييم" Width="60px"></asp:Label>
                                                                                                                                                                                                                                                                                                                            </span>

                                                                                                                                                                                                                                                                                                                        </span>

                                                                                                                                                                                                                                                                                                                    </span>

                                                                                                                                                                                                                                                                                                                </span>

                                                                                                                                                                                                                                                                                                            </span>

                                                                                                                                                                                                                                                                                                        </span>

                                                                                                                                                                                                                                                                                                    </span>

                                                                                                                                                                                                                                                                                                </span>

                                                                                                                                                                                                                                                                                            </span>

                                                                                                                                                                                                                                                                                        </span>

                                                                                                                                                                                                                                                                                    </span>
                                                                                                                                                                                                                                                                                </span>

                                                                                                                                                                                                                                                                            </span>

                                                                                                                                                                                                                                                                        </span>

                                                                                                                                                                                                                                                                    </span>
                                                                                                                                                                                                                                                                    <asp:Label ID="lbl_result" runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="14pt" ForeColor="Blue" Text="0" Width="60px"></asp:Label>
                                                                                                                                                                                                                                                                </span>

                                                                                                                                                                                                                                                            </span>

                                                                                                                                                                                                                                                        </span>

                                                                                                                                                                                                                                                    </span>

                                                                                                                                                                                                                                                </span>

                                                                                                                                                                                                                                            </span>

                                                                                                                                                                                                                                        </span>

                                                                                                                                                                                                                                    </span>

                                                                                                                                                                                                                                </span>

                                                                                                                                                                                                                            </span>

                                                                                                                                                                                                                        </span>
                                                                                                                                                                                                                    </span>

                                                                                                                                                                                                                </span>

                                                                                                                                                                                                            </span>

                                                                                                                                                                                                        </span>

                                                                                                                                                                                                    </span>

                                                                                                                                                                                                </span>

                                                                                                                                                                                            </span>

                                                                                                                                                                                        </span>

                                                                                                                                                                                    </span>

                                                                                                                                                                                </span>

                                                                                                                                                                            </span>
                                                                                                                                                                        </span>

                                                                                                                                                                    </span>

                                                                                                                                                                </span>

                                                                                                                                                            </span>

                                                                                                                                                        </span>

                                                                                                                                                    </span>
                                                                                                                                                    <asp:Label ID="lbl_grad" runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="14pt" ForeColor="Blue"></asp:Label>
                                                                                                                                                </span>

                                                                                                                                            </span>

                                                                                                                                        </span>

                                                                                                                                    </span>

                                                                                                                                </span>

                                                                                                                            </span>

                                                                                                                        </span>

                                                                                                                    </span>

                                                                                                                </span>

                                                                                                            </span>

                                                                                                        </span>
                                                                                                    </span>

                                                                                                </span>

                                                                                            </span>

                                                                                        </span>

                                                                                    </span>

                                                                                </span>

                                                                            </span>

                                                                        </span>

                                                                    </span>

                                                                </span>

                                                            </span>
                                                        </span>

                                                    </span>

                                                </span>

                                            </span>

                                        </span>

                                    </span>
                                </td>
                                <td align="right">
                                    <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                        <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                            <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                                <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                                    <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                                        <asp:Label ID="Label4" runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="14pt" ForeColor="Black" Text="الإدارة" Width="80px"></asp:Label>
                                                        <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                                            <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                                                <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                                                    <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                                                        <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                                                            <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                                                                <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                                                                    <asp:Label ID="lblDepartment" runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="14pt" ForeColor="Blue" Text=""></asp:Label>
                                                                                    <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                                                                        <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                                                                            <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                                                                                <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                                                                                    <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                                                                                        <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                                                                                            <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                                                                                                <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                                                                                                    <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                                                                                                        <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                                                                                                            <asp:Label ID="Label6" runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="9pt" ForeColor="White"></asp:Label>
                                                                                                                        </span>

                                                                                                                    </span>

                                                                                                                </span>

                                                                                                            </span>

                                                                                                        </span>

                                                                                                    </span>

                                                                                                </span>

                                                                                            </span>

                                                                                        </span>

                                                                                    </span>

                                                                                </span>
                                                                            </span>

                                                                        </span>

                                                                    </span>

                                                                </span>

                                                            </span>

                                                        </span>

                                                    </span>

                                                </span>

                                            </span>

                                        </span>

                                    </span>
                                </td>
                            </tr>
                            <tr>
                                <td align="center" colspan="4">
                                    <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                        <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                            <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                                <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                                    <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                                        <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                                            <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                                                <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                                                    <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                                                        <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                                                            <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                                                                <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                                                                    <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                                                                        <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                                                                            <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                                                                                <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                                                                                    <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                                                                                        <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                                                                                            <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                                                                                                <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                                                                                                    <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                                                                                                        <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                                                                                                            <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                                                                                                                <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                                                                                                                    <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                                                                                                                        <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                                                                                                                            <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                                                                                                                                <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                                                                                                                                    <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                                                                                                                                        <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                                                                                                                                            <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                                                                                                                                                <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                                                                                                                                                    <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                                                                                                                                                        <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                                                                                                                                                            <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                                                                                                                                                                <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                                                                                                                                                                    <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                                                                                                                                                                        <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                                                                                                                                                                            <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                                                                                                                                                                                <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                                                                                                                                                                                    <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                                                                                                                                                                                        <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                                                                                                                                                                                            <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                                                                                                                                                                                                <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                                                                                                                                                                                                    <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                                                                                                                                                                                                        <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                                                                                                                                                                                                            <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                                                                                                                                                                                                                <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                                                                                                                                                                                                                    <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                                                                                                                                                                                                                        <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                                                                                                                                                                                                                            <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                                                                                                                                                                                                                                <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                                                                                                                                                                                                                                    <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                                                                                                                                                                                                                                        <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                                                                                                                                                                                                                                            <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                                                                                                                                                                                                                                                <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                                                                                                                                                                                                                                                    <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                                                                                                                                                                                                                                                        <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                                                                                                                                                                                                                                                            <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                                                                                                                                                                                                                                                                <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                                                                                                                                                                                                                                                                    <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                                                                                                                                                                                                                                                                        <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                                                                                                                                                                                                                                                                            <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                                                                                                                                                                                                                                                                                <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                                                                                                                                                                                                                                                                                    <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                                                                                                                                                                                                                                                                                        <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                                                                                                                                                                                                                                                                                            <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                                                                                                                                                                                                                                                                                                <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                                                                                                                                                                                                                                                                                                    <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                                                                                                                                                                                                                                                                                                        <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                                                                                                                                                                                                                                                                                                            <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                                                                                                                                                                                                                                                                                                                <asp:Label ID="lbl_print_status" runat="server" CssClass="badge" Font-Bold="True"
                                                                                                                                                                                                                                                                                                                                    Font-Names="Arial" Font-Size="14pt" ForeColor="Black"></asp:Label>
                                                                                                                                                                                                                                                                                                                            </span></span></span></span></span></span></span></span></span></span></span>
                                                                                                                                                                                                                                                                                </span></span></span></span></span></span></span></span></span></span></span>
                                                                                                                                                                                                                                    </span></span></span></span></span></span></span></span></span></span></span>
                                                                                                                                                                                        </span></span></span></span></span></span></span></span></span></span></span>
                                                                                                                                            </span></span></span></span></span></span></span></span></span></span></span>
                                                                                                </span></span></span></span></span></span></span></span></span></span></span>
                                                    </span></span></span></span></span>
                                </td>
                            </tr>
                        </table>
                        <br />
                        <br />
                        <br />
                    </asp:Panel>
                </td>
            </tr>
            <tr>
                <td align="center" colspan="4">
                    <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;" __designer:mapid="86">
                        <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;" __designer:mapid="87">
                            <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;" __designer:mapid="88">
                                <asp:Label ID="lbl_version" runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="9pt" ForeColor="Blue" Text="الاصدار" Visible="False"></asp:Label>
                            </span></span></span>
                </td>
            </tr>
            <tr>
                <td align="center" colspan="4">
                    <asp:Panel ID="Panel2" runat="server" class="panel panel-default" ScrollBars="Vertical" BorderStyle="Ridge" Width="90%">
                        <table align="center" style="width: 100%; float: right">
                            <tr>
                                <td align="center" style="width: 100%; float: right;">
                                    <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                        <asp:Button ID="btn_save" runat="server" BackColor="#F79222" class="btn btn-primary" Font-Bold="True" Font-Names="Arial" Font-Size="12pt" OnClick="btn_save_Click" Text="حفظ مـؤقت" Width="150px" />
                                        <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                            <asp:Button ID="btn_calc" runat="server" BackColor="#F79222" class="btn btn-primary" Font-Bold="True" Font-Names="Arial" Font-Size="12pt" OnClick="btn_calc_Click" Text="احتساب النتيجة" Width="150px" />
                                            <span dir="rtl" style="font-size: 11pt; font-weight: bold; color: #F79222; font-family: Courier New;">
                                                <button class="btn btn-primary" onclick="PrintPanel()" style="font-weight: bold; background-color: #F79222; font-family: Arial; font-size: 15px;" type="button">
                                                    طــباعة
                                                </button>

                                            </span></span></span>
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                </td>
            </tr>
            <tr>
                <td align="center" colspan="4">
                    <div class="container" dir="ltr">
                        <button type="button" runat="server" class="btn btn-info  collapsed" data-toggle="collapse" data-target="#demo1" style="width: 100%; font-family: Arial; font-size: 16px; font-weight: bold; background-color: #F79222;">
                            مقياس تقييم الأهداف للقطاع التجاري
                        </button>
                        <div id="demo1" class="collapse">
                            <table width="100%" border="1" style="border: thin solid #000000; font-family: Arial; font-size: 15px;">
                                <tbody>
                                    <tr>
                                        <td colspan="3" width="882" bgcolor="#71625F" style="color: #FFFFFF; font-weight: bold; font-size: 15px;">
                                            <p align="center"><strong style="font-size: 19px">مقياس تقييم الأهداف للقطاع التجاري </strong></p>
                                            <p align="center"><b><span>Commercial Departments (Sales, CS, Project sales, FQA & Export)</span></b></p>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th style="background-color: #00CC99">
                                            <p style="text-align: center;"><strong>Grade</strong> - <strong>درجة التقييم </strong></p>
                                        </th>
                                        <th style="width: 532px; background-color: #00CC99">
                                            <p style="text-align: center;"><strong>FROM</strong></p>
                                        </th>
                                        <th style="background-color: #00CC99">
                                            <p style="text-align: center;"><strong>To</strong></p>
                                        </th>
                                    </tr>
                                    <tr>
                                        <td width="206">
                                            <p align="center"><strong>1</strong></p>
                                        </td>
                                        <td style="width: 532px">
                                            <p align="center">0%</p>
                                        </td>
                                        <td width="626">
                                            <p align="center">69%</p>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="206">
                                            <p align="center"><strong>2</strong></p>
                                        </td>
                                        <td style="width: 532px">
                                            <p align="center">70%</p>
                                        </td>
                                        <td width="626">
                                            <p align="center">84%</p>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td width="206">
                                            <p align="center"><strong>3</strong></p>
                                        </td>
                                        <td style="width: 532px">
                                            <p align="center">85%</p>
                                        </td>
                                        <td width="626">
                                            <p align="center">100%</p>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="206">
                                            <p align="center"><strong>4</strong></p>
                                        </td>
                                        <td style="width: 532px">
                                            <p align="center">101%</p>
                                        </td>
                                        <td width="626">
                                            <p align="center">115%</p>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="206">
                                            <p align="center"><strong>5</strong></p>
                                        </td>
                                        <td colspan="2">
                                            <p align="center">Above 116%</p>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    &nbsp;

                </td>
            </tr>
            <tr >
                <td align="center" colspan="4">
                    <div class="container" dir="ltr" runat="server" ID="divObjectiveCompetencyScale">
                        <button type="button" id="btn_comp_ex" runat="server" class="btn btn-info" data-toggle="collapse" data-target="#demo2" style="width: 100%; font-family: Arial; font-size: 16px; font-weight: bold; background-color: #F79222;">
                            مقياس تقييم الأهداف والكفاءات &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  Objective & Competency Scale</button>
                        <div id="demo2" class="collapse in">
                            <table width="100%" border="1" style="border: thin solid #000000; font-family: Arial; font-size: 15px;">
                                <tbody>
                                    <tr>
                                        <td colspan="3" width="882" bgcolor="#71625F" style="color: #FFFFFF; font-weight: bold; font-size: 15px;">
                                            <p align="center"><strong style="font-size: 22px">Objective & Competency Scale</strong></p>
                                            <p align="center"><strong style="font-size: 19px">مقياس تقييم الأهداف والكفاءات</strong></p>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="206">
                                            <p><strong>Exceptional</strong></p>
                                            <p><strong>إستثنائي</strong></p>
                                        </td>
                                        <td width="51">
                                            <p align="center"><strong>5</strong></p>
                                        </td>
                                        <td width="626">
                                            <p><strong>Always</strong> demonstrates exceptional skills. Acts as a role model and reference</p>
                                            <p align="right"><strong>دائماَ يظهر مهارات استثنائية</strong>، ويعمل كقدوة ومرجع للآخرين.</p>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="206">
                                            <p><strong>Exceed Expectations</strong></p>
                                            <p><strong>يفوق التوقعات</strong></p>
                                        </td>
                                        <td width="51">
                                            <p align="center"><strong>4</strong></p>
                                        </td>
                                        <td width="626">
                                            <p><strong>Often</strong> demonstrates exceptional behaviors.</p>
                                            <p align="right"><strong>فى كثير من الاحيان</strong> يظهر سلوك إستثنائى ويفوق التوقعات.</p>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="206">
                                            <p><strong>Meet Expectations</strong></p>
                                            <p><strong>يحقق التوقعات</strong></p>
                                        </td>
                                        <td width="51">
                                            <p align="center"><strong>3</strong></p>
                                        </td>
                                        <td width="626">
                                            <p><strong>Consistently</strong> fulfills expectations and demonstrates effective behaviors.</p>
                                            <p align="right"><strong>يحقق التوقعات باستمرار</strong> ويظهر السلوكيات الفعالة،ويرتقى فى أغلب الأحيان إلى مستوى التوقعات</p>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="206">
                                            <p><strong>Below Expectations</strong></p>
                                            <p><strong>أقل </strong><strong>من التوقعات</strong></p>
                                        </td>
                                        <td width="51">
                                            <p align="center"><strong>2</strong></p>
                                        </td>
                                        <td width="626">
                                            <p><strong>Rarely</strong> demonstrates the required competency, Needs improvement.</p>
                                            <p align="right"><strong>نادراَ ما يظهر الكفاءة المطلوبة</strong> , ويحتاج إلى تحسين .</p>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="206">
                                            <p><strong>Unsatisfactory</strong></p>
                                            <p><strong>غير مرضي</strong></p>
                                        </td>
                                        <td width="51">
                                            <p align="center"><strong>1</strong></p>
                                        </td>
                                        <td width="626">
                                            <p><strong>Significantly below </strong>criteria required for successful job performance.</p>
                                            <p align="right"><strong>أقل بكثير من المعايير المطلوبة</strong> لأداء عمله بنجاح ، وأدائه دون التوقعات بشكل مستمر ولا يتحسن</p>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    &nbsp;</td>
            </tr>
            <tr>
                <td align="center" colspan="4">
                    <asp:Panel ID="panelObjectives" class="panel panel-default" runat="server" Width="98%" GroupingText=" الأهداف&nbsp; &nbsp;Objectives " Font-Names="Arial" Font-Size="13pt">
                    </asp:Panel>
                </td>
            </tr>

            <tr>
                <td align="center" colspan="4">
                    <asp:Panel ID="panelCompetencies" class="panel panel-default" runat="server" ScrollBars="Both" Width="90%" GroupingText="الكفاءات &nbsp; &nbsp; Competencies" Font-Names="Arial" Font-Size="13pt">
                    </asp:Panel>
                </td>
            </tr>

            <tr>
                <td align="center" colspan="4">
                    <asp:Panel ID="panelNote" class="panel panel-default" runat="server"
                        ScrollBars="Both" Width="90%"
                        GroupingText=" نقاط القوة الخاصة بأداء الموظف &nbsp; &nbsp; Area of strengths in the employee Performance"
                        Font-Names="Arial" Font-Size="13pt">
                        <asp:TextBox ID="txtNotes" runat="server" class="form-control" Font-Size="12pt" Height="65px" TextMode="MultiLine" Width="100%" placeholder="برجاء ذكر المهارات الفنية والسلوكية الموجودة لدى الموظف التي تدعم اداءه بشكل ايجابي   kindly mention the technical and behavioral skills that impact the employee Performance in a positive Manner "></asp:TextBox>
                    </asp:Panel>
                </td>
            </tr>
            <tr>
                <td align="center" colspan="4">
                    <asp:Panel ID="panelTrainingProgram" class="panel panel-default" runat="server"
                        ScrollBars="Both" Width="90%"
                        GroupingText=" نقاط التطوير الخاصة بأداء الموظف   &nbsp; &nbsp; Area of development in the employee performance"
                        Font-Names="Arial" Font-Size="13pt">
                        <asp:TextBox ID="txtDevProgram" runat="server" class="form-control" Font-Size="11pt" Height="65px" TextMode="MultiLine" Width="100%" placeholder="برجاء ذكر المهارات الفنية والسلوكية المراد تطويرها و التي تؤثر على أداءه  بشكل سلبي   kindly mention the behavioral and technical that needs to be developed that impact the employee performance in a negative Manner"></asp:TextBox>
                    </asp:Panel>
                </td>
            </tr>
            <tr>
                <td align="center" colspan="4">
                    <asp:Panel ID="panelNextYearObjectives" class="panel panel-default" runat="server"
                        ScrollBars="Both" Width="90%"
                        GroupingText="أهداف العام المقبل&nbsp; &nbsp;Next Year Objectives " Font-Names="Arial" Font-Size="13pt">
                    </asp:Panel>
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td align="center">&nbsp;</td>
                <td align="center">&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td align="center" colspan="4" dir="rtl">
                    <asp:Panel ID="panelSignature" runat="server" class="panel panel-default" BorderStyle="Ridge"
                        Width="90%" Height="60px">
                        <table align="center" style="width: 100%; float: right">
                            <tr>
                                <td align="right">
                                    <span
                                        style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;"
                                        dir="ltr">
                                        <asp:Label ID="Label15" runat="server" Font-Bold="True" Font-Names="Arial"
                                            Font-Size="16pt" ForeColor="Black" Text="توقيع الموظف"></asp:Label>
                                    </span></td>
                                <td>&nbsp;</td>
                                <td align="left" dir="ltr">
                                    <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                        <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                            <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;"
                                                dir="ltr">
                                                <asp:Label ID="Label17" runat="server" Font-Bold="True" Font-Names="Arial"
                                                    Font-Size="16pt" ForeColor="Black" Text="توقيع المدير"></asp:Label>
                                            </span></span></span></td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                            </tr>
                        </table>
                        <br />
                        <br />
                        <br />
                    </asp:Panel>
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
        </table>
        <br />
    </p>

    <div class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">نظام تقييم الموظفين</h4>
                </div>
                <div class="modal-body">
                    <asp:Label ID="lblMessage" runat="server" Font-Bold="True" Font-Size="12pt"
                        Font-Names="Arial" ForeColor="Red"></asp:Label>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

</asp:Content>

