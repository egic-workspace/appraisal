﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

using System.Drawing;


public partial class Final_Version_NewAssesment_Ass_approve : System.Web.UI.Page
{
    AppService ser = new AppService();
    
    DataTable Dst = new DataTable();
    DataTable dt = new DataTable();
    // string Job_Code = "";
     string Year = "";
     string Version = "";

     protected void Page_Init(object sender, EventArgs e)
     {
         //Session["ASS_APPAISAL_TYPE"] 
         //Session["ASS_Comp_Weight"] 
         //Session["ASS_Obj_Weight"] 
         //Session["ASS_EMP_ID"] 
         //Session["ASS_EMP_NAME"]
         //Session["ASS_JOB_ID"]
         //Session["OPEN_YEAR"] 
         //Session["OPEN_VERSION"]
         //****************************************************************|| Session["SEL_APP_DEPT"] == null
         Session["SEL_APP_DEPT"] = Session["SEL_DEPT"];
         if (Session["Logged_User"] == null  )
         {
             Response.Redirect("~/Default.aspx");
         }
         if (Session["SEL_DEPT"] == null)
         {
             Response.Redirect("~/FinalVersion/NewAssesment/SelDepts.aspx");
         }
         int APPROVER = ser.Check_Approver(Session["EMP_NO"].ToString());
         if (APPROVER == 0)
         { Response.Redirect("~/Setting/Unauthorized_Pg.aspx"); }
         Session["OPEN_YEAR"] = ser.Get_Ass_Open_Year().ToString();
         Session["OPEN_VERSION"] = ser.Get_Ass_Open_Version().ToString();

         // status 1 = تم التسجيل
         // status 0 = لم يتم التسجيل
         // status 2 = تمت الموافقة
     }
    protected void Page_Load(object sender, EventArgs e)
    {

       // string ass_options = Request.QueryString["option"];

        if (!IsPostBack)
        {
            if (Session["Logged_User"] == null)
            {
                Response.Redirect("~/Default.aspx");
            }
            //****************************
            Year = Session["OPEN_YEAR"].ToString();
            Version = Session["OPEN_VERSION"].ToString();
            //lbl_open_year.Text = Year;
            //lbl_open_version.Text = Version;
            //*****************************
            Fill_Grid_ALL("EGIC", GridView1);
            Fill_Grid_ALL("OutSource", GridView3);
            Fill_Grid_NOT_APRV_ALL("ALL");
            //*****************************
            bool disableApp;
            string disableAppStr = ConfigurationManager.AppSettings["DisableApp"];
            if (!bool.TryParse(disableAppStr, out disableApp))
                disableApp = false;
            if (disableApp)
            {
                btn_approve_egic.Visible = false;
                btn_approve_outsource.Visible = false;
            }
        }

        //Select_Options(ass_options);
        EGIC_GridviewLoop();
        Outsource_GridviewLoop();
    }
    private void Select_Options(string options)
    {
        if (options != null)
        {
            if (options == "0")
            {
                Fill_Grid_ALL("EGIC", GridView1);
                Fill_Grid_ALL("OutSource", GridView3);
                Fill_Grid_NOT_APRV_ALL("ALL");
            }
            else if (options == "1")
            {
                Fill_Grid_First_Level();
                Fill_Grid_NOT_APRV_First_Level();
            }
        }
    }

    private void Fill_Grid_ALL(string company , GridView GVIW )
    {
        try
        {
            // status 1 = تم التسجيل
            // status 0 = لم يتم التسجيل
            // status 2 = تمت الموافقة
            Clear_Grids(GVIW);
            
            string EMP_NO = Session["EMP_NO"].ToString();
            dt = ser.Bind_New_ASS_APPROVE_EMP_GRID(EMP_NO, Session["OPEN_YEAR"].ToString(), Session["OPEN_VERSION"].ToString(), "1", Session["SEL_APP_DEPT"].ToString(), company);
            GVIW.DataSource = dt;
            GVIW.DataBind();
           
        }
        catch
        {

        }
    }

    private void Fill_Grid_First_Level()
    {
        try
        {
            Clear_Grids(GridView1);
            
            string EMP_NO = Session["EMP_NO"].ToString();
            dt = ser.Bind_New_ASS_APPROVE_EMP_FRST_LEVEL_GRID(EMP_NO , Session["OPEN_YEAR"].ToString(), Session["OPEN_VERSION"].ToString(), "1");
            GridView1.DataSource = dt;
            GridView1.DataBind();
        }
        catch
        {

        }
    }

    private void Fill_Grid_NOT_APRV_ALL( string company)
    {
        try
        {
            Clear_Grids_Two();
            
            string EMP_NO = Session["EMP_NO"].ToString();
            Dst = ser.Bind_New_ASS_APPROVE_EMP_GRID(EMP_NO, Session["OPEN_YEAR"].ToString(), Session["OPEN_VERSION"].ToString(), "0", Session["SEL_APP_DEPT"].ToString(), company);
            GridView2.DataSource = Dst;
            GridView2.DataBind();
        }
        catch
        {

        }
    }

    private void Fill_Grid_NOT_APRV_First_Level()
    {
        try
        {
            Clear_Grids_Two();
            
            string EMP_NO = Session["EMP_NO"].ToString();
            Dst = ser.Bind_New_ASS_APPROVE_EMP_FRST_LEVEL_GRID(EMP_NO, Session["OPEN_YEAR"].ToString(), Session["OPEN_VERSION"].ToString(), "0");
            GridView2.DataSource = Dst;
            GridView2.DataBind();
        }
        catch
        {

        }
    }

    private void Clear_Grids(GridView GVIW)
    {
        GVIW.DataSource = null;
        GVIW.DataBind();
    }
    private void Clear_Grids_Two()
    {
        GridView2.DataSource = null;
        GridView2.DataBind();
    }

    protected void btn_approve_Click(object sender, EventArgs e)
    {
        int APRV = 0;
        int APRV_STS = 0;
        //int APRV_STS_LVL = 0;
        string confirmValue = Request.Form["confirm_value"];
        if (confirmValue == "No")
        {

        }
        else
        {
            double Act_bad = ser.Get_Ass_Bad_Results(Session["Logged_User"].ToString(), "00", Session["EMP_NO"].ToString(), Session["OPEN_YEAR"].ToString(), Session["OPEN_VERSION"].ToString(), "EGIC", Session["SEL_APP_DEPT"].ToString().Trim());
            double Act_mid = ser.Get_Ass_Midum_Results(Session["Logged_User"].ToString(), "00", Session["EMP_NO"].ToString(), Session["OPEN_YEAR"].ToString(), Session["OPEN_VERSION"].ToString(), "EGIC", Session["SEL_APP_DEPT"].ToString().Trim());
            double Act_god = ser.Get_Ass_Good_Results(Session["Logged_User"].ToString(), "00", Session["EMP_NO"].ToString(), Session["OPEN_YEAR"].ToString(), Session["OPEN_VERSION"].ToString(), "EGIC", Session["SEL_APP_DEPT"].ToString().Trim());
            double Act_vgd = ser.Get_Ass_Very_Good_Results(Session["Logged_User"].ToString(), "00", Session["EMP_NO"].ToString(), Session["OPEN_YEAR"].ToString(), Session["OPEN_VERSION"].ToString(), "EGIC", Session["SEL_APP_DEPT"].ToString().Trim());
            double Act_ext = ser.Get_Ass_Exellent_Results(Session["Logged_User"].ToString(), "00", Session["EMP_NO"].ToString(), Session["OPEN_YEAR"].ToString(), Session["OPEN_VERSION"].ToString(), "EGIC", Session["SEL_APP_DEPT"].ToString().Trim());


            int Pln_bad = ser.Get_Ass_Planned_MOD(Session["EMP_NO"].ToString(), "EGIC", Session["SEL_APP_DEPT"].ToString(), Session["OPEN_YEAR"].ToString(), Session["OPEN_VERSION"].ToString());
            int Pln_god = ser.Get_Ass_Planned_GOOD(Session["EMP_NO"].ToString(), "EGIC", Session["SEL_APP_DEPT"].ToString(), Session["OPEN_YEAR"].ToString(), Session["OPEN_VERSION"].ToString());
            int Pln_vgd = ser.Get_Ass_Planned_VGOD(Session["EMP_NO"].ToString(), "EGIC", Session["SEL_APP_DEPT"].ToString(), Session["OPEN_YEAR"].ToString(), Session["OPEN_VERSION"].ToString());
            int Pln_ext = ser.Get_Ass_Planned_Exll(Session["EMP_NO"].ToString(), "EGIC", Session["SEL_APP_DEPT"].ToString(), Session["OPEN_YEAR"].ToString(), Session["OPEN_VERSION"].ToString());

            if (Act_bad + Act_mid > Pln_bad || Act_god > Pln_god ||
                Act_vgd > Pln_vgd || Act_ext > Pln_ext)
            {
                RadNotification1.Text = "نسب التقييم المقدمه غير مطابقه للخطه برجاء المراجعه";
                RadNotification1.Title = "نظام التقييمات ";
                RadNotification1.TitleIcon = "info";
                RadNotification1.ContentIcon = "info";
                RadNotification1.Show();
                return;
            }
            else
            {

                foreach (GridViewRow row in GridView1.Rows)
                {
                    Label chk = row.FindControl("lblstatus") as Label;

                    if (chk != null && chk.Text != "2")
                    {
                        string emp_no = (row.Cells[2].FindControl("lblemp_no") as Label).Text;
                        string AprvLevel = (row.Cells[10].FindControl("lbllevel") as Label).Text;
                        //*********** 
                        //if (AprvLevel.Trim() == "2")
                        //{
                        APRV = ser.Make_ASS_Approve(emp_no, Session["OPEN_YEAR"].ToString(), Session["OPEN_VERSION"].ToString(), Session["Logged_User"].ToString());
                        if (APRV == 0) { APRV_STS++; }
                        //}
                        //else
                        //{
                        //    APRV_STS_LVL++;
                        //}
                    }
                }
            }

            if (APRV == 1 && APRV_STS == 0)
            {
                //Show_Modal_Message("تمت الموافقة بنجاح ");
                RadNotification1.Text = "تمت الموافقة بنجاح";
                RadNotification1.Title = "نظام التقييمات ";
                RadNotification1.TitleIcon = "info";
                RadNotification1.ContentIcon = "info";
                RadNotification1.Show();

                Fill_Grid_ALL("EGIC", GridView1);
                Fill_Grid_ALL("OutSource", GridView3);
                Fill_Grid_NOT_APRV_ALL("ALL");
                EGIC_GridviewLoop();
                Outsource_GridviewLoop();
            }
            else
            {
                RadNotification1.Text = " جميع التقييمات تمت الموافقه عليها ";
                //RadNotification1.Text = "توجد مشكلة في التقييمات برجاء الرجوع الي مدير النظام";
                RadNotification1.Title = "نظام التقييمات ";
                RadNotification1.TitleIcon = "info";
                RadNotification1.ContentIcon = "info";
                RadNotification1.Show();

                return;
            }
        }
    }
    protected void EGIC_GridviewLoop()
    {
        for (int i = 0; i < GridView1.Rows.Count; i++)
        {
            GridViewRow x = GridView1.Rows[i];
            Label vip = ((Label)x.FindControl("lblstatus"));
            //ButtonField agree = ((ButtonField)x.FindControl("APPROVE"));
            TableCell name = x.Cells[3];
            if (vip.Text.Trim() == "2") 
            {
               // name.ForeColor = Color.Red;
                 
                 x.Cells[0].Text = "تمت الموافقة";
               
            }
        }
    }
    protected void Outsource_GridviewLoop()
    {
        for (int i = 0; i < GridView3.Rows.Count; i++)
        {
            GridViewRow x = GridView3.Rows[i];
            Label vip = ((Label)x.FindControl("lblstatus1"));
            //ButtonField agree = ((ButtonField)x.FindControl("APPROVE"));
            TableCell name = x.Cells[3];
            if (vip.Text.Trim() == "2")
            {
                // name.ForeColor = Color.Red;

                x.Cells[0].Text = "تمت الموافقة";

            }
        }
    }
    protected void GridView1_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
    protected void GridView1_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
    {

    }
    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {

    }
    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {

    }
    protected void GridView1_DataBound(object sender, EventArgs e)
    {

    }
    protected void GridView1_DataBinding(object sender, EventArgs e)
    {

    }
    private void Show_Modal_Message(string msg)
    {
        try
        {
            lblMessage.Text = msg;//showConfModal() 
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "showModal();", true);
        }
        catch
        {

        }

    }
    private void Show_Modal_COnfirm(string msg)
    {
        try
        {
            lblConf_Message.Text = msg;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "showConfModal();", true);
        }
        catch
        {

        }

    }

    public static string Base64EncodingMethod(string Data)
    {
        byte[] toEncodeAsBytes = System.Text.Encoding.UTF8.GetBytes(Data);
        string sReturnValues = System.Convert.ToBase64String(toEncodeAsBytes);
        return sReturnValues;
    }

    protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        //Determine the RowIndex of the Row whose Button was clicked.
        int rowIndex = Convert.ToInt32(e.CommandArgument);
        //Reference the GridView Row.
        GridViewRow row = GridView1.Rows[rowIndex];

        //Fetch value of Name.
        string Emp_Code = (row.FindControl("lblemp_no") as Label).Text;
        string Emp_Name = (row.FindControl("lblemp_name") as Label).Text;
        string Job_code = (row.FindControl("lbljob_code") as Label).Text;
        string Status = (row.FindControl("lblstatus") as Label).Text;
        string Level = (row.FindControl("lbllevel") as Label).Text;
        Year = Session["OPEN_YEAR"].ToString();
        Version = Session["OPEN_VERSION"].ToString();
        //*********************************************************************
        Session["ASS_APPAISAL_TYPE"] = ser.GetAppaisalType(Session["OPEN_YEAR"].ToString(), Session["OPEN_VERSION"].ToString() , Job_code).ToString();
        Session["ASS_Comp_Weight"] = ser.Get_Ass_Job_Comp_Weight(Session["OPEN_YEAR"].ToString(), Session["OPEN_VERSION"].ToString() , Job_code);
        Session["ASS_Obj_Weight"] = ser.Get_Ass_Job_Obj_Weight(Session["OPEN_YEAR"].ToString(), Session["OPEN_VERSION"].ToString() , Job_code);
        Session["ASS_EMP_ID"] = Emp_Code;
        Session["ASS_APRV_LVL"] = Level;
        //*********************************************************************
        if (e.CommandName == "Display")
        {
            Session["ASS_EMP_ID"] = Emp_Code;
            Session["ASS_EMP_NAME"] = Emp_Name;
            Session["ASS_JOB_ID"] = Job_code;
            Session["ASS_STATUS"] = Status;
            Session["Emp_Level"] = Level;
            Session["ASS_DISPLAY_ONLY"] = "Y";

            Response.Redirect("~/FinalVersion/NewAssesment/dynamic_assesment.aspx?sec_path=" + Base64EncodingMethod(Session["ASS_EMP_ID"].ToString()));
        }
        else
            if (e.CommandName == "APPROVE")
            {
                //if (Session["ASS_APRV_LVL"].ToString().Trim() == "2")
                //{
                //    Show_Modal_COnfirm("هل تريد الموافقة علي هذا التقييم ؟");
                //}
                //else
                //{
                //    Show_Modal_Message(" لا يمكنك الموافقة علي التقييم ,, المدير المباشر فقط ");
                //    return;
                //}
                double Act_bad = ser.Get_Ass_Bad_Results(Session["Logged_User"].ToString(), "00", Session["EMP_NO"].ToString(), Session["OPEN_YEAR"].ToString(), Session["OPEN_VERSION"].ToString(), "EGIC", Session["SEL_APP_DEPT"].ToString().Trim());
                double Act_mid = ser.Get_Ass_Midum_Results(Session["Logged_User"].ToString(), "00", Session["EMP_NO"].ToString(), Session["OPEN_YEAR"].ToString(), Session["OPEN_VERSION"].ToString(), "EGIC", Session["SEL_APP_DEPT"].ToString().Trim());
                double Act_god = ser.Get_Ass_Good_Results(Session["Logged_User"].ToString(), "00", Session["EMP_NO"].ToString(), Session["OPEN_YEAR"].ToString(), Session["OPEN_VERSION"].ToString(), "EGIC", Session["SEL_APP_DEPT"].ToString().Trim());
                double Act_vgd = ser.Get_Ass_Very_Good_Results(Session["Logged_User"].ToString(), "00", Session["EMP_NO"].ToString(), Session["OPEN_YEAR"].ToString(), Session["OPEN_VERSION"].ToString(), "EGIC", Session["SEL_APP_DEPT"].ToString().Trim());
                double Act_ext = ser.Get_Ass_Exellent_Results(Session["Logged_User"].ToString(), "00", Session["EMP_NO"].ToString(), Session["OPEN_YEAR"].ToString(), Session["OPEN_VERSION"].ToString(), "EGIC", Session["SEL_APP_DEPT"].ToString().Trim());

                int Pln_bad = ser.Get_Ass_Planned_MOD(Session["EMP_NO"].ToString(), "EGIC", Session["SEL_APP_DEPT"].ToString(), Session["OPEN_YEAR"].ToString(), Session["OPEN_VERSION"].ToString());
                int Pln_god = ser.Get_Ass_Planned_GOOD(Session["EMP_NO"].ToString(), "EGIC", Session["SEL_APP_DEPT"].ToString(), Session["OPEN_YEAR"].ToString(), Session["OPEN_VERSION"].ToString());
                int Pln_vgd = ser.Get_Ass_Planned_VGOD(Session["EMP_NO"].ToString(), "EGIC", Session["SEL_APP_DEPT"].ToString(), Session["OPEN_YEAR"].ToString(), Session["OPEN_VERSION"].ToString());
                int Pln_ext = ser.Get_Ass_Planned_Exll(Session["EMP_NO"].ToString(), "EGIC", Session["SEL_APP_DEPT"].ToString(), Session["OPEN_YEAR"].ToString(), Session["OPEN_VERSION"].ToString());

                if (Act_bad + Act_mid > Pln_bad || Act_god > Pln_god ||
                    Act_vgd > Pln_vgd || Act_ext > Pln_ext)
                {
                    RadNotification1.Text = "نسب التقييم المقدمه غير مطابقه للخطه برجاء المراجعه";
                    RadNotification1.Title = "نظام التقييمات ";
                    RadNotification1.TitleIcon = "info";
                    RadNotification1.ContentIcon = "info";
                    RadNotification1.Show();
                    return;
                }
                else
                {
                    int APRV = ser.Make_ASS_Approve(Emp_Code, Session["OPEN_YEAR"].ToString(), Session["OPEN_VERSION"].ToString(), Session["Logged_User"].ToString());
                    if (APRV == 1)
                    {
                        Fill_Grid_ALL("EGIC", GridView1);
                        Fill_Grid_ALL("OutSource", GridView3);
                        Fill_Grid_NOT_APRV_ALL("ALL");
                        EGIC_GridviewLoop();
                        Outsource_GridviewLoop();

                        //Show_Modal_Message("تمت الموافقة بنجاح ");
                        RadNotification1.Text = "تمت الموافقة بنجاح";
                        RadNotification1.Title = "نظام التقييمات ";
                        RadNotification1.TitleIcon = "info";
                        RadNotification1.ContentIcon = "info";
                        RadNotification1.Show();
                        //***********
                     
                    }
                    else
                    {
                        // Show_Modal_Message(" هناك مشكله في عمليه الموافقة ");
                        RadNotification1.Text = "توجد مشكلة في التقييمات برجاء الرجوع الي مدير النظام";
                        RadNotification1.Title = "نظام التقييمات ";
                        RadNotification1.TitleIcon = "info";
                        RadNotification1.ContentIcon = "info";
                        RadNotification1.Show();
                    }
                }
            }

    }
    protected void Button1_Click(object sender, EventArgs e)
    {
    //    Show_Modal_COnfirm("هل تريد الموافقة علي هذا التقييم ؟"); 
    }

    protected void btnapp_Click(object sender, EventArgs e)
    {


        //int APRV = ser.Make_ASS_Approve(Session["ASS_EMP_ID"].ToString(), Session["OPEN_YEAR"].ToString(), Session["OPEN_VERSION"].ToString(), Session["Logged_User"].ToString());
        //if (APRV == 1)
        //{
        //    //data-toggle="modal" data-target="#confirm-delete"
        //    Show_Modal_Message("تمت الموافقة بنجاح ");
        //    Fill_Grid_ALL("EGIC", GridView1);
        //    Fill_Grid_ALL("OutSource", GridView3);
        //    Fill_Grid_NOT_APRV_ALL("ALL");
        //}
        //else
        //{
        //    //Alert.Show(" هناك خطأ في عملية الموافقة برجاء الاتصال بادارة النظم ");
        //    Show_Modal_Message("هناك خطأ في عملية الموافقة برجاء الاتصال بادارة النظم ");
        //    return;
        //}
      
    }
    protected void GridView3_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        // for outsource
        //Determine the RowIndex of the Row whose Button was clicked.
        int rowIndex = Convert.ToInt32(e.CommandArgument);
        //Reference the GridView Row.
        GridViewRow row = GridView3.Rows[rowIndex];

        //Fetch value of Name.
        string Emp_Code = (row.FindControl("lblemp_no1") as Label).Text;
        string Emp_Name = (row.FindControl("lblemp_name1") as Label).Text;
        string Job_code = (row.FindControl("lbljob_code0") as Label).Text;
        string Status = (row.FindControl("lblstatus1") as Label).Text;
        string Level = (row.FindControl("lbllevel1") as Label).Text;
        Year = Session["OPEN_YEAR"].ToString();
        Version = Session["OPEN_VERSION"].ToString();
        //*********************************************************************
        Session["ASS_APPAISAL_TYPE"] = ser.GetAppaisalType(Session["OPEN_YEAR"].ToString(), Session["OPEN_VERSION"].ToString(), Job_code).ToString();
        Session["ASS_Comp_Weight"] = ser.Get_Ass_Job_Comp_Weight(Session["OPEN_YEAR"].ToString(), Session["OPEN_VERSION"].ToString(), Job_code);
        Session["ASS_Obj_Weight"] = ser.Get_Ass_Job_Obj_Weight(Session["OPEN_YEAR"].ToString(), Session["OPEN_VERSION"].ToString(), Job_code);
        Session["ASS_EMP_ID"] = Emp_Code;
        Session["ASS_APRV_LVL"] = Level;
        //*********************************************************************
        if (e.CommandName == "Display")
        {
            Session["ASS_EMP_ID"] = Emp_Code;
            Session["ASS_EMP_NAME"] = Emp_Name;
            Session["ASS_JOB_ID"] = Job_code;
            Session["ASS_STATUS"] = Status;
            Session["Emp_Level"] = Level;
            Session["ASS_DISPLAY_ONLY"] = "Y";

            Response.Redirect("~/FinalVersion/NewAssesment/dynamic_assesment.aspx");
        }
        else
            if (e.CommandName == "APPROVE")
            {
                //if (Session["ASS_APRV_LVL"].ToString().Trim() == "2")
                //{
                //    Show_Modal_COnfirm("هل تريد الموافقة علي هذا التقييم ؟");
                //}
                //else
                //{
                //    Show_Modal_Message(" لا يمكنك الموافقة علي التقييم ,, المدير المباشر فقط ");
                //    return;
                //}

                int APRV = ser.Make_ASS_Approve(Emp_Code, Session["OPEN_YEAR"].ToString(), Session["OPEN_VERSION"].ToString(), Session["Logged_User"].ToString());
                if (APRV == 1)
                {
                    Fill_Grid_ALL("EGIC", GridView1);
                    Fill_Grid_ALL("OutSource", GridView3);
                    Fill_Grid_NOT_APRV_ALL("ALL");
                    EGIC_GridviewLoop();
                    Outsource_GridviewLoop();
                   // Show_Modal_Message("تمت الموافقة بنجاح ");
                    RadNotification1.Text = "تمت الموافقة بنجاح";
                    RadNotification1.Title = "نظام التقييمات ";
                    RadNotification1.TitleIcon = "info";
                    RadNotification1.ContentIcon = "info";
                    RadNotification1.Show();

                    
                }
                else
                {
                    //Show_Modal_Message(" هناك مشكله في عمليه الموافقة ");
                    RadNotification1.Text = "هناك مشكله في عمليه الموافقة";
                    RadNotification1.Title = "نظام التقييمات ";
                    RadNotification1.TitleIcon = "info";
                    RadNotification1.ContentIcon = "info";
                    RadNotification1.Show();
                }
            }
    }
    protected void btn_approve_outsource_Click(object sender, EventArgs e)
    {
        // approve all out source
        int APRV = 0;
        int APRV_STS = 0;
        int APRV_STS_LVL = 0;
        string confirmValue = Request.Form["confirm_value"];
        if (confirmValue == "No")
        {

        }
        else
        {
            foreach (GridViewRow row in GridView3.Rows)
            {
                Label chk = row.FindControl("lblstatus1") as Label;

                if (chk != null && chk.Text != "2")
                {
                    string emp_no = (row.Cells[2].FindControl("lblemp_no1") as Label).Text;
                    string AprvLevel = (row.Cells[10].FindControl("lbllevel1") as Label).Text;
                    //if (AprvLevel.Trim() == "2")
                    //{
                    APRV = ser.Make_ASS_Approve(emp_no, Session["OPEN_YEAR"].ToString(), Session["OPEN_VERSION"].ToString(), Session["Logged_User"].ToString());
                    if (APRV == 0) { APRV_STS++; }
                    //}
                    //else
                    //{
                    //    APRV_STS_LVL++;
                    //}

                }
            }

            if (APRV == 1 && APRV_STS == 0)
            {

                Fill_Grid_ALL("EGIC", GridView1);
                Fill_Grid_ALL("OutSource", GridView3);
                Fill_Grid_NOT_APRV_ALL("ALL");
                EGIC_GridviewLoop();
                Outsource_GridviewLoop();
                // Show_Modal_Message("تمت الموافقة بنجاح ");
                RadNotification1.Text = "تمت الموافقة بنجاح";
                RadNotification1.Title = "نظام التقييمات ";
                RadNotification1.TitleIcon = "info";
                RadNotification1.ContentIcon = "info";
                RadNotification1.Show();

                
            }
            else
            {
                RadNotification1.Text = " جميع التقييمات تمت الموافقه عليها ";
                //RadNotification1.Text = "توجد مشكلة في التقييمات برجاء الرجوع الي مدير النظام";
                RadNotification1.Title = "نظام التقييمات ";
                RadNotification1.TitleIcon = "info";
                RadNotification1.ContentIcon = "info";
                RadNotification1.Show();
                return;
            }
        }
    }
}
