﻿using System;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Final_Version_NewAssesment_dynamic_assesment : System.Web.UI.Page
{
    AppService _appService = new AppService();
    DataTable compIndicatoresDT = new DataTable();

    private string _year;
    private string _jobCode;
    private string _version;
    private string _employeeNo;
    private string _employeeId;
    private string _employeeName;
    private AppraisalTypes _appraisalType;

    public string jsScript = string.Empty;

    protected void Page_Init(object sender, EventArgs e)
    {
        try
        {
            if (Request.QueryString["sec_path"] != null)
            {

                if (Session["OPEN_YEAR"] != null)
                    _year = Session["OPEN_YEAR"].ToString();

                if (Session["OPEN_VERSION"] != null)
                    _version = Session["OPEN_VERSION"].ToString();

                if (Session["ASS_EMP_ID"] != null)
                    _employeeId = Session["ASS_EMP_ID"].ToString();
                else
                {
                    _employeeId = Base64DecodingMethod(Request.QueryString["sec_path"].ToString());
                    Session["ASS_EMP_ID"] = _employeeId;
                }

                if (Session["EMP_NO"] != null)
                    _employeeNo = Session["EMP_NO"].ToString();

                if (Session["ASS_JOB_ID"] != null)
                    _jobCode = Session["ASS_JOB_ID"].ToString();
                else
                {
                    _jobCode = _appService.Get_Employee_Selected_DATA("Job_code", _employeeId, _employeeNo, _year, _version);
                    Session["ASS_JOB_ID"] = _jobCode;
                }

                if (Session["ASS_APPAISAL_TYPE"] != null && int.TryParse(Session["ASS_APPAISAL_TYPE"].ToString(), out int appType))
                    _appraisalType = (AppraisalTypes)appType;
                else
                {
                    _appraisalType = _appService.GetAppaisalType(_year, _version, _jobCode);
                    Session["ASS_APPAISAL_TYPE"] = (int)_appraisalType;
                }

                if (Session["ASS_EMP_NAME"] != null)
                    _employeeName = Session["ASS_EMP_NAME"].ToString();
                else
                {
                    _employeeName = _appService.Get_Employee_Selected_DATA("Emp_Name", _employeeId, _employeeNo, _year, _version);
                    Session["ASS_EMP_NAME"] = _employeeName;
                }
                Session["ASS_STATUS"] = _appService.Get_Employee_Selected_DATA("Status", _employeeId, _employeeNo, _year, _version);
                Session["Emp_Level"] = _appService.Get_Employee_Selected_DATA("Level", _employeeId, _employeeNo, _year, _version);
                Session["ASS_Comp_Weight"] = _appService.Get_Ass_Job_Comp_Weight(_year, _version, _jobCode);
                Session["ASS_Obj_Weight"] = _appService.Get_Ass_Job_Obj_Weight(_year, _version, _jobCode);

                if (Session["Emp_Level"].ToString().Trim() == "2")
                {
                    Session["ASS_DISPLAY_ONLY"] = "N";
                }
                else
                {
                    Session["ASS_DISPLAY_ONLY"] = "Y";
                }
            }

            string disableAppStr = ConfigurationManager.AppSettings["DisableApp"];
            if (!bool.TryParse(disableAppStr, out bool disableApp))
                disableApp = false;
            if (disableApp)
            {
                btn_save.Enabled = false;
                btn_calc.Enabled = false;
                txtNotes.Enabled = false;
                txtDevProgram.Enabled = false;
                panelCompetencies.Enabled = false;
                panelObjectives.Enabled = false;
                panelNote.Enabled = false;
            }

            if (Session["ASS_STATUS"].ToString().Trim() == "2" || Session["ASS_DISPLAY_ONLY"].ToString().Trim() == "Y")  // approval tacked
            {
                btn_save.Visible = false;
                btn_calc.Visible = false;
                txtNotes.Enabled = false;
                txtDevProgram.Enabled = false;
            }

            panelObjectives.Visible = false;
            panelNextYearObjectives.Visible = false;
            panelCompetencies.Visible = false;
            panelNote.Visible = true;
            //divObjectiveCompetencyScale.Visible = false;
            Page.ClientScript.RegisterStartupScript(this.GetType(), "Hidelabel", "Hidelabel();", true);
        }
        catch
        {
            Response.Redirect("~/Setting/Page_Not_Found.aspx");
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {

        if (!IsPostBack)
        {
            if ((Session["Emp_Level"].ToString().Trim() != "2") && (Session["ASS_STATUS"].ToString().Trim() == "0" || Session["ASS_STATUS"].ToString().Trim() == "1"))
            {
                Show_Modal_Message(" لا يمكنك إدخال التقييم لهذا الموظف , المستوي المباشر فقط ");
                return;
            }

            if (string.IsNullOrWhiteSpace(_year) || string.IsNullOrWhiteSpace(_version))
            {
                Show_Modal_Message(" لا توجد سنه أو اصدار مفتوح للتقييم  ");
                panelObjectives.Visible = false;
                panelNextYearObjectives.Visible = false;
                panelCompetencies.Visible = false;
                return;
            }

        }
        FillPageData();
    }

    protected void Page_LoadComplete(object sender, EventArgs e)
    {
        Expand_GridTextBoxs();
        if (_appraisalType == AppraisalTypes.ObjectivesAndComptences || _appraisalType == AppraisalTypes.ObjectivesOnly)
            Expand_GridTextBoxs_into_printing();
    }

    protected void btn_calc_Click(object sender, EventArgs e)
    {
        SaveData(SaveTypes.Save);
    }

    protected void btn_save_Click(object sender, EventArgs e)
    {
        SaveData(SaveTypes.Draft);
    }

    /*********************/
    // Methods

    private void FillPageData()
    {
        lbl_year.Text = _year;
        lbl_version.Text = _version;
        lbl_emp_no.Text = _employeeId;
        lbl_emp_name.Text = _employeeName;
        
        try
        {
            var empInfo = _appService.Get_Emp_Location_Dep(_employeeId);
            if (empInfo != null && empInfo.Rows.Count > 0)
            {
                lblLocation.Text = empInfo.Rows[0]["LOCATION_DESC"].ToString();
                lblDepartment.Text = empInfo.Rows[0]["SECTION_NAME"].ToString();
            }

            var empAppraisalHeader = _appService.GetEmployeeAppraisalHeader(_year, _version, _employeeId);
            if (empAppraisalHeader != null && empAppraisalHeader.Rows.Count > 0)
            {
                /*
                        ASSESSOR_NAME, 
                        ASS_DATE, 
                        RESULT, 
                        APPROVED, 
                        SAVE_TYPE, 
                        NYO_COMMENT, 
                        COMP_RESULT, 
                        OBJ_RESULT,
                        EMP_FINAL,
                        DEV_TRN_PLAN
                */
                var empAppraisalInfo = empAppraisalHeader.Rows[0];
                string result = empAppraisalInfo["RESULT"].ToString();
                lbl_result.Text = result ?? "0";
                if (double.TryParse(result, out double _result))
                    lbl_grad.Text = Return_Grad(_result);

                if (!double.TryParse(Session["ASS_Comp_Weight"].ToString(), out double compWeight))
                    compWeight = 30;
                compWeight /= 100;

                if (!double.TryParse(Session["ASS_Obj_Weight"].ToString(), out double objectiveWeight))
                    objectiveWeight = 70;
                objectiveWeight /= 100;

                string empTotalComptencesScoreStr = "0", empTotalObjectivesScoreStr = "0";
                if (double.TryParse(empAppraisalInfo["COMP_RESULT"].ToString(), out double empTotalComptencesScore))
                    empTotalComptencesScoreStr = Math.Round((empTotalComptencesScore * compWeight), 1).ToString();

                if (double.TryParse(empAppraisalInfo["OBJ_RESULT"].ToString(), out double empTotalObjectivesScore))
                    empTotalObjectivesScoreStr = Math.Round((empTotalObjectivesScore * objectiveWeight), 1).ToString();

                lbl_comp_result.Text = empTotalComptencesScoreStr;
                lbl_obj_result.Text = empTotalObjectivesScoreStr;

                if (!IsPostBack)
                {
                    txtDevProgram.Text = empAppraisalInfo["DEV_TRN_PLAN"].ToString();
                    txtNotes.Text = empAppraisalInfo["NYO_COMMENT"].ToString();
                }
            }
            lbl_comp_rslt_title.Visible = false;
            lbl_comp_result.Visible = false;

            lbl_obj_rslt_title.Visible = false;
            lbl_obj_result.Visible = false;

            switch (_appraisalType)
            {
                case AppraisalTypes.None:
                    break;
                case AppraisalTypes.ObjectivesOnly:
                    lbltype.Text = "O";

                    panelNote.Visible = true;
                    //divObjectiveCompetencyScale.Visible = false;
                    panelObjectives.Visible = true;
                    panelNextYearObjectives.Visible = true;

                    lbl_obj_rslt_title.Visible = true;
                    lbl_obj_result.Visible = true;

                    Display_Objectives_Data();
                    Display_Next_Year_Objectives_Data("O");
                    Display_Next_Year_Objectives_Data("D");

                    break;
                case AppraisalTypes.ComptencesOnly:
                    lbltype.Text = "C";

                    panelCompetencies.Visible = true;
                    //divObjectiveCompetencyScale.Visible = true;

                    lbl_comp_rslt_title.Visible = true;
                    lbl_comp_result.Visible = true;

                    Display_Comptences_Data();

                    break;
                case AppraisalTypes.ObjectivesAndComptences:
                    lbltype.Text = "A";

                    panelCompetencies.Visible = true;
                    panelNote.Visible = true;
                    //divObjectiveCompetencyScale.Visible = true;
                    panelObjectives.Visible = true;
                    panelNextYearObjectives.Visible = true;

                    lbl_comp_rslt_title.Visible = true;
                    lbl_comp_result.Visible = true;

                    lbl_obj_rslt_title.Visible = true;
                    lbl_obj_result.Visible = true;

                    Display_Objectives_Data();
                    Display_Comptences_Data();
                    Display_Next_Year_Objectives_Data("O");
                    Display_Next_Year_Objectives_Data("D");

                    break;
                default:
                    break;
            }
        }
        catch
        {
            lblDepartment.Text = "غير محدد";
            lblLocation.Text = "غير محدد";
        }

    }

    private string Base64DecodingMethod(string data)
    {
        byte[] encodedDataAsBytes = System.Convert.FromBase64String(data);
        string returnValue = System.Text.Encoding.UTF8.GetString(encodedDataAsBytes);
        return returnValue;
    }

    private string Return_Grad(double degree)
    {
        string ret_grad;
        if (degree >= 4.6)
        {
            ret_grad = "إستثنائي";
        }
        else if (degree >= 3.6 && degree <= 4.5)
        {
            ret_grad = "يفوق التوقعات";
        }
        else if (degree >= 2.6 && degree <= 3.5)
        {
            ret_grad = "يحقق التوقعات";
        }
        else if (degree >= 1.6 && degree <= 2.5)
        {
            ret_grad = "أقل من التوقعات ";
        }
        else if (degree <= 1.5)
        {
            ret_grad = "غير مرضي";
        }
        else
        {
            ret_grad = " -- ";
        }
        return ret_grad;
    }

    private void Expand_GridTextBoxs()
    {
        Final_Version_Objective_Control OBJC = (Final_Version_Objective_Control)panelObjectives.FindControl("OBJ_CONTROL_DATA");
        //*************
        Final_Version_NY_Objectives NYOBJC = (Final_Version_NY_Objectives)panelObjectives.FindControl("OBJ_CONTROL_DATA_NY");
        Final_Version_Group_questions GQT = (Final_Version_Group_questions)panelCompetencies.FindControl("Full_Comptences");
        //**************
        string script = "window.onload = function() { ";
        if (OBJC != null && OBJC.gvObjectives != null)
            foreach (GridViewRow row in OBJC.gvObjectives.Rows)
            {
                string OclintID = ((TextBox)row.FindControl("txtObjective")).ClientID;
                //string NclintID = ((TextBox)row.FindControl("txtResult")).ClientID;
                string DclintID = ((TextBox)row.FindControl("txtDevelopmentPoints")).ClientID;
                string TclintID = ((TextBox)row.FindControl("txtTrainingCourses")).ClientID;
                script = script + " AutoExpand(" + OclintID + "); " + " AutoExpand(" + DclintID + "); " + " AutoExpand(" + TclintID + "); ";
            }

        if (GQT != null && GQT.Romko_GV != null)
            foreach (GridViewRow row in GQT.Romko_GV.Rows)
            {
                string DclintID = ((TextBox)row.FindControl("txtDevelopmentPoints")).ClientID;
                string TclintID = ((TextBox)row.FindControl("txtTrainingCourses")).ClientID;
                script = script + " AutoExpand(" + DclintID + "); " + " AutoExpand(" + TclintID + "); ";
            }
        if (NYOBJC != null && NYOBJC.GV_Objectes != null)
            foreach (GridViewRow row in NYOBJC.GV_Objectes.Rows)
            {
                string OclintID = ((TextBox)row.FindControl("txtNYObjective")).ClientID;
                script = script + " AutoExpand(" + OclintID + "); ";
            }

        script = script + " AutoExpand_comments(" + txtNotes.ClientID + ");" + " };";
        Page.ClientScript.RegisterStartupScript(this.GetType(), "AutoExpand", script, true);

    }

    private void Expand_GridTextBoxs_into_printing()
    {
        Final_Version_Objective_Control OBJC = (Final_Version_Objective_Control)panelObjectives.FindControl("OBJ_CONTROL_DATA");
        //*************
        jsScript = " window.onload = function() { ";
        if (OBJC != null)
        {
            foreach (GridViewRow row in OBJC.gvObjectives.Rows)
            {
                string OclintID = ((TextBox)row.FindControl("txtObjective")).ClientID;
                //string NclintID = ((TextBox)row.FindControl("txtResult")).ClientID;
                string DclintID = ((TextBox)row.FindControl("txtDevelopmentPoints")).ClientID;
                string TclintID = ((TextBox)row.FindControl("txtTrainingCourses")).ClientID;
                jsScript += " AutoExpand(" + OclintID + "); " + " AutoExpand(" + DclintID + "); " + " AutoExpand(" + TclintID + "); ";
            }
        }
        jsScript += " AutoExpand(" + txtNotes.ClientID + ");" + " };";
    }

    private void Display_Next_Year_Objectives_Data(string Type)
    {
        try
        {
            DataTable odt = new DataTable();
            Final_Version_NY_Objectives OBJ = (Final_Version_NY_Objectives)Page.LoadControl("~/FinalVersion/MyControles/NY_Objectives.ascx");
            if (Type == "O") // Operational objectives 
            {
                OBJ.ID = "OBJ_CONTROL_DATA_NY";
                OBJ.Grid_Title = "Operational objectives";
            }
            else if (Type == "D") // Developmental objectives 
            {
                OBJ.ID = "OBJ_CONTROL_DATA_NY2";
                OBJ.Grid_Title = "Developmental objectives";
            }
            odt = _appService.Bind_ASS_EMP_NY_Objectives_ByType_GRID(_employeeId, _year, _version, Type);
            if (Session["ASS_STATUS"].ToString().Trim() == "2" || Session["ASS_DISPLAY_ONLY"].ToString().Trim() == "Y")
            {
                OBJ.Static_Mode = "Y";
            }
            if (odt.Rows.Count != 0)
            {
                OBJ.Display_Mode = "Y"; // display mode
                OBJ.dtCurrentNYObjectivesTable = odt;
            }
            OBJ.Control_Type = Type;
            panelNextYearObjectives.Controls.Add(OBJ);
            
        }
        catch
        { }
    }

    private void Display_Objectives_Data()
    {
        try
        {
            DataTable odt = new DataTable();
            Final_Version_Objective_Control OBJ = (Final_Version_Objective_Control)Page.LoadControl("~/FinalVersion/MyControles/Objective_Control.ascx");
            OBJ.ID = "OBJ_CONTROL_DATA";
            odt = _appService.Bind_ASS_EMP_Objectives_GRID(_employeeId, _year, _version);
            if (odt.Rows.Count == 0)
            {
                odt = _appService.Mid_Bind_ASS_EMP_Objectives_GRID(_employeeId, _year, _version);
            }
            string Static_Target = _appService.Emp_Static_Target(_employeeId);

            if (Static_Target.Trim() == "1" || Session["ASS_STATUS"].ToString().Trim() == "2" || Session["ASS_DISPLAY_ONLY"].ToString().Trim() == "Y")
            {
                OBJ.staticMode = "Y";
            }
            if (odt.Rows.Count != 0)
            {
                OBJ.displayMode = "Y"; // display mode
                OBJ.dtCurrentObjectivesTable = odt;
            }
            panelObjectives.Controls.Add(OBJ);
        }
        catch (Exception ex)
        {
            string error = ex.Message;
        }
    }

    private void Display_Comptences_Data()
    {
        try
        {
            compIndicatoresDT.Rows.Clear();

            compIndicatoresDT = _appService.Bind_Final_ASS_EMP_Indicatores_GRID(_employeeId, _jobCode, _year, _version);
            if (compIndicatoresDT.Rows.Count == 0) 
                compIndicatoresDT = _appService.Get_Indicatores(_jobCode, _year, _version);

            Final_Version_Group_questions GQ = (Final_Version_Group_questions)Page.LoadControl("~/FinalVersion/MyControles/Group_questions.ascx");
            if (Session["ASS_STATUS"].ToString().Trim() == "2" || Session["ASS_DISPLAY_ONLY"].ToString().Trim() == "Y")
            { GQ.displayMode = "Y"; }  // Display Mode False
            else
            { GQ.displayMode = "N"; }  // display mode true
            GQ.compIndicatoresDT = compIndicatoresDT;
            GQ.ID = "Full_Comptences";
            //string Static_Comp= _appService.Emp_Static_Comp(_employeeId);

            //if (Static_Comp.Trim() == "1" || Session["ASS_STATUS"].ToString().Trim() == "2" || Session["ASS_DISPLAY_ONLY"].ToString().Trim() == "Y")
            //{
            //    GQ.staticMode = "Y";
            //}
            panelCompetencies.Controls.Add(GQ);
            CalculateComptences();
        }
        catch (Exception ex)
        {
            string error = ex.Message;
        }
    }

    private void SaveData(SaveTypes saveType)
    {

        if (_appraisalType == AppraisalTypes.None)
        {
            Show_Modal_Message("خطأ: غير قادر على تحديد نوع التقييم");
            return;
        }


        double empTotalObjectivesScore = 0;
        double empTotalComptencesScore = 0;
        DataTable dtObjectives = new DataTable();
        DataTable dtComptences = new DataTable();
        DataTable dtNextYearOperationalObjectives = new DataTable();
        DataTable dtNextYearDevelopmentalObjectives = new DataTable();
        if (_appraisalType == AppraisalTypes.ObjectivesAndComptences || _appraisalType == AppraisalTypes.ObjectivesOnly)
        {
            // objectives grid view
            Final_Version_Objective_Control OBJC = (Final_Version_Objective_Control)panelObjectives.FindControl("OBJ_CONTROL_DATA");
            //var gvObjectives = panelObjectives.Controls[0];
            dtObjectives.Columns.Add("obj_seq");
            dtObjectives.Columns.Add("obj_desc");
            dtObjectives.Columns.Add("obj_weight");
            dtObjectives.Columns.Add("obj_actual");
            dtObjectives.Columns.Add("obj_notes");
            dtObjectives.Columns.Add("develop_points");
            dtObjectives.Columns.Add("train_prog");
            dtObjectives.Rows.Clear();
            int obj_seq = 0;
            if (saveType == SaveTypes.Save && OBJC.gvObjectives.Rows.Count == 0)
            {
                Show_Modal_Message("يجب إدخال هدف واحد على الأقل");
                return;
            }

            double totalObjectivesWeight = 0;
            //if (gvObjectives != null && gvObjectives.gvObjectives != null)
                foreach (GridViewRow row in OBJC.gvObjectives.Rows)
                {
                    obj_seq++;
                    string obj_desc = ((TextBox)row.FindControl("txtObjective")).Text.Trim();
                    string obj_weight = ((TextBox)row.FindControl("txtWeight")).Text.Trim();
                    string obj_actual = ((DropDownList)row.FindControl("txtFinalEvaluation")).SelectedValue;
                    string develop_points = ((TextBox)row.FindControl("txtDevelopmentPoints")).Text.Trim();
                    string train_prog = ((TextBox)row.FindControl("txtTrainingCourses")).Text.Trim();

                    // validate objectives
                    if (saveType == SaveTypes.Save)
                    {
                        if (string.IsNullOrWhiteSpace(obj_desc) || string.IsNullOrWhiteSpace(obj_weight) || string.IsNullOrWhiteSpace(obj_actual))
                        {
                            Show_Modal_Message("برجاء ادخال الحقول الفارغة بالأهداف ");
                            return;
                        }
                        if (!int.TryParse(obj_actual, out int _actualDegree) || _actualDegree == 0)
                        {
                            Show_Modal_Message("يجب اختيار التقييم النهائي لكل هدف");
                            return;
                        }
                        if ((_actualDegree == 1 || _actualDegree == 4 || _actualDegree == 5) && string.IsNullOrWhiteSpace(develop_points))
                        {
                            Show_Modal_Message("يجب ادخال النقاط التطويرية للأهداف التي يساوي فيها التقييم النهائي لـ 1 أو 4 أو 5");
                            return;
                        }
                    if (double.TryParse(obj_weight, out double _weight) && _weight > 0)
                    {
                        totalObjectivesWeight += _weight;
                         totalObjectivesWeight = Math.Round(totalObjectivesWeight, 2);
                    }
                    else
                    {
                        Show_Modal_Message("يجب ادخال قيمة صحيحة اكبر من 0 للوزن في الأهداف");
                        return;
                    }
                        empTotalObjectivesScore += Math.Round((_actualDegree * (_weight / 100)), 1);
                    }

                    dtObjectives.NewRow();
                    dtObjectives.Rows.Add(obj_seq, obj_desc, obj_weight, obj_actual, string.Empty, develop_points, train_prog);
                }

            if (saveType == SaveTypes.Save && totalObjectivesWeight != 100)
            {
                Show_Modal_Message("مجموع الاوزان في الأهداف لابد ان يساوي 100");
                return;
            }

            // next year operational objectives
            Final_Version_NY_Objectives gvNextYearOperationalObjectives = (Final_Version_NY_Objectives)panelNextYearObjectives.FindControl("OBJ_CONTROL_DATA_NY");
            dtNextYearOperationalObjectives.Columns.Add("obj_seq");
            dtNextYearOperationalObjectives.Columns.Add("obj_desc");
            dtNextYearOperationalObjectives.Columns.Add("obj_weight");
            dtNextYearOperationalObjectives.Columns.Add("obj_quart");
            dtNextYearOperationalObjectives.Rows.Clear();
            obj_seq = 0;


            if (saveType == SaveTypes.Save && gvNextYearOperationalObjectives.GV_Objectes.Rows.Count == 0)
            {
                Show_Modal_Message("(Operational) يجب ادخال اهداف العام المقبل");
                return;
            }

            double totalNextYearObjectivesWeight = 0;
            foreach (GridViewRow row in gvNextYearOperationalObjectives.GV_Objectes.Rows)
            {
                obj_seq++;
                string seqc = obj_seq.ToString();
                string objc = ((TextBox)row.FindControl("txtNYObjective")).Text.Trim();
                string weight = ((TextBox)row.FindControl("txtNYWeight")).Text.Trim();
                string quarter = ((DropDownList)row.FindControl("ddlDate")).SelectedValue;

                // validate next year operational objectives
                if (saveType == SaveTypes.Save)
                {
                    if (gvNextYearOperationalObjectives.GV_Objectes.Rows.Count == 1 && string.IsNullOrWhiteSpace(objc) && (string.IsNullOrEmpty(weight) || weight == "0") && (string.IsNullOrWhiteSpace(quarter) || quarter == "0"))
                    {
                        Show_Modal_Message("(Operational) يجب ادخال اهداف العام المقبل");
                        return;
                    }

                    if (string.IsNullOrEmpty(objc) || (string.IsNullOrEmpty(weight) || weight == "0") || (string.IsNullOrWhiteSpace(quarter) || quarter == "0"))
                    {
                        Show_Modal_Message("(Operational) يجب ادخال البيانات الناقصة لأهداف العام المقبل");
                        return;
                    }

                    double _weight;
                    if (double.TryParse(weight, out _weight) && _weight > 0)
                        totalNextYearObjectivesWeight += _weight;
                    else
                    {
                        Show_Modal_Message("(Operational) يجب ادخال قيمة صحيحة اكبر من 0 للوزن");
                        return;
                    }
                }

                dtNextYearOperationalObjectives.NewRow();
                dtNextYearOperationalObjectives.Rows.Add(seqc, objc, weight, quarter);
            }

            // next year devleopmental objectives
            Final_Version_NY_Objectives gvNextYearDevelopmentalObjectives = (Final_Version_NY_Objectives)panelNextYearObjectives.FindControl("OBJ_CONTROL_DATA_NY2");
            dtNextYearDevelopmentalObjectives.Columns.Add("obj_seq");
            dtNextYearDevelopmentalObjectives.Columns.Add("obj_desc");
            dtNextYearDevelopmentalObjectives.Columns.Add("obj_weight");
            dtNextYearDevelopmentalObjectives.Columns.Add("obj_quart");
            dtNextYearDevelopmentalObjectives.Rows.Clear();
            obj_seq = 0;
            foreach (GridViewRow row in gvNextYearDevelopmentalObjectives.GV_Objectes.Rows)
            {
                obj_seq++;
                string seqc = obj_seq.ToString();
                string objc = ((TextBox)row.FindControl("txtNYObjective")).Text.Trim();
                string weight = ((TextBox)row.FindControl("txtNYWeight")).Text.Trim();
                string quarter = ((DropDownList)row.FindControl("ddlDate")).SelectedValue;

                // validate next year devleopmental objectives
                if (saveType == SaveTypes.Save)
                {
                    double _weight = 0;
                    if (string.IsNullOrEmpty(objc) && (string.IsNullOrEmpty(weight) || weight == "0") && (string.IsNullOrEmpty(quarter) || quarter == "0"))
                        continue;
                    else if (string.IsNullOrEmpty(objc) || (string.IsNullOrEmpty(weight) || weight == "0") || (string.IsNullOrEmpty(quarter) || quarter == "0"))
                    {
                        Show_Modal_Message("(Developmental) يجب ادخال البيانات الناقصة لأهداف العام المقبل");
                        return;
                    }

                    if (double.TryParse(weight, out _weight) && _weight > 0)
                        totalNextYearObjectivesWeight += _weight;
                    else
                    {
                        Show_Modal_Message("(Developmental) يجب ادخال قيمة صحيحة اكبر من 0 للوزن لأهداف العام المقبل");
                        return;
                    }
                }

                dtNextYearDevelopmentalObjectives.NewRow();
                dtNextYearDevelopmentalObjectives.Rows.Add(seqc, objc, weight, quarter);
            }


            if (saveType == SaveTypes.Save && totalNextYearObjectivesWeight != 100)
            {
                Show_Modal_Message("مجموع الاوزان لابد ان يكون 100 لأهداف العام المقبل");
                return;
            }

        }
        if (_appraisalType == AppraisalTypes.ObjectivesAndComptences || _appraisalType == AppraisalTypes.ComptencesOnly)
        {
            dtComptences.Columns.Add("comp_code");
            dtComptences.Columns.Add("comp_result");
            dtComptences.Columns.Add("develop_points");
            dtComptences.Columns.Add("train_prog");
            dtComptences.Rows.Clear();
            int comptencesCount = 0;
            foreach (Control c in panelCompetencies.Controls)
            {
                if (c is Final_Version_Group_questions)
                {
                    Final_Version_Group_questions GQTT = (Final_Version_Group_questions)panelCompetencies.FindControl("Full_Comptences");
                    comptencesCount = GQTT.Romko_GV.Rows.Count;
                    foreach (GridViewRow row in GQTT.Romko_GV.Rows)
                    {
                        string comp_code = ((Label)row.FindControl("lblCompcode")).Text;
                        string answer = ((DropDownList)row.FindControl("ddlanswers")).SelectedValue;
                        string developmentPoints = ((TextBox)row.FindControl("txtDevelopmentPoints")).Text.Trim();
                        string trainingCourses = ((TextBox)row.FindControl("txtTrainingCourses")).Text.Trim();

                        // validate comptences
                        if (saveType == SaveTypes.Save)
                        {
                            if (string.IsNullOrWhiteSpace(answer) || !int.TryParse(answer, out int answerValue) || answerValue == 0)
                            {
                                Show_Modal_Message("يجب اختيار تقييم الكفائة");
                                return;
                            }
                            if ((answerValue == 1 || answerValue == 5) && string.IsNullOrEmpty(developmentPoints))
                            {
                                Show_Modal_Message("يجب ادخال النقاط التطويرية للكفاءات التى تقيمها يساوي 1 او يساوي 5");
                                return;
                            }
                            empTotalComptencesScore += answerValue;
                        }

                        dtComptences.NewRow();
                        dtComptences.Rows.Add(comp_code, answer, developmentPoints, trainingCourses);
                    }
                }
            }

            if (saveType == SaveTypes.Save)
                empTotalComptencesScore = Math.Round((empTotalComptencesScore / comptencesCount), 1);

        }

        double compWeight = 0, objectiveWeight = 0;
        double empTotalPerformanceResult = 0;
        if (saveType == SaveTypes.Save)
        {

            if (!double.TryParse(Session["ASS_Comp_Weight"].ToString(), out compWeight))
                compWeight = 30;
            compWeight /= 100;

            if (!double.TryParse(Session["ASS_Obj_Weight"].ToString(), out objectiveWeight))
                objectiveWeight = 70;
            objectiveWeight /= 100;

            empTotalPerformanceResult = Math.Round((Math.Round((empTotalObjectivesScore * objectiveWeight), 1) + Math.Round((empTotalComptencesScore * compWeight), 1)), 1);
        }
        string empTrainingProgram = txtDevProgram.Text;
        if (saveType == SaveTypes.Save && empTotalPerformanceResult <= 2.5 && string.IsNullOrWhiteSpace(empTrainingProgram))
        {
            Show_Modal_Message("يجب ادخال خطة تطوير الأداء و البرامج التدريبية اللازمة للموظف");
            return;
        }
        string notes = txtNotes.Text;
        string assessor_name = Session["Logged_User"].ToString();
        bool dataSavedsuccessfully = _appService.SaveEmplyeeAppraisal(_employeeId, _year, _version, saveType, assessor_name, empTotalComptencesScore, empTotalObjectivesScore, empTotalPerformanceResult, notes, empTrainingProgram, dtObjectives, dtComptences, dtNextYearOperationalObjectives, dtNextYearDevelopmentalObjectives);
        if (dataSavedsuccessfully)
        {
            if (saveType == SaveTypes.Save)
            {
                Session["COMP_RESULT"] = empTotalComptencesScore;
                Session["OBJ_RESULT"] = empTotalObjectivesScore;
                empTotalPerformanceResult = Math.Round(empTotalPerformanceResult, 1);
                lbl_result.Text = Math.Round(empTotalPerformanceResult, 1).ToString();
                lbl_comp_result.Text = Math.Round((empTotalComptencesScore * compWeight), 1).ToString();
                lbl_obj_result.Text = Math.Round((empTotalObjectivesScore * objectiveWeight), 1).ToString();
                lbl_grad.Text = Return_Grad(empTotalPerformanceResult);
                Session["ASS_STATUS"] = "1"; //تم التسجيل
            }
            else
            {
                lbl_result.Text = "0";
                lbl_obj_result.Text = "0";
                lbl_comp_result.Text = "0";
                lbl_grad.Text = "0";
                Session["ASS_STATUS"] = "0"; //لم يتم التسجيل
            }
            Show_Modal_Message("تم حفظ بيانات التقييم بنجاح ");
        }
        else
        {
            lbl_result.Text = "0";
            lbl_obj_result.Text = "0";
            lbl_comp_result.Text = "0";
            lbl_grad.Text = "0";
            Session["ASS_STATUS"] = "0"; //لم يتم التسجيل

            Show_Modal_Message("توجد مشكلة في عمليه حفظ البيانات");

        }
        CalculateComptences();
        CalculateObjectives();
    }

    private void CalculateComptences()
    {
        //double comptencySum = 0;
        double comptencyTotal = 0;
        double res = 0;
        int comptencyIndex = 0;
        int parentCode = 0;
        int comptencyDetailCount = 0;
        int comptencesCount = 0;
        try
        {
            foreach (Control c in panelCompetencies.Controls)
            {
                if (c is Final_Version_Group_questions)
                {
                    Final_Version_Group_questions GQTT = (Final_Version_Group_questions)panelCompetencies.FindControl("Full_Comptences");
                    comptencesCount += GQTT.Romko_GV.Rows.Count;

                    foreach (GridViewRow row in GQTT.Romko_GV.Rows)
                    {
                        DropDownList ddlAnswers = ((DropDownList)row.FindControl("ddlanswers"));
                        string comp_code = ((Label)row.FindControl("lblCompcode")).Text;
                        string parentCodeStr = ((Label)row.FindControl("lblParentCode")).Text;
                        string answer = "0";
                        if (ddlAnswers.SelectedIndex != 0)
                        {
                            answer = ddlAnswers.SelectedValue;
                            //comptencySum += Ret_Comp_value(answer);
                            if (int.TryParse(answer, out int _answer))
                                comptencyTotal += _answer;
                        }
                        comptencyIndex++;
                        int _parentCode = int.Parse(parentCodeStr);
                        if (parentCode == 0)
                            parentCode = _parentCode;

                        if (parentCode == _parentCode)
                        {
                            res = res + double.Parse(answer);
                            comptencyDetailCount++;
                            if (comptencyIndex == GQTT.Romko_GV.Rows.Count)
                            {
                                for (int i = 0; i < comptencyDetailCount; i++)
                                {
                                    int y = comptencyIndex - i;
                                    ((Label)GQTT.Romko_GV.Rows[y - 1].FindControl("lblCompResult")).Text = (Math.Round(res / comptencyDetailCount, 1)).ToString();
                                }
                            }
                        }
                        else
                        {
                            for (int i = 0; i < GQTT.Romko_GV.Rows.Count; i++)
                            {
                                int pCode = int.Parse(((Label)GQTT.Romko_GV.Rows[i].FindControl("lblParentCode")).Text);
                                if (parentCode == pCode)
                                {
                                    ((Label)GQTT.Romko_GV.Rows[i].FindControl("lblCompResult")).Text = (Math.Round(res / comptencyDetailCount, 1)).ToString();
                                }
                            }

                            parentCode = _parentCode;
                            comptencyDetailCount = 1;
                            res = double.Parse(answer); ;
                        }
                    }

                    double comb_weight = double.Parse(Session["ASS_Comp_Weight"].ToString());
                    //double result = (comptencySum / comptencesCount) * (comb_weight / 100);
                    ((Label)GQTT.FindControl("lblTotalCompResult")).Text = Math.Round((comptencyTotal / comptencesCount), 1).ToString();
                }
            }
        }
        catch
        { }
    }

    private void CalculateObjectives()
    {
        foreach (Control c in panelObjectives.Controls)
        {
            if (c is Final_Version_Objective_Control)
            {
                Final_Version_Objective_Control controlObjectives = (Final_Version_Objective_Control)panelObjectives.FindControl("OBJ_CONTROL_DATA");
                double totalActual = 0;
                foreach (GridViewRow row in controlObjectives.gvObjectives.Rows)
                {
                    string obj_weight = ((TextBox)row.FindControl("txtWeight")).Text.Trim();
                    string obj_actual = ((DropDownList)row.FindControl("txtFinalEvaluation")).SelectedValue;
                    if (double.TryParse(obj_actual, out double _actual) && _actual > 0 && double.TryParse(obj_weight, out double _weight) && _weight > 0)
                        totalActual += _actual * (_weight / 100);
                }
                controlObjectives.gvObjectives.FooterRow.Cells[4].Text = Math.Round(totalActual, 1).ToString();
                controlObjectives.gvObjectives.FooterRow.Cells[4].ForeColor = Color.White;
                controlObjectives.gvObjectives.FooterRow.Cells[4].BackColor = System.Drawing.ColorTranslator.FromHtml("#71625F");
                controlObjectives.gvObjectives.FooterRow.Cells[4].Font.Bold = true;
                controlObjectives.gvObjectives.FooterRow.Cells[4].Font.Size = 16;

                controlObjectives.gvObjectives.FooterRow.Cells[3].Text = "الإجمالي";
                controlObjectives.gvObjectives.FooterRow.Cells[3].ForeColor = Color.White;
                controlObjectives.gvObjectives.FooterRow.Cells[3].BackColor = System.Drawing.ColorTranslator.FromHtml("#71625F");
                controlObjectives.gvObjectives.FooterRow.Cells[3].Font.Bold = true;
                controlObjectives.gvObjectives.FooterRow.Cells[3].Font.Size = 16;
            }
        }
    }

    private void Show_Modal_Message(string msg)
    {
        try
        {
            lblMessage.Text = msg;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "showModal();", true);
        }
        catch
        { }
    }
}