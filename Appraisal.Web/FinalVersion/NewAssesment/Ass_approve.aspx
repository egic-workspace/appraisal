﻿<%@ Page Language="C#" MasterPageFile="~/Main_Master_Page.master" AutoEventWireup="true" CodeFile="~/FinalVersion/NewAssesment/Ass_approve.aspx.cs" Inherits="Final_Version_NewAssesment_Ass_approve" Title="صفحة الموافقات" %>

<%@ Register assembly="Telerik.Web.UI" namespace="Telerik.Web.UI" tagprefix="telerik" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <meta name="viewport" content="width=device-width, initial-scale=1">
     <link href="../CSS/StyleSheet2.css" rel="stylesheet" type="text/css" />
   <link rel="Stylesheet"   href="../CSS/bootstrap.min.css"/>
  
  <script src="../JavaScript/jquery.min.js"></script>
  <script src="../JavaScript/bootstrap.min.js"></script>
   <script type="text/javascript">
        function showModal() {
            $("#myModal").modal('show');
        }

        $(function () {
            $("#btnShow").click(function () {
                showModal();
            });
        });
        
         function showConfModal() {
            $("#confirm-delete").modal('show');
        }

        $(function () {
            $("#btnShow").click(function () {
                showConfModal();
            });
        });
        
   
    </script>
<table style="width: 100%;" align="center">
 <tr>
    <td style="background-color:#71625F; height: 2px; width: 70%;" >
    <span  style="font-size:11pt; font-weight:bold; color:#FFFFFF; font-family:Arial;">
        &nbsp; موافقات التقييم للموظفين </span>
    </td></tr>
 </table>
    <p>
        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>
    </p>
    <p>
         <table align="center" dir="rtl" style="width: 98%; height: 98%; float: right">
        <tr>
            <td align="center" colspan="4">
                                        <span style="font-size:11pt; font-weight:bold; color:#000000; font-family:Courier New;" 
                        __designer:mapid="b4">
                    <telerik:RadTabStrip ID="RadTabStrip1" runat="server" 
                    CausesValidation="False" Height="26px"
                        MultiPageID="RadMultiPage1" ScrollButtonsPosition="Middle" SelectedIndex="0"
                        Skin="WebBlue">
                        <Tabs>
                            <telerik:RadTab runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="15pt"
                                PageViewID="RadPageView1" SelectedImageUrl="~/Images/_thumb_11836.png" 
                                Text="EGIC موظفي" Selected="True">
                            </telerik:RadTab>
                            <telerik:RadTab runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="15pt"
                                PageViewID="RadPageView2" SelectedImageUrl="~/Images/_thumb_11836.png" 
                                Text="Outsourse موظفي">
                            </telerik:RadTab>
                        </Tabs>
                    </telerik:RadTabStrip>
                                        </span>
                </td>
        </tr>
        <tr>
            <td align="center" colspan="4">
                    <telerik:RadMultiPage ID="RadMultiPage1" runat="server" SelectedIndex="0" 
                        Width="100%">
                        <telerik:RadPageView ID="RadPageView1" runat="server" BorderStyle="Solid" Selected="True"
                            Width="100%" BorderWidth="1px">
                            <table align="center" class="style7">
                                <tr>
                                    <td>
                                        &nbsp;</td>
                                    <td>
                                        &nbsp;</td>
                                    <td>
                                        &nbsp;</td>
                                </tr>
                                <tr>
                                    <td align="center" colspan="3">
                                        <asp:GridView ID="GridView1" runat="server" AlternatingRowStyle-CssClass="alt" 
                                            AutoGenerateColumns="False" 
                                            Caption="الموظفين المطلوب الموافقة علي تقييمهم EGIC" CaptionAlign="Top" 
                                            CssClass="mGrid" DataKeyNames="EMP_NO" 
                                            EmptyDataText="لا يوجد موظفين مطلوب الموافقة عليهم" Font-Names="Arial" 
                                            Font-Size="14pt" ondatabinding="GridView1_DataBinding" 
                                            ondatabound="GridView1_DataBound" 
                                            onpageindexchanging="GridView1_PageIndexChanging" 
                                            onrowcommand="GridView1_RowCommand" onrowdatabound="GridView1_RowDataBound" 
                                            onselectedindexchanged="GridView1_SelectedIndexChanged" 
                                            onselectedindexchanging="GridView1_SelectedIndexChanging" 
                                            PagerStyle-CssClass="pgr" Width="950px">
                                            <Columns>
                                                <asp:ButtonField ButtonType="Button" Visible="false" CommandName="APPROVE" 
                                                    ControlStyle-CssClass="btn btn-success" ControlStyle-Width="85px" Text="موافق">
                                                    <ControlStyle CssClass="btn btn-success" Width="85px" />
                                                </asp:ButtonField>
                                                <asp:ButtonField ButtonType="Button" CommandName="Display" 
                                                    ControlStyle-CssClass="btn btn-warning" ControlStyle-Width="100px" 
                                                    Text="عرض التقييم">
                                                    <ControlStyle CssClass="btn btn-warning" Width="100px" />
                                                </asp:ButtonField>
                                                <asp:TemplateField HeaderText="" Visible="false">
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="chk_data" runat="server" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="كود الموظف" SortExpression="Value">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblemp_no" runat="server" Text='<%# Bind("EMP_NO") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="اسم الموظف">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblemp_name" runat="server" Text='<%# Bind("EMP_NAME") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="كود الوظيفة" Visible="false">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbljob_code" runat="server" Text='<%# Bind("JOB_NO") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="الوظيفة">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbljob_name" runat="server" Text='<%# Bind("JOB_NAME") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="التقييم" Visible="false">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblstatus" runat="server" Text='<%# Bind("STATUS") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="التقييم" Visible="true">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblfstatus" runat="server" Text='<%# Bind("FSTATUS") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="النتيجة">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblrslt" runat="server" Text='<%# Bind("RSLT") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                 <asp:BoundField DataField="GRAD" HeaderText="وصف النتيجة" />
                                                <asp:BoundField DataField="emp_company" HeaderText="الشركة" />
                                                <asp:TemplateField HeaderText="المستوي" Visible="false">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbllevel" runat="server" Text='<%# Bind("LEVEL") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <PagerStyle />
                                            <AlternatingRowStyle />
                                        </asp:GridView>
                                        <span style="font-size:11pt; font-weight:bold; color:#000000; font-family:Courier New;">
                                        <asp:Button ID="btn_approve_egic" runat="server" class="btn btn-success" 
                                            Font-Bold="True" Font-Names="Arial" Font-Size="12pt" ForeColor="White" 
                                            onclick="btn_approve_Click" OnClientClick="Confirm()" 
                                            Text="الموافقة علي جميع موظفي EGIC" Width="250px" />
                                        </span>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        &nbsp;</td>
                                    <td>
                                        &nbsp;</td>
                                    <td>
                                        &nbsp;</td>
                                </tr>
                            </table>
                        </telerik:RadPageView>
                        <telerik:RadPageView ID="RadPageView2" runat="server" BorderStyle="Solid" TabIndex="1"
                            Width="100%" BorderWidth="1px">
                            <table align="center" class="style7">
                                <tr>
                                    <td>
                                        &nbsp;</td>
                                    <td>
                                        &nbsp;</td>
                                    <td>
                                        &nbsp;</td>
                                </tr>
                                <tr>
                                    <td align="center" colspan="3">
                                        <asp:GridView ID="GridView3" runat="server" AlternatingRowStyle-CssClass="alt" 
                                            AutoGenerateColumns="False" 
                                            Caption="الموظفين المطلوب الموافقة علي تقييمهم Outsource" CaptionAlign="Top" 
                                            CssClass="mGrid" DataKeyNames="EMP_NO" 
                                            EmptyDataText="لا يوجد موظفين مطلوب الموافقة عليهم" Font-Names="Arial" 
                                            Font-Size="14pt" 
                                            onrowcommand="GridView3_RowCommand" 
                                            PagerStyle-CssClass="pgr" Width="950px">
                                            <Columns>
                                                <asp:ButtonField ButtonType="Button" Visible="false" CommandName="APPROVE" 
                                                    ControlStyle-CssClass="btn btn-success" ControlStyle-Width="85px" Text="موافق">
                                                    <ControlStyle CssClass="btn btn-success" Width="85px" />
                                                </asp:ButtonField>
                                                <asp:ButtonField ButtonType="Button" CommandName="Display" 
                                                    ControlStyle-CssClass="btn btn-warning" ControlStyle-Width="100px" 
                                                    Text="عرض التقييم">
                                                    <ControlStyle CssClass="btn btn-warning" Width="100px" />
                                                </asp:ButtonField>
                                                <asp:TemplateField HeaderText="" Visible="false">
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="chk_data0" runat="server" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="كود الموظف" SortExpression="Value">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblemp_no1" runat="server" Text='<%# Bind("EMP_NO") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="اسم الموظف">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblemp_name1" runat="server" Text='<%# Bind("EMP_NAME") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="كود الوظيفة" Visible="false">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbljob_code0" runat="server" Text='<%# Bind("JOB_NO") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="الوظيفة">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbljob_name1" runat="server" Text='<%# Bind("JOB_NAME") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="التقييم" Visible="false">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblstatus1" runat="server" Text='<%# Bind("STATUS") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="التقييم" Visible="true">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblfstatus1" runat="server" Text='<%# Bind("FSTATUS") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="النتيجة">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblrslt1" runat="server" Text='<%# Bind("RSLT") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="GRAD" HeaderText="وصف النتيجة" />
                                                <asp:BoundField DataField="emp_company" HeaderText="الشركة" />
                                                <asp:TemplateField HeaderText="المستوي" Visible="false">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbllevel1" runat="server" Text='<%# Bind("LEVEL") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <PagerStyle />
                                            <AlternatingRowStyle />
                                        </asp:GridView>
                                        <span style="font-size:11pt; font-weight:bold; color:#000000; font-family:Courier New;">
                                        <asp:Button ID="btn_approve_outsource" runat="server" class="btn btn-success" 
                                            Font-Bold="True" Font-Names="Arial" Font-Size="12pt" ForeColor="White" 
                                            onclick="btn_approve_outsource_Click" OnClientClick="Confirm()" 
                                            Text="الموافقة علي جميع موظفي Outsource" Width="250px" />
                                        </span>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        &nbsp;</td>
                                    <td>
                                        &nbsp;</td>
                                    <td>
                                        &nbsp;</td>
                                </tr>
                            </table>
                        </telerik:RadPageView>
                    </telerik:RadMultiPage>
                </td>
        </tr>
        <tr>
            <td align="center" colspan="4">
                &nbsp;</td>
        </tr>
        <tr>
            <td style="height: 15px">
                &nbsp;</td>
            <td style="height: 15px" align="center" colspan="2">
                &nbsp;</td>
            <td style="height: 15px" align="center">
                &nbsp;</td>
        </tr>
        <tr>
            <td style="height: 15px">
                &nbsp;</td>
            <td style="height: 15px" align="center" colspan="2">
                &nbsp;</td>
            <td style="height: 15px" align="center">
                &nbsp;</td>
        </tr>
        <tr>
            <td align="center" colspan="3">
                
            <asp:GridView ID="GridView2" runat="server" 
                EmptyDataText="ليس لديك بيانات موظفين مسجلة" 
                    Caption="باقي الموظفين  المطلوب الموافقة علي تقييمهم" AutoGenerateColumns="False" 
                CaptionAlign="Top" onselectedindexchanged="GridView1_SelectedIndexChanged" 
                CssClass="mGrid" PagerStyle-CssClass="pgr" 
                AlternatingRowStyle-CssClass="alt" 
                DataKeyNames="EMP_NO" onpageindexchanging="GridView1_PageIndexChanging" Width="950px" 
                        Font-Names="Arial" Font-Size="14pt" 
                    onselectedindexchanging="GridView1_SelectedIndexChanging">
                
                <Columns>
   
                        <asp:TemplateField HeaderText="كود الموظف" SortExpression="Value">
                            <ItemTemplate>
                                <asp:Label ID="lblemp_no0" runat="server" Text='<%# Bind("EMP_NO") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                     
                             <asp:TemplateField HeaderText="اسم الموظف">
                          <ItemTemplate>
                                <asp:Label ID="lblemp_name0" runat="server" Text='<%# Bind("EMP_NAME") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        
                        <asp:TemplateField HeaderText="الوظيفة">
                         <ItemTemplate>
                                <asp:Label ID="lbljob_name0" runat="server" Text='<%# Bind("JOB_NAME") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        
                        
                          <asp:TemplateField HeaderText="التقييم" Visible="false">
                         <ItemTemplate>
                                <asp:Label ID="lblstatus0" runat="server" Text='<%# Bind("STATUS") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        
                           <asp:TemplateField HeaderText="التقييم" Visible="false" >
                         <ItemTemplate>
                                <asp:Label ID="lblfstatus0" runat="server" Text='<%# Bind("FSTATUS") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        
                           <asp:TemplateField HeaderText="النتيجة">
                         <ItemTemplate>
                                <asp:Label ID="lblrslt0" runat="server" Text='<%# Bind("RSLT") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        
                          <asp:BoundField DataField="emp_company" HeaderText="الشركة" />
                          
                          <asp:TemplateField HeaderText="المستوي" Visible="false">
                         <ItemTemplate>
                                <asp:Label ID="lbllevel0" runat="server" Text='<%# Bind("LEVEL") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        
                    </Columns>
                
                <PagerStyle />
                <AlternatingRowStyle />
            </asp:GridView>
                </td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td align="center">
                    <telerik:RadNotification ID="RadNotification1" runat="server" Height="150px" 
                        Position="Center" Skin="Web20" Width="250px">
                        <NotificationMenu ID="TitleMenu">
                        </NotificationMenu>
                    </telerik:RadNotification>
                </td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
    </table><br />
    </p>
 <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog modal-sm">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">نظام تقييم الموظفين</h4>
        </div>
        <div class="modal-body">
          <asp:Label ID="lblMessage" runat="server" Font-Bold="True" Font-Size="12pt" 
                Font-Names="Arial" ForeColor="Red"></asp:Label>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>
  <div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
             نظام تقييم الموظفين  
            </div>
            <div class="modal-body">
               <asp:Label ID="lblConf_Message" runat="server" Font-Bold="True" Font-Size="12pt"  Font-Names="Arial" ForeColor="Red"></asp:Label>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">خروج</button>
                <asp:Button ID="Button2" class="btn btn-danger btn-ok" runat="server" onclick="btnapp_Click" Text="موافق" />
            </div>
        </div>
    </div>
</div>
</asp:Content>
