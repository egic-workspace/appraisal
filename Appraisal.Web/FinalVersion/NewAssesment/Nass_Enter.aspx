﻿<%@ Page Language="C#" MasterPageFile="~/Main_Master_Page.master" AutoEventWireup="true"
    CodeFile="~/FinalVersion/NewAssesment/Nass_Enter.aspx.cs" Inherits="Final_Version_NewAssesment_Nass_Enter"
    Title="  ادخال تقييمات الموظفين" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="../../CSS/bootstrap.min.css">

    <script src="../../JavaScript/jquery.min.js"></script>

    <script src="../../JavaScript/bootstrap.min.js"></script>

    <script type="text/javascript" src="JavaScript/loader.js"></script>
 <%--<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script> --%>
    <%-- https://www.gstatic.com/charts/loader.js--%>

    <script type="text/javascript">
       //html { overflow-y: hidden; }
      google.charts.load("current", {packages:["corechart"]});
      
      google.charts.setOnLoadCallback(drawChart);
      google.charts.setOnLoadCallback(draw_Standard_Chart);
       
       
      google.charts.setOnLoadCallback(init);
      google.charts.setOnLoadCallback(inits);
function init() {
    document.getElementById('ramydev').onclick = drawChart2;
}

function inits() {
    document.getElementById('ramydev2').onclick = draw_Standard_Chart2;
}

       function drawChart() {

           var exll = document.getElementById('<%=lbl_exelent.ClientID%>').innerHTML.replace("%", "").replace(",", ".");
           var good = document.getElementById('<%=lbl_good.ClientID%>').innerHTML.replace("%", "").replace(",", ".");
           var vgood = document.getElementById('<%=lbl_very_good.ClientID%>').innerHTML.replace("%", "").replace(",", ".");
           var bad = document.getElementById('<%=lbl_bad.ClientID%>').innerHTML.replace("%", "").replace(",", ".");
      var data = google.visualization.arrayToDataTable([
          ['Rank', 'Percentage'],
          ['إستثنائي',   Math.ceil(exll)],
          ['يقوق التوقعات',  Math.ceil(vgood)],
           ['يحقق التوقعات',  Math.ceil(good) ],
          ['أقل من التوقعات – غير مرضي', Math.ceil(bad)]
        ]);

        var options = {
          title: 'محقق التقييم',
          is3D: true,
        };

       // var chart = new google.visualization.PieChart(document.getElementById('piechart_3d'));
         var chart = new google.visualization.LineChart(document.getElementById('piechart_3d'));
        chart.draw(data, options);
      }
      //*************************************
      function drawChart2() {
       
          var exll = document.getElementById('<%=lbl_exelent0.ClientID%>').innerHTML.replace("%", "").replace(",", ".") ;
          var good = document.getElementById('<%=lbl_good0.ClientID%>').innerHTML.replace("%", "").replace(",", ".");
          var vgood = document.getElementById('<%=lbl_very_good0.ClientID%>').innerHTML.replace("%", "").replace(",", ".");
          var bad = document.getElementById('<%=lbl_bad0.ClientID%>').innerHTML.replace("%", "").replace(",", ".");
      var data = google.visualization.arrayToDataTable([
          ['Rank', 'Percentage'],
          ['إستثنائي',   Math.ceil(exll)],
          ['يقوق التوقعات',  Math.ceil(vgood)],
           ['يحقق التوقعات',     Math.ceil(good) ],
          ['أقل من التوقعات – غير مرضي', Math.ceil(bad)]
        ]);

        var options = {
          title: 'محقق التقييم',
          is3D: true,
        };

       // var chart = new google.visualization.PieChart(document.getElementById('piechart_3d'));
        var chart = new google.visualization.LineChart(document.getElementById('piechart_3d4'));
        chart.draw(data, options);
      }
      
     //**************************************
    function draw_Standard_Chart() {
       
      var data = google.visualization.arrayToDataTable([
          ['Rank', 'Percentage'],
          ['إستثنائي',   5 ],
          ['يفوق التوقعات',  15 ],
          ['يحقق التوقعات',    70 ],
          ['أقل من التوقعات – غير مرضي', 10 ]
        ]);

        var options = {
          title: 'هدف التقييم',
          is3D: true,
        };

       // var chart = new google.visualization.PieChart(document.getElementById('piechart_3d2'));
       var chart = new google.visualization.LineChart(document.getElementById('piechart_3d2'));
        chart.draw(data, options);

      }
      
      //***************************
       function draw_Standard_Chart2() {
       
      var data2 = google.visualization.arrayToDataTable([
          ['Rank', 'Percentage'],
          ['إستثنائي',   5],
          ['يفوق التوقعات',  15],
          ['يحقق التوقعات',    70 ],
          ['أقل من التوقعات – غير مرضي', 10]
        ]);

        var options = {
          title: 'هدف التقييم',
          is3D: true,
        };

       var chart = new google.visualization.LineChart(document.getElementById('piechart_3d3'));
        chart.draw(data2, options);
      }
      //*********************************
      function ZoomDiv1() {
           var panel = document.getElementById('piechart_3d');

        var printWindow = window.open('', '', 'scrollbars=yes,  resizable=yes, width=900, height=500');
        
        printWindow.document.write('<html><head><title> My Charts </title>');
        printWindow.document.write('');
        printWindow.document.write('</head><body dir="rtl" style="zoom: 2" >');
        printWindow.document.write('');
        printWindow.document.write(panel.innerHTML);
        printWindow.document.write('</body></html>');
        printWindow.document.close();

          return false;
    }
    
      function ZoomDiv2() {

        var panel2 = document.getElementById('piechart_3d2');

        var printWindow = window.open('', '', 'scrollbars=yes,  resizable=yes, width=900, height=500');
        	
        printWindow.document.write('<html><head><title> My Charts </title>');
        printWindow.document.write('');
        printWindow.document.write('</head><body dir="rtl" style="zoom: 2">');
        printWindow.document.write('');
        printWindow.document.write(panel2.innerHTML);
        printWindow.document.write('</body></html>');
        printWindow.document.close();

          return false;
    }
    
       function ZoomDiv3() {

        var panel2 = document.getElementById('piechart_3d3');

        var printWindow = window.open('', '', 'scrollbars=yes,  resizable=yes, width=900, height=500');
        	
        printWindow.document.write('<html><head><title> My Charts </title>');
        printWindow.document.write('');
        printWindow.document.write('</head><body dir="rtl" style="zoom: 2">');
        printWindow.document.write('');
        printWindow.document.write(panel2.innerHTML);
        printWindow.document.write('</body></html>');
        printWindow.document.close();

          return false;
    }
    
       function ZoomDiv4() {

        var panel2 = document.getElementById('piechart_3d4');

        var printWindow = window.open('', '', 'scrollbars=yes,  resizable=yes, width=900, height=500');
        	
        printWindow.document.write('<html><head><title> My Charts </title>');
        printWindow.document.write('');
        printWindow.document.write('</head><body dir="rtl" style="zoom: 2">');
        printWindow.document.write('');
        printWindow.document.write(panel2.innerHTML);
        printWindow.document.write('</body></html>');
        printWindow.document.close();

          return false;
    }
    
     function showModal() {
            $("#myModal").modal('show');
        }

        $(function () {
            $("#btnShow").click(function () {
                showModal();
            });
        });
        
       function ModalDiv() {

        var chartss = document.getElementById('piechart_3d2');
         $('.modal-body').empty(); 
         $('.modal-body').append(chartss.innerHTML);
        showModal();
    }
       function ModalDiv2() {

        var chartss = document.getElementById('piechart_3d');
         $('.modal-body').empty(); 
         $('.modal-body').append(chartss.innerHTML);
        showModal();
    }
    
       function ModalDiv3() {

        var chartss = document.getElementById('piechart_3d3');
         $('.modal-body').empty(); 
         $('.modal-body').append(chartss.innerHTML);
        showModal();
    }
    
        function ModalDiv4() {

        var chartss = document.getElementById('piechart_3d4');
         $('.modal-body').empty(); 
         $('.modal-body').append(chartss.innerHTML);
        showModal();
    }
    </script>

    <table style="width: 100%;" align="center" >
        <tr>
            <td style="background-color: #71625F; height: 2px; width: 70%;">
                <span style="font-size: 11pt; font-weight: bold; color: #ffffff; font-family: Courier New;">
                    &nbsp; ادخال التقييم للموظفين </span>
            </td>
        </tr>
    </table>
    <p>
        <asp:ScriptManager ID="ScriptManager1" runat="server" >
        </asp:ScriptManager>
    </p>
    <p>
        <table align="center" dir="rtl" style="width: 98%; height: 98%; float: right">
            <tr>
                <td align="center" colspan="5" dir="rtl">
                    <asp:GridView ID="GridView1" runat="server" EmptyDataText="ليس لديك بيانات موظفين مسجلة"
                        Caption="الموظفين المطلوب تقييمهم" AutoGenerateColumns="False" CaptionAlign="Top"
                        CssClass="mGrid" PagerStyle-CssClass="pgr" AlternatingRowStyle-CssClass="alt"
                        DataKeyNames="EMP_NO" Width="950px" Font-Names="Arial" Font-Size="14pt" OnRowDataBound="GridView1_RowDataBound1">
                        <Columns>
                            <asp:TemplateField HeaderText="كود الموظف" SortExpression="Value">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lblemp_no" runat="server" class="label label-warning" CommandName='<%# Bind("JOB_NO") %>'
                                        Width="90%" OnClick="EMP_no_Click" ToolTip='<%# Bind("EMP_NAME") %>' Text='<%# Bind("EMP_NO") %>'></asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="اسم الموظف">
                                <ItemTemplate>
                                    <asp:Label ID="lblemp_name" runat="server" Text='<%# Bind("EMP_NAME") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="كود الوظيفة" Visible="false">
                                <ItemTemplate>
                                    <asp:Label ID="lbljob_code" runat="server" Text='<%# Bind("JOB_NO") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="الوظيفة">
                                <ItemTemplate>
                                    <asp:Label ID="lbljob_name" runat="server" Text='<%# Bind("JOB_NAME") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="emp_company" HeaderText="الشركة" />
                            <asp:TemplateField HeaderText="التقييم" Visible="false">
                                <ItemTemplate>
                                    <asp:Label ID="lblstatus" runat="server" Text='<%# Bind("STATUS") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="التقييم">
                                <ItemTemplate>
                                    <asp:Label ID="lblfstatus" runat="server" Text='<%# Bind("FSTATUS") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="المستوي" Visible="false">
                                <ItemTemplate>
                                    <asp:Label ID="lbllevel" runat="server" Text='<%# Bind("LEVEL") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="RSLT" HeaderText="النتيجة" />
                            <asp:BoundField DataField="GRAD" HeaderText="وصف النتيجة" />
                        </Columns>
                        <PagerStyle />
                        <AlternatingRowStyle />
                    </asp:GridView>
                </td>
            </tr>
            <tr>
                <td style="height: 15px">
                    &nbsp;
                </td>
                <td style="height: 15px" align="center">
                    &nbsp;
                </td>
                <td style="height: 15px" align="center" colspan="2">
                    &nbsp;
                </td>
                <td style="height: 15px" align="center">
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td style="height: 15px">
                    &nbsp;
                </td>
                <td style="height: 15px" align="center">
                    &nbsp;
                </td>
                <td style="height: 15px" align="center" colspan="2">
                <div id="ramydev"  >
                 <div id="ramydev2"  >
                    <telerik:RadTabStrip ID="RadTabStrip1" runat="server" CausesValidation="False" Height="26px"
                        MultiPageID="RadMultiPage1" ScrollButtonsPosition="Middle" SelectedIndex="0"
                        Skin="WebBlue">
                        <Tabs>
                            <telerik:RadTab runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="15pt"
                                PageViewID="RadPageView1" SelectedImageUrl="~/Images/_thumb_11836.png" 
                                Text="نتــــــائـــج EGIC" Selected="True">
                            </telerik:RadTab>
                            <telerik:RadTab runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="15pt"
                                PageViewID="RadPageView2" SelectedImageUrl="~/Images/_thumb_11836.png" 
                                Text="نتــــــائـــج Outsourse">
                            </telerik:RadTab>
                        </Tabs>
                    </telerik:RadTabStrip>
                   </div>
                    </div>
                </td>
                <td style="height: 15px" align="center">
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td style="height: 15px">
                    &nbsp;
                </td>
                <td style="height: 15px" align="center">
                    &nbsp;
                </td>
                <td style="height: 15px" align="center" colspan="2">
                    <telerik:RadMultiPage ID="RadMultiPage1" runat="server" SelectedIndex="0" 
                        Width="100%">
                        <telerik:RadPageView ID="RadPageView1" runat="server" BorderStyle="Solid" Selected="True"
                            Width="100%">
                            <div align="center" style="width: 100%">
                                <asp:Panel ID="Panel1" runat="server">
                                    <table id="curve_tbl" align="center" border="1" dir="rtl" frame="border" onload="drawChart()"
                                        style="padding: inherit; margin: inherit; border: medium solid #F79222; background-color: #FFFFFF;"
                                        width="100%">
                                        <tr bgcolor="#71625F">
                                            <td align="center">
                                                <span style="font-size: 11pt; font-weight: bold; color: #71625F; font-family: Courier New;">
                                                    <asp:Label ID="Label10" runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="15pt"
                                                        ForeColor="White" Text="التقييم"></asp:Label></span>
                                            </td>
                                            <td align="center">
                                                <span style="font-size: 11pt; font-weight: bold; color: #71625F; font-family: Courier New;">
                                                    <asp:Label ID="Label9" runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="15pt"
                                                        ForeColor="White" Text="المخطط" Width="60px"></asp:Label></span>
                                            </td>
                                            <td align="center">
                                                <span style="font-size: 11pt; font-weight: bold; color: #71625F; font-family: Courier New;">
                                                    <asp:Label ID="Label12" runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="15pt"
                                                        ForeColor="White" Text="الفعلى"></asp:Label></span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                                    <asp:Label ID="Label23" runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="15pt"
                                                        ForeColor="Black" Text="إستثنائي - Exceptional"></asp:Label></span>
                                            </td>
                                            <td>
                                                <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                                    <asp:Label ID="Label32" runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="15pt"
                                                        ForeColor="Black" Text="5%"></asp:Label></span>
                                            </td>
                                            <td>
                                                <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                                    <asp:Label ID="lbl_exelent" runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="15pt"
                                                        ForeColor="Black" Text="0%"></asp:Label></span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <span dir="rtl" style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                                    <asp:Label ID="Label22" runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="15pt"
                                                        ForeColor="Black" Text="يفوق التوقعات - Exceed Expectations"></asp:Label></span>
                                            </td>
                                            <td>
                                                <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                                    <asp:Label ID="Label31" runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="15pt"
                                                        ForeColor="Black" Text="15%"></asp:Label></span>
                                            </td>
                                            <td>
                                                <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                                    <asp:Label ID="lbl_very_good" runat="server" Font-Bold="True" Font-Names="Arial"
                                                        Font-Size="15pt" ForeColor="Black" Text="0%"></asp:Label></span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                                    <asp:Label ID="Label21" runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="15pt"
                                                        ForeColor="Black" Text="يحقق التوقعات - Meet Expectations"></asp:Label></span>
                                            </td>
                                            <td>
                                                <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                                    <asp:Label ID="Label65" runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="15pt"
                                                        ForeColor="Black" Text="70%"></asp:Label></span>
                                            </td>
                                            <td>
                                                <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                                    <asp:Label ID="lbl_good" runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="15pt"
                                                        ForeColor="Black" Text="0%"></asp:Label></span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                                    <asp:Label ID="Label14" runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="15pt"
                                                        ForeColor="Black" Text="أقل من التوقعات – غير مرضي  Below Expectation - Unsatisfactory"></asp:Label></span>
                                            </td>
                                            <td>
                                                <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                                    <asp:Label ID="Label64" runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="15pt"
                                                        ForeColor="Black" Text="10%"></asp:Label></span>
                                            </td>
                                            <td>
                                                <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                                    <asp:Label ID="lbl_bad" runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="15pt"
                                                        ForeColor="Black" Text="0%"></asp:Label></span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div id="piechart_3d2" align="center" dir="rtl" onclick="ModalDiv()"  style="height: 100%;
                                                    width: 400px; overflow-x: hidden; overflow-y: hidden; padding-bottom: 10px;">
                                                </div>
                                            </td>
                                            <td>
                                                &#160;
                                            </td>
                                            <td>
                                                <div id="piechart_3d" align="center" dir="rtl" onclick="ModalDiv2()" style="height: 100%;
                                                    width: 400px; overflow-x: hidden; overflow-y: hidden; padding-bottom: 10px;">
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </div>
                        </telerik:RadPageView>
                        <telerik:RadPageView ID="RadPageView2" runat="server" BorderStyle="Solid" TabIndex="1"
                            Width="100%">
                            <asp:Panel ID="Panel2" runat="server">
                                <table id="curve_tbl0" align="center" border="1" dir="rtl" frame="border" onload="drawChart2()"
                                    style="padding: inherit; margin: inherit; border: medium solid #F79222; background-color: #FFFFFF;"
                                    width="100%">
                                    <tr bgcolor="#71625F">
                                        <td align="center">
                                            <span style="font-size: 11pt; font-weight: bold; color: #71625F; font-family: Courier New;">
                                                <asp:Label ID="Label66" runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="15pt"
                                                    ForeColor="White" Text="التقييم"></asp:Label></span>
                                        </td>
                                        <td align="center">
                                            <span style="font-size: 11pt; font-weight: bold; color: #71625F; font-family: Courier New;">
                                                <asp:Label ID="Label67" runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="15pt"
                                                    ForeColor="White" Text="المخطط" Width="60px"></asp:Label></span>
                                        </td>
                                        <td align="center">
                                            <span style="font-size: 11pt; font-weight: bold; color: #71625F; font-family: Courier New;">
                                                <asp:Label ID="Label68" runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="15pt"
                                                    ForeColor="White" Text="الفعلى"></asp:Label></span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                                <asp:Label ID="Label69" runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="15pt"
                                                    ForeColor="Black" Text="إستثنائي - Exceptional"></asp:Label></span>
                                        </td>
                                        <td>
                                            <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                                <asp:Label ID="Label70" runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="15pt"
                                                    ForeColor="Black" Text="5%"></asp:Label></span>
                                        </td>
                                        <td>
                                            <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                                <asp:Label ID="lbl_exelent0" runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="15pt"
                                                    ForeColor="Black" Text="0%"></asp:Label></span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <span dir="rtl" style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                                <asp:Label ID="Label71" runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="15pt"
                                                    ForeColor="Black" Text="يفوق التوقعات - Exceed Expectations"></asp:Label></span>
                                        </td>
                                        <td>
                                            <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                                <asp:Label ID="Label72" runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="15pt"
                                                    ForeColor="Black" Text="15%"></asp:Label></span>
                                        </td>
                                        <td>
                                            <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                                <asp:Label ID="lbl_very_good0" runat="server" Font-Bold="True" Font-Names="Arial"
                                                    Font-Size="15pt" ForeColor="Black" Text="0%"></asp:Label></span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                                <asp:Label ID="Label73" runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="15pt"
                                                    ForeColor="Black" Text="يحقق التوقعات - Meet Expectations"></asp:Label></span>
                                        </td>
                                        <td>
                                            <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                                <asp:Label ID="Label74" runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="15pt"
                                                    ForeColor="Black" Text="70%"></asp:Label></span>
                                        </td>
                                        <td>
                                            <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                                <asp:Label ID="lbl_good0" runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="15pt"
                                                    ForeColor="Black" Text="0%"></asp:Label></span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                                <asp:Label ID="Label75" runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="15pt"
                                                    ForeColor="Black" Text="أقل من التوقعات – غير مرضي  Below Expectation - Unsatisfactory"></asp:Label></span>
                                        </td>
                                        <td>
                                            <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                                <asp:Label ID="Label76" runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="15pt"
                                                    ForeColor="Black" Text="10%"></asp:Label></span>
                                        </td>
                                        <td>
                                            <span style="font-size: 11pt; font-weight: bold; color: #000000; font-family: Courier New;">
                                                <asp:Label ID="lbl_bad0" runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="15pt"
                                                    ForeColor="Black" Text="0%"></asp:Label></span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div id="piechart_3d3" align="center" dir="rtl" onclick="ModalDiv3()" style="height: 100%;
                                                width: 400px; overflow-x: hidden; overflow-y: hidden; padding-bottom: 10px;">
                                            </div>
                                        </td>
                                        <td>
                                            &#160;
                                        </td>
                                        <td>
                                            <div id="piechart_3d4" align="center" dir="rtl" onclick="ModalDiv4()" style="height: 100%;
                                                width: 400px; overflow-x: hidden; overflow-y: hidden; padding-bottom: 10px;">
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                        </telerik:RadPageView>
                    </telerik:RadMultiPage>
                </td>
                <td style="height: 15px" align="center">
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
            </tr>
        </table>
        <br />
    </p>
    <div class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog modal-sm">
            <div class="modal-content" style="width: 650px">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">
                        &times;</button>
                    <h4 class="modal-title">
                        نظام تقييم الموظفين</h4>
                </div>
                <div class="modal-body" style="zoom: 1.5">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">
                        Close</button>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
