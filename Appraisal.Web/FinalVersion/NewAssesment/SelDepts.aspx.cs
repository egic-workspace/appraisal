﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;


public partial class FinalVersion_NewAssesment_SelDepts : System.Web.UI.Page
{
    AppService ser = new AppService();
    
    DataTable Dst = new DataTable();
    DataTable dt = new DataTable();
    // string Job_Code = "";Bind_ASS_Departments_combo

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Session["Logged_User"] == null)
            {
                Response.Redirect("~/Default.aspx");
            }
            else
            {
                Fill_Depts_DDL();
            }
        }
    }
    protected void btn_approve_Click(object sender, EventArgs e)
    {
        // to enter assdfs
        if (ddl_departments.SelectedIndex == 0)
        {
            RadNotification1.Text = "برجاء اختيار الإدارة ";
            RadNotification1.Title = "نظام تقييم الموظفين";
            RadNotification1.TitleIcon = "info";
            RadNotification1.ContentIcon = "info";
            RadNotification1.Show();
        }
        else
        {
            string finalVersionUrl = MidYearVersionHelper.IsMidYearVersion(Session["OPEN_YEAR"].ToString(), Session["OPEN_VERSION"].ToString()) ? "" : "FinalVersion/";
            Session["SEL_DEPT"] = ddl_departments.SelectedValue.ToString();
            string MyURL = string.Format("~/{0}NewAssesment/Nass_Enter.aspx", finalVersionUrl);
            Response.Redirect(MyURL);
           // Response.Redirect("~/FinalVersion/NewAssesment/Nass_Enter.aspx");
        }
    }
    protected void btnapp_Click(object sender, EventArgs e)
    {

    }
    private void Fill_Depts_DDL()
    {
        dt = ser.Bind_ASS_Departments_combo(Session["EMP_NO"].ToString());
        ddl_departments.DataSource = dt;
        ddl_departments.DataTextField = "SECTION_NAME";
        ddl_departments.DataValueField = "SECTION_NO";
        ddl_departments.DataBind();
        //ddl_departments.Items.Insert(0, new RadComboBoxItem("اختر الموظف", string.Empty));
        ListItem lst = new ListItem("***** اختر الإدارة *****", "0");
        ddl_departments.Items.Insert(0, lst);
    }
}
