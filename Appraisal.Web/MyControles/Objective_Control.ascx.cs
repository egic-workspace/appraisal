﻿using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;


public partial class MyControles_Objective_Control : System.Web.UI.UserControl
{
    public string Display_Mode = "";
    public string Static_Mode = "";
    public DataTable dtCurrentTable = new DataTable();
    public GridView GV_Objectes;

    protected void Page_Init(object sender, EventArgs e)
    {
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            if (Static_Mode.Trim() == "Y")
            {
                Gridview1.Enabled = false;
            }
            if (Display_Mode.Trim() == "Y")
            {
                Bind_GridView();
            }
            else
            {
                SetInitialRow();
            }
        }
        GV_Objectes = Gridview1;
    }

    protected void ButtonAdd_Click(object sender, EventArgs e)
    {
        if (Static_Mode.Trim() == "Y")
        {
            Alert.Show("لا يمكنك اضافة اهداف أخري");
        }
        else
        {
            AddNewRowToGrid();
        }
    }

    protected void OnRowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        try
        {
            if (Gridview1.Rows.Count == 1)
            {
                Alert.Show(" لا يمكن حذف اخر صف بالجدول ");
                return;
            }

            int index = Convert.ToInt32(e.RowIndex);
            DataTable OBJDT = new DataTable("OBJDT");
            OBJDT.Columns.Add("RowNumber");
            OBJDT.Columns.Add("obj_desc");
            OBJDT.Columns.Add("obj_weight");
            OBJDT.Columns.Add("obj_actual");
            OBJDT.Columns.Add("obj_notes");
            OBJDT.Rows.Clear();
            int i = 0;
            foreach (GridViewRow row in Gridview1.Rows)
            {
                if (i != index)
                {
                    string objc = ((TextBox)row.FindControl("txt_obj_desc")).Text;
                    string weight = ((TextBox)row.FindControl("txt_obj_weight")).Text;
                    string degree = string.Empty;
                    string note = ((TextBox)row.FindControl("txt_obj_notes")).Text;
                    OBJDT.NewRow();
                    OBJDT.Rows.Add(i + 1, objc, weight, degree, note);
                }
                i++;
            }
            Gridview1.DataSource = OBJDT;
            Gridview1.DataBind();

            ViewState["CurrentTable"] = OBJDT;
        }
        catch (Exception ex)
        {
            string error = ex.Message;
        }
    }

    private void Bind_GridView()
    {
        ViewState["CurrentTable"] = dtCurrentTable;
        Gridview1.DataSource = dtCurrentTable;
        Gridview1.DataBind();
        GV_Objectes = Gridview1;
    }

    private void AddNewRowToGrid()
    {
        int rowIndex = 0;
        if (ViewState["CurrentTable"] != null)
        {
            dtCurrentTable = (DataTable)ViewState["CurrentTable"];
            DataRow drCurrentRow = null;
            if (dtCurrentTable.Rows.Count > 0)
            {
                try
                {
                    for (int i = 1; i <= dtCurrentTable.Rows.Count; i++)
                    {
                        TextBox txt_obj_desc = (TextBox)Gridview1.Rows[rowIndex].Cells[1].FindControl("txt_obj_desc");
                        TextBox txt_obj_weight = (TextBox)Gridview1.Rows[rowIndex].Cells[2].FindControl("txt_obj_weight");
                        TextBox txt_obj_notes = (TextBox)Gridview1.Rows[rowIndex].Cells[2].FindControl("txt_obj_notes");
                        drCurrentRow = dtCurrentTable.NewRow();
                        try
                        {
                            string ROW = dtCurrentTable.Rows[i]["RowNumber"].ToString();
                            drCurrentRow["RowNumber"] = ROW;
                        }
                        catch
                        {
                            drCurrentRow["RowNumber"] = i + 1;
                        }
                        dtCurrentTable.Rows[i - 1]["obj_desc"] = txt_obj_desc.Text;
                        dtCurrentTable.Rows[i - 1]["obj_weight"] = txt_obj_weight.Text;
                        dtCurrentTable.Rows[i - 1]["obj_notes"] = txt_obj_notes.Text;
                        rowIndex++;
                    }

                    dtCurrentTable.Rows.Add(drCurrentRow);
                    dtCurrentTable.AcceptChanges();
                    ViewState["CurrentTable"] = dtCurrentTable;
                    Gridview1.DataSource = dtCurrentTable;
                    Gridview1.DataBind();
                }
                catch
                {
                    Alert.Show("برجاء ادخال بيانات الهدف");
                    return;
                }
            }
        }
        else
        {
            Response.Write("ViewState is null");
        }

        //Set Previous Data on Postbacks
        SetPreviousData();
    }

    private void SetInitialRow()
    {
        DataTable dt = new DataTable();
        DataRow dr = null;
        dt.Columns.Add(new DataColumn("RowNumber", typeof(string)));
        dt.Columns.Add(new DataColumn("obj_desc", typeof(string)));
        dt.Columns.Add(new DataColumn("obj_weight", typeof(string)));
        dt.Columns.Add(new DataColumn("obj_actual", typeof(string)));
        dt.Columns.Add(new DataColumn("obj_notes", typeof(string)));
        dr = dt.NewRow();

        dr["RowNumber"] = 1;
        dr["obj_desc"] = string.Empty;
        dr["obj_weight"] = string.Empty;
        dr["obj_actual"] = string.Empty;
        dr["obj_notes"] = string.Empty;
        dt.Rows.Add(dr);
        ViewState["CurrentTable"] = dt;
        Gridview1.DataSource = dt;
        Gridview1.DataBind();
    }

    private void SetPreviousData()
    {
        int rowIndex = 0;
        if (ViewState["CurrentTable"] != null)
        {
            DataTable dt = (DataTable)ViewState["CurrentTable"];
            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    TextBox txt_obj_desc = (TextBox)Gridview1.Rows[rowIndex].Cells[1].FindControl("txt_obj_desc");
                    TextBox txt_obj_weight = (TextBox)Gridview1.Rows[rowIndex].Cells[2].FindControl("txt_obj_weight");
                    TextBox txt_obj_notes = (TextBox)Gridview1.Rows[rowIndex].Cells[2].FindControl("txt_obj_notes");

                    txt_obj_desc.Text = dt.Rows[i]["obj_desc"].ToString();
                    txt_obj_weight.Text = dt.Rows[i]["obj_weight"].ToString();
                    txt_obj_notes.Text = dt.Rows[i]["obj_notes"].ToString();
                    rowIndex++;
                }
            }
        }
    }
}