﻿using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;


public partial class MyControles_EmpObjectivesControl : UserControl
{
    public DataTable dtCurrentTable = new DataTable();
    public GridView GV_Objectes;

    protected void Page_Init(object sender, EventArgs e)
    {
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            ViewState["CurrentTable"] = dtCurrentTable;
            Gridview1.DataSource = dtCurrentTable;
            Gridview1.DataBind();
        }
        GV_Objectes = Gridview1;
    }
}