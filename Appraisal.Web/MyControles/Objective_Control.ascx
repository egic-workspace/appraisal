﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="~/MyControles/Objective_Control.ascx.cs" Inherits="MyControles_Objective_Control" %>
  <link href="../CSS/StyleSheet.css" rel="stylesheet" type="text/css" />
  <link href="../CSS/StyleSheet2.css" rel="stylesheet" type="text/css" />
  <link rel="stylesheetxx" href="../CSS/bootstrap.min.css"/>
  <script>

     function romko(txtbox) 
        {
        alert(txtbox);
        }
function Loop_Text()
{

   var grid = document.getElementById("<%= Gridview1.ClientID%>");  

    alert(grid.rows.length);
    for (j = 0; j < grid.rows.length - 1; j++) {
         var total ="GridView1_TextBox1_"+ j;
        var tt = document.getElementById(total);
         alert(document.getElementById(total).content);
       }
}
var oldText = "";
var newText = "";

function txtOnFocus(txtBox,expand){
    oldText = txtBox.value;
    if(expand){
        AutoExpand(txtBox);
        }
}
function txtOnBlur(txtBox,expand){
    newText = txtBox.value;
    if(newText != oldText)
    {
        var grid = document.getElementById("<%= Gridview1.ClientID%>");
        var gridId = grid.id+"_";
        
        var txtId = txtBox.id;
        
        var n = txtId.lastIndexOf('_');
        var txtClientId = txtId.substring(n + 1);
        
        var hiddenId = gridId + txtId.replace(gridId,"").replace(txtClientId,"")+"hidden1";
        var hidden1 = document.getElementById(hiddenId);
        if(hidden1 != null)
            hidden1.value = "true";
    }
    if(expand){
    AutoExpand(txtBox);
    }
}
function CheckNumeric(e) {
 
            if (window.event) //
            {
                if ((e.keyCode < 48 || e.keyCode > 57) & e.keyCode != 8) {
                    event.returnValue = false;
                    return false;
 
                }
            }
            else { // Fire Fox
                if ((e.which < 48 || e.which > 57) & e.which != 8) {
                    e.preventDefault();
                    return false;
 
                }
            }
        }
       
  </script>
<asp:GridView ID="Gridview1" runat="server" ShowFooter="true"  Width="100%" CssClass="mGrid"  AlternatingRowStyle-CssClass="alt" OnRowDeleting="OnRowDeleting" PagerStyle-CssClass="pgr"   AutoGenerateColumns="false"  Font-Names="Arial" >
        <Columns>

        <asp:BoundField DataField="RowNumber" HeaderText="م" Visible="false" />

        <asp:TemplateField HeaderStyle-Width="350px"  HeaderText="الهدف">
            <ItemTemplate>
            <asp:TextBox ID="txt_obj_desc" class="form-control" Width="100%" onbeforeprint="AutoExpand(this)" onfocus="txtOnFocus(this,'true')"  onBlur="txtOnBlur(this,'true')" onkeyup="AutoExpand(this)" Font-Size="13pt" TextMode="MultiLine" Text='<%# Bind("obj_desc") %>' runat="server"></asp:TextBox>
            <asp:HiddenField ID="hidden1" runat="server" Value="" />
            </ItemTemplate>
        </asp:TemplateField>
        
        <asp:TemplateField HeaderStyle-Width="50px" HeaderText="الوزن">
            <ItemTemplate>
                <asp:TextBox ID="txt_obj_weight" class="form-control" Width="90px" onkeypress="CheckNumeric(event);" Font-Size="13pt" Text='<%# Bind("obj_weight") %>' runat="server" onBlur="txtOnBlur(this)" onfocus="txtOnFocus(this)" ></asp:TextBox>
            </ItemTemplate>
        </asp:TemplateField>

        <asp:TemplateField HeaderStyle-Width="250px"  HeaderText="سبب التغيير" >
            <ItemTemplate>
                <asp:TextBox ID="txt_obj_notes" TextMode="MultiLine" Width="100%" onfocus="AutoExpand(this)"   onkeydown="AutoExpand(this)" onkeyup="AutoExpand(this)" class="form-control" Font-Size="13pt" Text='<%# Bind("obj_notes") %>'   runat="server"></asp:TextBox>
            </ItemTemplate>
            <FooterStyle HorizontalAlign="Right" />
            <FooterTemplate>
             <asp:Button ID="ButtonAdd" runat="server" class="btn btn-primary"  BackColor="#F79222" Font-Bold="True" Font-Names="Arial" Font-Size="12pt" Width="80%" onclick="ButtonAdd_Click" Text="إضافة هدف جديد" CausesValidation="False" />
            </FooterTemplate>
        </asp:TemplateField>

        <asp:TemplateField HeaderStyle-Width="90px" HeaderText="">
            <ItemTemplate>
            <asp:Button ID="Buttondelete" runat="server" class="btn btn-primary"  BackColor="#F79222" Font-Bold="True" Font-Names="Arial" Font-Size="12pt" Width="70px" CommandName="Delete" OnRowDataBound="OnRowDataBound"  Text="حـذف" CausesValidation="False" />
            </ItemTemplate>
        </asp:TemplateField>

        </Columns>
</asp:GridView>
 
<p>
  <asp:Image ID="Image1" runat="server" ImageUrl="~/Images/line3.png" Width="100%" Height="10px" />
</p>