﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

using Telerik.Web.UI;

public partial class Basic_Data_Exception_add : System.Web.UI.Page
{
    AppService ser = new AppService();
    
    DataTable Dst = new DataTable();
    DataTable dt = new DataTable();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Session["Logged_User"] == null)
            {
                Response.Redirect("~/Default.aspx");
            }
            else
            {
                int APPROVER = ser.Check_Approver(Session["EMP_NO"].ToString());
                if (APPROVER == 0)
                { Response.Redirect("~/Setting/Unauthorized_Pg.aspx"); }

                Session["OPEN_YEAR"] = ser.Get_Ass_Open_Year().ToString();
                Session["OPEN_VERSION"] = ser.Get_Ass_Open_Version().ToString();
                int ISdirector = ser.Check_Ass_Departments(Session["EMP_NO"].ToString());
                if (ISdirector == 0 || ISdirector == 1)
                {
                    ddl_departments.Visible = false;
                    Label9.Visible = false;
                    Fill_Employee_DDL("NO");
                    Fill_Grades_DDL();
                    ddl_departments.AutoPostBack = false;
                    Fill_Grid("ALL");

                }
                else
                {
                    ddl_departments.AutoPostBack = true;
                    ddl_departments.Visible = true;
                    Label9.Visible = true;
                    Fill_Depts_DDL();
                    Fill_Grades_DDL();
                    Fill_Grid("ALL");
                }
            }
        }
    }
    protected void btn_save_Click(object sender, EventArgs e)
    {
        //ddl_departments.SelectedIndex == 0 
        if (ddl_emp.SelectedIndex == 0 || ddl_perf_grade.SelectedIndex == 0)
        {
            lbl_MSG.Text = "برجاء اختيار البيانات ";
            return;
        }
        else
        {
            int RESLT = ser.Save_ASS_Exception(ddl_emp.SelectedValue.ToString(), ddl_emp.SelectedItem.Text.ToString(), ddl_perf_grade.SelectedValue.ToString(), Session["Logged_User"].ToString(), Session["OPEN_YEAR"].ToString(), Session["OPEN_VERSION"].ToString());
            if (RESLT == 1)
            {
                Fill_Grid("ALL");
                lbl_MSG.Text = "تم حفظ البيانات بنجاح";
                RadNotification1.Text = "تم حفظ البيانات بنجاح";
                RadNotification1.Title = "Title";
                RadNotification1.TitleIcon = "info";
                RadNotification1.ContentIcon = "info";
                RadNotification1.Show();
                Clear();
            }
            else
            {
                lbl_MSG.Text = "لقد تم ادخال الموظف من قبل .. برجاء التأكد";
            }
        }
    }
    private void Fill_Grid(string dept)
    {
        try
        {
            //Clear_Grids();
            
            dt = ser.Bind_ASS_Exceptions_GRID(Session["EMP_NO"].ToString(), dept , Session["OPEN_YEAR"].ToString(), Session["OPEN_VERSION"].ToString());
            RadGrid1.DataSource = dt;
            RadGrid1.DataBind();
        }
        catch
        {

        }
    }
    private void Clear_Grids()
    {
        //RadGrid1.DataSource = null;
        //RadGrid1.DataBind();
    }
    private void Clear()
    {
        try
        {
            ddl_emp.SelectedIndex = 0;
            ddl_perf_grade.SelectedIndex = 0;
           // ddl_departments.SelectedIndex = 0;
        }
        catch
        { }
    }
    private void Fill_Depts_DDL()
    {
        dt = ser.Bind_ASS_Departments_combo(Session["EMP_NO"].ToString());
        ddl_departments.DataSource = dt;
        ddl_departments.DataTextField = "SECTION_NAME";
        ddl_departments.DataValueField = "SECTION_NO";
        ddl_departments.DataBind();
   
        ListItem lst = new ListItem("***** اختر الإدارة *****", "0");
        ddl_departments.Items.Insert(0, lst);
    }

    private void Fill_Employee_DDL(string dept)
    {
        dt = ser.Bind_ASS_EMPLOYE_DDL(Session["EMP_NO"].ToString(), dept);
        ddl_emp.DataSource = dt;
        ddl_emp.DataTextField = "EMP_NAME";
        ddl_emp.DataValueField = "EMP_NO";
        ddl_emp.DataBind();

        ListItem lst = new ListItem("***** اختر الموظف *****", "0");
        ddl_emp.Items.Insert(0, lst);
    }

    private void Fill_Grades_DDL()
    {
        dt = ser.Bind_ASS_PERF_GRADS_DDL();
        ddl_perf_grade.DataSource = dt;
        ddl_perf_grade.DataTextField = "name";
        ddl_perf_grade.DataValueField = "code";
        ddl_perf_grade.DataBind();

        ListItem lst = new ListItem("***** اختر التقييم *****", "0");
        ddl_perf_grade.Items.Insert(0, lst);
    }
    protected void ddl_departments_SelectedIndexChanged(object sender, EventArgs e)
    {
        
    }
    protected void ddl_departments_SelectedIndexChanged1(object sender, EventArgs e)
    {
        Fill_Employee_DDL(ddl_departments.SelectedValue.ToString());
    }
    protected void RadGrid1_NeedDataSource1(object sender, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
    {
        Fill_Grid("ALL"); 
    }
    protected void RadGrid1_ItemCreated(object sender, Telerik.Web.UI.GridItemEventArgs e)
    {

    }
    protected void RadGrid1_DeleteCommand(object sender, Telerik.Web.UI.GridCommandEventArgs e)
    {
        try
        {

            GridDataItem item = e.Item as GridDataItem;
            string id = (item.FindControl("IDLabel") as Label).Text;
            string status = (item.FindControl("approvalLabel") as Label).Text;
            //int deleted = ser.Check_Deleted_Plants(id);
            if (status.Trim() == "Pending")
            {
            int RESLT = ser.Delete_ASS_Exception(id);
                if (RESLT == 1)
                {
                    //Fill_Grid();
                    Fill_Grid("ALL");
                    RadNotification1.Text = "تم حذف البيانات بنجاح";
                    RadNotification1.Title = "Title";
                    RadNotification1.TitleIcon = "info";
                    RadNotification1.ContentIcon = "info";
                    RadNotification1.Show();
                    Clear();
                }
                else
                {
                    RadNotification1.Text = "يوجد مشكله في عملية الحذف";
                    RadNotification1.Title = "Title";
                    RadNotification1.TitleIcon = "info";
                    RadNotification1.ContentIcon = "info";
                    RadNotification1.Show();
                }
            }
            else
            {
                RadNotification1.Text = "لا يمكنك حذف الموظف حيث تمت الموافقة عليه برجاء الرجوع الي ادارة النظم";
                RadNotification1.Title = "Title";
                RadNotification1.TitleIcon = "info";
                RadNotification1.ContentIcon = "info";
                RadNotification1.Show();
                return;
            }

            // Fill_Grid();
        }
        catch
        {

        }
    }
}
