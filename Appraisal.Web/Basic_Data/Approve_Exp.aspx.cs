﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Telerik.Web.UI;

public partial class Basic_Data_Approve_Exp : System.Web.UI.Page
{
    AppService ser = new AppService();
    
    DataTable Dst = new DataTable();
    DataTable dt = new DataTable();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Session["Logged_User"] == null)
            {
                Response.Redirect("~/Default.aspx");
            }
            else
            {
                Session["OPEN_YEAR"] = ser.Get_Ass_Open_Year().ToString();
                Session["OPEN_VERSION"] = ser.Get_Ass_Open_Version().ToString();
                int XX = ser.Check_HR_User(Session["Logged_User"].ToString());
                if (XX == 0) { Response.Redirect("~/Setting/Unauthorized_Pg.aspx"); }
                Fill_Grid("ALL");
            }
        }
    }

    private void Fill_Grid(string dept)
    {
        try
        {
            //Clear_Grids();
            
            dt = ser.Bind_ASS_Approve_Exceptions_GRID(Session["OPEN_YEAR"].ToString(), Session["OPEN_VERSION"].ToString());
            RadGrid1.DataSource = dt;
            RadGrid1.DataBind();
        }
        catch
        {

        }
    }
    protected void RadGrid1_NeedDataSource(object sender, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
    {
        Fill_Grid("ALL"); 
    }
    protected void RadGrid1_UpdateCommand(object sender, Telerik.Web.UI.GridCommandEventArgs e)
    {
        try
        {

            GridDataItem item = e.Item as GridDataItem;
            string id = (item.FindControl("IDLabel") as Label).Text;

            //int deleted = ser.Check_Deleted_Plants(id);
            //if (deleted == 0)
            //{
            int RESLT = ser.Approve_ASS_Exception(id, Session["Logged_User"].ToString(), Session["OPEN_YEAR"].ToString(), Session["OPEN_VERSION"].ToString());
            if (RESLT == 1)
            {
                //Fill_Grid();
                Fill_Grid("ALL");
                RadNotification1.Text = "تم عمل الموافقة بنجاح";
                RadNotification1.Title = "Title";
                RadNotification1.TitleIcon = "info";
                RadNotification1.ContentIcon = "info";
                RadNotification1.Show();
               // Clear();
            }
            else
            {
                RadNotification1.Text = "يوجد مشكله في عملية الموافقة";
                RadNotification1.Title = "Title";
                RadNotification1.TitleIcon = "info";
                RadNotification1.ContentIcon = "info";
                RadNotification1.Show();
            }
            //}
            //else
            //{
            //    RadNotification1.Text = "لا يمكنك حذف المصنع لوجود بيانات فرعية تابعة له";
            //    RadNotification1.Title = "Title";
            //    RadNotification1.TitleIcon = "info";
            //    RadNotification1.ContentIcon = "info";
            //    RadNotification1.Show();
            //    return;
            //}

            // Fill_Grid();
        }
        catch
        {

        }
    }
    protected void RadGrid1_ItemDataBound(object sender, GridItemEventArgs e)
    {

    }
    protected void RadGrid1_ItemCreated(object sender, GridItemEventArgs e)
    {

    }
    protected void RadGrid1_ItemCommand(object sender, GridCommandEventArgs e)
    {
        if (e.CommandName == "APPROVE")
        {
            GridDataItem item = e.Item as GridDataItem;
            Label IDlbl = item.FindControl("IDLabel") as Label;

            int RESLT = ser.Approve_ASS_Exception(IDlbl.Text, Session["Logged_User"].ToString(), Session["OPEN_YEAR"].ToString(), Session["OPEN_VERSION"].ToString());
            if (RESLT == 1)
            {
                //Fill_Grid();
                Fill_Grid("ALL");
                RadNotification1.Text = "تم عمل الموافقة بنجاح";
                RadNotification1.Title = "Title";
                RadNotification1.TitleIcon = "info";
                RadNotification1.ContentIcon = "info";
                RadNotification1.Show();
                // Clear();
            }
            else
            {
                RadNotification1.Text = "يوجد مشكله في عملية الموافقة";
                RadNotification1.Title = "Title";
                RadNotification1.TitleIcon = "info";
                RadNotification1.ContentIcon = "info";
                RadNotification1.Show();
            }
           
        }
    }
}
