﻿<%@ Page Language="C#" MasterPageFile="~/Main_Master_Page.master" AutoEventWireup="true" CodeFile="AS_Grads.aspx.cs" Inherits="Basic_Data_AS_Grads" Title="المستويات الوظيفية" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<script type = "text/javascript">
        function Confirm() {
            var confirm_value = document.createElement("INPUT");
            confirm_value.type = "hidden";
            confirm_value.name = "confirm_value";
            if (confirm("هل تريد حذف هذه البيانات ?")) {
                confirm_value.value = "Yes";
            } else {
                confirm_value.value = "No";
            }
            document.forms[0].appendChild(confirm_value);
        }
    </script>
      <table style="width: 100%;" align="center">
 <tr>
    <td style="background-color:#e6f1f8; height: 2px; width: 70%;" >
    <span  style="font-size:11pt; font-weight:bold; color:#000080; font-family:Courier New;">
        &nbsp; المستويات الوظيفية </span>
    </td></tr>
 </table><p>
    </p>
    <p>
        <table align="center" 
            
            style="width: 80%; height: 100px; float: right; margin-top: 50px; margin-right: 30px; ">
            <tr>
                <td align="center">
            <asp:Label ID="lbl_MSG" runat="server" Font-Bold="True" ForeColor="Red" 
                        Font-Size="12pt" Font-Names="Arial"></asp:Label>
                </td>
                <td>
                    &nbsp;</td>
                <td rowspan="11">
            <asp:GridView ID="GridView1" runat="server" AllowPaging="True" 
                EmptyDataText="لا يوجد مستويات الوظيفية" Caption="المستويات الوظيفية" AutoGenerateColumns="False" 
                CaptionAlign="Top" onselectedindexchanged="GridView1_SelectedIndexChanged" 
                CssClass="mGrid" PagerStyle-CssClass="pgr" 
                AlternatingRowStyle-CssClass="alt" AutoGenerateSelectButton="True" 
                DataKeyNames="CODE" onpageindexchanging="GridView1_PageIndexChanging" 
                onselectedindexchanging="GridView1_SelectedIndexChanging" Width="500px" 
                        Font-Names="Arial" Font-Size="13pt">
                
                <Columns>
                        <asp:TemplateField HeaderText="رقم المستوي" SortExpression="Value">
                            <ItemTemplate>
                                <asp:Label ID="lblgrd_code" runat="server" Text='<%# Bind("CODE") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                             <asp:TemplateField HeaderText="اسم المستوي">
                          <ItemTemplate>
                                <asp:Label ID="lblgrd_name" runat="server" Text='<%# Bind("NAME") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                            <asp:TemplateField HeaderText="النموذج" Visible="false">
                          <ItemTemplate>
                                <asp:Label ID="lblform_id" runat="server" Text='<%# Bind("FORM_ID") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                         <asp:TemplateField HeaderText="النموذج">
                          <ItemTemplate>
                                <asp:Label ID="lblform_name" runat="server" Text='<%# Bind("FORM_NAME") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        
                    </Columns>
                
                <PagerStyle />
                <AlternatingRowStyle />
            </asp:GridView>
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td>
            <asp:Label ID="Label9" runat="server" Font-Bold="True" Text="المستوي الوظيفي" 
                Width="110px" Font-Size="12pt" Font-Names="Arial" ForeColor="#99CCFF"></asp:Label>
                    <asp:DropDownList ID="dd_grads" runat="server" Font-Bold="True" 
                        Font-Size="12pt" Width="250px">
                    </asp:DropDownList>
                </td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td>
            <asp:Label ID="Label10" runat="server" Font-Bold="True" Text="رقم النموذج" 
                Width="110px" Font-Size="12pt" Font-Names="Arial" ForeColor="#99CCFF"></asp:Label>
                    <asp:DropDownList ID="dd_forms" runat="server" Font-Bold="True" 
                        Font-Size="12pt" Width="250px">
                    </asp:DropDownList>
                </td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td>
            <asp:Button ID="btn_new" runat="server" BackColor="#3366FF" Font-Bold="True" 
                Font-Size="15pt" ForeColor="White" Text="جديد" Width="100px" 
                onclick="btn_new_Click" />
            <asp:Button ID="btn_save" runat="server" BackColor="#3366FF" Font-Bold="True" 
                Font-Size="15pt" ForeColor="White" Text="تسجيل" Width="100px" 
                onclick="btn_save_Click" />
            <asp:Button ID="btn_update" runat="server" BackColor="#3366FF" Font-Bold="True" 
                Font-Size="15pt" ForeColor="White" Text="تعديل" Width="100px" 
                onclick="btn_update_Click" />
            <asp:Button ID="btn_delete" runat="server" BackColor="#3366FF" Font-Bold="True" 
                Font-Size="15pt" ForeColor="White" Text="إلغاء" Width="100px" 
                onclick="btn_delete_Click" onclientclick="Confirm()" />
                </td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
        </table>
        <br />
    </p>
</asp:Content>

