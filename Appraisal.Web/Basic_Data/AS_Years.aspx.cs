﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;


public partial class Basic_Data_AS_Years : System.Web.UI.Page
{
    AppService ser = new AppService();
    
    DataTable dt = new DataTable();

    protected void Page_PreInit(object sender, EventArgs e)
    {
        if (Session["Logged_User"] == null)
        {
            Response.Redirect("~/Default.aspx");
        }
        int XX = ser.Check_HR_User(Session["Logged_User"].ToString());
        if (XX == 0) { Response.Redirect("~/Setting/Unauthorized_Pg.aspx"); }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        lbl_MSG.Text = "";
        if (!IsPostBack)
        {
            if (Session["Logged_User"] == null)
            {
                Response.Redirect("~/Default.aspx");
            }
            //*************************
            Fill_Grid();
            //*************************
            btn_update.Visible = false;
            btn_delete.Visible = false;
            btn_save.Visible = true;
            //**************************
        }
    }
    private void Fill_Grid()
    {
        try
        {
            Clear_Grids();
            
            dt = ser.Bind_YEARS_GRID();
            GridView1.DataSource = dt;
            GridView1.DataBind();
        }
        catch
        {

        }
    }
    private void Clear_Grids()
    {
        GridView1.DataSource = null;
        GridView1.DataBind();
    }
    private void Clear()
    {

        txt_year.Text = "";
        Session["YEAR_ID"] = "";
        //rd_close.Checked = false;
        //rd_open.Checked = false;
        //*********************************
        btn_save.Visible = true;
        btn_update.Visible = false;
        btn_delete.Visible = false;
        //***********************************
    }
    protected void btn_delete_Click(object sender, EventArgs e)
    {
        try
        {
            if (txt_year.Text == "")
            {
                lbl_MSG.Text = "برجاء ملئ البيانات الفارغة";
                return;
            }
            else
            {
                int RESLT = ser.Delete_Years_Data(Session["YEAR_ID"].ToString());
                if (RESLT == 1)
                {
                    Fill_Grid();
                    lbl_MSG.Text = "تم حذف البيانات بنجاح";
                    Clear();
                }
                else
                {
                    lbl_MSG.Text = "يوجد مشكله في عملية الحذف";
                }
            }
        }
        catch
        {
            lbl_MSG.Text = "يوجد مشكله في عملية الحذف";
        }
    }
    private string Get_Status()
    {
        string val = "";
        if (rd_open.Checked == true) { val = "Y"; } else
            if (rd_close.Checked == true) { val = "N"; }
        return val;
    }

    private string Get_Survey_Status()
    {
        string val = "";
        if (rd_open_survy.Checked == true) { val = "Y"; }
        else
            if (rd_close_survy.Checked == true) { val = "N"; }
        return val;
    }

    protected void btn_save_Click(object sender, EventArgs e)
    {
        try
        {
            if (txt_year.Text == "")
            {
                lbl_MSG.Text = "برجاء ملئ البيانات الفارغة";
                return;
            }
            else
            {
                int OPENY = ser.Check_Open_Years("APP");
                int SURVEY_OPEN = ser.Check_Open_Years("SUR");
                string stus = Get_Status().ToString();
                string survey_stus = Get_Survey_Status().ToString();
                if ((stus == "Y" && OPENY > 0) || ( survey_stus == "Y" && SURVEY_OPEN > 0 ))
                {
                    Alert.Show(" يوجد سنة مفتوحة لا يمكن فتح اكثر من سنة ");
                    rd_close.Checked = true;
                    rd_open.Checked = false;

                    rd_close_survy.Checked = true;
                    rd_open_survy.Checked = false;

                    return;
                }
                else
                {
                    int RESLT = ser.Save_Years_Data(txt_year.Text, stus, survey_stus);
                    if (RESLT == 1)
                    {
                        Fill_Grid();
                        lbl_MSG.Text = "تم حفظ البيانات بنجاح";
                        Clear();
                    }
                    else
                    {
                        lbl_MSG.Text = "يوجد مشكله في عملية الادخال";
                    }
                }
            }
        }
        catch
        {
            lbl_MSG.Text = "يوجد مشكله في عملية الادخال";
        }
    }
    protected void btn_new_Click(object sender, EventArgs e)
    {
        Clear();
    }
    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        Fill_Grid();
        GridView1.PageIndex = e.NewPageIndex;
        GridView1.DataBind();
    }
    protected void GridView1_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            Session["YEAR_ID"] = ((Label)GridView1.SelectedRow.FindControl("lblyear")).Text;
            txt_year.Text = ((Label)GridView1.SelectedRow.FindControl("lblyear")).Text;
            string Status = ((Label)GridView1.SelectedRow.FindControl("lblstatus")).Text;
            if (Status.Trim() == "Y")
            { 
                rd_open.Checked = true;
                rd_close.Checked = false;
            }
            else 
            {
                rd_open.Checked = false;
                rd_close.Checked = true;
            }
            //***********************************************
            string SStatus = ((Label)GridView1.SelectedRow.FindControl("lblsurvey")).Text;
            if (SStatus.Trim() == "Y")
            {
                rd_open_survy.Checked = true;
                rd_close_survy.Checked = false;
            }
            else
            {
                rd_open_survy.Checked = false;
                rd_close_survy.Checked = true;
            }
            //********************************* display buttons
            btn_save.Visible = false;
            btn_update.Visible = true;
            btn_delete.Visible = true;
            //*************************************************
        }
        catch
        {
            Clear();
        }
    }
    protected void GridView1_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
    {

    }
    protected void RadioButton1_CheckedChanged(object sender, EventArgs e)
    {

    }
    protected void btn_update_Click(object sender, EventArgs e)
    {
        try
        {
            if (txt_year.Text == "")
            {
                lbl_MSG.Text = "برجاء ملئ البيانات الفارغة";
                return;
            }
            else
            {
                int OPENY = ser.Check_Open_Years("APP");
                int SURVEY_OPEN = ser.Check_Open_Years("SUR");
                string stus = Get_Status().ToString();
                string survey_stus = Get_Survey_Status().ToString();
                if ((stus == "Y" && OPENY > 0) || (survey_stus == "Y" && SURVEY_OPEN > 0))
                {
                    Alert.Show(" يوجد سنة مفتوحة لا يمكن فتح اكثر من سنة ");
                    rd_close.Checked = true;
                    rd_open.Checked = false;

                    rd_close_survy.Checked = true;
                    rd_open_survy.Checked = false;

                    return;
                }
                else
                {
                    int RESLT = ser.Update_Years_Data(Session["YEAR_ID"].ToString(), stus, survey_stus );
                    if (RESLT == 1)
                    {
                        Fill_Grid();
                        lbl_MSG.Text = "تم نعديل البيانات بنجاح";
                    }
                    else
                    {
                        lbl_MSG.Text = "يوجد مشكله في عملية التعديل";
                    }
                }
            }
        }
        catch
        {
            lbl_MSG.Text = "يوجد مشكله في عملية التعديل";
        }
    }
}
