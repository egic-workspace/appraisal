﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;


public partial class Basic_Data_AS_Grads : System.Web.UI.Page
{
    AppService ser = new AppService();
    
    DataTable dt = new DataTable();

    protected void Page_PreInit(object sender, EventArgs e)
    {
        if (Session["Logged_User"] == null)
        {
            Response.Redirect("~/Default.aspx");
        }
        int XX = ser.Check_HR_User(Session["Logged_User"].ToString());
        if (XX == 0) { Response.Redirect("~/Setting/Unauthorized_Pg.aspx"); }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        lbl_MSG.Text = "";
        if (!IsPostBack)
        {
            if ( Session["Logged_User"] == null)
            {
                Response.Redirect("~/Default.aspx");
            }
            //****************************
            dt = ser.Bind_Forms_DDL();
            dd_forms.DataSource = dt;
            dd_forms.DataTextField = "FORM_NAME";
            dd_forms.DataValueField = "FORM_ID";
            dd_forms.DataBind();
            ListItem st = new ListItem("***** اختر النموذج *****", "0");
            dd_forms.Items.Insert(0, st);
            //****************************
            dt = ser.Bind_GRADS_DDL();
            dd_grads.DataSource = dt;
            dd_grads.DataTextField = "LEVEL_NAME";
            dd_grads.DataValueField = "LEVEL_CODE";
            dd_grads.DataBind();
            ListItem sto = new ListItem("***** اختر المستوي *****", "0");
            dd_grads.Items.Insert(0, sto);
            //****************************
            Fill_Grid();
            //*************************
            btn_update.Visible = false;
            btn_delete.Visible = false;
            btn_save.Visible = true;
            //**************************
        }
    }
    private void Fill_Grid()
    {
        try
        {
            Clear_Grids();
            
            dt = ser.Bind_GRAD_GRID();
            GridView1.DataSource = dt;
            GridView1.DataBind();
        }
        catch
        {

        }
    }
    private void Clear_Grids()
    {
        GridView1.DataSource = null;
        GridView1.DataBind();
    }
    protected void btn_new_Click(object sender, EventArgs e)
    {
        Clear();
    }
    protected void btn_save_Click(object sender, EventArgs e)
    {
        try
        {
            if (dd_grads.SelectedIndex == 0 || dd_forms.SelectedIndex == 0)
            {
                lbl_MSG.Text = "برجاء ملئ البيانات الفارغة";
                return;
            }
            else
            {    
                    string Oyear = ser.Get_Open_Year();
                    int XX = ser.Check_Forms(dd_forms.SelectedValue.ToString(), Oyear);
                    if (XX == 0)
                    {
                        int RESLT = ser.Save_Grad_Data(dd_grads.SelectedValue.ToString(), dd_grads.SelectedItem.ToString(), dd_forms.SelectedValue.ToString());
                        if (RESLT == 1)
                        {
                            Fill_Grid();
                            lbl_MSG.Text = "تم حفظ البيانات بنجاح";
                            Clear();
                        }
                        else
                        {
                            lbl_MSG.Text = "يوجد مشكله في عملية الادخال";
                        }
                    }
                    else
                    {
                        Alert.Show(" هذا النموذج تم عمل تقييمات عليه في هذا العام لعدد من الموظفين "+XX.ToString());
                        return;
                    }
            }
        }
        catch
        {
            lbl_MSG.Text = "يوجد مشكله في عملية الادخال";
        }
    }
    protected void btn_update_Click(object sender, EventArgs e)
    {
        try
        {
            if (dd_grads.SelectedIndex == 0 || dd_forms.SelectedIndex == 0 )
            {
                lbl_MSG.Text = "برجاء ملئ البيانات الفارغة";
                return;
            }
            else
            {
                    string Oyear = ser.Get_Open_Year();
                    int XX = ser.Check_Forms(dd_forms.SelectedValue.ToString(), Oyear);
                    if (XX == 0)
                    {
                        int RESLT = ser.Update_Grad_Data(Session["GRDCODE"].ToString(), dd_grads.SelectedItem.ToString(), dd_forms.SelectedValue.ToString());
                        if (RESLT == 1)
                        {
                            Fill_Grid();
                            lbl_MSG.Text = "تم نعديل البيانات بنجاح";
                        }
                        else
                        {
                            lbl_MSG.Text = "يوجد مشكله في عملية التعديل";
                        }
                    }
                    else
                    {
                        Alert.Show(" هذا النموذج تم عمل تقييمات عليه في هذا العام لعدد من الموظفين " + XX.ToString());
                        return;
                    }
            }
        }
        catch
        {
            lbl_MSG.Text = "يوجد مشكله في عملية التعديل";
        }
    }
    protected void btn_delete_Click(object sender, EventArgs e)
    {
        try
        {
            if (dd_grads.SelectedIndex == 0)
            {
                lbl_MSG.Text = "برجاء ملئ البيانات الفارغة";
                return;
            }
            else
            {
                   string Oyear = ser.Get_Open_Year();
                   int XX = ser.Check_Forms(dd_forms.SelectedValue.ToString(), Oyear);
                    if (XX == 0)
                    {
                        int RESLT = ser.Delete_Grad_Data(Session["GRDCODE"].ToString());
                        if (RESLT == 1)
                        {
                            Fill_Grid();
                            lbl_MSG.Text = "تم حذف البيانات بنجاح";
                            Clear();
                        }
                        else
                        {
                            lbl_MSG.Text = "يوجد مشكله في عملية الحذف";
                        }
                    }
                    else
                    {
                        Alert.Show(" هذا النموذج تم عمل تقييمات عليه في هذا العام لعدد من الموظفين " + XX.ToString());
                        return;
                    }
            }
        }
        catch
        {
            lbl_MSG.Text = "يوجد مشكله في عملية الحذف";
        }
    }
    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        Fill_Grid();
        GridView1.PageIndex = e.NewPageIndex;
        GridView1.DataBind();
    }
    protected void GridView1_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            Session["GRDCODE"] = ((Label)GridView1.SelectedRow.FindControl("lblgrd_code")).Text;
            //txtgrd_no.Text = ((Label)GridView1.SelectedRow.FindControl("lblgrd_code")).Text;
            //txtgrd_name.Text = ((Label)GridView1.SelectedRow.FindControl("lblgrd_name")).Text;
            if (((Label)GridView1.SelectedRow.FindControl("lblgrd_code")).Text != "")
            {
                dd_grads.ClearSelection();
                string grdd = ((Label)GridView1.SelectedRow.FindControl("lblgrd_code")).Text.ToString().Trim();
                dd_grads.Items.FindByValue(grdd).Selected = true;
            }
            if (((Label)GridView1.SelectedRow.FindControl("lblform_id")).Text != "")
            {
                dd_forms.ClearSelection();
                string frm = ((Label)GridView1.SelectedRow.FindControl("lblform_id")).Text.ToString().Trim();
                dd_forms.Items.FindByValue(frm).Selected = true;
            }
            //********************************* display buttons  lblform_id
            btn_save.Visible = false;
            btn_update.Visible = true;
            btn_delete.Visible = true;
            //*************************************************
        }
        catch
        {
            Clear();
        }
    }
    private void Clear()
    {

        //txtgrd_no.Text = "";
        //txtgrd_name.Text = "";
        dd_forms.SelectedIndex = 0;
        dd_grads.SelectedIndex = 0;
        //*********************************
        btn_save.Visible = true;
        btn_update.Visible = false;
        btn_delete.Visible = false;
        //***********************************
    }
    protected void GridView1_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
    {

    }
}
