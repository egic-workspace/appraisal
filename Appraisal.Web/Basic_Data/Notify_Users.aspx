﻿<%@ Page Language="C#" MasterPageFile="~/Main_Master_Page.master" AutoEventWireup="true" CodeFile="Notify_Users.aspx.cs" Inherits="Basic_Data_Notify_Users" Title="تنبيهات" validateRequest="false"%>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
   
    <link type="text/css" rel="stylesheet" href="../CSS/select2.min.css" />
    <script src="../JavaScript/jQuery.v1.9.1.min.js" type="text/javascript"></script>

<input type="hidden" runat="server" id="hiddenMailsTo" class="hiddenMailsTo" value=""/>
<input type="hidden" runat="server" id="hiddenMailsCC" class="hiddenMailsCC" value=""/>

<table style="width: 100%;" align="center">
 <tr>
    <td style="background-color:#71625F; height: 2px; width: 70%;" >
    <span  style="font-size:11pt; font-weight:bold; color:#FFFFFF; font-family:Courier New;">
        تنبيهات الموظفين
    </span>
    </td></tr>
 </table><p>
    </p>
    <p>
        <table align="center" style="margin-top:50px;padding: 15px;margin-left: auto;margin-right: auto;">
            <tr>
                <td align="center" style="text-align: left">
                    &nbsp;</td>
                <td>
            <asp:Label ID="lbl_MSG" runat="server" Font-Bold="True" ForeColor="Red" 
                        Font-Size="12pt" Font-Names="Arial"></asp:Label>
                </td>
                <td rowspan="11">
                    &nbsp;</td>
            </tr>
            
            <tr>
                <td>
                    &nbsp;</td>
                <td></td>
            </tr>
            <tr>
                <td style="text-align: left">
                    <asp:Label ID="Label8" runat="server" Font-Bold="True" Text="مُرسل إلي: " 
                        Width="76px" Font-Size="12pt" Font-Names="Arial" ForeColor="#231F20"></asp:Label>
                </td>
                <td>
<asp:DropDownList ID="ddlMailTo" Width="650px" runat="server" multiple="multiple" CssClass="form-control js-example-placeholder-single"
    ToolTip="Select ">
</asp:DropDownList>
                </td>
            </tr>
            
            <tr>
                <td>
                    &nbsp;</td>
                <td></td>
            </tr>
            <tr>
                <td style="text-align: left">
                    <asp:Label ID="Label1" runat="server" Font-Bold="True" Text="نسخة إلي: " 
                        Width="76px" Font-Size="12pt" Font-Names="Arial" ForeColor="#231F20"></asp:Label>
                </td>
                <td>
<asp:DropDownList ID="ddlMailCC" Width="650px" runat="server" multiple="multiple" CssClass="form-control js-example-placeholder-single"
    ToolTip="Select ">
</asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td style="text-align: left">
                    &nbsp;</td>
                <td></td></tr>
            <tr>
                <td align="center" style="text-align: left">
                    <asp:Label ID="Label9" runat="server" Font-Bold="True" Text="الموضوع: " 
                        Width="76px" Font-Size="12pt" Font-Names="Arial" ForeColor="#231F20"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txt_subject" runat="server" Font-Bold="True" Font-Size="13pt" 
                        Width="650px"></asp:TextBox>
                </td>
            </tr>
            
            <tr>
                <td style="text-align: left">
                    &nbsp;</td>
                <td></td></tr>
            <tr>
                <td style="text-align: left" valign="top">
                    <asp:Label ID="Label10" runat="server" Font-Bold="True" Text="نص الرسالة: " 
                        Width="76px" Font-Size="12pt" Font-Names="Arial" ForeColor="#231F20"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txt_message_body" runat="server" Font-Bold="True" Font-Size="13pt" 
                        Width="650px" Height="150px" TextMode="MultiLine" CssClass=""></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td></td>
                <td>
            <asp:Button ID="btn_send" runat="server" class="btn btn-primary" BackColor="#F79222"  Font-Bold="True" 
                Font-Size="15pt" ForeColor="White" Text="ارسال" Width="100px" 
                        onclick="btn_send_Click" />
                </td>
            </tr>
            <tr>
                <td style="text-align: left">
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
        </table>
        <br />
    </p>
    <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog modal-sm">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">تنبيهات الموظفين</h4>
        </div>
        <div class="modal-body">
          <asp:Label ID="lblMessage" runat="server" Font-Bold="True" Font-Size="12pt" 
                Font-Names="Arial" ForeColor="Green"></asp:Label>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>
  <script type="text/javascript">
    $('.select2-selection--multiple, .mceEditor').hide();
</script>
    <script src="../JavaScript/select2.min.js" type="text/javascript"></script>
    <script src="../JavaScript/tiny_mce.js" type="text/javascript"></script>
<script type="text/javascript">

    tinyMCE.init({
        // General options
        mode: "textareas",
        theme: "advanced",
        plugins: "pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups"           
    });
    $(".js-example-placeholder-single").select2({
            placeholder: "اختر",
            allowClear: true
    });
    $(function () {
        $("[id$='ddlMailTo']").on("select2:select select2:unselect", function (e) {
            var items = $(this).val();
            var selectedItemsStr = items.join();
            $('.hiddenMailsTo').val(selectedItemsStr);
        });
        $("[id$='ddlMailCC']").on("select2:select select2:unselect", function (e) {
            var items = $(this).val();
            var selectedItemsStr = items.join();
            $('.hiddenMailsCC').val(selectedItemsStr);
        });
        $('.select2-selection--multiple, .mceEditor').show();
    });
</script>
</asp:Content>

