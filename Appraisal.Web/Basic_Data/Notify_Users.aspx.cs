﻿using System;
using System.Collections;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data;
using System.Data.SqlClient;

using System.Net;
using System.Web.Mail;
using System.DirectoryServices;
using System.Collections.Generic;

public partial class Basic_Data_Notify_Users : System.Web.UI.Page
{
    AppService ser = new AppService();
    protected void Page_PreInit(object sender, EventArgs e)
    {
        if (Session["Logged_User"] == null)
        {
            Response.Redirect("~/Default.aspx");
        }
        int XX = ser.Check_HR_User(Session["Logged_User"].ToString());
        if (XX == 0) { Response.Redirect("~/Setting/Unauthorized_Pg.aspx"); }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        lbl_MSG.Text = "";
        if (!IsPostBack)
        {
            if (Session["Logged_User"] == null)
            {
                Response.Redirect("~/Default.aspx");
            }
            PopulateDropDownList();
        }
    }

    protected void btn_send_Click(object sender, EventArgs e)
    {
        try
        {
            if (string.IsNullOrEmpty(txt_subject.Text))
            {
                lbl_MSG.Text = "برجاء ادخال موضوع الرسالة";
                return;
            }
            else if (string.IsNullOrEmpty(txt_message_body.Text))
            {
                lbl_MSG.Text = "برجاء ادخال نص الرسالة";
                return;
            }
            else
            {
                //send mail here
                string mailsTo = hiddenMailsTo.Value;
                if (string.IsNullOrEmpty(mailsTo))
                {
                    lbl_MSG.Text = "برجاء تحديد المُرسل إلي";
                    return;
                }
                
                string currentUserMail = Session["MAIL"].ToString();
                string mailsCC = 
                    string.IsNullOrEmpty(hiddenMailsCC.Value) ? currentUserMail 
                    : hiddenMailsCC.Value.Contains(currentUserMail) ? hiddenMailsCC.Value 
                    : string.Format("{0},{1}", currentUserMail, hiddenMailsCC.Value);

                Send_Mail(mailsTo, mailsCC, txt_subject.Text, txt_message_body.Text);

            }
        }
        catch
        {
            lbl_MSG.Text = "يوجد مشكله في عملية الادخال";
        }
    }

    private void Clear()
    {
        txt_message_body.Text = "";
        txt_subject.Text = "";
        string currentUserMail = Session["MAIL"].ToString();
        for (int i = 0; i < ddlMailCC.Items.Count; i++)
            if (!ddlMailCC.Items[i].Value.Equals(currentUserMail, StringComparison.InvariantCultureIgnoreCase))
                ddlMailCC.Items[i].Selected = false;
        
        for (int i = 0; i < ddlMailTo.Items.Count; i++)
            ddlMailTo.Items[i].Selected = false;
    }

    private void PopulateDropDownList()
    {
        string currentUserMail = Session["MAIL"].ToString();
        var allUsers = ser.Get_All_Users();
        int selectedIndex = 0;
        List<KeyValuePair<string, string>> usersWithoutMails = new List<KeyValuePair<string, string>>();
        for (int i = 0; i < allUsers.Rows.Count; i++)
        {
            string name = allUsers.Rows[i]["eemp_name"].ToString();
            string mail = allUsers.Rows[i]["email"].ToString();            
            if (string.IsNullOrEmpty(name))
                name = mail;
            if (string.IsNullOrEmpty(mail))
            {
                usersWithoutMails.Add(new KeyValuePair<string,string>(allUsers.Rows[i]["emp_no"].ToString(),name));
                continue;
            }
            ddlMailTo.Items.Add(new ListItem(name, mail));
            ddlMailCC.Items.Add(new ListItem(name, mail));
            if (currentUserMail.Equals(mail, StringComparison.InvariantCultureIgnoreCase))
                selectedIndex = i > usersWithoutMails.Count ? i - usersWithoutMails.Count:usersWithoutMails.Count - i;
        }
        if (ddlMailCC.Items.Count > 0 )
            ddlMailCC.Items[selectedIndex].Selected = true;
        if (usersWithoutMails.Count > 0)
        {
            string connection = ConfigurationManager.ConnectionStrings["ADConnection"].ToString();
            DirectorySearcher dssearch = new DirectorySearcher(connection);
            
            
            for (int i = 0; i < usersWithoutMails.Count; i++)
            {
                string id = usersWithoutMails[i].Key;
                dssearch.Filter = string.Format("(description={0})", id);
                SearchResult sresult = dssearch.FindOne();

                if (sresult == null)
                {
                    ser.Update_Emp_Email(id, "---");
                    continue;
                }

                DirectoryEntry dsresult = sresult.GetDirectoryEntry();
                var mails = dsresult.Properties["mail"];
                if (mails.Count > 0)
                {
                    string name = usersWithoutMails[i].Value;
                    string mail = mails[0].ToString();
                    ddlMailTo.Items.Add(new ListItem(name, mail));
                    ddlMailCC.Items.Add(new ListItem(name, mail));
                    ser.Update_Emp_Email(id, mail);
                }
                else
                    ser.Update_Emp_Email(id, "---");
            }
        }
        ddlMailTo.DataBind();
        ddlMailCC.DataBind();
        
    }

    private void Send_Mail(string mailsTo, string cc_mails, string subject, string body)
    {
        try
        {
            MailMessage mm = new MailMessage();
            mm.To = mailsTo;
            mm.From = ConfigurationManager.AppSettings["Appraisal_Mail"];
            mm.Cc = cc_mails;
            mm.Bcc = "";
            mm.Subject = subject;
            body = string.Format("<pre>{0}</pre><p>----<br/>{1}</p>", body, ConfigurationManager.AppSettings["Appraisal_Mail_Name"]);
            mm.Body = body;
            mm.BodyFormat = MailFormat.Html;
            SmtpMail.SmtpServer = "Mail2.egic.com.eg";
            SmtpMail.Send(mm);
            Clear();
            Show_Modal_Message("تم إرسال الرسالة بنجاح");
        }
        catch (Exception ex)
        {
            
        }
    }

    private void Show_Modal_Message(string msg)
    {
        try
        {
            lblMessage.Text = msg;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "showModal();", true);
        }
        catch
        {

        }

    }
}