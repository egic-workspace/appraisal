﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;


public partial class Basic_Data_AS_Users : System.Web.UI.Page
{
    AppService ser = new AppService();
    
    DataTable dt = new DataTable();

    protected void Page_PreInit(object sender, EventArgs e)
    {
        if (Session["Logged_User"] == null)
        {
            Response.Redirect("~/Default.aspx");
        }
        int XX = ser.Check_HR_User(Session["Logged_User"].ToString());
        if (XX == 0) { Response.Redirect("~/Setting/Unauthorized_Pg.aspx"); }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        lbl_MSG.Text = "";
        if (!IsPostBack)
        {
            if (Session["Logged_User"] == null)
            {
                Response.Redirect("~/Default.aspx");
            }
            //*************************
            Fill_Grid();
            //*************************
           // btn_update.Visible = false;
            btn_delete.Visible = false;
            btn_save.Visible = true;
            //**************************
        }
    }
    private void Fill_Grid()
    {
        try
        {
            Clear_Grids();
            
            dt = ser.Bind_Users_GRID();
            GridView1.DataSource = dt;
            GridView1.DataBind();
        }
        catch
        {

        }
    }
    private void Clear_Grids()
    {
        GridView1.DataSource = null;
        GridView1.DataBind();
    }
    private void Clear()
    {

        txt_user_name.Text = "";
        Session["USER_ID"] = "";
      
        //*********************************
        btn_save.Visible = true;
        //btn_update.Visible = false;
        btn_delete.Visible = false;
        //***********************************
    }
    protected void btn_delete_Click(object sender, EventArgs e)
    {
        try
        {
            if (txt_user_name.Text == "")
            {
                lbl_MSG.Text = "برجاء ملئ البيانات الفارغة";
                return;
            }
            else
            {
                int RESLT = ser.Delete_User_Data(Session["USER_ID"].ToString());
                if (RESLT == 1)
                {
                    Fill_Grid();
                    lbl_MSG.Text = "تم حذف البيانات بنجاح";
                    Clear();
                }
                else
                {
                    lbl_MSG.Text = "يوجد مشكله في عملية الحذف";
                }
            }
        }
        catch
        {
            lbl_MSG.Text = "يوجد مشكله في عملية الحذف";
        }
    }

    protected void btn_save_Click(object sender, EventArgs e)
    {
        try
        {
            if (txt_user_name.Text == "")
            {
                lbl_MSG.Text = "برجاء ملئ البيانات الفارغة";
                return;
            }
            else
            {
              
                int RESLT = ser.Save_User_Data(txt_user_name.Text);
                if (RESLT == 1)
                {
                    Fill_Grid();
                    lbl_MSG.Text = "تم حفظ البيانات بنجاح";
                    Clear();
                }
                else
                {
                    lbl_MSG.Text = "يوجد مشكله في عملية الادخال";
                }
            }
        }
        catch
        {
            lbl_MSG.Text = "يوجد مشكله في عملية الادخال";
        }
    }

    protected void btn_new_Click(object sender, EventArgs e)
    {
        Clear();
    }
    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        Fill_Grid();
        GridView1.PageIndex = e.NewPageIndex;
        GridView1.DataBind();
    }
    protected void GridView1_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            Session["USER_ID"] = ((Label)GridView1.SelectedRow.FindControl("lbluser")).Text;
            txt_user_name.Text = ((Label)GridView1.SelectedRow.FindControl("lbluser")).Text;
           
            //********************************* display buttons
            btn_save.Visible = false;
            //btn_update.Visible = true;
            btn_delete.Visible = true;
            //*************************************************
        }
        catch
        {
            Clear();
        }
    }
    protected void GridView1_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
    {

    }

}
