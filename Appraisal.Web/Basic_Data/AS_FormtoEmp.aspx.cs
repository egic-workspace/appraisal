﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;


public partial class Basic_Data_AS_FormtoEmp : System.Web.UI.Page
{
    AppService ser = new AppService();
    
    DataTable dt = new DataTable();

    protected void Page_PreInit(object sender, EventArgs e)
    {
        if (Session["Logged_User"] == null)
        {
            Response.Redirect("~/Default.aspx");
        }
        int XX = ser.Check_HR_User(Session["Logged_User"].ToString());
        if (XX == 0) { Response.Redirect("~/Setting/Unauthorized_Pg.aspx"); }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        lbl_MSG.Text = "";
        if (!IsPostBack)
        {
            if (Session["Logged_User"] == null)
            {
                Response.Redirect("~/Default.aspx");
            }
            //*************************
            Fill_Grid();
            //*************************
            dt = ser.Bind_EMP_DDL();
            dd_emp_name.DataSource = dt;
            dd_emp_name.DataTextField = "EMP_NAME";
            dd_emp_name.DataValueField = "EMP_NO";
            dd_emp_name.DataBind();
            ListItem lst = new ListItem("***** اختر الموظف *****", "0");
            dd_emp_name.Items.Insert(0, lst);
            //****************************
            dt = ser.Bind_Forms_DDL();
            dd_forms.DataSource = dt;
            dd_forms.DataTextField = "FORM_NAME";
            dd_forms.DataValueField = "FORM_ID";
            dd_forms.DataBind();
            ListItem st = new ListItem("***** اختر النموذج *****", "0");
            dd_forms.Items.Insert(0, st);
            //****************************
            btn_update.Visible = false;
            btn_delete.Visible = false;
            btn_save.Visible = true;
            //**************************
        }
    }
    private void Fill_Grid()
    {
        try
        {
            string Oyear = ser.Get_Open_Year();
            Clear_Grids();
            
            dt = ser.Bind_EMP_FORMS_GRID(Oyear);
            GridView1.DataSource = dt;
            GridView1.DataBind();
        }
        catch
        {

        }
    }
    private void Clear_Grids()
    {
        GridView1.DataSource = null;
        GridView1.DataBind();
    }
    private void Clear()
    {

        txtnotes.Text = "";
        dd_emp_name.SelectedIndex = 0;
        dd_forms.SelectedIndex = 0;
        //*********************************
        btn_save.Visible = true;
        btn_update.Visible = false;
        btn_delete.Visible = false;
        //***********************************
    }
    protected void btn_new_Click(object sender, EventArgs e)
    {
        Clear();
    }
    protected void btn_save_Click(object sender, EventArgs e)
    {
        try
        {
            if ( dd_emp_name.SelectedIndex == 0 || dd_forms.SelectedIndex == 0 )
            {
                lbl_MSG.Text = "برجاء ملئ البيانات الفارغة";
                return;
            }
            else
            {
                string Oyear = ser.Get_Open_Year();
                int ZZ = ser.Check_EMP_IN_FORMS(dd_emp_name.SelectedValue.ToString(),Oyear);
                if (ZZ >= 1)
                {
                    Alert.Show(" يوجد موظف مسجل من قبل ");
                    return;
                }
                else
                {
                   // string oyear = ser.Get_Open_Year();
                    int XX = ser.Check_Saved_Forms(dd_emp_name.SelectedValue.ToString(), Oyear);
                    if (XX == 0)
                    {
                        int RESLT = ser.Save_FRM_EMP_Data(dd_emp_name.SelectedValue.ToString(), dd_forms.SelectedValue.ToString(), txtnotes.Text,Oyear);
                        if (RESLT == 1)
                        {
                            Fill_Grid();
                            lbl_MSG.Text = "تم حفظ البيانات بنجاح";
                            Clear();
                        }
                        else
                        {
                            lbl_MSG.Text = "يوجد مشكله في عملية الادخال";
                        }
                    }
                    else
                    {
                        Alert.Show(" هذا الموظف له تقييم في هذا العام برجاء الغاؤه قبل حفظ النموذج ");
                        return;
                    }
                }
            }
        }
        catch
        {
            lbl_MSG.Text = "يوجد مشكله في عملية الادخال";
        }
    }
    protected void btn_update_Click(object sender, EventArgs e)
    {
        try
        {
            if (dd_emp_name.SelectedIndex == 0 || dd_forms.SelectedIndex == 0)
            {
                lbl_MSG.Text = "برجاء ملئ البيانات الفارغة";
                return;
            }
            else
            {
                    string Oyear = ser.Get_Open_Year();
                    int XX = ser.Check_Saved_Forms(Session["FRMEMPID"].ToString(), Oyear);
                    if (XX == 0)
                    {
                        int RESLT = ser.Update_FRM_EMP_Data(Session["FRMEMPID"].ToString(), dd_forms.SelectedValue.ToString(), txtnotes.Text, Oyear);
                        if (RESLT == 1)
                        {
                            Fill_Grid();
                            lbl_MSG.Text = "تم نعديل البيانات بنجاح";
                        }
                        else
                        {
                            lbl_MSG.Text = "يوجد مشكله في عملية التعديل";
                        }
                    }
                    else
                    {
                        Alert.Show(" هذا الموظف له تقييم في هذا العام برجاء الغاؤه قبل تغيير النموذج ");
                        return;
                    }

             }
        }
        catch
        {
            lbl_MSG.Text = "يوجد مشكله في عملية التعديل";
        }
    }
    protected void btn_delete_Click(object sender, EventArgs e)
    {
        try
        {
            if (dd_emp_name.SelectedIndex == 0 || dd_forms.SelectedIndex == 0)
            {
                lbl_MSG.Text = "برجاء ملئ البيانات الفارغة";
                return;
            }
            else
            {
                    string Oyear = ser.Get_Open_Year();
                    int XX = ser.Check_Saved_Forms(Session["FRMEMPID"].ToString(), Oyear);
                    if (XX == 0)
                    {
                        int RESLT = ser.Delete_FRM_EMP_Data(Session["FRMEMPID"].ToString(), Oyear);
                        if (RESLT == 1)
                        {
                            Fill_Grid();
                            lbl_MSG.Text = "تم حذف البيانات بنجاح";
                            Clear();
                        }
                        else
                        {
                            lbl_MSG.Text = "يوجد مشكله في عملية الحذف";
                        }
                    }
                    else
                    {
                        Alert.Show(" هذا الموظف له تقييم في هذا العام برجاء الغاؤه قبل حذف النموذج ");
                        return;
                    }
            }
        }
        catch
        {
            lbl_MSG.Text = "يوجد مشكله في عملية الحذف";
        }
    }
    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        Fill_Grid();
        GridView1.PageIndex = e.NewPageIndex;
        GridView1.DataBind();
    }
    protected void GridView1_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            Session["FRMEMPID"] = ((Label)GridView1.SelectedRow.FindControl("lblemp_no")).Text;
            if (((Label)GridView1.SelectedRow.FindControl("lblemp_no")).Text != "")
            {
                dd_emp_name.ClearSelection();
                string emp = ((Label)GridView1.SelectedRow.FindControl("lblemp_no")).Text.ToString().Trim();
                dd_emp_name.Items.FindByValue(emp).Selected = true;
            }
            if (((Label)GridView1.SelectedRow.FindControl("lblform_id")).Text != "")
            {
                dd_forms.ClearSelection();
                string frm = ((Label)GridView1.SelectedRow.FindControl("lblform_id")).Text.ToString().Trim();
                dd_forms.Items.FindByValue(frm).Selected = true;
            }
            
            txtnotes.Text = ((Label)GridView1.SelectedRow.FindControl("lblnotes")).Text;
            //********************************* display buttons
            btn_save.Visible = false;
            btn_update.Visible = true;
            btn_delete.Visible = true;
            //*************************************************
        }
        catch
        {
            Clear();
        }
    }
    protected void GridView1_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
    {

    }
}
