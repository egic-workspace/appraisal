﻿<%@ Page Language="C#" MasterPageFile="~/Main_Master_Page.master" AutoEventWireup="true" CodeFile="AS_Years.aspx.cs" Inherits="Basic_Data_AS_Years" Title="سنوات التقييم" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <script type = "text/javascript">
        function Confirm() {
            var confirm_value = document.createElement("INPUT");
            confirm_value.type = "hidden";
            confirm_value.name = "confirm_value";
            if (confirm("هل تريد حذف هذه البيانات ?")) {
                confirm_value.value = "Yes";
            } else {
                confirm_value.value = "No";
            }
            document.forms[0].appendChild(confirm_value);
        }
    </script>
      <table style="width: 100%;" align="center">
 <tr>
    <td style="background-color:#e6f1f8; height: 2px; width: 70%;" >
    <span  style="font-size:11pt; font-weight:bold; color:#000080; font-family:Courier New;">
        &nbsp; سنوات التقييم </span>
    </td></tr>
 </table><p>
    </p>
    <p>
        <table align="center" 
            
            style="width: 80%; height: 100px; float: right; margin-top: 50px; margin-right: 30px; ">
            <tr>
                <td align="center">
            <asp:Label ID="lbl_MSG" runat="server" Font-Bold="True" ForeColor="Red" 
                        Font-Size="12pt" Font-Names="Arial"></asp:Label>
                </td>
                <td>
                    &nbsp;</td>
                <td rowspan="11">
            <asp:GridView ID="GridView1" runat="server" AllowPaging="True" 
                EmptyDataText="لا توجد سنوات مسجلة" Caption="سنوات التقييم" AutoGenerateColumns="False" 
                CaptionAlign="Top" onselectedindexchanged="GridView1_SelectedIndexChanged" 
                CssClass="mGrid" PagerStyle-CssClass="pgr" 
                AlternatingRowStyle-CssClass="alt" AutoGenerateSelectButton="True" 
                DataKeyNames="YEAR" onpageindexchanging="GridView1_PageIndexChanging" 
                onselectedindexchanging="GridView1_SelectedIndexChanging" Width="500px" 
                        Font-Names="Arial" Font-Size="13pt">
                
                <Columns>
                        <asp:TemplateField HeaderText="رقم السنة" SortExpression="Value">
                            <ItemTemplate>
                                <asp:Label ID="lblyear" runat="server" Text='<%# Bind("YEAR") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                         <asp:TemplateField HeaderText="حالة التقييم" >
                            <ItemTemplate>
                                <asp:Label ID="lblnstatus" runat="server" Text='<%# Bind("NSTATUS") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                          <asp:TemplateField HeaderText="status id" Visible="false" >
                            <ItemTemplate>
                                <asp:Label ID="lblstatus" runat="server" Text='<%# Bind("STATUS") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                           <asp:TemplateField HeaderText="حالة الاستقصاء" >
                            <ItemTemplate>
                                <asp:Label ID="lblsstatus" runat="server" Text='<%# Bind("SSTATUS") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                          <asp:TemplateField HeaderText="status id" Visible="false" >
                            <ItemTemplate>
                                <asp:Label ID="lblsurvey" runat="server" Text='<%# Bind("SURVEY") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        
                    </Columns>
                
                <PagerStyle />
                <AlternatingRowStyle />
            </asp:GridView>
                </td>
            </tr>
            <tr>
                <td>
            <asp:Label ID="Label8" runat="server" Font-Bold="True" Text="رقم السنة" 
                Width="90px" Font-Size="12pt" Font-Names="Arial" ForeColor="#0000CC"></asp:Label>
                    <asp:TextBox ID="txt_year" runat="server" Font-Bold="True" Font-Size="13pt" 
                        Width="150px"></asp:TextBox>
                </td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td>
            <asp:Label ID="Label9" runat="server" Font-Bold="True" Text="حالة التقييم" 
                Width="110px" Font-Size="12pt" Font-Names="Arial" ForeColor="#0000CC"></asp:Label>
                    <asp:RadioButton ID="rd_close" runat="server" Font-Bold="True" 
                        Font-Size="12pt" ForeColor="#003366" GroupName="A" 
                        oncheckedchanged="RadioButton1_CheckedChanged" Text="مغلقة" />
                    <asp:RadioButton ID="rd_open" runat="server" Font-Bold="True" 
                        Font-Size="12pt" ForeColor="#003366" GroupName="A" Text="مفتوحة" 
                        Checked="True" />
                </td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td align="right">
            <asp:Label ID="Label11" runat="server" Font-Bold="True" Text="حالة الاستقصاء" 
                Width="110px" Font-Size="12pt" Font-Names="Arial" ForeColor="#0000CC"></asp:Label>
                    <asp:RadioButton ID="rd_close_survy" runat="server" Font-Bold="True" 
                        Font-Size="12pt" ForeColor="#003366" GroupName="B" 
                        oncheckedchanged="RadioButton1_CheckedChanged" Text="مغلقة" />
                    <asp:RadioButton ID="rd_open_survy" runat="server" Font-Bold="True" 
                        Font-Size="12pt" ForeColor="#003366" GroupName="B" Text="مفتوحة" 
                        Checked="True" />
                    </td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td>
            <asp:Label ID="Label10" runat="server" Font-Bold="True" Text="لابد من وجود سنة واحدة مفتوحة فقط" 
                Width="250px" Font-Size="12pt" Font-Names="Arial" ForeColor="#FF0066"></asp:Label>
                    </td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td>
            <asp:Button ID="btn_new" runat="server" BackColor="#3366FF" Font-Bold="True" 
                Font-Size="15pt" ForeColor="White" Text="جديد" Width="100px" 
                onclick="btn_new_Click" />
            <asp:Button ID="btn_save" runat="server" BackColor="#3366FF" Font-Bold="True" 
                Font-Size="15pt" ForeColor="White" Text="تسجيل" Width="100px" 
                onclick="btn_save_Click" />
            <asp:Button ID="btn_update" runat="server" BackColor="#3366FF" Font-Bold="True" 
                Font-Size="15pt" ForeColor="White" Text="تعديل" Width="100px" 
                onclick="btn_update_Click" />
            <asp:Button ID="btn_delete" runat="server" BackColor="#3366FF" Font-Bold="True" 
                Font-Size="15pt" ForeColor="White" Text="إلغاء" Width="100px" 
                onclick="btn_delete_Click" onclientclick="Confirm()" />
                </td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
        </table>
        <br />
    </p>
</asp:Content>

