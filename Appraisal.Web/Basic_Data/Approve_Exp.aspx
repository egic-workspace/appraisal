﻿<%@ Page Language="C#" MasterPageFile="~/Main_Master_Page.master" AutoEventWireup="true" CodeFile="Approve_Exp.aspx.cs" Inherits="Basic_Data_Approve_Exp" Title="موافقات الإستثناءات" %>
<%@ Register assembly="Telerik.Web.UI" namespace="Telerik.Web.UI" tagprefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <link type="text/css" rel="stylesheet" href="../CSS/select2.min.css" />
    <script src="../JavaScript/jQuery.v1.9.1.min.js" type="text/javascript"></script>

<input type="hidden" runat="server" id="hiddenMailsTo" class="hiddenMailsTo" value=""/>
<input type="hidden" runat="server" id="hiddenMailsCC" class="hiddenMailsCC" value=""/>

<table style="width: 100%;" align="center">
 <tr>
    <td style="background-color:#71625F; height: 2px; width: 70%;" >
    <span  style="font-size:11pt; font-weight:bold; color:#FFFFFF; font-family:Courier New;">
         موافقات الإستثناءات
    </span>
    </td></tr>
 </table><p>
                <asp:ScriptManager ID="ScriptManager1" runat="server">
                </asp:ScriptManager>
    </p>
    <p>
        <table align="center" width="80%">
            <tr>
                <td align="center" style="text-align: left">
                    &nbsp;</td>
                <td>
                    <telerik:RadGrid ID="RadGrid1" runat="server" AllowFilteringByColumn="True" 
                        AllowPaging="True" AllowSorting="True" CellSpacing="0" GridLines="None" 
                        ShowGroupPanel="True" Skin="WebBlue" AutoGenerateColumns="False" 
                        OnNeedDataSource="RadGrid1_NeedDataSource" 
                        OnItemDataBound="RadGrid1_ItemDataBound" 
                        OnUpdateCommand="RadGrid1_UpdateCommand" OnItemCreated="RadGrid1_ItemCreated" 
                        Height="800px" onitemcommand="RadGrid1_ItemCommand">
                            <HeaderContextMenu CssClass="GridContextMenu GridContextMenu_Default">
                            </HeaderContextMenu>
                            <MasterTableView CommandItemDisplay="Top" AllowPaging="true">
                                <CommandItemSettings ShowExportToCsvButton="true" ShowExportToExcelButton="true" ShowExportToWordButton="true" ShowAddNewRecordButton="false" ShowExportToPdfButton="false" />
                                <RowIndicatorColumn FilterControlAltText="Filter RowIndicator column">
                                </RowIndicatorColumn>
                                <ExpandCollapseColumn FilterControlAltText="Filter ExpandColumn column">
                                </ExpandCollapseColumn>
                                <Columns>
                                 <telerik:GridButtonColumn ButtonType="ImageButton"  CommandName="APPROVE" FilterControlAltText="Filter column column" Text="موافقة" UniqueName="column" ImageUrl="~/Images/xp-ok.gif">
                                        <ItemStyle BackColor="Transparent" BorderStyle="Solid" Font-Bold="True" Font-Names="Arial" Font-Size="10pt" ForeColor="Black" HorizontalAlign="Center" />
                                    </telerik:GridButtonColumn>
                                    <telerik:GridTemplateColumn DataField="EMP_NO" FilterControlAltText="Filter EMP_NO column" GroupByExpression="EMP_NO Group By EMP_NO" HeaderText="كود الموظف" SortExpression="EMP_NO" UniqueName="EMP_NO">
                                        <ItemTemplate>
                                            <asp:Label ID="IDLabel" runat="server" Text='<%# Eval("EMP_NO") %>'></asp:Label>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                    
                                    <telerik:GridTemplateColumn DataField="EMP_NAME" FilterControlAltText="Filter EMP_NAME column" GroupByExpression="EMP_NAME Group By EMP_NAME" HeaderText="اسم الموظف" SortExpression="EMP_NAME" UniqueName="EMP_NAME">
                                        <ItemTemplate>
                                            <asp:Label ID="NAMELabel" runat="server" Text='<%# Eval("EMP_NAME") %>'></asp:Label>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                    
                                    <telerik:GridTemplateColumn DataField="SECTION_NAME" FilterControlAltText="Filter SECTION_NAME column" GroupByExpression="SECTION_NAME Group By SECTION_NAME" HeaderText="الإدارة" SortExpression="SECTION_NAME" UniqueName="SECTION_NAME">
                                        <ItemTemplate>
                                            <asp:Label ID="SNAMELabel" runat="server" Text='<%# Eval("SECTION_NAME") %>'></asp:Label>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                    
                                    <telerik:GridTemplateColumn DataField="GRADE_NAME" FilterControlAltText="Filter GRADE_NAME column" GroupByExpression="GRADE_NAME Group By GRADE_NAME" HeaderText="التقييم" SortExpression="GRADE_NAME" UniqueName="GRADE_NAME">
                                        <ItemTemplate>
                                            <asp:Label ID="gradLabel" runat="server" Text='<%# Eval("GRADE_NAME") %>'></asp:Label>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                    
                                    <telerik:GridTemplateColumn DataField="APPROVAL" FilterControlAltText="Filter APPROVAL column" GroupByExpression="APPROVAL Group By APPROVAL" HeaderText="الحالة" SortExpression="APPROVAL" UniqueName="APPROVAL">
                                        <ItemTemplate>
                                            <asp:Label ID="approvalLabel" runat="server" Text='<%# Eval("APPROVAL") %>'></asp:Label>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                    
                                    <telerik:GridTemplateColumn DataField="REC_DATE" FilterControlAltText="Filter REC_DATE column" GroupByExpression="REC_DATE Group By REC_DATE" HeaderText="تاريخ التسجيل" SortExpression="REC_DATE" UniqueName="REC_DATE">
                                        <ItemTemplate>
                                            <asp:Label ID="recdateLabel" runat="server" Text='<%# Eval("REC_DATE") %>'></asp:Label>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                    
                                    <telerik:GridTemplateColumn DataField="REC_USER" FilterControlAltText="Filter REC_USER column" GroupByExpression="REC_USER Group By REC_USER" HeaderText=" المستخدم" SortExpression="REC_USER" UniqueName="REC_USER">
                                        <ItemTemplate>
                                            <asp:Label ID="recuserLabel" runat="server" Text='<%# Eval("REC_USER") %>'></asp:Label>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                    
                                    <telerik:GridTemplateColumn DataField="approve_date" FilterControlAltText="Filter approve_date column" GroupByExpression="approve_date Group By approve_date" HeaderText="تاريخ الموافقة" SortExpression="approve_date" UniqueName="approve_date">
                                        <ItemTemplate>
                                            <asp:Label ID="approve_dateLabel" runat="server" Text='<%# Eval("approve_date") %>'></asp:Label>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                    
                                    <telerik:GridTemplateColumn DataField="approve_user" FilterControlAltText="Filter approve_user column" GroupByExpression="approve_user Group By approve_user" HeaderText=" المستخدم الموافق" SortExpression="approve_user" UniqueName="approve_user">
                                        <ItemTemplate>
                                            <asp:Label ID="approve_userLabel" runat="server" Text='<%# Eval("approve_user") %>'></asp:Label>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                </Columns>
                                <EditFormSettings>
                                    <EditColumn FilterControlAltText="Filter EditCommandColumn column">
                                    </EditColumn>
                                </EditFormSettings>
                            </MasterTableView><ValidationSettings EnableModelValidation="false" EnableValidation="false" ValidationGroup="10" />
                            <ClientSettings AllowColumnsReorder="True" AllowDragToGroup="True" ReorderColumnsOnClient="True">
                                <Selecting AllowRowSelect="True" />
                                <Scrolling AllowScroll="True" UseStaticHeaders="True" />
                            </ClientSettings>
                            <FilterMenu EnableImageSprites="False">
                            </FilterMenu>
                        </telerik:RadGrid></td>
                <td rowspan="4">
                    &nbsp;</td>
            </tr>
            
            <tr>
                <td valign="top" align="center" colspan="2">
                    &nbsp;</td>
            </tr>
            <tr>
                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td></td>
                <td>
                    <telerik:RadNotification ID="RadNotification1" runat="server" Height="150px" 
                        Position="Center" Skin="Web20" Width="250px">
                        <NotificationMenu ID="TitleMenu">
                        </NotificationMenu>
                    </telerik:RadNotification>
                        </td>
            </tr>
            <tr>
                <td style="text-align: left">
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
        </table>
        <br />
    </p>
    <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog modal-sm">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">تنبيهات الموظفين</h4>
        </div>
        <div class="modal-body">
          <asp:Label ID="lblMessage" runat="server" Font-Bold="True" Font-Size="12pt" 
                Font-Names="Arial" ForeColor="Green"></asp:Label>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>
  <script type="text/javascript">
    $('.select2-selection--multiple, .mceEditor').hide();
</script>
    <script src="../JavaScript/select2.min.js" type="text/javascript"></script>
    <script src="../JavaScript/tiny_mce.js" type="text/javascript"></script>
<script type="text/javascript">

    tinyMCE.init({
        // General options
        mode: "textareas",
        theme: "advanced",
        plugins: "pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups"           
    });
    $(".js-example-placeholder-single").select2({
            placeholder: "اختر",
            allowClear: true
    });
    $(function () {
        $("[id$='ddlMailTo']").on("select2:select select2:unselect", function (e) {
            var items = $(this).val();
            var selectedItemsStr = items.join();
            $('.hiddenMailsTo').val(selectedItemsStr);
        });
        $("[id$='ddlMailCC']").on("select2:select select2:unselect", function (e) {
            var items = $(this).val();
            var selectedItemsStr = items.join();
            $('.hiddenMailsCC').val(selectedItemsStr);
        });
        $('.select2-selection--multiple, .mceEditor').show();
    });
</script>
</asp:Content>

