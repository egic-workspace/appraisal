﻿
/// <summary>
/// Summary description for MidYearVersionHelper
/// </summary>
public static class MidYearVersionHelper
{
    private static bool? isMidYearVersion = null;
    public static bool IsMidYearVersion(string year, string version)
    {
        if (isMidYearVersion == null)
        {
            AppService ser = new AppService();

            isMidYearVersion = !ser.IsFinalVersion(year, version);
        }
        return isMidYearVersion.Value;
    }
    public static void ResetIsMidYearVersion()
    {
        isMidYearVersion = null;
    }
}