﻿using Oracle.ManagedDataAccess.Client;
using System;
using System.Configuration;
using System.Data;


/// <summary>
/// Summary description for OracleAcess
/// </summary>
public class OracleAcess
{
    private string connectionstring;
    private OracleConnection oracc;
    private bool WithTransaction;
    private OracleTransaction _transaction;

    public OracleAcess()
    {

    }

    public OracleAcess(string ConnectionName)
    {
        WithTransaction = false;
        if (!string.IsNullOrEmpty(ConnectionName))
        {
            connectionstring = ConfigurationManager.ConnectionStrings[ConnectionName].ConnectionString;
        }
        oracc = new OracleConnection(connectionstring);
    }

    public OracleTransaction Transaction
    {
        get { return this._transaction; }
        set { this._transaction = value; }
    }

    public int ExecuteNonQuery(string sql)
    {
        try
        {
            OracleCommand cmd = new OracleCommand(sql, oracc);
            if (oracc.State != ConnectionState.Open)
            {
                oracc.Open();
            }
            if ((WithTransaction))
            {
                cmd.Transaction = Transaction;
            }
            int retval = cmd.ExecuteNonQuery();
            if ((!WithTransaction))
            {
                oracc.Close();
            }
            return retval;
        }
        catch (Exception ex)
        {
            AppService.LogError(ex.Message, nameof(ExecuteNonQuery), nameof(OracleAcess));
        }
        return 0;
    }

    public int ExecuteNonQuery(string sql, OracleParameter[] @params)
    {
        try
        {
            OracleCommand cmd = new OracleCommand(sql, oracc);
            for (int i = 0; i <= @params.Length - 1; i++)
            {
                cmd.Parameters.Add(@params[i]);
            }

            if (oracc.State != ConnectionState.Open)
            {
                oracc.Open();
            }
            if ((WithTransaction))
            {
                cmd.Transaction = Transaction;
            }
            int retval = cmd.ExecuteNonQuery();
            if ((!WithTransaction))
            {
                oracc.Close();
            }
            return retval;
        }
        catch (Exception ex)
        {
            AppService.LogError(ex.Message, nameof(ExecuteNonQuery), nameof(OracleAcess));
        }
        return 0;
    }

    public void SaveData(DataTable dt, string Sql)
    {
        try
        {
            OracleDataAdapter Adp = new OracleDataAdapter(Sql, oracc);
            OracleCommandBuilder CmAdp = new OracleCommandBuilder(Adp);

            Adp.InsertCommand = CmAdp.GetInsertCommand();
            Adp.DeleteCommand = CmAdp.GetDeleteCommand();
            Adp.UpdateCommand = CmAdp.GetUpdateCommand();
            Adp.Update(dt);
        }
        catch (Exception ex)
        {
            AppService.LogError(ex.Message, nameof(SaveData), nameof(OracleAcess));
        }
    }

    public void BeginTransaction()
    {
        try
        {
            WithTransaction = true;
            if (oracc.State != ConnectionState.Open)
            {
                oracc.Open();
            }
            Transaction = oracc.BeginTransaction(IsolationLevel.ReadCommitted);
        }
        catch (Exception ex)
        {
            AppService.LogError(ex.Message, nameof(BeginTransaction), nameof(OracleAcess));
        }
    }

    public void Rollback()
    {
        try
        {
            WithTransaction = false;
            Transaction.Rollback();
            if (oracc.State != ConnectionState.Closed)
            {
                oracc.Close();
            }
        }
        catch (Exception ex)
        {
            AppService.LogError(ex.Message, nameof(Rollback), nameof(OracleAcess));
        }
    }

    public void Commit()
    {
        try
        {
            WithTransaction = false;
            Transaction.Commit();
            if (oracc.State != ConnectionState.Closed)
            {
                oracc.Close();
            }
        }
        catch (Exception ex)
        {
            AppService.LogError(ex.Message, nameof(Commit), nameof(OracleAcess));
        }
    }

    public object ExecuteScalar(string sql, OracleParameter[] @params)
    {
        try
        {
            OracleConnection cnn = new OracleConnection(connectionstring);
            OracleCommand cmd = new OracleCommand(sql, cnn);
            for (int i = 0; i <= @params.Length - 1; i++)
            {
                cmd.Parameters.Add(@params[i]);
            }

            if (cnn.State != ConnectionState.Open)
            {
                cnn.Open();
            }
            if ((WithTransaction))
            {
                cmd.Transaction = Transaction;
            }
            object retval = cmd.ExecuteScalar();
            if ((!WithTransaction))
            {
                cnn.Close();
            }
            return retval;
        }
        catch (Exception ex)
        {
            AppService.LogError(ex.Message, nameof(ExecuteScalar), nameof(OracleAcess));
        }
        return null;
    }

    public object ExecuteScalar(string sql)
    {
        try
        {
            oracc = new OracleConnection(connectionstring);
            OracleCommand cmd = new OracleCommand(sql, oracc);
            if (oracc.State != ConnectionState.Open)
            {
                oracc.Open();
            }
            if ((WithTransaction))
            {
                cmd.Transaction = Transaction;
            }
            object retval = cmd.ExecuteScalar();
            if ((!WithTransaction))
            {
                oracc.Close();
            }
            return retval;
        }
        catch (Exception ex)
        {
            AppService.LogError(ex.Message, nameof(ExecuteScalar), nameof(OracleAcess));
        }
        return null;
    }

    public DataTable ExecuteDataTable(string sql)
    {
        DataTable dt = new DataTable();
        try
        {
            OracleDataAdapter da = new OracleDataAdapter(sql, oracc);
            da.Fill(dt);
        }
        catch (Exception ex)
        {
            AppService.LogError(ex.Message, nameof(ExecuteDataTable), nameof(OracleAcess));
        }
        return dt;
    }

    public DataTable ExecuteDataTable(string sql, OracleParameter[] @params)
    {
        DataTable dt = new DataTable();
        try
        {
            OracleDataAdapter da = new OracleDataAdapter(sql, connectionstring);
            for (int i = 0; i <= @params.Length - 1; i++)
            {
                da.SelectCommand.Parameters.Add(@params[i]);
            }
            da.Fill(dt);
        }
        catch (Exception ex)
        {
            AppService.LogError(ex.Message, nameof(ExecuteDataTable), nameof(OracleAcess));
        }
        return dt;
    }

    public int ExcuteStoredProcdure(string PRC, OracleParameter[] @params)
    {
        try
        {
            OracleCommand cmd = new OracleCommand();
            cmd.Connection = oracc;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = PRC;

            for (int i = 0; i <= @params.Length - 1; i++)
            {
                cmd.Parameters.Add(@params[i]);
            }

            if (oracc.State != ConnectionState.Open)
            {
                oracc.Open();
            }
            if ((WithTransaction))
            {
                cmd.Transaction = Transaction;
            }
            int retval = cmd.ExecuteNonQuery();
            if ((!WithTransaction))
            {
                oracc.Close();
            }
            return retval;
        }
        catch (Exception ex)
        {
            AppService.LogError(ex.Message, nameof(ExcuteStoredProcdure), nameof(OracleAcess));
        }
        return 0;
    }
}