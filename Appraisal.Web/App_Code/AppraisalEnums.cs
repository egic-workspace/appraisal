﻿public enum SaveTypes : byte
{
    Draft = 1,
    Save
}
public enum AppraisalTypes : byte
{
    None = 0,
    ObjectivesOnly = 1,
    ComptencesOnly,
    ObjectivesAndComptences
}
