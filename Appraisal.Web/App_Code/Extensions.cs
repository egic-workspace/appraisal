﻿using System;
using System.Configuration;
using System.IO;
public enum LogTypes
{
    Error,
    Trace
}
/// <summary>
/// Summary description for Extensions
/// </summary>
public static class Extensions
{

    public static void LogError(this Exception ex, string customeMessage = null)
    {
        WriteLog(ex: ex, customeMessage: customeMessage,logType: LogTypes.Error);
    }
    public static void WriteLog(string customeMessage = null, Exception ex = null, LogTypes logType = LogTypes.Trace)
    {
        string dirPath = @"C:\appraisal_logs";

        string filePath = $"{dirPath}";
        if (logType == LogTypes.Error)
            filePath += @"\errors_log.txt";
        else if (logType == LogTypes.Trace)
        {
            if (ConfigurationManager.AppSettings["TraceEnabled"].ToUpper() == "TRUE")
                filePath += @"\trace_logger.txt";
            else
                return;
        }
        if (!Directory.Exists(dirPath))
            _ = Directory.CreateDirectory(dirPath);


        var fileInfo = new FileInfo(filePath);
        if (fileInfo.Exists)
            using (StreamWriter streamWriter = new StreamWriter(filePath, true))
            {
                string txt = $"Time: {DateTime.Now.ToString()}\r\n";
                if (ex != null)
                    txt += $"Source: {ex.Source}\r\nFunction Name: {ex.TargetSite.Name}\r\nError Message: {ex.Message}\r\n================================= *** =================================\r\n";

                if (!string.IsNullOrWhiteSpace(customeMessage))
                    txt += $"\r\n{customeMessage}";

                txt += "\r\n================================= *** =================================\r\n";
                streamWriter.WriteLine(txt);
                streamWriter.Close();
            }
        else
        {
            fileInfo.Create();
        }
    }
    public static string FixSingleQuotes(this string text)
    {
        if (text.Length > 0 && text.Contains("'"))
            text = text.Replace("'", "''");
        return text;
    }
    public static string FixNumbersFormat(this string text)
    {
        if (text.Length > 0 && text.Contains(","))
            text = text.Replace(",", ".");
        return text;
    }
    public static string FixNumbersFormat(this double text)
    {
        string _text = text.ToString();
        if (text > 0 && _text.ToString().Contains(","))
            _text = _text.Replace(",", ".");
        return _text;
    }
    public static string FixNumbersFormat(this decimal text)
    {
        string _text = text.ToString();
        if (text > 0 && _text.ToString().Contains(","))
            _text = _text.Replace(",", ".");
        return _text;
    }
}