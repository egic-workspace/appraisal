﻿using Oracle.ManagedDataAccess.Client;
using System;
using System.Data;
using System.DirectoryServices;
using System.IO;

/// <summary>
/// Summary description for AppService
/// </summary>
public class AppService
{
    private OracleAcess oracleacess = new OracleAcess("ASS_ConnectionString");
    private string SQL = "";
    private DataTable dt = new DataTable();

    public AppService()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    public static void LogError(string errorMessage, string functionName, string className = nameof(AppService))
    {
        string dirPath = @"C:\appraisal_logs";
        if (!Directory.Exists(dirPath))
            _ = Directory.CreateDirectory(dirPath);

        string filePath = $@"{dirPath}\errors_log.txt";
        var fileInfo = new FileInfo(filePath);
        if (fileInfo.Exists)
            using (StreamWriter streamWriter = new StreamWriter(filePath, true))
            {
                string txt = $"Time: {DateTime.Now.ToString()}\r\nClass Name: {className}\r\nFunction Name: {functionName}\r\nError Message: {errorMessage}\r\n================================= *** =================================\r\n";
                streamWriter.WriteLine(txt);
                streamWriter.Close();
            }
        else
            fileInfo.Create();

    }

    public static string RemoveSp(string str)
    {
        //return str;
        //return Regex.Replace(str, "[^a-zA-Z0-9_.]+", "", RegexOptions.Compiled);
        // string[] chars = new string[] { ",", ".", "@","$", "%", "&", "*", "'", ";" };
        ////string[] chars = new string[] { ",", ".", "/", "!", "@", "#", "$", "%", "^", "&", "*", "'", "\"", ";", "_" , ":", "|" };
        ////  string[] chars = new string[] { ",", ".", "/", "!", "@", "#", "$", "%", "^", "&", "*", "'", "\"", ";", "_", "(", ")", ":", "|", "[", "]" };


        string[] chars = new string[] { ",", "'" };
        for (int i = 0; i < chars.Length; i++)
        {
            if (str.Contains(chars[i]))
            {
                str = str.Replace(chars[i], "");
            }
        }
        return str;
    }

    //*********************************************************************** ( Users $ Password  ) ***************************
    #region " User Settings "


    public int Check_HR_User(string user_name)
    {
        int ret_val = 0;

        try
        {
            SQL = $"SELECT COUNT(USER_NAME) FROM AS_USERS WHERE UPPER(USER_NAME) = UPPER('{user_name}')";
            object Pass = oracleacess.ExecuteScalar(SQL);
            ret_val = int.Parse(Pass.ToString());
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Check_HR_User >> To check User status ");
            ret_val = 0;
        }

        return ret_val;
    }

    public int Check_Locked_Department(string user_Code)
    {
        int ret_val = 0;

        try
        {
            SQL = " select eg_appraisal_avl('" + user_Code + "')  from dual  ";
            object Pass = oracleacess.ExecuteScalar(SQL);
            ret_val = int.Parse(Pass.ToString());
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Check_Locked_Department >> To Check_Locked_Department ");
            ret_val = 0;
        }

        return ret_val;
    }

    #endregion

    //*********************************************************************** ( Internal Func     ) *****************************
    #region " Internal Functions "

    #endregion
    //*********************************************************************** ( Customers         ) *****************************
    #region " AS Grads  "


    public DataTable Bind_GRADS_DDL()
    {
        try
        {
            SQL = " select level_code , level_name from pay_egic.app_job_level order by level_code ";

            dt = oracleacess.ExecuteDataTable(SQL);

        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Bind_GRADS_DDL >> Fill grads to DDL");
        }
        return dt;
    }


    public DataTable Bind_GRAD_GRID()
    {
        try
        {
            SQL = " select A.CODE , A.NAME , A.FORM_ID , B.FORM_NAME from as_grads A , as_forms B where A.FORM_ID = B.FORM_ID(+) ORDER BY A.CODE ";

            dt = oracleacess.ExecuteDataTable(SQL);

        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Bind_GRAD_GRID >> Fill Grads Data to GridView");
        }
        return dt;
    }


    public int Save_Grad_Data(string grd_no, string grd_name, string frm_id)
    {
        int val = 0;
        try
        {
            SQL = " INSERT INTO as_grads ( code , name , form_id ) ";
            SQL += " VALUES (  '" + grd_no + "' ,  '" + grd_name + "', '" + frm_id + "' ) ";
            oracleacess.ExecuteNonQuery(SQL);
            val = 1;
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Save_Grad_Data >> save grads Data ");
            val = 0;
        }

        return val;
    }

    public int Update_Grad_Data(string grd_no, string grd_name, string frm_id)
    {
        int val = 0;
        try
        {

            SQL = " Update as_grads Set name = '" + grd_name + "',  form_id = '" + frm_id + "'  Where Code = '" + grd_no + "' ";
            oracleacess.ExecuteNonQuery(SQL);
            val = 1;
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Update_Grad_Data >> Update grads Data ");
            val = 0;
        }

        return val;
    }


    public int Delete_Grad_Data(string grd_no)
    {
        int val = 0;
        try
        {

            SQL = " Delete From as_grads  WHERE CODE = '" + grd_no + "'  ";
            oracleacess.ExecuteNonQuery(SQL);
            val = 1;
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Delet_Grad_Data >> delete grads Data ");
            val = 0;
        }

        return val;
    }


    public int Check_Forms(string Form_id, string year)
    {
        int ret_val = 0;

        try
        {
            SQL = " select count(0) from as_entr_hdr where FORM_ID = '" + Form_id + "' and year = '" + year + "'  ";
            object Pass = oracleacess.ExecuteScalar(SQL);
            ret_val = int.Parse(Pass.ToString());
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Check_Forms >> To check saved forms for all");
        }

        return ret_val;
    }

    #endregion
    //************************************** ( Forms Register  ) *************************************************
    #region " AS Forms  "


    public DataTable Bind_FORMS_GRID()
    {
        try
        {
            SQL = " select FORM_ID , FORM_NAME , PAGE_NAME from as_forms ";

            dt = oracleacess.ExecuteDataTable(SQL);

        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Bind_FORMS_GRID >> Fill forms Data to GridView");
        }
        return dt;
    }


    public int Save_FORMS_Data(string frm_id, string frm_name, string page_name)
    {
        int val = 0;
        try
        {
            SQL = " INSERT INTO as_forms ( FORM_ID , FORM_NAME , PAGE_NAME ) ";
            SQL += " VALUES (  '" + frm_id + "' ,  '" + frm_name + "' ,  '" + page_name + "' ) ";
            oracleacess.ExecuteNonQuery(SQL);
            val = 1;
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Save_Grad_Data >> save grads Data ");
            val = 0;
        }

        return val;
    }


    public int Update_FORMS_Data(string frm_id, string frm_name, string page_name)
    {
        int val = 0;
        try
        {

            SQL = " Update as_forms Set FORM_NAME = '" + frm_name + "', PAGE_NAME = '" + page_name + "'  Where FORM_ID = '" + frm_id + "' ";
            oracleacess.ExecuteNonQuery(SQL);
            val = 1;
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Update_FORMS_Data >> Update Forms Data ");
            val = 0;
        }

        return val;
    }


    public int Delete_FORMS_Data(string FORM_ID)
    {
        int val = 0;
        try
        {

            SQL = " Delete From as_forms  WHERE FORM_ID = '" + FORM_ID + "'  ";
            oracleacess.ExecuteNonQuery(SQL);
            val = 1;
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Delete_FORMS_Data >> delete forms Data ");
            val = 0;
        }

        return val;
    }

    #endregion
    //************************************** ( Assigne Form to Employee ) ****************************************
    #region " Form to Employee "


    public DataTable Bind_EMP_DDL()
    {
        try
        {
            SQL = " select emp_no , emp_no ||' - '|| emp_name as emp_name from pay_egic.APP_EMP  WHERE FINSHED = '2'  ORDER BY emp_no  ";

            dt = oracleacess.ExecuteDataTable(SQL);

        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Bind_EMP_DDL >> Fill emp to DDL");
        }
        return dt;
    }


    public DataTable Bind_Forms_DDL()
    {
        try
        {
            SQL = " select Form_ID , Form_Name from AS_FORMS order by Form_id ";

            dt = oracleacess.ExecuteDataTable(SQL);

        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Bind_EMP_DDL >> Fill emp to DDL");
        }
        return dt;
    }


    public DataTable Bind_EMP_FORMS_GRID(string year)
    {
        try
        {
            SQL = " select a.emp_no , a.emp_name , b.form_id , b.form_name , c.notes  from as_form_emp C , as_forms B , pay_egic.APP_EMP A WHERE  B.FORM_ID = C.FORM_ID AND  A.EMP_NO = C.EMP_NO AND A.FINSHED = '2' AND C.YEAR = '" + year + "' order by A.Emp_No  ";

            dt = oracleacess.ExecuteDataTable(SQL);

        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Bind_EMP_FORMS_GRID >> Fill employee forms Data to GridView");
        }
        return dt;
    }


    public int Save_FRM_EMP_Data(string emp_no, string form_id, string notes, string year)
    {
        int val = 0;
        try
        {
            SQL = " INSERT INTO as_form_emp ( EMP_NO , FORM_ID , NOTES , YEAR ) ";
            SQL += " VALUES (  '" + emp_no + "' ,  '" + form_id + "' ,  '" + notes + "','" + year + "' ) ";
            oracleacess.ExecuteNonQuery(SQL);
            val = 1;
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Save_FRM_EMP_Data >> save form 2 emp Data ");
            val = 0;
        }

        return val;
    }


    public int Update_FRM_EMP_Data(string emp_no, string form_id, string notes, string year)
    {
        int val = 0;
        try
        {

            SQL = " Update as_form_emp Set  FORM_ID = '" + form_id + "', NOTES = '" + notes + "'  Where EMP_NO = '" + emp_no + "' AND YEAR = '" + year + "' ";
            oracleacess.ExecuteNonQuery(SQL);
            val = 1;
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Update_FRM_EMP_Data >> Update emp Forms Data ");
            val = 0;
        }

        return val;
    }


    public int Delete_FRM_EMP_Data(string EMP_NO, string year)
    {
        int val = 0;
        try
        {
            SQL = " Delete From as_form_emp  WHERE EMP_NO = '" + EMP_NO + "' AND YEAR = '" + year + "'  ";
            oracleacess.ExecuteNonQuery(SQL);
            val = 1;
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Delete_FRM_EMP_Data >> delete forms Data ");
            val = 0;
        }

        return val;
    }


    public int Check_EMP_IN_FORMS(string emp_no, string year)
    {
        int ret_val = 0;

        try
        {
            SQL = " select count(emp_no) from as_form_emp where EMP_no = '" + emp_no + "' AND YEAR = '" + year + "' ";
            object Pass = oracleacess.ExecuteScalar(SQL);
            ret_val = int.Parse(Pass.ToString());
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Check_EMP_IN_FORMS >> To check emp in forms status ");
            ret_val = 0;
        }

        return ret_val;
    }


    public int Check_Saved_Forms(string emp_no, string year)
    {
        int ret_val = 0;

        try
        {
            SQL = " select count(0) from as_entr_hdr where EMP_NO = '" + emp_no + "' and year = '" + year + "'  ";
            object Pass = oracleacess.ExecuteScalar(SQL);
            ret_val = int.Parse(Pass.ToString());
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Check_Saved_Forms >> To check saved forms for this Employee ");
        }

        return ret_val;
    }

    #endregion
    //***************************************( Approvers Table )**************************************************
    #region " Approvers"


    public DataTable Bind_APPROVERS_GRID()
    {
        try
        {
            SQL = " select A.EMP_NO , B.EMP_NAME from as_approver A , pay_egic.APP_EMP B where A.EMP_NO = B.EMP_NO AND B.FINSHED = '2'  order by A.EMP_NO ";

            dt = oracleacess.ExecuteDataTable(SQL);

        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Bind_APPROVERS_GRID >> Fill employee forms Data to GridView");
        }
        return dt;
    }


    public int Save_APPROVER_Data(string emp_no)
    {
        int val = 0;
        try
        {
            SQL = " INSERT INTO as_approver ( EMP_NO ) ";
            SQL += " VALUES (  '" + emp_no + "' ) ";
            oracleacess.ExecuteNonQuery(SQL);
            val = 1;
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Save_APPROVER_Data >> save approvers Data ");
            val = 0;
        }

        return val;
    }


    public int Delete_APPROVER_Data(string EMP_NO)
    {
        int val = 0;
        try
        {
            SQL = " Delete From as_approver  WHERE EMP_NO = '" + EMP_NO + "'  ";
            oracleacess.ExecuteNonQuery(SQL);
            val = 1;
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Delete_APPROVER_Data >> delete approver Data ");
            val = 0;
        }

        return val;
    }

    #endregion
    //***************************************( Assessment Years )*************************************************
    #region " AS Years"


    public DataTable Bind_YEARS_GRID()
    {
        try
        {
            // SQL = " select YEAR , case when Status ='Y' then 'مفتوحة' when Status = 'N' then 'مغلقة' else 'فارغ' end as NStatus , status from as_validity ";
            SQL = " select YEAR , case when Status ='Y' then 'مفتوحة' when Status = 'N' then 'مغلقة' else 'فارغ' end as NStatus , status  , case when Survey ='Y' then 'مفتوحة' when Survey = 'N' then 'مغلقة' else 'فارغ' end as SStatus , survey from as_validity ";

            dt = oracleacess.ExecuteDataTable(SQL);

        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Bind_YEARS_GRID >> Fill years Data to GridView");
        }
        return dt;
    }


    public int Save_Years_Data(string year, string status, string survey)
    {
        int val = 0;
        try
        {
            SQL = " INSERT INTO as_validity ( YEAR , Status , Survey ) ";
            SQL += " VALUES (  '" + year + "' , '" + status + "' , '" + survey + "' ) ";
            oracleacess.ExecuteNonQuery(SQL);
            val = 1;
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Save_Years_Data >> save years Data ");
            val = 0;
        }

        return val;
    }


    public int Update_Years_Data(string year, string status, string survey)
    {
        int val = 0;
        try
        {

            SQL = " Update as_validity Set Status = '" + status + "' , Survey = '" + survey + "'  Where Year = '" + year + "' ";
            oracleacess.ExecuteNonQuery(SQL);
            val = 1;
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Update_Years_Data >> Update years Data ");
            val = 0;
        }

        return val;
    }


    public int Delete_Years_Data(string year)
    {
        int val = 0;
        try
        {
            SQL = " Delete From as_validity  WHERE Year = '" + year + "'  ";
            oracleacess.ExecuteNonQuery(SQL);
            val = 1;
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Delete_Years_Data >> delete year Data ");
            val = 0;
        }

        return val;
    }

    #endregion
    //***************************************( Users )************************************************************
    #region " AS Users "


    public DataTable Bind_Users_GRID()
    {
        try
        {
            SQL = " Select USER_NAME from AS_USERS ";

            dt = oracleacess.ExecuteDataTable(SQL);

        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Bind_Users_GRID >> Fill users Data to GridView");
        }
        return dt;
    }


    public int Save_User_Data(string usr)
    {
        int val = 0;
        try
        {
            SQL = " INSERT INTO AS_USERS ( USER_NAME ) ";
            SQL += " VALUES (  '" + usr + "' ) ";
            oracleacess.ExecuteNonQuery(SQL);
            val = 1;
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Save_User_Data >> save user Data ");
            val = 0;
        }

        return val;
    }


    public int Delete_User_Data(string usr)
    {
        int val = 0;
        try
        {
            SQL = " Delete From AS_USERS  WHERE UPPER(USER_NAME) = UPPER('" + usr + "')";
            oracleacess.ExecuteNonQuery(SQL);
            val = 1;
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Delete_User_Data >> delete user Data ");
            val = 0;
        }

        return val;
    }

    #endregion
    //***************************************( Enter Form Assessment  )******************************************************************
    #region " AS Enter Form "


    public DataTable Bind_ENTER_EMP_GRID(string emp_no, string year)
    {
        string ret_val = "";
        try
        {
            SQL = "  SELECT AS_ADMIN_USER(" + emp_no + ") FROM DUAL";
            object admn = oracleacess.ExecuteScalar(SQL);
            ret_val = admn.ToString();
            if (ret_val == "N")
            {
                SQL = "  SELECT  EMP_NO,EMP_NAME  , GET_EMP_title(job_no) as JOB_NAME , get_emp_form(emp_no,'" + year + "') as form_ID , get_emp_app_status(emp_no,'" + year + "', get_emp_form(emp_no,'" + year + "') ) as Status , case when get_emp_app_status(emp_no, '" + year + "', get_emp_form(emp_no,'" + year + "')) = 0 then 'لم يتم التسجيل'  when get_emp_app_status(emp_no, '" + year + "', get_emp_form(emp_no,'" + year + "')) = 1 then 'تم التسجيل' when get_emp_app_status(emp_no, '" + year + "' , get_emp_form(emp_no,'" + year + "')) = 2 then 'تمت الموافقة'  end  as  fStatus , EG_EMP_TYPE(EMP_NO) emp_company , level FROM pay_egic.APP_EMP  where NVL(NO_APPRAISAL,0) = 0 AND emp_no <> '" + emp_no + "' AND FINSHED = '2'  START WITH EMP_NO = '" + emp_no + "' CONNECT BY PRIOR EMP_NO = D_EMP_NO ";
            }
            else
            {
                SQL = "  SELECT  EMP_NO,EMP_NAME  , GET_EMP_title(job_no) as JOB_NAME , get_emp_form(emp_no,'" + year + "') as form_ID , get_emp_app_status(emp_no,'" + year + "', get_emp_form(emp_no,'" + year + "') ) as Status , case when get_emp_app_status(emp_no, '" + year + "', get_emp_form(emp_no,'" + year + "')) = 0 then 'لم يتم التسجيل'  when get_emp_app_status(emp_no, '" + year + "', get_emp_form(emp_no,'" + year + "')) = 1 then 'تم التسجيل' when get_emp_app_status(emp_no, '" + year + "' , get_emp_form(emp_no,'" + year + "')) = 2 then 'تمت الموافقة'  end  as  fStatus , EG_EMP_TYPE(EMP_NO) emp_company , level FROM pay_egic.APP_EMP  where NVL(NO_APPRAISAL,0) = 0 AND (emp_no <> '" + emp_no + "' OR as_admin_user(" + emp_no + ") = 'Y') AND FINSHED = '2' ";
            }


            dt = oracleacess.ExecuteDataTable(SQL);

        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Bind_ENTER_EMP_GRID >> Fill employee Data to GridView");
        }
        return dt;
    }


    public DataTable Bind_ENTER_EMP_FRST_LEVEL_GRID(string emp_no, string year)
    {
        try
        {

            SQL = "  SELECT  EMP_NO,EMP_NAME  , GET_EMP_title(job_no) as JOB_NAME , get_emp_form(emp_no,'" + year + "') as form_ID , get_emp_app_status(emp_no, '" + year + "' , get_emp_form(emp_no,'" + year + "') ) as Status , case when get_emp_app_status(emp_no, '" + year + "' , get_emp_form(emp_no,'" + year + "')) = 0 then 'لم يتم التسجيل'  when get_emp_app_status(emp_no, '" + year + "' , get_emp_form(emp_no,'" + year + "')) = 1 then 'تم التسجيل' when get_emp_app_status(emp_no,'" + year + "', get_emp_form(emp_no,'" + year + "')) = 2 then 'تمت الموافقة'  end  as  fStatus , level , EG_EMP_TYPE(EMP_NO) emp_company FROM pay_egic.APP_EMP where NVL(NO_APPRAISAL,0) = 0 AND emp_no <> '" + emp_no + "' AND LEVEL = 2 AND FINSHED = '2'  START WITH EMP_NO = '" + emp_no + "' CONNECT BY PRIOR EMP_NO = D_EMP_NO ";

            dt = oracleacess.ExecuteDataTable(SQL);

        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Bind_ENTER_EMP_GRID >> Fill employee Data to GridView");
        }
        return dt;
    }


    public string Get_Page_Name(string form_id)
    {
        string ret_val = "";

        try
        {
            SQL = " select page_name from as_forms where form_id = '" + form_id + "' ";
            object Pass = oracleacess.ExecuteScalar(SQL);
            ret_val = Pass.ToString();
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Get_Page_Name >> To get page name");
        }

        return ret_val;
    }


    public int Check_Approver(string emp_no)
    {
        int ret_val = 0;

        try
        {
            SQL = " select count(emp_no) as xx from as_approver where emp_no = '" + emp_no + "'  ";
            object Pass = oracleacess.ExecuteScalar(SQL);
            ret_val = int.Parse(Pass.ToString());
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Check_Approver >> To check approver status ");
            ret_val = 0;
        }

        return ret_val;
    }


    public string Get_Open_Year()
    {
        string ret_val = "";

        try
        {
            SQL = " SELECT YEAR FROM AS_VALIDITY WHERE STATUS = 'Y' ";
            object Pass = oracleacess.ExecuteScalar(SQL);
            ret_val = Pass.ToString();
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Get_Open_Year >> To get page name");
        }

        return ret_val;
    }


    public string Get_Survey_Open_Year()
    {
        string ret_val = "";

        try
        {
            SQL = " SELECT YEAR FROM AS_VALIDITY WHERE SURVEY = 'Y' ";
            object Pass = oracleacess.ExecuteScalar(SQL);
            ret_val = Pass.ToString();
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Get_Survey_Open_Year >> To get page name");
        }

        return ret_val;
    }


    public int Check_Open_Years(string type)
    {
        int ret_val = 0;

        try
        {
            if (type == "APP")
            { SQL = " select count(year) from As_validity where status = 'Y' "; }
            else if (type == "SUR")
            { SQL = " select count(year) from As_validity where survey = 'Y' "; }
            object Pass = oracleacess.ExecuteScalar(SQL);
            ret_val = int.Parse(Pass.ToString());
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Check_Open_Years >> To check year status ");
            ret_val = 0;
        }

        return ret_val;
    }


    public int Get_First_Emp_Level_Count(string emp_no, string company, string dept)
    {
        int ret_val = 0;

        try
        {
            SQL = " SELECT  count(EMP_NO)  FROM pay_egic.APP_EMP  where NVL(NO_APPRAISAL,0) = 0 AND emp_no <> '" + emp_no + "' AND FINSHED = '2' AND LEVEL = 2  AND ( EG_EMP_TYPE(EMP_NO) = '" + company + "' OR '" + company + "' = 'ALL' ) AND ( substr(section_no,1,4) = '" + dept + "' or '" + dept + "' = 'ALL' ) START WITH EMP_NO = '" + emp_no + "' CONNECT BY PRIOR EMP_NO = D_EMP_NO  ";
            object Pass = oracleacess.ExecuteScalar(SQL);
            ret_val = int.Parse(Pass.ToString());
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Get_First_Level_Count >> To First level Count");
            ret_val = 0;
        }

        return ret_val;
    }


    public int Get_All_Emp_Level_Count(string emp_no, string company, string dept)
    {
        int ret_val = 0;

        try
        {
            SQL = " SELECT  count(EMP_NO)  FROM pay_egic.APP_EMP  where NVL(NO_APPRAISAL,0) = 0 AND emp_no <> '" + emp_no + "' AND FINSHED = '2' AND ( EG_EMP_TYPE(EMP_NO) = '" + company + "'  OR   '" + company + "'  = 'ALL' ) AND ( substr(section_no,1,4) = '" + dept + "' or '" + dept + "' = 'ALL' )  START WITH EMP_NO = '" + emp_no + "' CONNECT BY PRIOR EMP_NO = D_EMP_NO  ";
            object Pass = oracleacess.ExecuteScalar(SQL);
            ret_val = int.Parse(Pass.ToString());
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Get_All_Emp_Level_Count >> To First level Count");
            ret_val = 0;
        }

        return ret_val;
    }


    public double Get_Bad_Results(string user_name, string level, string manger_code)
    {
        double ret_val = 0;
        try
        {
            string o_year = Get_Open_Year().ToString();
            if (level == "2")
            {
                SQL = " Select count(0) from  as_entr_hdr a where year = '" + o_year + "' AND EG_EMP_TYPE(A.EMP_NO) = 'EGIC' AND get_emp_result(a.emp_no,'" + o_year + "',a.form_id) < 60 AND pay_egic.get_emp_level('" + manger_code + "' , a.emp_no) = 2  ";
                //AND (a.user_name = '" + user_name + "' OR a.approve_name = '" + user_name + "' ) 
            }
            else
            {
                SQL = " Select count(0) from  as_entr_hdr a where year = '" + o_year + "' AND EG_EMP_TYPE(A.EMP_NO) = 'EGIC' AND get_emp_result(a.emp_no,'" + o_year + "',a.form_id) < 60 AND pay_egic.get_emp_level('" + manger_code + "' , a.emp_no) >= 2   ";
                //AND  ( a.user_name = '" + user_name + "' OR a.approve_name = '" + user_name + "') AND
            }
            object Pass = oracleacess.ExecuteScalar(SQL);
            if (!double.TryParse(Pass.ToString(), out ret_val)) ret_val = 0;
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Get_Bad_Results >> To get bad results ");
            ret_val = 0;
        }

        return ret_val;
    }


    public double Get_Midum_Results(string user_name, string level, string manger_code)
    {
        double ret_val = 0;

        try
        {
            string o_year = Get_Open_Year().ToString();
            if (level == "2")
            {
                SQL = " select count(0) from  as_entr_hdr a where year = '" + o_year + "' AND EG_EMP_TYPE(A.EMP_NO) = 'EGIC' AND get_emp_result(a.emp_no,'" + o_year + "',a.form_id) between 60 and 70.499  and pay_egic.get_emp_level('" + manger_code + "',a.emp_no) = 2   ";
                //AND ( a.user_name = '" + user_name + "' or a.approve_name = '" + user_name + "' )
            }
            else
            {
                SQL = " select count(0) from  as_entr_hdr a where year = '" + o_year + "' AND EG_EMP_TYPE(A.EMP_NO) = 'EGIC' AND get_emp_result(a.emp_no,'" + o_year + "',a.form_id) between 60 and 70.499  and pay_egic.get_emp_level('" + manger_code + "',a.emp_no) >= 2   ";
                //AND ( a.user_name = '" + user_name + "' OR a.approve_name = '" + user_name + "' )
            }
            object Pass = oracleacess.ExecuteScalar(SQL);
            if (!double.TryParse(Pass.ToString(), out ret_val)) ret_val = 0;
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Get_Midum_Results >> To get miduam results ");
            ret_val = 0;
        }

        return ret_val;
    }


    public double Get_Good_Results(string user_name, string level, string manger_code)
    {
        double ret_val = 0;

        try
        {
            string o_year = Get_Open_Year().ToString();
            if (level == "2")
            {
                SQL = " select count(0) from  as_entr_hdr a where year = '" + o_year + "' AND EG_EMP_TYPE(A.EMP_NO) = 'EGIC' AND get_emp_result(a.emp_no,'" + o_year + "',a.form_id) between 70.5 and 85.499 AND  pay_egic.get_emp_level('" + manger_code + "',a.emp_no) = 2    ";
                //( a.user_name = '" + user_name + "' or a.approve_name = '" + user_name + "' ) and
            }
            else
            {
                SQL = " select count(0) from  as_entr_hdr a where year = '" + o_year + "' AND EG_EMP_TYPE(A.EMP_NO) = 'EGIC' AND get_emp_result(a.emp_no,'" + o_year + "',a.form_id) between 70.5 and 85.499 AND  pay_egic.get_emp_level('" + manger_code + "',a.emp_no) >= 2     ";
                //( a.user_name = '" + user_name + "' or a.approve_name = '" + user_name + "' ) and
            }
            object Pass = oracleacess.ExecuteScalar(SQL);
            if (!double.TryParse(Pass.ToString(), out ret_val)) ret_val = 0;
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Get_Good_Results >> To get good results ");
            ret_val = 0;
        }

        return ret_val;
    }


    public double Get_Very_Good_Results(string user_name, string level, string manger_code)
    {
        double ret_val = 0;

        try
        {
            string o_year = Get_Open_Year().ToString();
            if (level == "2")
            {
                SQL = " select count(0) from  as_entr_hdr a where year = '" + o_year + "' AND EG_EMP_TYPE(A.EMP_NO) = 'EGIC' AND get_emp_result(a.emp_no,'" + o_year + "',a.form_id) between 85.5 and 94.999 AND  pay_egic.get_emp_level('" + manger_code + "',a.emp_no) = 2    ";
                //( a.user_name = '" + user_name + "' or a.approve_name = '" + user_name + "' ) and
            }
            else
            {
                SQL = " select count(0) from  as_entr_hdr a where year = '" + o_year + "' AND EG_EMP_TYPE(A.EMP_NO) = 'EGIC' AND get_emp_result(a.emp_no,'" + o_year + "',a.form_id) between 85.5 and 94.999 AND  pay_egic.get_emp_level('" + manger_code + "',a.emp_no) >= 2   ";
                //( a.user_name = '" + user_name + "' or a.approve_name = '" + user_name + "' ) and
            }
            object Pass = oracleacess.ExecuteScalar(SQL);
            if (!double.TryParse(Pass.ToString(), out ret_val)) ret_val = 0;
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Get_Very_Good_Results >> To get very good results ");
            ret_val = 0;
        }

        return ret_val;
    }


    public double Get_Exellent_Results(string user_name, string level, string manger_code)
    {
        double ret_val = 0;

        try
        {
            string o_year = Get_Open_Year().ToString();
            if (level == "2")
            {
                SQL = " select count(0) from  as_entr_hdr a where year = '" + o_year + "' AND EG_EMP_TYPE(A.EMP_NO) = 'EGIC' AND get_emp_result(a.emp_no,'" + o_year + "',a.form_id) >= 95 AND  pay_egic.get_emp_level('" + manger_code + "', a.emp_no) = 2     ";
                //( a.user_name = '" + user_name + "' or a.approve_name = '" + user_name + "')  and
            }
            else
            {
                SQL = " select count(0) from  as_entr_hdr a where year = '" + o_year + "' AND EG_EMP_TYPE(A.EMP_NO) = 'EGIC' AND get_emp_result(a.emp_no,'" + o_year + "',a.form_id) >= 95 AND  pay_egic.get_emp_level('" + manger_code + "', a.emp_no) >= 2     ";
                //( a.user_name = '" + user_name + "' or a.approve_name = '" + user_name + "' ) and
            }
            object Pass = oracleacess.ExecuteScalar(SQL);
            if (!double.TryParse(Pass.ToString(), out ret_val)) ret_val = 0;
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Get_Exellent_Results >> To get exellentresults ");
            ret_val = 0;
        }

        return ret_val;
    }

    #endregion
    //***************************************( Form A  )******************************************************************
    #region " AS For Form (1) "


    public string Get_Next_ID_HDR()
    {
        string ret_val = "";

        try
        {
            SQL = " select max(ID)+1 from as_entr_hdr  ";
            object Pass = oracleacess.ExecuteScalar(SQL);
            if (Pass.ToString() == "")
            {
                ret_val = "1";
            }
            else
            {
                ret_val = Pass.ToString();
            }
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Get_Next_ID_HDR >> To get next assessment hder  ");
        }

        return ret_val;
    }


    public int Save_Form_HDR(string EMP_NO, string EMP_NAME, string FORM_ID, string STATUS, string YEAR, string APPROVE_FLAG, string USER_NAME)
    {
        int val = 0;
        try
        {
            string ID = Get_Next_ID_HDR().ToString();
            SQL = " INSERT INTO as_entr_hdr  ( ID , EMP_NO , EMP_NAME , FORM_ID , STATUS , YEAR , APPROVE_FLAG , USER_NAME , REC_DATE ) ";
            SQL += " VALUES ( '" + ID + "', '" + EMP_NO + "', '" + EMP_NAME + "', '" + FORM_ID + "', '" + STATUS + "','" + YEAR + "','0','" + USER_NAME + "' , to_date('" + (System.DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss")) + "','MM/DD/YYYY HH24:MI:SS') ) ";
            oracleacess.ExecuteNonQuery(SQL);
            val = int.Parse(ID.ToString());
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Save_Form_HDR >> save form header Data ");
            val = 0;
        }

        return val;
    }


    public int Make_Approve(string ID, string EMP_NO, string Approver, string year)
    {
        int val = 0;
        try
        {
            SQL = " Update as_entr_hdr Set  STATUS = '1' , APPROVE_FLAG = '1' , approve_name = '" + Approver + "' , approve_date = to_date('" + (System.DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss")) + "','MM/DD/YYYY HH24:MI:SS')  WHERE ID = '" + ID + "' AND EMP_NO = '" + EMP_NO + "' AND YEAR = '" + year + "' ";
            oracleacess.ExecuteNonQuery(SQL);
            val = 1;
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Make_Approve >> Make Approve");
            val = 0;
        }

        return val;
    }


    public int Save_Form_A_DTL(string HDR_ID, string EMP_NAME, string OUT_QTY_EVAL, string OUT_QAL_JUST, string DELIVERY_TIME_EVAL, string DELIVERY_TIME_JUST, string WORK_QLTY_EVAL, string WORK_QLTY_JUST, string SAFETY_EVAL, string SAFETY_JUST, string TEAM_WORK_EVAL, string TEAM_WORK_JUST, string COMMIT_EVAL, string COMMIT_JUST, string FLEXIBILTY_EVAL, string FLEXIBILTY_JUST, string HORSE_POWER_EVAL, string HORSE_POWER_JUST, string PROBLEM_SOLVING_EVAL, string PROBLEM_SOLVING_JUST, string COMPTENCY_EVAL, string COMPTENCY_JUST, string FINAL_RESULT)
    {
        int val = 0;
        try
        {
            SQL = " INSERT INTO as_form1_dtl ( HDR_ID ,EMP_NAME ,OUT_QTY_EVAL ,OUT_QAL_JUST ,DELIVERY_TIME_EVAL ,DELIVERY_TIME_JUST ,WORK_QLTY_EVAL ,WORK_QLTY_JUST ,SAFETY_EVAL ,SAFETY_JUST ,TEAM_WORK_EVAL , TEAM_WORK_JUST , COMMIT_EVAL , COMMIT_JUST ,FLEXIBILTY_EVAL ,FLEXIBILTY_JUST , HORSE_POWER_EVAL ,HORSE_POWER_JUST , PROBLEM_SOLVING_EVAL ,PROBLEM_SOLVING_JUST ,COMPTENCY_EVAL ,COMPTENCY_JUST ,FINAL_RESULT ) ";
            SQL += " VALUES ( '" + HDR_ID + "' ,'" + EMP_NAME + "' ,'" + OUT_QTY_EVAL + "' ,'" + RemoveSp(OUT_QAL_JUST) + "' ,'" + DELIVERY_TIME_EVAL + "' ,'" + RemoveSp(DELIVERY_TIME_JUST) + "' ,'" + WORK_QLTY_EVAL + "' ,'" + RemoveSp(WORK_QLTY_JUST) + "' ,'" + SAFETY_EVAL + "' ,'" + RemoveSp(SAFETY_JUST) + "' ,'" + TEAM_WORK_EVAL + "' , '" + RemoveSp(TEAM_WORK_JUST) + "' , '" + COMMIT_EVAL + "' , '" + RemoveSp(COMMIT_JUST) + "' ,'" + FLEXIBILTY_EVAL + "' ,'" + RemoveSp(FLEXIBILTY_JUST) + "' ,'" + HORSE_POWER_EVAL + "' ,'" + RemoveSp(HORSE_POWER_JUST) + "' , '" + PROBLEM_SOLVING_EVAL + "' ,'" + RemoveSp(PROBLEM_SOLVING_JUST) + "' ,'" + COMPTENCY_EVAL + "' ,'" + RemoveSp(COMPTENCY_JUST) + "','" + FINAL_RESULT + "' ) ";
            oracleacess.ExecuteNonQuery(SQL);
            val = 1;
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Save_Form_A_DTL >> save form (A) Data ");
            val = 0;
        }

        return val;
    }


    public int Update_Form_A_DTL(string HDR_ID, string EMP_NAME, string OUT_QTY_EVAL, string OUT_QAL_JUST, string DELIVERY_TIME_EVAL, string DELIVERY_TIME_JUST, string WORK_QLTY_EVAL, string WORK_QLTY_JUST, string SAFETY_EVAL, string SAFETY_JUST, string TEAM_WORK_EVAL, string TEAM_WORK_JUST, string COMMIT_EVAL, string COMMIT_JUST, string FLEXIBILTY_EVAL, string FLEXIBILTY_JUST, string HORSE_POWER_EVAL, string HORSE_POWER_JUST, string PROBLEM_SOLVING_EVAL, string PROBLEM_SOLVING_JUST, string COMPTENCY_EVAL, string COMPTENCY_JUST, string FINAL_RESULT)
    {
        int val = 0;
        try
        {
            SQL = " Update as_form1_dtl Set OUT_QTY_EVAL = '" + OUT_QTY_EVAL + "' , OUT_QAL_JUST = '" + RemoveSp(OUT_QAL_JUST) + "' , DELIVERY_TIME_EVAL = '" + DELIVERY_TIME_EVAL + "' , DELIVERY_TIME_JUST = '" + RemoveSp(DELIVERY_TIME_JUST) + "' ,WORK_QLTY_EVAL = '" + WORK_QLTY_EVAL + "' , WORK_QLTY_JUST = '" + RemoveSp(WORK_QLTY_JUST) + "' , SAFETY_EVAL = '" + SAFETY_EVAL + "' , SAFETY_JUST = '" + RemoveSp(SAFETY_JUST) + "' , TEAM_WORK_EVAL = '" + TEAM_WORK_EVAL + "' , TEAM_WORK_JUST = '" + RemoveSp(TEAM_WORK_JUST) + "' , COMMIT_EVAL = '" + COMMIT_EVAL + "' , COMMIT_JUST = '" + RemoveSp(COMMIT_JUST) + "' ,FLEXIBILTY_EVAL = '" + FLEXIBILTY_EVAL + "' ,FLEXIBILTY_JUST = '" + RemoveSp(FLEXIBILTY_JUST) + "' , HORSE_POWER_EVAL = '" + HORSE_POWER_EVAL + "' ,HORSE_POWER_JUST = '" + RemoveSp(HORSE_POWER_JUST) + "' , PROBLEM_SOLVING_EVAL = '" + PROBLEM_SOLVING_EVAL + "' ,PROBLEM_SOLVING_JUST = '" + RemoveSp(PROBLEM_SOLVING_JUST) + "' ,COMPTENCY_EVAL = '" + COMPTENCY_EVAL + "' ,COMPTENCY_JUST = '" + RemoveSp(COMPTENCY_JUST) + "' , FINAL_RESULT = '" + FINAL_RESULT + "'  Where  HDR_ID = '" + HDR_ID + "' ";
            oracleacess.ExecuteNonQuery(SQL);
            val = 1;
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Update_Form_A_DTL >> save form (A) Data ");
            val = 0;
        }

        return val;
    }


    public int Check_Redandancy(string emp_no, string year)
    {
        int ret_val = 0;

        try
        {
            //select COUNT(0) from as_entr_hdr where EMP_NO = '1609' AND YEAR = '2014'
            SQL = " select count(ID) from as_entr_hdr where emp_no = " + emp_no + " and year = " + year;
            object Pass = oracleacess.ExecuteScalar(SQL);
            ret_val = int.Parse(Pass.ToString());
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Check_Redandancy >> To check insert of emp data in the sam year ");
        }

        return ret_val;
    }


    public string Check_Year(string year)
    {
        string ret_val = "";

        try
        {
            SQL = " select status from as_validity where year = '" + year + "' ";
            object Pass = oracleacess.ExecuteScalar(SQL);
            ret_val = Pass.ToString();
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Check_Year >> To check year lock  ");
        }

        return ret_val;
    }


    public string Get_ID(string emp_no, string year)
    {
        string ret_val = "";

        try
        {
            SQL = " select ID from as_entr_hdr where emp_no = '" + emp_no + "' and year = '" + year + "' ; ";
            object Pass = oracleacess.ExecuteScalar(SQL);
            ret_val = Pass.ToString();
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Get_ID >> To My ID ");
        }

        return ret_val;
    }


    public DataTable Get_Form_A_Data(string Emp_NO, string Year)
    {
        try
        {
            SQL = " Select A.ID , A.EMP_NO , A.EMP_NAME , A.FORM_ID , B.OUT_QTY_EVAL , B.OUT_QAL_JUST , B.DELIVERY_TIME_EVAL , B.DELIVERY_TIME_JUST , B.WORK_QLTY_EVAL , B.WORK_QLTY_JUST , B.SAFETY_EVAL , B.Safety_Just , B.TEAM_WORK_EVAL , B.Team_Work_Just , B.COMMIT_EVAL , B.Commit_Just , B.FLEXIBILTY_EVAL , B.Flexibilty_Just , B.HORSE_POWER_EVAL , B.HORSE_POWER_JUST , B.Problem_Solving_Eval , B.Problem_Solving_Just , B.COMPTENCY_EVAL , B.Comptency_Just , B.FINAL_RESULT ,  A.USER_NAME , E.SECTION_NO  from as_entr_hdr A , as_form1_dtl B , pay_egic.app_emp E where A.ID = B.HDR_ID AND A.EMP_NO = E.EMP_NO AND A.EMP_NO = '" + Emp_NO + "' AND A.YEAR = '" + Year + "'  ";

            dt = oracleacess.ExecuteDataTable(SQL);

        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Get_Form_A_Data >> Get Form A Data as DataTable");
        }
        return dt;
    }


    public DataTable Print_Form_A(string Emp_NO, string Year)
    {
        try
        {
            SQL = " Select A.ID , A.EMP_NO , A.EMP_NAME , GET_MANG_NAME(A.EMP_NO) AS DIRECT_MANG_NAME , A.FORM_ID , A.YEAR , B.FINAL_RESULT , B.OUT_QTY_EVAL , B.OUT_QAL_JUST , B.DELIVERY_TIME_EVAL , B.DELIVERY_TIME_JUST , B.WORK_QLTY_EVAL , B.WORK_QLTY_JUST , B.SAFETY_EVAL , B.Safety_Just , B.TEAM_WORK_EVAL , B.Team_Work_Just , B.COMMIT_EVAL , B.Commit_Just , B.FLEXIBILTY_EVAL , B.Flexibilty_Just , B.HORSE_POWER_EVAL , B.HORSE_POWER_JUST , B.Problem_Solving_Eval , B.Problem_Solving_Just , B.COMPTENCY_EVAL , B.Comptency_Just  ,  A.USER_NAME , S.SECTION_NAME , J.JOB_NAME from as_entr_hdr A , as_form1_dtl B , pay_egic.app_emp E , pay_egic.app_section S , pay_egic.app_job J where A.ID = B.HDR_ID AND A.EMP_NO = E.EMP_NO AND E.SECTION_NO = S.SECTION_NO AND E.JOB_NO = J.JOB_NO AND A.EMP_NO = '" + Emp_NO + "' AND A.YEAR = '" + Year + "'  ";

            dt = oracleacess.ExecuteDataTable(SQL);

        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Print_Form_A >> Get Form A Data as DataTable");
        }
        return dt;
    }


    public int Delete_Form_Header(string ID)
    {
        int val = 0;
        try
        {
            SQL = " delete from as_entr_hdr where id = '" + ID + "'  ";
            oracleacess.ExecuteNonQuery(SQL);
            val = 1;
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Delete_Form_Header >> ");
            val = 0;
        }

        return val;
    }


    public int Delete_Form1(string ID)
    {
        int val = 0;
        try
        {

            SQL = " delete from as_form1_dtl where hdr_id = '" + ID + "' ";
            oracleacess.ExecuteNonQuery(SQL);
            Delete_Form_Header(ID);
            val = 1;
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Delete_Form1 >> ");
            val = 0;
        }

        return val;
    }


    public int Delete_Form2(string ID)
    {
        int val = 0;
        try
        {
            SQL = " delete from as_form2_dtl where hdr_id = '" + ID + "' ";
            oracleacess.ExecuteNonQuery(SQL);
            Delete_Form_Header(ID);
            val = 1;
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Delete_Form2 >> ");
            val = 0;
        }

        return val;
    }


    public int Delete_Form3(string ID)
    {
        int val = 0;
        try
        {
            SQL = " delete from as_form3_dtl where hdr_id = '" + ID + "' ";
            oracleacess.ExecuteNonQuery(SQL);
            Delete_Form_Header(ID);
            val = 1;
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Delete_Form3 >> ");
            val = 0;
        }

        return val;
    }


    public int Delete_Form4(string ID)
    {
        int val = 0;
        try
        {
            SQL = " delete from as_form4_dtl where hdr_id = '" + ID + "' ";
            oracleacess.ExecuteNonQuery(SQL);
            Delete_Form_Header(ID);
            val = 1;
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Delete_Form4 >> ");
            val = 0;
        }

        return val;
    }

    #endregion
    //***************************************( Form B  )******************************************************************
    #region " AS For Form (2) "


    public int Save_Form_B_DTL(string HDR_ID, string EMP_NAME, string G1_NAME, string G1_WEIGHT, string G1_ACTUAL, string G1_JUST, string G2_NAME, string G2_WEIGHT, string G2_ACTUAL, string G2_JUST, string G3_NAME, string G3_WEIGHT, string G3_ACTUAL, string G3_JUST, string G4_NAME, string G4_WEIGHT, string G4_ACTUAL, string G4_JUST, string G5_NAME, string G5_WEIGHT, string G5_ACTUAL, string G5_JUST, string G6_NAME, string G6_WEIGHT, string G6_ACTUAL, string G6_JUST, string G1NY_NAME, string G1NY_WEIGHT, string G1NY_ACTUAL, string G1NY_JUST, string G2NY_NAME, string G2NY_WEIGHT, string G2NY_ACTUAL, string G2NY_JUST, string G3NY_NAME, string G3NY_WEIGHT, string G3NY_ACTUAL, string G3NY_JUST, string G4NY_NAME, string G4NY_WEIGHT, string G4NY_ACTUAL, string G4NY_JUST, string G5NY_NAME, string G5NY_WEIGHT, string G5NY_ACTUAL, string G5NY_JUST, string G6NY_NAME, string G6NY_WEIGHT, string G6NY_ACTUAL, string G6NY_JUST, string OUT_QTY_EVAL, string OUT_QAL_JUST, string DELIVERY_TIME_EVAL, string DELIVERY_TIME_JUST, string WORK_QLTY_EVAL, string WORK_QLTY_JUST, string HORSE_POWER_EVAL, string HORSE_POWER_JUST, string PROBLEM_SOLVING_EVAL, string PROBLEM_SOLVING_JUST, string FINAL_RESULT)
    {
        int val = 0;
        try
        {
            SQL = " INSERT INTO as_form2_dtl ( HDR_ID ,EMP_NAME ,G1_NAME ,G1_WEIGHT ,G1_ACTUAL ,G1_JUST ,G2_NAME ,G2_WEIGHT ,G2_ACTUAL ,G2_JUST ,G3_NAME ,G3_WEIGHT ,G3_ACTUAL ,G3_JUST ,G4_NAME ,G4_WEIGHT ,G4_ACTUAL ,G4_JUST ,G5_NAME ,G5_WEIGHT ,G5_ACTUAL ,G5_JUST ,G6_NAME ,G6_WEIGHT ,G6_ACTUAL ,G6_JUST ,G1NY_NAME ,G1NY_WEIGHT ,G1NY_ACTUAL ,G1NY_JUST ,G2NY_NAME ,G2NY_WEIGHT ,G2NY_ACTUAL ,G2NY_JUST ,G3NY_NAME ,G3NY_WEIGHT ,G3NY_ACTUAL ,G3NY_JUST ,G4NY_NAME ,G4NY_WEIGHT ,G4NY_ACTUAL ,G4NY_JUST ,G5NY_NAME ,G5NY_WEIGHT ,G5NY_ACTUAL ,G5NY_JUST ,G6NY_NAME ,G6NY_WEIGHT ,G6NY_ACTUAL ,G6NY_JUST ,OUT_QTY_EVAL ,OUT_QAL_JUST ,DELIVERY_TIME_EVAL ,DELIVERY_TIME_JUST ,WORK_QLTY_EVAL ,WORK_QLTY_JUST ,HORSE_POWER_EVAL ,HORSE_POWER_JUST ,PROBLEM_SOLVING_EVAL ,PROBLEM_SOLVING_JUST , FINAL_RESULT ) ";
            SQL += " VALUES ( '" + HDR_ID + "' ,'" + EMP_NAME + "' ,'" + RemoveSp(G1_NAME) + "' ,'" + G1_WEIGHT + "' ,'" + G1_ACTUAL + "' ,'" + RemoveSp(G1_JUST) + "' ,'" + RemoveSp(G2_NAME) + "' ,'" + G2_WEIGHT + "' ,'" + G2_ACTUAL + "' ,'" + RemoveSp(G2_JUST) + "' ,'" + RemoveSp(G3_NAME) + "' ,'" + G3_WEIGHT + "' ,'" + G3_ACTUAL + "' ,'" + RemoveSp(G3_JUST) + "' ,'" + RemoveSp(G4_NAME) + "' ,'" + G4_WEIGHT + "' ,'" + G4_ACTUAL + "' ,'" + RemoveSp(G4_JUST) + "' ,'" + RemoveSp(G5_NAME) + "' ,'" + G5_WEIGHT + "' ,'" + G5_ACTUAL + "' ,'" + RemoveSp(G5_JUST) + "' ,'" + RemoveSp(G6_NAME) + "' ,'" + G6_WEIGHT + "' ,'" + G6_ACTUAL + "' ,'" + RemoveSp(G6_JUST) + "' ,'" + RemoveSp(G1NY_NAME) + "' ,'" + G1NY_WEIGHT + "' ,'" + G1NY_ACTUAL + "' ,'" + RemoveSp(G1NY_JUST) + "' ,'" + RemoveSp(G2NY_NAME) + "' ,'" + G2NY_WEIGHT + "' ,'" + G2NY_ACTUAL + "' ,'" + RemoveSp(G2NY_JUST) + "' ,'" + RemoveSp(G3NY_NAME) + "' ,'" + G3NY_WEIGHT + "' ,'" + G3NY_ACTUAL + "' ,'" + RemoveSp(G3NY_JUST) + "' ,'" + RemoveSp(G4NY_NAME) + "' ,'" + G4NY_WEIGHT + "' ,'" + G4NY_ACTUAL + "' ,'" + RemoveSp(G4NY_JUST) + "' ,'" + RemoveSp(G5NY_NAME) + "' ,'" + G5NY_WEIGHT + "' ,'" + G5NY_ACTUAL + "' ,'" + RemoveSp(G5NY_JUST) + "' ,'" + RemoveSp(G6NY_NAME) + "' ,'" + G6NY_WEIGHT + "' ,'" + G6NY_ACTUAL + "' ,'" + RemoveSp(G6NY_JUST) + "' ,'" + OUT_QTY_EVAL + "' ,'" + RemoveSp(OUT_QAL_JUST) + "' ,'" + DELIVERY_TIME_EVAL + "' ,'" + RemoveSp(DELIVERY_TIME_JUST) + "' ,'" + WORK_QLTY_EVAL + "' ,'" + RemoveSp(WORK_QLTY_JUST) + "' ,'" + HORSE_POWER_EVAL + "' ,'" + RemoveSp(HORSE_POWER_JUST) + "' ,'" + PROBLEM_SOLVING_EVAL + "' ,'" + RemoveSp(PROBLEM_SOLVING_JUST) + "' , '" + FINAL_RESULT + "' ) ";

            oracleacess.ExecuteNonQuery(SQL);
            val = 1;
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Save_Form_B_DTL >> save form (B) Data ");
            val = 0;
        }

        return val;
    }


    public int Update_Form_B_DTL(string HDR_ID, string EMP_NAME, string G1_NAME, string G1_WEIGHT, string G1_ACTUAL, string G1_JUST, string G2_NAME, string G2_WEIGHT, string G2_ACTUAL, string G2_JUST, string G3_NAME, string G3_WEIGHT, string G3_ACTUAL, string G3_JUST, string G4_NAME, string G4_WEIGHT, string G4_ACTUAL, string G4_JUST, string G5_NAME, string G5_WEIGHT, string G5_ACTUAL, string G5_JUST, string G6_NAME, string G6_WEIGHT, string G6_ACTUAL, string G6_JUST, string G1NY_NAME, string G1NY_WEIGHT, string G1NY_ACTUAL, string G1NY_JUST, string G2NY_NAME, string G2NY_WEIGHT, string G2NY_ACTUAL, string G2NY_JUST, string G3NY_NAME, string G3NY_WEIGHT, string G3NY_ACTUAL, string G3NY_JUST, string G4NY_NAME, string G4NY_WEIGHT, string G4NY_ACTUAL, string G4NY_JUST, string G5NY_NAME, string G5NY_WEIGHT, string G5NY_ACTUAL, string G5NY_JUST, string G6NY_NAME, string G6NY_WEIGHT, string G6NY_ACTUAL, string G6NY_JUST, string OUT_QTY_EVAL, string OUT_QAL_JUST, string DELIVERY_TIME_EVAL, string DELIVERY_TIME_JUST, string WORK_QLTY_EVAL, string WORK_QLTY_JUST, string HORSE_POWER_EVAL, string HORSE_POWER_JUST, string PROBLEM_SOLVING_EVAL, string PROBLEM_SOLVING_JUST, string FINAL_RESULT)
    {
        int val = 0;
        try
        {
            SQL = " UPDATE as_form2_dtl Set G1_NAME = '" + RemoveSp(G1_NAME) + "' ,G1_WEIGHT = '" + G1_WEIGHT + "' ,G1_ACTUAL = '" + G1_ACTUAL + "' ,G1_JUST = '" + RemoveSp(G1_JUST) + "' ,G2_NAME = '" + RemoveSp(G2_NAME) + "' ,G2_WEIGHT = '" + G2_WEIGHT + "' ,G2_ACTUAL = '" + G2_ACTUAL + "' ,G2_JUST = '" + RemoveSp(G2_JUST) + "' ,G3_NAME = '" + RemoveSp(G3_NAME) + "' ,G3_WEIGHT = '" + G3_WEIGHT + "' ,G3_ACTUAL = '" + G3_ACTUAL + "' ,G3_JUST = '" + RemoveSp(G3_JUST) + "' ,G4_NAME = '" + RemoveSp(G4_NAME) + "' ,G4_WEIGHT = '" + G4_WEIGHT + "',G4_ACTUAL = '" + G4_ACTUAL + "' ,G4_JUST = '" + RemoveSp(G4_JUST) + "' ,G5_NAME = '" + RemoveSp(G5_NAME) + "' ,G5_WEIGHT = '" + G5_WEIGHT + "' ,G5_ACTUAL = '" + G5_ACTUAL + "' ,G5_JUST = '" + RemoveSp(G5_JUST) + "' ,G6_NAME = '" + RemoveSp(G6_NAME) + "' ,G6_WEIGHT = '" + G6_WEIGHT + "' ,G6_ACTUAL = '" + G6_ACTUAL + "' ,G6_JUST = '" + RemoveSp(G6_JUST) + "' ,G1NY_NAME = '" + RemoveSp(G1NY_NAME) + "' ,G1NY_WEIGHT = '" + G1NY_WEIGHT + "' ,G1NY_ACTUAL = '" + G1NY_ACTUAL + "' ,G1NY_JUST = '" + RemoveSp(G1NY_JUST) + "' ,G2NY_NAME = '" + RemoveSp(G2NY_NAME) + "' ,G2NY_WEIGHT = '" + G2NY_WEIGHT + "' ,G2NY_ACTUAL = '" + G2NY_ACTUAL + "' ,G2NY_JUST = '" + RemoveSp(G2NY_JUST) + "' ,G3NY_NAME = '" + RemoveSp(G3NY_NAME) + "' ,G3NY_WEIGHT = '" + G3NY_WEIGHT + "' ,G3NY_ACTUAL = '" + G3NY_ACTUAL + "' ,G3NY_JUST = '" + RemoveSp(G3NY_JUST) + "' ,G4NY_NAME = '" + RemoveSp(G4NY_NAME) + "' ,G4NY_WEIGHT = '" + G4NY_WEIGHT + "' ,G4NY_ACTUAL = '" + G4NY_ACTUAL + "' ,G4NY_JUST = '" + RemoveSp(G4NY_JUST) + "' ,G5NY_NAME = '" + RemoveSp(G5NY_NAME) + "' ,G5NY_WEIGHT = '" + G5NY_WEIGHT + "' ,G5NY_ACTUAL = '" + G5NY_ACTUAL + "' ,G5NY_JUST = '" + RemoveSp(G5NY_JUST) + "' ,G6NY_NAME = '" + RemoveSp(G6NY_NAME) + "' ,G6NY_WEIGHT = '" + G6NY_WEIGHT + "' ,G6NY_ACTUAL = '" + G6NY_ACTUAL + "' ,G6NY_JUST = '" + RemoveSp(G6NY_JUST) + "' ,OUT_QTY_EVAL = '" + OUT_QTY_EVAL + "' ,OUT_QAL_JUST = '" + RemoveSp(OUT_QAL_JUST) + "' ,DELIVERY_TIME_EVAL = '" + DELIVERY_TIME_EVAL + "' ,DELIVERY_TIME_JUST = '" + RemoveSp(DELIVERY_TIME_JUST) + "' ,WORK_QLTY_EVAL = '" + WORK_QLTY_EVAL + "' ,WORK_QLTY_JUST = '" + RemoveSp(WORK_QLTY_JUST) + "' ,HORSE_POWER_EVAL = '" + HORSE_POWER_EVAL + "' ,HORSE_POWER_JUST = '" + RemoveSp(HORSE_POWER_JUST) + "' ,PROBLEM_SOLVING_EVAL = '" + PROBLEM_SOLVING_EVAL + "' ,PROBLEM_SOLVING_JUST = '" + RemoveSp(PROBLEM_SOLVING_JUST) + "' , FINAL_RESULT = '" + FINAL_RESULT + "' Where  HDR_ID = '" + HDR_ID + "' ";
            oracleacess.ExecuteNonQuery(SQL);
            val = 1;
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Update_Form_B_DTL >> Update form (B) Data ");
            val = 0;
        }

        return val;
    }


    public DataTable Get_Form_B_Data(string Emp_NO, string Year)
    {
        try
        {
            SQL = " select A.ID , A.EMP_NO , A.EMP_NAME , A.FORM_ID  , B.G1_NAME , B.G1_WEIGHT , B.G1_ACTUAL , B.G1_JUST , B.G2_NAME , B.G2_WEIGHT , B.G2_ACTUAL , B.G2_JUST , B.G3_NAME , B.G3_WEIGHT , B.G3_ACTUAL , B.G3_JUST , B.G4_NAME , B.G4_WEIGHT , B.G4_ACTUAL , B.G4_JUST , B.G5_NAME , B.G5_WEIGHT , B.G5_ACTUAL , B.G5_JUST , B.G6_NAME , B.G6_WEIGHT , B.G6_ACTUAL , B.G6_JUST , B.G1NY_NAME , B.G1NY_WEIGHT , B.G1NY_ACTUAL , B.G1NY_JUST , B.G2NY_NAME , B.G2NY_WEIGHT , B.G2NY_ACTUAL , B.G2NY_JUST , B.G3NY_NAME , B.G3NY_WEIGHT , B.G3NY_ACTUAL , B.G3NY_JUST , B.G4NY_NAME , B.G4NY_WEIGHT , B.G4NY_ACTUAL , B.G4NY_JUST , B.G5NY_NAME , B.G5NY_WEIGHT , B.G5NY_ACTUAL , B.G5NY_JUST , B.G6NY_NAME , B.G6NY_WEIGHT , B.G6NY_ACTUAL , B.G6NY_JUST , B.OUT_QTY_EVAL , B.OUT_QAL_JUST , B.DELIVERY_TIME_EVAL , B.DELIVERY_TIME_JUST , B.WORK_QLTY_EVAL , B.WORK_QLTY_JUST , B.HORSE_POWER_EVAL , B.HORSE_POWER_JUST , B.PROBLEM_SOLVING_EVAL , B.PROBLEM_SOLVING_JUST , B.FINAL_RESULT , A.USER_NAME FROM  as_entr_hdr A , as_form2_dtl B Where A.ID = B.HDR_ID AND A.EMP_NO = '" + Emp_NO + "'  AND  A.YEAR = '" + Year + "'  ";

            dt = oracleacess.ExecuteDataTable(SQL);

        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Get_Form_B_Data >> Get Form B Data as DataTable");
        }
        return dt;
    }


    public DataTable Print_Form_B(string Emp_NO, string Year)
    {
        try
        {
            SQL = " select A.ID , A.EMP_NO , A.EMP_NAME , GET_MANG_NAME(A.EMP_NO) AS DIRECT_MANG_NAME , A.FORM_ID  , A.YEAR , B.G1_NAME , B.G1_WEIGHT , B.G1_ACTUAL , B.G1_JUST , B.G2_NAME , B.G2_WEIGHT , B.G2_ACTUAL , B.G2_JUST , B.G3_NAME , B.G3_WEIGHT , B.G3_ACTUAL , B.G3_JUST , B.G4_NAME , B.G4_WEIGHT , B.G4_ACTUAL , B.G4_JUST , B.G5_NAME , B.G5_WEIGHT , B.G5_ACTUAL , B.G5_JUST , B.G6_NAME , B.G6_WEIGHT , B.G6_ACTUAL , B.G6_JUST , B.G1NY_NAME , B.G1NY_WEIGHT , B.G1NY_ACTUAL , B.G1NY_JUST , B.G2NY_NAME , B.G2NY_WEIGHT , B.G2NY_ACTUAL , B.G2NY_JUST , B.G3NY_NAME , B.G3NY_WEIGHT , B.G3NY_ACTUAL , B.G3NY_JUST , B.G4NY_NAME , B.G4NY_WEIGHT , B.G4NY_ACTUAL , B.G4NY_JUST , B.G5NY_NAME , B.G5NY_WEIGHT , B.G5NY_ACTUAL , B.G5NY_JUST , B.G6NY_NAME , B.G6NY_WEIGHT , B.G6NY_ACTUAL , B.G6NY_JUST , B.OUT_QTY_EVAL , B.OUT_QAL_JUST , B.DELIVERY_TIME_EVAL , B.DELIVERY_TIME_JUST , B.WORK_QLTY_EVAL , B.WORK_QLTY_JUST , B.HORSE_POWER_EVAL , B.HORSE_POWER_JUST , B.PROBLEM_SOLVING_EVAL , B.PROBLEM_SOLVING_JUST , B.FINAL_RESULT , A.USER_NAME , S.SECTION_NAME , J.JOB_NAME FROM  as_entr_hdr A , as_form2_dtl B , pay_egic.app_emp E , pay_egic.app_section S , pay_egic.app_job J Where A.ID = B.HDR_ID AND A.EMP_NO = E.EMP_NO AND E.SECTION_NO = S.SECTION_NO AND E.JOB_NO = J.JOB_NO AND A.EMP_NO = '" + Emp_NO + "'  AND  A.YEAR = '" + Year + "'  ";

            dt = oracleacess.ExecuteDataTable(SQL);

        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Print_Form_B >> Get Form B Data as DataTable");
        }
        return dt;
    }

    #endregion
    //***************************************( Form C  )******************************************************************
    #region " AS For Form (3) "


    public int Save_Form_C_DTL(string HDR_ID, string EMP_NAME, string G1_NAME, string G1_WEIGHT, string G1_ACTUAL, string G1_JUST, string G2_NAME, string G2_WEIGHT, string G2_ACTUAL, string G2_JUST, string G3_NAME, string G3_WEIGHT, string G3_ACTUAL, string G3_JUST, string G4_NAME, string G4_WEIGHT, string G4_ACTUAL, string G4_JUST, string G5_NAME, string G5_WEIGHT, string G5_ACTUAL, string G5_JUST, string G6_NAME, string G6_WEIGHT, string G6_ACTUAL, string G6_JUST, string HORSE_POWER_EVAL, string HORSE_POWER_JUST, string PROBLEM_SOLVING_EVAL, string PROBLEM_SOLVING_JUST, string D_QLTY_EVAL, string D_QLTY_JUST, string BE_TEAM_EVAL, string BE_TEAM_JUST, string ORIENTED_EVAL, string ORIENTED_JUST, string NO_SUPPORT_EVAL, string NO_SUPPORT_JUST, string RESOURCES_EVAL, string RESOURCES_JUST, string VALUE_ADDED_EVAL, string VALUE_ADDED_JUST, string G1NY_NAME, string G1NY_WEIGHT, string G1NY_ACTUAL, string G1NY_JUST, string G2NY_NAME, string G2NY_WEIGHT, string G2NY_ACTUAL, string G2NY_JUST, string G3NY_NAME, string G3NY_WEIGHT, string G3NY_ACTUAL, string G3NY_JUST, string G4NY_NAME, string G4NY_WEIGHT, string G4NY_ACTUAL, string G4NY_JUST, string G5NY_NAME, string G5NY_WEIGHT, string G5NY_ACTUAL, string G5NY_JUST, string G6NY_NAME, string G6NY_WEIGHT, string G6NY_ACTUAL, string G6NY_JUST, string FINAL_RESULT)
    {
        int val = 0;
        try
        {
            SQL = " INSERT INTO as_form3_dtl ( HDR_ID , EMP_NAME ,G1_NAME, G1_WEIGHT ,G1_ACTUAL ,G1_JUST ,G2_NAME ,G2_WEIGHT ,G2_ACTUAL ,G2_JUST ,G3_NAME ,G3_WEIGHT ,G3_ACTUAL ,G3_JUST ,G4_NAME ,G4_WEIGHT ,G4_ACTUAL ,G4_JUST ,G5_NAME ,G5_WEIGHT ,G5_ACTUAL ,G5_JUST ,G6_NAME ,G6_WEIGHT ,G6_ACTUAL ,G6_JUST , HORSE_POWER_EVAL ,HORSE_POWER_JUST ,PROBLEM_SOLVING_EVAL ,PROBLEM_SOLVING_JUST ,D_QLTY_EVAL ,D_QLTY_JUST ,BE_TEAM_EVAL ,BE_TEAM_JUST ,ORIENTED_EVAL ,ORIENTED_JUST ,NO_SUPPORT_EVAL ,NO_SUPPORT_JUST ,RESOURCES_EVAL ,RESOURCES_JUST ,VALUE_ADDED_EVAL ,VALUE_ADDED_JUST ,G1NY_NAME ,G1NY_WEIGHT ,G1NY_ACTUAL ,G1NY_JUST ,G2NY_NAME ,G2NY_WEIGHT ,G2NY_ACTUAL ,G2NY_JUST ,G3NY_NAME ,G3NY_WEIGHT ,G3NY_ACTUAL ,G3NY_JUST ,G4NY_NAME ,G4NY_WEIGHT ,G4NY_ACTUAL ,G4NY_JUST ,G5NY_NAME ,G5NY_WEIGHT ,G5NY_ACTUAL ,G5NY_JUST ,G6NY_NAME ,G6NY_WEIGHT, G6NY_ACTUAL,G6NY_JUST ,FINAL_RESULT ) ";
            SQL += " VALUES ( '" + HDR_ID + "' ,   '" + EMP_NAME + "' ,   '" + RemoveSp(G1_NAME) + "' ,   '" + G1_WEIGHT + "' ,   '" + G1_ACTUAL + "' ,   '" + RemoveSp(G1_JUST) + "' ,   '" + RemoveSp(G2_NAME) + "' ,   '" + G2_WEIGHT + "' ,   '" + G2_ACTUAL + "' ,   '" + RemoveSp(G2_JUST) + "' ,   '" + RemoveSp(G3_NAME) + "' ,   '" + G3_WEIGHT + "' ,   '" + G3_ACTUAL + "' ,   '" + RemoveSp(G3_JUST) + "' ,   '" + RemoveSp(G4_NAME) + "' ,   '" + G4_WEIGHT + "' ,   '" + G4_ACTUAL + "' ,   '" + RemoveSp(G4_JUST) + "' ,   '" + RemoveSp(G5_NAME) + "' ,   '" + G5_WEIGHT + "' ,   '" + G5_ACTUAL + "' ,   '" + RemoveSp(G5_JUST) + "' ,   '" + RemoveSp(G6_NAME) + "' ,   '" + G6_WEIGHT + "' ,   '" + G6_ACTUAL + "' ,   '" + RemoveSp(G6_JUST) + "' ,   '" + HORSE_POWER_EVAL + "' ,   '" + RemoveSp(HORSE_POWER_JUST) + "' ,   '" + PROBLEM_SOLVING_EVAL + "' ,   '" + RemoveSp(PROBLEM_SOLVING_JUST) + "' ,   '" + D_QLTY_EVAL + "' ,   '" + RemoveSp(D_QLTY_JUST) + "' ,   '" + BE_TEAM_EVAL + "' ,   '" + RemoveSp(BE_TEAM_JUST) + "' ,   '" + ORIENTED_EVAL + "' ,   '" + RemoveSp(ORIENTED_JUST) + "' ,   '" + NO_SUPPORT_EVAL + "' ,   '" + RemoveSp(NO_SUPPORT_JUST) + "' ,   '" + RESOURCES_EVAL + "' ,   '" + RemoveSp(RESOURCES_JUST) + "' ,   '" + VALUE_ADDED_EVAL + "' ,   '" + RemoveSp(VALUE_ADDED_JUST) + "' ,   '" + RemoveSp(G1NY_NAME) + "' ,   '" + G1NY_WEIGHT + "' ,   '" + G1NY_ACTUAL + "' ,   '" + RemoveSp(G1NY_JUST) + "' ,   '" + RemoveSp(G2NY_NAME) + "' ,   '" + G2NY_WEIGHT + "' ,   '" + G2NY_ACTUAL + "' ,   '" + RemoveSp(G2NY_JUST) + "' ,   '" + RemoveSp(G3NY_NAME) + "' ,   '" + G3NY_WEIGHT + "' ,   '" + G3NY_ACTUAL + "' ,   '" + RemoveSp(G3NY_JUST) + "' ,   '" + RemoveSp(G4NY_NAME) + "' ,   '" + G4NY_WEIGHT + "' ,   '" + G4NY_ACTUAL + "' ,   '" + RemoveSp(G4NY_JUST) + "' ,   '" + RemoveSp(G5NY_NAME) + "' ,   '" + G5NY_WEIGHT + "' ,   '" + G5NY_ACTUAL + "' ,   '" + RemoveSp(G5NY_JUST) + "' ,   '" + RemoveSp(G6NY_NAME) + "' ,   '" + G6NY_WEIGHT + "' ,   '" + G6NY_ACTUAL + "' ,   '" + RemoveSp(G6NY_JUST) + "' ,   '" + FINAL_RESULT + "' ) ";
            oracleacess.ExecuteNonQuery(SQL);
            val = 1;
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Save_Form_B_DTL >> save form (B) Data ");
            val = 0;
        }

        return val;
    }


    public int Update_Form_C_DTL(string HDR_ID, string EMP_NAME, string G1_NAME, string G1_WEIGHT, string G1_ACTUAL, string G1_JUST, string G2_NAME, string G2_WEIGHT, string G2_ACTUAL, string G2_JUST, string G3_NAME, string G3_WEIGHT, string G3_ACTUAL, string G3_JUST, string G4_NAME, string G4_WEIGHT, string G4_ACTUAL, string G4_JUST, string G5_NAME, string G5_WEIGHT, string G5_ACTUAL, string G5_JUST, string G6_NAME, string G6_WEIGHT, string G6_ACTUAL, string G6_JUST, string HORSE_POWER_EVAL, string HORSE_POWER_JUST, string PROBLEM_SOLVING_EVAL, string PROBLEM_SOLVING_JUST, string D_QLTY_EVAL, string D_QLTY_JUST, string BE_TEAM_EVAL, string BE_TEAM_JUST, string ORIENTED_EVAL, string ORIENTED_JUST, string NO_SUPPORT_EVAL, string NO_SUPPORT_JUST, string RESOURCES_EVAL, string RESOURCES_JUST, string VALUE_ADDED_EVAL, string VALUE_ADDED_JUST, string G1NY_NAME, string G1NY_WEIGHT, string G1NY_ACTUAL, string G1NY_JUST, string G2NY_NAME, string G2NY_WEIGHT, string G2NY_ACTUAL, string G2NY_JUST, string G3NY_NAME, string G3NY_WEIGHT, string G3NY_ACTUAL, string G3NY_JUST, string G4NY_NAME, string G4NY_WEIGHT, string G4NY_ACTUAL, string G4NY_JUST, string G5NY_NAME, string G5NY_WEIGHT, string G5NY_ACTUAL, string G5NY_JUST, string G6NY_NAME, string G6NY_WEIGHT, string G6NY_ACTUAL, string G6NY_JUST, string FINAL_RESULT)
    {
        int val = 0;
        try
        {
            SQL = " UPDATE as_form3_dtl Set  G1_NAME   =   '" + RemoveSp(G1_NAME) + "' , G1_WEIGHT = '" + G1_WEIGHT + "' ,  G1_ACTUAL  =   '" + G1_ACTUAL + "' ,  G1_JUST  =   '" + RemoveSp(G1_JUST) + "' ,  G2_NAME  =   '" + RemoveSp(G2_NAME) + "' ,   G2_WEIGHT =   '" + G2_WEIGHT + "' ,   G2_ACTUAL =   '" + G2_ACTUAL + "' ,   G2_JUST =   '" + RemoveSp(G2_JUST) + "' ,   G3_NAME =   '" + RemoveSp(G3_NAME) + "' ,   G3_WEIGHT =   '" + G3_WEIGHT + "' ,  G3_ACTUAL  =   '" + G3_ACTUAL + "' ,  G3_JUST  =   '" + RemoveSp(G3_JUST) + "' ,   G4_NAME =   '" + RemoveSp(G4_NAME) + "' ,   G4_WEIGHT =   '" + G4_WEIGHT + "' ,   G4_ACTUAL =   '" + G4_ACTUAL + "' ,   G4_JUST =   '" + RemoveSp(G4_JUST) + "' ,  G5_NAME  =   '" + RemoveSp(G5_NAME) + "' ,   G5_WEIGHT =   '" + G5_WEIGHT + "' ,   G5_ACTUAL =   '" + G5_ACTUAL + "' ,  G5_JUST  =   '" + RemoveSp(G5_JUST) + "' ,   G6_NAME =   '" + RemoveSp(G6_NAME) + "' ,   G6_WEIGHT =   '" + G6_WEIGHT + "' ,  G6_ACTUAL  =   '" + G6_ACTUAL + "' ,  G6_JUST  =   '" + RemoveSp(G6_JUST) + "' ,   HORSE_POWER_EVAL =   '" + HORSE_POWER_EVAL + "' ,  HORSE_POWER_JUST  =   '" + RemoveSp(HORSE_POWER_JUST) + "' ,   PROBLEM_SOLVING_EVAL =   '" + PROBLEM_SOLVING_EVAL + "' ,   PROBLEM_SOLVING_JUST =   '" + RemoveSp(PROBLEM_SOLVING_JUST) + "' ,   D_QLTY_EVAL =   '" + D_QLTY_EVAL + "' ,  D_QLTY_JUST  =   '" + RemoveSp(D_QLTY_JUST) + "' ,  BE_TEAM_EVAL  =   '" + BE_TEAM_EVAL + "' ,  BE_TEAM_JUST  =   '" + RemoveSp(BE_TEAM_JUST) + "' ,   ORIENTED_EVAL =   '" + ORIENTED_EVAL + "' ,   ORIENTED_JUST =   '" + RemoveSp(ORIENTED_JUST) + "' ,  NO_SUPPORT_EVAL  =   '" + NO_SUPPORT_EVAL + "' ,  NO_SUPPORT_JUST  =   '" + RemoveSp(NO_SUPPORT_JUST) + "' ,   RESOURCES_EVAL =   '" + RESOURCES_EVAL + "' ,   RESOURCES_JUST =   '" + RemoveSp(RESOURCES_JUST) + "' ,  VALUE_ADDED_EVAL  =   '" + VALUE_ADDED_EVAL + "' ,   VALUE_ADDED_JUST  =   '" + RemoveSp(VALUE_ADDED_JUST) + "' , G1NY_NAME =   '" + RemoveSp(G1NY_NAME) + "' , G1NY_WEIGHT  =   '" + G1NY_WEIGHT + "' ,  G1NY_ACTUAL =   '" + G1NY_ACTUAL + "' ,  G1NY_JUST  =   '" + RemoveSp(G1NY_JUST) + "' ,  G2NY_NAME  =   '" + RemoveSp(G2NY_NAME) + "' ,  G2NY_WEIGHT  =   '" + G2NY_WEIGHT + "' ,   G2NY_ACTUAL =   '" + G2NY_ACTUAL + "' , G2NY_JUST   =   '" + RemoveSp(G2NY_JUST) + "' ,  G3NY_NAME  =   '" + RemoveSp(G3NY_NAME) + "' ,  G3NY_WEIGHT  =   '" + G3NY_WEIGHT + "' ,  G3NY_ACTUAL  =   '" + G3NY_ACTUAL + "' ,   G3NY_JUST =   '" + RemoveSp(G3NY_JUST) + "' ,  G4NY_NAME  =   '" + RemoveSp(G4NY_NAME) + "' ,  G4NY_WEIGHT  =   '" + G4NY_WEIGHT + "' ,  G4NY_ACTUAL  =   '" + G4NY_ACTUAL + "' , G4NY_JUST   =   '" + RemoveSp(G4NY_JUST) + "' ,   G5NY_NAME =   '" + RemoveSp(G5NY_NAME) + "' ,   G5NY_WEIGHT =   '" + G5NY_WEIGHT + "' ,   G5NY_ACTUAL =   '" + G5NY_ACTUAL + "' ,   G5NY_JUST =   '" + RemoveSp(G5NY_JUST) + "' ,   G6NY_NAME =   '" + RemoveSp(G6NY_NAME) + "' ,   G6NY_WEIGHT =   '" + G6NY_WEIGHT + "' ,  G6NY_ACTUAL  =   '" + G6NY_ACTUAL + "' ,  G6NY_JUST  =   '" + RemoveSp(G6NY_JUST) + "' ,   FINAL_RESULT =   '" + FINAL_RESULT + "' WHERE HDR_ID = '" + HDR_ID + "' ";
            oracleacess.ExecuteNonQuery(SQL);
            val = 1;
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Save_Form_C_DTL >> save form (C) Data ");
            val = 0;
        }

        return val;
    }


    public DataTable Get_Form_C_Data(string Emp_NO, string YEAR)
    {
        try
        {
            SQL = " select A.ID , A.EMP_NO , A.EMP_NAME , A.FORM_ID , B.G1_NAME, B. G1_WEIGHT , B.G1_ACTUAL , B.G1_JUST , B.G2_NAME , B.G2_WEIGHT , B.G2_ACTUAL , B.G2_JUST , B.G3_NAME , B.G3_WEIGHT , B.G3_ACTUAL , B.G3_JUST , B.G4_NAME , B.G4_WEIGHT , B.G4_ACTUAL , B.G4_JUST , B.G5_NAME , B.G5_WEIGHT , B.G5_ACTUAL , B.G5_JUST , B.G6_NAME , B.G6_WEIGHT , B.G6_ACTUAL , B.G6_JUST , B. HORSE_POWER_EVAL , B.HORSE_POWER_JUST , B.PROBLEM_SOLVING_EVAL , B.PROBLEM_SOLVING_JUST , B.D_QLTY_EVAL , B.D_QLTY_JUST , B.BE_TEAM_EVAL , B.BE_TEAM_JUST , B.ORIENTED_EVAL , B.ORIENTED_JUST , B.NO_SUPPORT_EVAL , B.NO_SUPPORT_JUST , B.RESOURCES_EVAL , B.RESOURCES_JUST , B.VALUE_ADDED_EVAL , B.VALUE_ADDED_JUST , B.G1NY_NAME , B.G1NY_WEIGHT , B.G1NY_ACTUAL , B.G1NY_JUST , B.G2NY_NAME , B.G2NY_WEIGHT , B.G2NY_ACTUAL , B.G2NY_JUST , B.G3NY_NAME , B.G3NY_WEIGHT , B.G3NY_ACTUAL , B.G3NY_JUST , B.G4NY_NAME , B.G4NY_WEIGHT , B.G4NY_ACTUAL , B.G4NY_JUST , B.G5NY_NAME , B.G5NY_WEIGHT , B.G5NY_ACTUAL , B.G5NY_JUST , B.G6NY_NAME , B.G6NY_WEIGHT, B. G6NY_ACTUAL, B.G6NY_JUST , B.FINAL_RESULT , A.USER_NAME  from  as_entr_hdr A , as_form3_dtl B where A.ID = B.HDR_ID AND A.EMP_NO = '" + Emp_NO + "'  AND  A.YEAR = '" + YEAR + "'  ";

            dt = oracleacess.ExecuteDataTable(SQL);

        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Get_Form_C_Data >> Get Form C Data as DataTable");
        }
        return dt;
    }


    public DataTable Print_Form_C(string Emp_NO, string YEAR)
    {
        try
        {
            SQL = " select A.ID , A.EMP_NO , A.EMP_NAME , GET_MANG_NAME(A.EMP_NO) AS DIRECT_MANG_NAME , A.FORM_ID , A.YEAR , B.G1_NAME, B. G1_WEIGHT , B.G1_ACTUAL , B.G1_JUST , B.G2_NAME , B.G2_WEIGHT , B.G2_ACTUAL , B.G2_JUST , B.G3_NAME , B.G3_WEIGHT , B.G3_ACTUAL , B.G3_JUST , B.G4_NAME , B.G4_WEIGHT , B.G4_ACTUAL , B.G4_JUST , B.G5_NAME , B.G5_WEIGHT , B.G5_ACTUAL , B.G5_JUST , B.G6_NAME , B.G6_WEIGHT , B.G6_ACTUAL , B.G6_JUST , B. HORSE_POWER_EVAL , B.HORSE_POWER_JUST , B.PROBLEM_SOLVING_EVAL , B.PROBLEM_SOLVING_JUST , B.D_QLTY_EVAL , B.D_QLTY_JUST , B.BE_TEAM_EVAL , B.BE_TEAM_JUST , B.ORIENTED_EVAL , B.ORIENTED_JUST , B.NO_SUPPORT_EVAL , B.NO_SUPPORT_JUST , B.RESOURCES_EVAL , B.RESOURCES_JUST , B.VALUE_ADDED_EVAL , B.VALUE_ADDED_JUST , B.G1NY_NAME , B.G1NY_WEIGHT , B.G1NY_ACTUAL , B.G1NY_JUST , B.G2NY_NAME , B.G2NY_WEIGHT , B.G2NY_ACTUAL , B.G2NY_JUST , B.G3NY_NAME , B.G3NY_WEIGHT , B.G3NY_ACTUAL , B.G3NY_JUST , B.G4NY_NAME , B.G4NY_WEIGHT , B.G4NY_ACTUAL , B.G4NY_JUST , B.G5NY_NAME , B.G5NY_WEIGHT , B.G5NY_ACTUAL , B.G5NY_JUST , B.G6NY_NAME , B.G6NY_WEIGHT, B. G6NY_ACTUAL, B.G6NY_JUST , B.FINAL_RESULT , A.USER_NAME , S.SECTION_NAME , J.JOB_NAME from  as_entr_hdr A , as_form3_dtl B , pay_egic.app_emp E , pay_egic.app_section S , pay_egic.app_job J where A.ID = B.HDR_ID AND A.EMP_NO = E.EMP_NO AND E.SECTION_NO = S.SECTION_NO AND E.JOB_NO = J.JOB_NO AND A.EMP_NO = '" + Emp_NO + "'  AND  A.YEAR = '" + YEAR + "'  ";

            dt = oracleacess.ExecuteDataTable(SQL);

        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Print_Form_C >> Get Form C Data as DataTable");
        }
        return dt;
    }

    #endregion
    //***************************************( Form D )*******************************************************************
    #region " AS For Form (4) "


    public int Save_Form_D_DTL(string HDR_ID, string EMP_NAME, string G1_NAME, string G1_WEIGHT, string G1_ACTUAL, string G1_JUST, string G2_NAME, string G2_WEIGHT, string G2_ACTUAL, string G2_JUST, string G3_NAME, string G3_WEIGHT, string G3_ACTUAL, string G3_JUST, string G4_NAME, string G4_WEIGHT, string G4_ACTUAL, string G4_JUST, string G5_NAME, string G5_WEIGHT, string G5_ACTUAL, string G5_JUST, string G6_NAME, string G6_WEIGHT, string G6_ACTUAL, string G6_JUST, string HORSE_POWER_EVAL, string HORSE_POWER_JUST, string PROBLEM_SOLVING_EVAL, string PROBLEM_SOLVING_JUST, string MEASURING_WORK_EVAL, string MEASURING_WORK_JUST, string MOTIVATING_EVAL, string MOTIVATING_JUST, string PLANNING_EVAL, string PLANNING_JUST, string TEAM_CONT_EVAL, string TEAM_CONT_JUST, string WORK_HABITS_EVAL, string WORK_HABITS_JUST, string G1NY_NAME, string G1NY_WEIGHT, string G1NY_ACTUAL, string G1NY_JUST, string G2NY_NAME, string G2NY_WEIGHT, string G2NY_ACTUAL, string G2NY_JUST, string G3NY_NAME, string G3NY_WEIGHT, string G3NY_ACTUAL, string G3NY_JUST, string G4NY_NAME, string G4NY_WEIGHT, string G4NY_ACTUAL, string G4NY_JUST, string G5NY_NAME, string G5NY_WEIGHT, string G5NY_ACTUAL, string G5NY_JUST, string G6NY_NAME, string G6NY_WEIGHT, string G6NY_ACTUAL, string G6NY_JUST, string FINAL_RESULT)
    {
        int val = 0;
        try
        {
            SQL = " INSERT INTO as_form4_dtl ( HDR_ID ,EMP_NAME ,G1_NAME ,G1_WEIGHT ,G1_ACTUAL ,G1_JUST ,G2_NAME ,G2_WEIGHT ,G2_ACTUAL ,G2_JUST ,G3_NAME ,G3_WEIGHT ,G3_ACTUAL ,G3_JUST ,G4_NAME ,G4_WEIGHT ,G4_ACTUAL ,G4_JUST ,G5_NAME ,G5_WEIGHT ,G5_ACTUAL ,G5_JUST ,G6_NAME ,G6_WEIGHT ,G6_ACTUAL ,G6_JUST ,HORSE_POWER_EVAL ,HORSE_POWER_JUST ,PROBLEM_SOLVING_EVAL ,PROBLEM_SOLVING_JUST ,MEASURING_WORK_EVAL ,MEASURING_WORK_JUST ,MOTIVATING_EVAL ,MOTIVATING_JUST ,PLANNING_EVAL ,PLANNING_JUST ,TEAM_CONT_EVAL ,TEAM_CONT_JUST ,WORK_HABITS_EVAL ,WORK_HABITS_JUST ,G1NY_NAME ,G1NY_WEIGHT ,G1NY_ACTUAL ,G1NY_JUST ,G2NY_NAME ,G2NY_WEIGHT,G2NY_ACTUAL ,G2NY_JUST ,G3NY_NAME ,G3NY_WEIGHT ,G3NY_ACTUAL ,G3NY_JUST ,G4NY_NAME ,G4NY_WEIGHT ,G4NY_ACTUAL ,G4NY_JUST ,G5NY_NAME ,G5NY_WEIGHT ,G5NY_ACTUAL ,G5NY_JUST ,G6NY_NAME ,G6NY_WEIGHT ,G6NY_ACTUAL ,G6NY_JUST ,FINAL_RESULT ) ";
            SQL += " VALUES ( '" + HDR_ID + "' , '" + EMP_NAME + "' , '" + RemoveSp(G1_NAME) + "' , '" + G1_WEIGHT + "' , '" + G1_ACTUAL + "' , '" + RemoveSp(G1_JUST) + "' , '" + RemoveSp(G2_NAME) + "' , '" + G2_WEIGHT + "' , '" + G2_ACTUAL + "' , '" + RemoveSp(G2_JUST) + "' , '" + RemoveSp(G3_NAME) + "' , '" + G3_WEIGHT + "' , '" + G3_ACTUAL + "' , '" + RemoveSp(G3_JUST) + "' , '" + RemoveSp(G4_NAME) + "' , '" + G4_WEIGHT + "' , '" + G4_ACTUAL + "' , '" + RemoveSp(G4_JUST) + "' , '" + RemoveSp(G5_NAME) + "' , '" + G5_WEIGHT + "' , '" + G5_ACTUAL + "' , '" + RemoveSp(G5_JUST) + "' , '" + RemoveSp(G6_NAME) + "' , '" + G6_WEIGHT + "' , '" + G6_ACTUAL + "' , '" + RemoveSp(G6_JUST) + "' , '" + HORSE_POWER_EVAL + "' , '" + RemoveSp(HORSE_POWER_JUST) + "' , '" + PROBLEM_SOLVING_EVAL + "' , '" + RemoveSp(PROBLEM_SOLVING_JUST) + "' , '" + MEASURING_WORK_EVAL + "' , '" + RemoveSp(MEASURING_WORK_JUST) + "' , '" + MOTIVATING_EVAL + "' , '" + RemoveSp(MOTIVATING_JUST) + "' , '" + PLANNING_EVAL + "' , '" + RemoveSp(PLANNING_JUST) + "' , '" + TEAM_CONT_EVAL + "' , '" + RemoveSp(TEAM_CONT_JUST) + "' , '" + WORK_HABITS_EVAL + "' , '" + RemoveSp(WORK_HABITS_JUST) + "' , '" + RemoveSp(G1NY_NAME) + "' , '" + G1NY_WEIGHT + "' , '" + G1NY_ACTUAL + "' , '" + RemoveSp(G1NY_JUST) + "' , '" + RemoveSp(G2NY_NAME) + "' , '" + G2NY_WEIGHT + "' , '" + G2NY_ACTUAL + "' , '" + G2NY_JUST + "' , '" + RemoveSp(G3NY_NAME) + "' , '" + G3NY_WEIGHT + "' , '" + G3NY_ACTUAL + "' , '" + RemoveSp(G3NY_JUST) + "' , '" + RemoveSp(G4NY_NAME) + "' , '" + G4NY_WEIGHT + "' , '" + G4NY_ACTUAL + "' , '" + RemoveSp(G4NY_JUST) + "' , '" + RemoveSp(G5NY_NAME) + "' , '" + G5NY_WEIGHT + "' , '" + G5NY_ACTUAL + "' , '" + G5NY_JUST + "' , '" + RemoveSp(G6NY_NAME) + "' , '" + G6NY_WEIGHT + "' , '" + G6NY_ACTUAL + "' , '" + RemoveSp(G6NY_JUST) + "' , '" + FINAL_RESULT + "' ) ";
            oracleacess.ExecuteNonQuery(SQL);
            val = 1;
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Save_Form_D_DTL >> save form (D) Data ");
            val = 0;
        }

        return val;
    }


    public int Update_Form_D_DTL(string HDR_ID, string EMP_NAME, string G1_NAME, string G1_WEIGHT, string G1_ACTUAL, string G1_JUST, string G2_NAME, string G2_WEIGHT, string G2_ACTUAL, string G2_JUST, string G3_NAME, string G3_WEIGHT, string G3_ACTUAL, string G3_JUST, string G4_NAME, string G4_WEIGHT, string G4_ACTUAL, string G4_JUST, string G5_NAME, string G5_WEIGHT, string G5_ACTUAL, string G5_JUST, string G6_NAME, string G6_WEIGHT, string G6_ACTUAL, string G6_JUST, string HORSE_POWER_EVAL, string HORSE_POWER_JUST, string PROBLEM_SOLVING_EVAL, string PROBLEM_SOLVING_JUST, string MEASURING_WORK_EVAL, string MEASURING_WORK_JUST, string MOTIVATING_EVAL, string MOTIVATING_JUST, string PLANNING_EVAL, string PLANNING_JUST, string TEAM_CONT_EVAL, string TEAM_CONT_JUST, string WORK_HABITS_EVAL, string WORK_HABITS_JUST, string G1NY_NAME, string G1NY_WEIGHT, string G1NY_ACTUAL, string G1NY_JUST, string G2NY_NAME, string G2NY_WEIGHT, string G2NY_ACTUAL, string G2NY_JUST, string G3NY_NAME, string G3NY_WEIGHT, string G3NY_ACTUAL, string G3NY_JUST, string G4NY_NAME, string G4NY_WEIGHT, string G4NY_ACTUAL, string G4NY_JUST, string G5NY_NAME, string G5NY_WEIGHT, string G5NY_ACTUAL, string G5NY_JUST, string G6NY_NAME, string G6NY_WEIGHT, string G6NY_ACTUAL, string G6NY_JUST, string FINAL_RESULT)
    {
        int val = 0;
        try
        {
            SQL = " UPDATE as_form4_dtl Set   G1_NAME =  '" + RemoveSp(G1_NAME) + "' , G1_WEIGHT =  '" + G1_WEIGHT + "' , G1_ACTUAL =  '" + G1_ACTUAL + "' , G1_JUST = '" + RemoveSp(G1_JUST) + "' , G2_NAME  =  '" + RemoveSp(G2_NAME) + "' , G2_WEIGHT =  '" + G2_WEIGHT + "' , G2_ACTUAL =  '" + G2_ACTUAL + "' , G2_JUST =  '" + RemoveSp(G2_JUST) + "' ,  G3_NAME =  '" + RemoveSp(G3_NAME) + "' , G3_WEIGHT =  '" + G3_WEIGHT + "' , G3_ACTUAL =  '" + G3_ACTUAL + "' , G3_JUST =  '" + RemoveSp(G3_JUST) + "' , G4_NAME = '" + RemoveSp(G4_NAME) + "' ,  G4_WEIGHT =  '" + G4_WEIGHT + "' , G4_ACTUAL =  '" + G4_ACTUAL + "' , G4_JUST =  '" + RemoveSp(G4_JUST) + "' ,  G5_NAME =  '" + RemoveSp(G5_NAME) + "' , G5_WEIGHT =  '" + G5_WEIGHT + "' , G5_ACTUAL =  '" + G5_ACTUAL + "' ,  G5_JUST =  '" + RemoveSp(G5_JUST) + "' ,  G6_NAME =  '" + RemoveSp(G6_NAME) + "' , G6_WEIGHT =  '" + G6_WEIGHT + "' , G6_ACTUAL =  '" + G6_ACTUAL + "' , G6_JUST =  '" + RemoveSp(G6_JUST) + "' , HORSE_POWER_EVAL  =  '" + HORSE_POWER_EVAL + "' , HORSE_POWER_JUST =  '" + RemoveSp(HORSE_POWER_JUST) + "' , PROBLEM_SOLVING_EVAL =  '" + PROBLEM_SOLVING_EVAL + "' , PROBLEM_SOLVING_JUST =  '" + RemoveSp(PROBLEM_SOLVING_JUST) + "' , MEASURING_WORK_EVAL  =  '" + MEASURING_WORK_EVAL + "' , MEASURING_WORK_JUST =  '" + RemoveSp(MEASURING_WORK_JUST) + "' , MOTIVATING_EVAL =  '" + MOTIVATING_EVAL + "' ,  MOTIVATING_JUST =  '" + RemoveSp(MOTIVATING_JUST) + "' ,  PLANNING_EVAL =  '" + PLANNING_EVAL + "' , PLANNING_JUST =  '" + RemoveSp(PLANNING_JUST) + "' , TEAM_CONT_EVAL =  '" + TEAM_CONT_EVAL + "' , TEAM_CONT_JUST =  '" + RemoveSp(TEAM_CONT_JUST) + "' , WORK_HABITS_EVAL =  '" + WORK_HABITS_EVAL + "' , WORK_HABITS_JUST  =  '" + RemoveSp(WORK_HABITS_JUST) + "' , G1NY_NAME =  '" + RemoveSp(G1NY_NAME) + "' , G1NY_WEIGHT =  '" + G1NY_WEIGHT + "' , G1NY_ACTUAL =  '" + G1NY_ACTUAL + "' , G1NY_JUST =  '" + RemoveSp(G1NY_JUST) + "' , G2NY_NAME =  '" + RemoveSp(G2NY_NAME) + "' , G2NY_WEIGHT =  '" + G2NY_WEIGHT + "' ,  G2NY_ACTUAL =  '" + G2NY_ACTUAL + "' ,  G2NY_JUST =  '" + RemoveSp(G2NY_JUST) + "' , G3NY_NAME =  '" + RemoveSp(G3NY_NAME) + "' , G3NY_WEIGHT =  '" + G3NY_WEIGHT + "' , G3NY_ACTUAL =  '" + G3NY_ACTUAL + "' , G3NY_JUST  =  '" + RemoveSp(G3NY_JUST) + "' , G4NY_NAME =  '" + RemoveSp(G4NY_NAME) + "' , G4NY_WEIGHT =  '" + G4NY_WEIGHT + "' , G4NY_ACTUAL =  '" + G4NY_ACTUAL + "' , G4NY_JUST =  '" + RemoveSp(G4NY_JUST) + "' , G5NY_NAME =  '" + RemoveSp(G5NY_NAME) + "' , G5NY_WEIGHT =  '" + G5NY_WEIGHT + "' , G5NY_ACTUAL =  '" + G5NY_ACTUAL + "' , G5NY_JUST =  '" + RemoveSp(G5NY_JUST) + "' , G6NY_NAME  =  '" + RemoveSp(G6NY_NAME) + "' , G6NY_WEIGHT =  '" + G6NY_WEIGHT + "' , G6NY_ACTUAL =  '" + G6NY_ACTUAL + "' , G6NY_JUST =  '" + RemoveSp(G6NY_JUST) + "' , FINAL_RESULT =  '" + FINAL_RESULT + "' Where HDR_ID  = '" + HDR_ID + "'   ";
            oracleacess.ExecuteNonQuery(SQL);
            val = 1;
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Save_Form_D_DTL >> save form (D) Data ");
            val = 0;
        }

        return val;
    }


    public DataTable Get_Form_D_Data(string Emp_NO, string YEAR)
    {
        try
        {
            SQL = " select A.ID , A.EMP_NO , A.EMP_NAME ,A.FORM_ID , B.G1_NAME , B.G1_WEIGHT , B.G1_ACTUAL , B.G1_JUST , B.G2_NAME , B.G2_WEIGHT , B.G2_ACTUAL , B.G2_JUST , B.G3_NAME , B.G3_WEIGHT , B.G3_ACTUAL , B.G3_JUST , B.G4_NAME , B.G4_WEIGHT , B.G4_ACTUAL , B.G4_JUST , B.G5_NAME , B.G5_WEIGHT , B.G5_ACTUAL , B.G5_JUST , B.G6_NAME , B.G6_WEIGHT , B.G6_ACTUAL , B.G6_JUST , B.HORSE_POWER_EVAL , B.HORSE_POWER_JUST , B.PROBLEM_SOLVING_EVAL , B.PROBLEM_SOLVING_JUST , B.MEASURING_WORK_EVAL , B.MEASURING_WORK_JUST , B.MOTIVATING_EVAL , B.MOTIVATING_JUST , B.PLANNING_EVAL , B.PLANNING_JUST , B.TEAM_CONT_EVAL , B.TEAM_CONT_JUST , B.WORK_HABITS_EVAL , B.WORK_HABITS_JUST , B.G1NY_NAME , B.G1NY_WEIGHT , B.G1NY_ACTUAL , B.G1NY_JUST , B.G2NY_NAME , B.G2NY_WEIGHT, B.G2NY_ACTUAL , B.G2NY_JUST , B.G3NY_NAME , B.G3NY_WEIGHT , B.G3NY_ACTUAL , B.G3NY_JUST , B.G4NY_NAME , B.G4NY_WEIGHT , B.G4NY_ACTUAL , B.G4NY_JUST , B.G5NY_NAME , B.G5NY_WEIGHT , B.G5NY_ACTUAL , B.G5NY_JUST , B.G6NY_NAME , B.G6NY_WEIGHT , B.G6NY_ACTUAL , B.G6NY_JUST , B.FINAL_RESULT , A.USER_NAME from as_entr_hdr A , as_form4_dtl B  Where A.ID = B.HDR_ID And A.EMP_NO = '" + Emp_NO + "'  AND A.YEAR = '" + YEAR + "'  ";

            dt = oracleacess.ExecuteDataTable(SQL);

        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Get_Form_D_Data >> Get Form D Data as DataTable");
        }
        return dt;
    }


    public DataTable Print_Form_D(string Emp_NO, string YEAR)
    {
        try
        {
            SQL = " SELECT A.ID , A.EMP_NO , A.EMP_NAME , GET_MANG_NAME(A.EMP_NO) AS DIRECT_MANG_NAME ,A.FORM_ID , A.YEAR , B.G1_NAME , B.G1_WEIGHT , B.G1_ACTUAL , B.G1_JUST , B.G2_NAME , B.G2_WEIGHT , B.G2_ACTUAL , B.G2_JUST , B.G3_NAME , B.G3_WEIGHT , B.G3_ACTUAL , B.G3_JUST , B.G4_NAME , B.G4_WEIGHT , B.G4_ACTUAL , B.G4_JUST , B.G5_NAME , B.G5_WEIGHT , B.G5_ACTUAL , B.G5_JUST , B.G6_NAME , B.G6_WEIGHT , B.G6_ACTUAL , B.G6_JUST , B.HORSE_POWER_EVAL , B.HORSE_POWER_JUST , B.PROBLEM_SOLVING_EVAL , B.PROBLEM_SOLVING_JUST , B.MEASURING_WORK_EVAL , B.MEASURING_WORK_JUST , B.MOTIVATING_EVAL , B.MOTIVATING_JUST , B.PLANNING_EVAL , B.PLANNING_JUST , B.TEAM_CONT_EVAL , B.TEAM_CONT_JUST , B.WORK_HABITS_EVAL , B.WORK_HABITS_JUST , B.G1NY_NAME , B.G1NY_WEIGHT , B.G1NY_ACTUAL , B.G1NY_JUST , B.G2NY_NAME , B.G2NY_WEIGHT, B.G2NY_ACTUAL , B.G2NY_JUST , B.G3NY_NAME , B.G3NY_WEIGHT , B.G3NY_ACTUAL , B.G3NY_JUST , B.G4NY_NAME , B.G4NY_WEIGHT , B.G4NY_ACTUAL , B.G4NY_JUST , B.G5NY_NAME , B.G5NY_WEIGHT , B.G5NY_ACTUAL , B.G5NY_JUST , B.G6NY_NAME , B.G6NY_WEIGHT , B.G6NY_ACTUAL , B.G6NY_JUST , B.FINAL_RESULT , A.USER_NAME , S.SECTION_NAME , J.JOB_NAME from as_entr_hdr A , as_form4_dtl B , pay_egic.app_emp E , pay_egic.app_section S , pay_egic.app_job J Where A.ID = B.HDR_ID AND A.EMP_NO = E.EMP_NO AND E.SECTION_NO = S.SECTION_NO AND E.JOB_NO = J.JOB_NO And A.EMP_NO = '" + Emp_NO + "'  AND A.YEAR = '" + YEAR + "'  ";

            dt = oracleacess.ExecuteDataTable(SQL);

        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Print_Form_D >> Get Form D Data as DataTable");
        }
        return dt;
    }

    #endregion
    //****************************************( Approve main Form)**********************************************************************
    #region " Approve main form "


    public DataTable Bind_APPROVE_EMP_GRID(string emp_no, string syear)
    {
        try
        {
            SQL = "  SELECT  EMP_NO,EMP_NAME  , GET_EMP_title(job_no) as JOB_NAME , get_emp_form(emp_no,'" + syear + "') as form_ID , get_emp_app_status(emp_no, '" + syear + "', get_emp_form(emp_no,'" + syear + "') ) as Status , case when get_emp_app_status(emp_no, '" + syear + "' , get_emp_form(emp_no,'" + syear + "')) = 0 then 'لم يتم التسجيل'  when get_emp_app_status(emp_no, '" + syear + "' , get_emp_form(emp_no ,'" + syear + "')) = 1 then 'تم التسجيل' when get_emp_app_status(emp_no, '" + syear + "' , get_emp_form(emp_no , '" + syear + "')) = 2 then 'تمت الموافقة'  end  as  fStatus , GET_EMP_RESULT(EMP_NO,'" + syear + "', get_emp_form(emp_no , '" + syear + "') ) as rslt , level , EG_EMP_TYPE(EMP_NO) emp_company  FROM pay_egic.APP_EMP  where NVL(NO_APPRAISAL,0) = 0 AND emp_no <> '" + emp_no + "' AND FINSHED = '2'  AND get_emp_app_status(emp_no, '" + syear + "', get_emp_form(emp_no ,'" + syear + "' ) ) = '1'   START WITH EMP_NO = '" + emp_no + "' CONNECT BY PRIOR EMP_NO = D_EMP_NO ";

            dt = oracleacess.ExecuteDataTable(SQL);

        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Bind_APPROVE_EMP_GRID >> Fill employee Data to GridView");
        }
        return dt;
    }


    public DataTable Bind_APPROVE_EMP_FRST_LEVEL_GRID(string emp_no, string syear)
    {
        try
        {

            SQL = "  SELECT   EMP_NO,EMP_NAME  , GET_EMP_title(job_no) as JOB_NAME , get_emp_form(emp_no, '" + syear + "' ) as form_ID , get_emp_app_status(emp_no,'" + syear + "', get_emp_form(emp_no ,'" + syear + "' ) ) as Status , case when get_emp_app_status(emp_no,'" + syear + "', get_emp_form(emp_no , '" + syear + "')) = 0 then 'لم يتم التسجيل'  when get_emp_app_status(emp_no,'" + syear + "', get_emp_form(emp_no , '" + syear + "' )) = 1 then 'تم التسجيل' when get_emp_app_status(emp_no, '" + syear + "' , get_emp_form(emp_no , '" + syear + "' )) = 2 then 'تمت الموافقة'  end  as  fStatus , GET_EMP_RESULT(EMP_NO,'" + syear + "', get_emp_form(emp_no , '" + syear + "' ) ) as rslt FROM pay_egic.APP_EMP  where NVL(NO_APPRAISAL,0) = 0 AND emp_no <> '" + emp_no + "' AND LEVEL = 2 AND FINSHED = '2' AND get_emp_app_status(emp_no, '" + syear + "' , get_emp_form(emp_no , '" + syear + "' ) ) = '1'  START WITH EMP_NO = '" + emp_no + "' CONNECT BY PRIOR EMP_NO = D_EMP_NO ";

            dt = oracleacess.ExecuteDataTable(SQL);

        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Bind_APPROVE_EMP_FRST_LEVEL_GRID >> Fill employee Data to GridView");
        }
        return dt;
    }


    public int Make_Approve_by_Form(string EMP_NO, string form_ID, string Approver, string year)
    {
        int val = 0;
        try
        {
            SQL = " Update as_entr_hdr Set  STATUS = '1' , APPROVE_FLAG = '1' , approve_name = '" + Approver + "' , approve_date = to_date('" + (System.DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss")) + "','MM/DD/YYYY HH24:MI:SS')  WHERE  EMP_NO = '" + EMP_NO + "' AND FORM_ID = '" + form_ID + "'  AND YEAR = '" + year + "' ";
            oracleacess.ExecuteNonQuery(SQL);
            val = 1;
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Make_Approve_by_Form >> Make Approve");
            val = 0;
        }

        return val;
    }


    public DataTable Bind_NOT_APPROVE_EMP_GRID(string emp_no, string syear)
    {
        try
        {
            SQL = "  SELECT  EMP_NO,EMP_NAME  , GET_EMP_title(job_no) as JOB_NAME , get_emp_form(emp_no ,'" + syear + "' ) as form_ID , get_emp_app_status(emp_no, '" + syear + "', get_emp_form(emp_no ,'" + syear + "' ) ) as Status , case when get_emp_app_status(emp_no, '" + syear + "' , get_emp_form(emp_no , '" + syear + "')) = 0 then 'لم يتم التسجيل'  when get_emp_app_status(emp_no, '" + syear + "' , get_emp_form(emp_no , '" + syear + "' )) = 1 then 'تم التسجيل' when get_emp_app_status(emp_no, '" + syear + "' , get_emp_form(emp_no , '" + syear + "')) = 2 then 'تمت الموافقة'  end  as  fStatus , GET_EMP_RESULT(EMP_NO,'" + syear + "', get_emp_form(emp_no , '" + syear + "' ) ) as rslt FROM pay_egic.APP_EMP  where NVL(NO_APPRAISAL,0) = 0 AND emp_no <> '" + emp_no + "' AND FINSHED = '2' AND get_emp_app_status(emp_no, '" + syear + "', get_emp_form(emp_no , '" + syear + "' ) ) = '0'   START WITH EMP_NO = '" + emp_no + "' CONNECT BY PRIOR EMP_NO = D_EMP_NO ";

            dt = oracleacess.ExecuteDataTable(SQL);

        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Bind_NOT_APPROVE_EMP_GRID >>");
        }
        return dt;
    }


    public DataTable Bind_NOT_APPROVE_EMP_FRST_LEVEL_GRID(string emp_no, string syear)
    {
        try
        {

            SQL = "  SELECT   EMP_NO,EMP_NAME  , GET_EMP_title(job_no) as JOB_NAME , get_emp_form(emp_no ,'" + syear + "' ) as form_ID , get_emp_app_status(emp_no,'" + syear + "', get_emp_form(emp_no , '" + syear + "' ) ) as Status , case when get_emp_app_status(emp_no,'" + syear + "', get_emp_form(emp_no , '" + syear + "' )) = 0 then 'لم يتم التسجيل'  when get_emp_app_status(emp_no,'" + syear + "', get_emp_form(emp_no , '" + syear + "')) = 1 then 'تم التسجيل' when get_emp_app_status(emp_no, '" + syear + "' , get_emp_form(emp_no , '" + syear + "' )) = 2 then 'تمت الموافقة'  end  as  fStatus , GET_EMP_RESULT(EMP_NO,'" + syear + "', get_emp_form(emp_no , '" + syear + "' ) ) as rslt FROM pay_egic.APP_EMP  where NVL(NO_APPRAISAL,0) = 0 AND emp_no <> '" + emp_no + "' AND FINSHED = '2' AND LEVEL = 2 AND get_emp_app_status(emp_no, '" + syear + "' , get_emp_form(emp_no , '" + syear + "' ) ) = '0'  START WITH EMP_NO = '" + emp_no + "' CONNECT BY PRIOR EMP_NO = D_EMP_NO ";

            dt = oracleacess.ExecuteDataTable(SQL);

        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Bind_NOT_APPROVE_EMP_FRST_LEVEL_GRID >> ");
        }
        return dt;
    }

    #endregion
    //*******************************************( Static Target )*********************************************************************
    #region " Employee Static Target  "


    public string Emp_Static_Target(string emp_no)
    {
        string ret_val = "";

        try
        {
            SQL = " select nvl(static_target,0) from pay_egic.app_emp where emp_no = '" + emp_no + "' ";
            object Pass = oracleacess.ExecuteScalar(SQL);
            ret_val = Pass.ToString();
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Get_Page_Name >> Employee Static Target");
        }

        return ret_val;
    }

    #endregion

    //*******************************************( Static Competencies )*********************************************************************
    #region " Employee Static Competencies  "


    public string Emp_Static_Comp(string emp_no)
    {
        string ret_val = "";

        try
        {
            SQL = " select nvl(static_comp,0) from pay_egic.app_emp where emp_no = '" + emp_no + "' ";
            object Pass = oracleacess.ExecuteScalar(SQL);
            ret_val = Pass.ToString();
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Get_Page_Name >> Employee Static Competencies");
        }

        return ret_val;
    }

    #endregion
    //********************************************(OutSource)*****************************************************************
    #region " Get OutSource Results "

    public int Get_First_Emp_Level_Count_OS(string emp_no, string dept)
    {
        int ret_val = 0;

        try
        {
            SQL = " SELECT  count(EMP_NO)  FROM pay_egic.APP_EMP  where NVL(NO_APPRAISAL,0) = 0 AND emp_no <> '" + emp_no + "' AND FINSHED = '2' AND LEVEL = 2  AND EG_EMP_TYPE(EMP_NO) = 'OutSource' AND ( substr(section_no,1,4) = '" + dept + "' or '" + dept + "' = 'ALL' ) START WITH EMP_NO = '" + emp_no + "' CONNECT BY PRIOR EMP_NO = D_EMP_NO  ";
            object Pass = oracleacess.ExecuteScalar(SQL);
            ret_val = int.Parse(Pass.ToString());
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Get_First_Level_Count >> To First level Count");
            ret_val = 0;
        }

        return ret_val;
    }


    public int Get_All_Emp_Level_Count_OS(string emp_no, string dept)
    {
        int ret_val = 0;

        try
        {
            SQL = " SELECT  count(EMP_NO)  FROM pay_egic.APP_EMP  where NVL(NO_APPRAISAL,0) = 0 AND emp_no <> '" + emp_no + "' AND FINSHED = '2' AND EG_EMP_TYPE(EMP_NO) = 'OutSource' AND ( substr(section_no,1,4) = '" + dept + "' or '" + dept + "' = 'ALL' ) START WITH EMP_NO = '" + emp_no + "' CONNECT BY PRIOR EMP_NO = D_EMP_NO  ";
            object Pass = oracleacess.ExecuteScalar(SQL);
            ret_val = int.Parse(Pass.ToString());
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Get_First_Level_Count >> To First level Count");
            ret_val = 0;
        }

        return ret_val;
    }


    public double Get_Bad_Results_OS(string user_name, string level, string manger_code)
    {
        double ret_val = 0;
        try
        {
            string o_year = Get_Open_Year().ToString();
            if (level == "2")
            {
                SQL = " Select count(0) from  as_entr_hdr a where year = '" + o_year + "' AND EG_EMP_TYPE(A.EMP_NO) = 'OutSource' AND get_emp_result(a.emp_no,'" + o_year + "',a.form_id) < 60 AND pay_egic.get_emp_level('" + manger_code + "' , a.emp_no) = 2  ";
                //AND (a.user_name = '" + user_name + "' OR a.approve_name = '" + user_name + "' ) 
            }
            else
            {
                SQL = " Select count(0) from  as_entr_hdr a where year = '" + o_year + "' AND EG_EMP_TYPE(A.EMP_NO) = 'OutSource' AND get_emp_result(a.emp_no,'" + o_year + "',a.form_id) < 60 AND pay_egic.get_emp_level('" + manger_code + "' , a.emp_no) >= 2   ";
                //AND  ( a.user_name = '" + user_name + "' OR a.approve_name = '" + user_name + "') AND
            }
            object Pass = oracleacess.ExecuteScalar(SQL);
            if (!double.TryParse(Pass.ToString(), out ret_val)) ret_val = 0;
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Get_Bad_Results >> To get bad results ");
            ret_val = 0;
        }

        return ret_val;
    }


    public double Get_Midum_Results_OS(string user_name, string level, string manger_code)
    {
        double ret_val = 0;

        try
        {
            string o_year = Get_Open_Year().ToString();
            if (level == "2")
            {
                SQL = " select count(0) from  as_entr_hdr a where year = '" + o_year + "' AND EG_EMP_TYPE(A.EMP_NO) = 'OutSource' AND get_emp_result(a.emp_no,'" + o_year + "',a.form_id) between 60 and 70.499  and pay_egic.get_emp_level('" + manger_code + "',a.emp_no) = 2   ";
                //AND ( a.user_name = '" + user_name + "' or a.approve_name = '" + user_name + "' )
            }
            else
            {
                SQL = " select count(0) from  as_entr_hdr a where year = '" + o_year + "' AND EG_EMP_TYPE(A.EMP_NO) = 'OutSource' AND get_emp_result(a.emp_no,'" + o_year + "',a.form_id) between 60 and 70.499  and pay_egic.get_emp_level('" + manger_code + "',a.emp_no) >= 2   ";
                //AND ( a.user_name = '" + user_name + "' OR a.approve_name = '" + user_name + "' )
            }
            object Pass = oracleacess.ExecuteScalar(SQL);
            if (!double.TryParse(Pass.ToString(), out ret_val)) ret_val = 0;
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Get_Midum_Results >> To get miduam results ");
            ret_val = 0;
        }

        return ret_val;
    }


    public double Get_Good_Results_OS(string user_name, string level, string manger_code)
    {
        double ret_val = 0;

        try
        {
            string o_year = Get_Open_Year().ToString();
            if (level == "2")
            {
                SQL = " select count(0) from  as_entr_hdr a where year = '" + o_year + "' AND EG_EMP_TYPE(A.EMP_NO) = 'OutSource' AND get_emp_result(a.emp_no,'" + o_year + "',a.form_id) between 70.5 and 85.499 AND  pay_egic.get_emp_level('" + manger_code + "',a.emp_no) = 2    ";
                //( a.user_name = '" + user_name + "' or a.approve_name = '" + user_name + "' ) and
            }
            else
            {
                SQL = " select count(0) from  as_entr_hdr a where year = '" + o_year + "' AND EG_EMP_TYPE(A.EMP_NO) = 'OutSource' AND get_emp_result(a.emp_no,'" + o_year + "',a.form_id) between 70.5 and 85.499 AND  pay_egic.get_emp_level('" + manger_code + "',a.emp_no) >= 2     ";
                //( a.user_name = '" + user_name + "' or a.approve_name = '" + user_name + "' ) and
            }
            object Pass = oracleacess.ExecuteScalar(SQL);
            if (!double.TryParse(Pass.ToString(), out ret_val)) ret_val = 0;
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Get_Good_Results >> To get good results ");
            ret_val = 0;
        }

        return ret_val;
    }


    public double Get_Very_Good_Results_OS(string user_name, string level, string manger_code)
    {
        double ret_val = 0;

        try
        {
            string o_year = Get_Open_Year().ToString();
            if (level == "2")
            {
                SQL = " select count(0) from  as_entr_hdr a where year = '" + o_year + "' AND EG_EMP_TYPE(A.EMP_NO) = 'OutSource' AND get_emp_result(a.emp_no,'" + o_year + "',a.form_id) between 85.5 and 94.999 AND  pay_egic.get_emp_level('" + manger_code + "',a.emp_no) = 2    ";
                //( a.user_name = '" + user_name + "' or a.approve_name = '" + user_name + "' ) and
            }
            else
            {
                SQL = " select count(0) from  as_entr_hdr a where year = '" + o_year + "' AND EG_EMP_TYPE(A.EMP_NO) = 'OutSource' AND get_emp_result(a.emp_no,'" + o_year + "',a.form_id) between 85.5 and 94.999 AND  pay_egic.get_emp_level('" + manger_code + "',a.emp_no) >= 2   ";
                //( a.user_name = '" + user_name + "' or a.approve_name = '" + user_name + "' ) and
            }
            object Pass = oracleacess.ExecuteScalar(SQL);
            if (!double.TryParse(Pass.ToString(), out ret_val)) ret_val = 0;
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Get_Very_Good_Results >> To get very good results ");
            ret_val = 0;
        }

        return ret_val;
    }


    public double Get_Exellent_Results_OS(string user_name, string level, string manger_code)
    {
        double ret_val = 0;

        try
        {
            string o_year = Get_Open_Year().ToString();
            if (level == "2")
            {
                SQL = " select count(0) from  as_entr_hdr a where year = '" + o_year + "' AND EG_EMP_TYPE(A.EMP_NO) = 'OutSource' AND get_emp_result(a.emp_no,'" + o_year + "',a.form_id) >= 95 AND  pay_egic.get_emp_level('" + manger_code + "', a.emp_no) = 2     ";
                //( a.user_name = '" + user_name + "' or a.approve_name = '" + user_name + "')  and
            }
            else
            {
                SQL = " select count(0) from  as_entr_hdr a where year = '" + o_year + "' AND EG_EMP_TYPE(A.EMP_NO) = 'OutSource' AND get_emp_result(a.emp_no,'" + o_year + "',a.form_id) >= 95 AND  pay_egic.get_emp_level('" + manger_code + "', a.emp_no) >= 2     ";
                //( a.user_name = '" + user_name + "' or a.approve_name = '" + user_name + "' ) and
            }
            object Pass = oracleacess.ExecuteScalar(SQL);
            if (!double.TryParse(Pass.ToString(), out ret_val)) ret_val = 0;
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Get_Exellent_Results >> To get exellentresults ");
            ret_val = 0;
        }

        return ret_val;
    }

    #endregion
    //*********************************************(Survey)*****************************************************************
    #region " Managers Survey "


    public int Check_HIPO_Menu_User(string emp_no)
    {
        int ret_val = 0;

        try
        {
            SQL = " select count(EMP_NO)  from hipo_mang where EMP_NO = '" + emp_no + "' ";
            object Pass = oracleacess.ExecuteScalar(SQL);
            ret_val = int.Parse(Pass.ToString());
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Check_HIPO_Menu_User >> To check HIPO Menu ");
            ret_val = 0;
        }

        return ret_val;
    }


    public DataTable Bind_ENTER_SURVEY_EMP_GRID(string emp_no, string year)
    {
        string ret_val = "";
        try
        {
            SQL = "  SELECT AS_ADMIN_USER(" + emp_no + ") FROM DUAL";
            object admn = oracleacess.ExecuteScalar(SQL);
            ret_val = admn.ToString();
            if (ret_val == "N")
            {
                //SQL = "  SELECT  EMP_NO,EMP_NAME  , GET_EMP_title(job_no) as JOB_NAME  , EG_EMP_TYPE(EMP_NO) emp_company , level FROM pay_egic.APP_EMP  where  emp_no <> '" + emp_no + "' AND FINSHED = '2'  START WITH EMP_NO = '" + emp_no + "' CONNECT BY PRIOR EMP_NO = D_EMP_NO ";
                SQL = " SELECT  EMP_NO,EMP_NAME  , GET_EMP_title(job_no) as JOB_NAME  , EG_EMP_TYPE(EMP_NO) emp_company , level FROM pay_egic.APP_EMP  where emp_no not in(select emp_code from as_survey where rec_date <= as_last_surv_date) and emp_no <> '" + emp_no + "' AND FINSHED = '2'  And nvl(no_hipo,0) = 0  START WITH EMP_NO = '" + emp_no + "' CONNECT BY PRIOR EMP_NO = D_EMP_NO ";
            }
            else
            {
                //SQL = "  SELECT  EMP_NO,EMP_NAME  , GET_EMP_title(job_no) as JOB_NAME , EG_EMP_TYPE(EMP_NO) emp_company , level FROM pay_egic.APP_EMP  where  (emp_no <> '" + emp_no + "' OR as_admin_user(" + emp_no + ") = 'Y') AND FINSHED = '2' ";
                SQL = " SELECT  EMP_NO,EMP_NAME  , GET_EMP_title(job_no) as JOB_NAME , EG_EMP_TYPE(EMP_NO) emp_company , level FROM pay_egic.APP_EMP  where  emp_no not in(select emp_code from as_survey where rec_date <= as_last_surv_date) and (emp_no <> '" + emp_no + "' OR as_admin_user(" + emp_no + ") = 'Y') AND FINSHED = '2'  And nvl(no_hipo,0) = 0 START WITH EMP_NO = '" + emp_no + "' CONNECT BY PRIOR EMP_NO = D_EMP_NO  ";
            }


            dt = oracleacess.ExecuteDataTable(SQL);

        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Bind_ENTER_SURVEY_EMP_GRID >> Fill employee Data to GridView");
        }
        return dt;
    }


    public DataTable Bind_ENTER_EMP_SURVEY_FRST_LEVEL_GRID(string emp_no, string year)
    {
        try
        {

            //SQL = "  SELECT  EMP_NO,EMP_NAME  , GET_EMP_title(job_no) as JOB_NAME ,  EG_EMP_TYPE(EMP_NO) emp_company , level FROM pay_egic.APP_EMP where  emp_no <> '" + emp_no + "' AND LEVEL = 2 AND FINSHED = '2'  START WITH EMP_NO = '" + emp_no + "' CONNECT BY PRIOR EMP_NO = D_EMP_NO ";
            SQL = "  SELECT  EMP_NO,EMP_NAME  , GET_EMP_title(job_no) as JOB_NAME ,  EG_EMP_TYPE(EMP_NO) emp_company , level FROM pay_egic.APP_EMP where  emp_no not in(select emp_code from as_survey where rec_date <= as_last_surv_date) and emp_no <> '" + emp_no + "' AND LEVEL = 2 AND FINSHED = '2'  And nvl(no_hipo,0) = 0  START WITH EMP_NO = '" + emp_no + "' CONNECT BY PRIOR EMP_NO = D_EMP_NO ";

            dt = oracleacess.ExecuteDataTable(SQL);

        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Bind_ENTER_EMP_SURVEY_FRST_LEVEL_GRID >> Fill employee Data to GridView");
        }
        return dt;
    }


    public int Save_Survey(string year, string EMP_CODE, string EMP_NAME, string job_name, string q1, string q2, string q3, string q4, string q5, string q6, string q7, string q8, string q9, string q10, string result, string manager_id)
    {
        int val = 0;
        try
        {
            SQL = " INSERT INTO AS_Survey ( year , emp_code , EMP_NAME , JOB_NAME , REC_DATE , Q1 , Q2 , Q3 , Q4 , Q5 , Q6 , Q7 , Q8 , Q9 , Q10  , RESULT , MANAGER_ID ) ";
            SQL += " VALUES ( '" + year + "' ,'" + EMP_CODE + "' ,'" + EMP_NAME + "' ,'" + job_name + "' ,sysdate,'" + q1 + "' ,'" + q2 + "' ,'" + q3 + "' ,'" + q4 + "' ,'" + q5 + "' ,'" + q6 + "' ,'" + q7 + "' , '" + q8 + "' , '" + q9 + "' , '" + q10 + "'  ,'" + result + "' ,'" + manager_id + "'  ) ";
            oracleacess.ExecuteNonQuery(SQL);
            val = 1;
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Save_Survey >> save survey Data ");
            val = 0;
        }

        return val;
    }


    public DataTable Get_Survey_Data(string Emp_NO, string Year)
    {
        try
        {
            SQL = "  select year , emp_code , emp_name , job_name , rec_date , q1 , q2 , q3 , q4 ,q5 , q6 ,q7 , q8 , q9 , q10  , result , manager_id from as_survey where year = '" + Year + "' and emp_code = '" + Emp_NO + "' ";

            dt = oracleacess.ExecuteDataTable(SQL);

        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Get_Survey_Data >> Get survey Data as DataTable");
        }
        return dt;
    }


    public int Delete_Survey_Data(string Emp_NO, string Year)
    {
        int val = 0;
        try
        {
            SQL = " delete from as_survey where  year = '" + Year + "' and emp_code = '" + Emp_NO + "'  ";
            oracleacess.ExecuteNonQuery(SQL);
            val = 1;
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Delete_Survey_Data >> ");
            val = 0;
        }

        return val;
    }


    public int Check_Saved_Survey(string emp_no, string year)
    {
        int ret_val = 0;

        try
        {
            SQL = "  select count(emp_code) as cont from as_survey where emp_code = '" + emp_no + "' and year = '" + year + "' ";
            object Pass = oracleacess.ExecuteScalar(SQL);
            ret_val = int.Parse(Pass.ToString());
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Check_Saved_Survey >> Check_Saved_Survey");
        }

        return ret_val;
    }

    #endregion
    //************************************************( New Assessment 2017 )********************************************************************
    #region "New Appraisal 2017 == Enter Assessment "


    public DataTable Bind_New_ASS_ENTER_EMP_GRID(string emp_no, string year, string version, string company, string dept)
    {
        string ret_val = "";
        try
        {
            SQL = "  SELECT AS_ADMIN_USER(" + emp_no + ") FROM DUAL";
            object admn = oracleacess.ExecuteScalar(SQL);
            ret_val = admn.ToString();
            if (ret_val == "N")
            {
                SQL = "  SELECT ASS_EMP_ACCEPT(emp_no,'" + year + "', '" + version + "' ) EMP_ACCEPT, EMP_NO,EMP_NAME , job_no , GET_EMP_title(job_no) as JOB_NAME  , ASS_get_emp_app_status(emp_no,'" + year + "', '" + version + "' ) as Status , case when ASS_get_emp_app_status(emp_no, '" + year + "', '" + version + "') = 0 then 'لم يتم التسجيل'  when ASS_get_emp_app_status(emp_no, '" + year + "', '" + version + "') = 1 then 'تم التسجيل' when ASS_get_emp_app_status(emp_no, '" + year + "' , '" + version + "' ) = 2 then 'تمت الموافقة'  end  as  fStatus , EG_EMP_TYPE(EMP_NO) emp_company , level , ASS_GET_EMP_RESULT(EMP_NO,'" + year + "','" + version + "')  as RSLT , ASS_GET_EMP_GRADE(EMP_NO,'" + year + "','" + version + "')  as GRAD FROM pay_egic.APP_EMP  where ( EG_EMP_TYPE(EMP_NO) = '" + company + "' OR '" + company + "' = 'ALL'  ) AND NVL(NO_APPRAISAL,0) = 0 AND emp_no <> '" + emp_no + "' AND FINSHED = '2' AND ( substr(section_no,1,4) = '" + dept + "' or '" + dept + "' = 'ALL' ) START WITH EMP_NO = '" + emp_no + "' CONNECT BY PRIOR EMP_NO = D_EMP_NO  ";
            }
            else
            {
                SQL = "  SELECT ASS_EMP_ACCEPT(emp_no,'" + year + "', '" + version + "' ) EMP_ACCEPT, EMP_NO,EMP_NAME , job_no , GET_EMP_title(job_no) as JOB_NAME  , ASS_get_emp_app_status(emp_no,'" + year + "', '" + version + "' ) as Status , case when ASS_get_emp_app_status(emp_no, '" + year + "', '" + version + "') = 0 then 'لم يتم التسجيل'  when ASS_get_emp_app_status(emp_no, '" + year + "', '" + version + "') = 1 then 'تم التسجيل' when ASS_get_emp_app_status(emp_no, '" + year + "' , '" + version + "' ) = 2 then 'تمت الموافقة'  end  as  fStatus , EG_EMP_TYPE(EMP_NO) emp_company , level , ASS_GET_EMP_RESULT(EMP_NO,'" + year + "','" + version + "')  as RSLT , ASS_GET_EMP_GRADE(EMP_NO,'" + year + "','" + version + "')  as GRAD FROM pay_egic.APP_EMP  where  ( EG_EMP_TYPE(EMP_NO) = '" + company + "' OR '" + company + "' = 'ALL'  ) AND NVL(NO_APPRAISAL,0) = 0 AND ( emp_no <> '" + emp_no + "' OR as_admin_user(" + emp_no + ") = 'Y' ) AND FINSHED = '2' AND ( substr(section_no,1,4) = '" + dept + "' or '" + dept + "' = 'ALL' ) START WITH EMP_NO = '" + emp_no + "' CONNECT BY PRIOR EMP_NO = D_EMP_NO ";
            }


            dt = oracleacess.ExecuteDataTable(SQL);

        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Bind_New_ASS_ENTER_EMP_GRID >> Fill employee Data to GridView");
        }
        return dt;
    }
    //***************

    public DataTable Bind_New_ASS_ENTER_EMP_FRST_LEVEL_GRID(string emp_no, string year, string version, string company, string dept)
    {
        try
        {

            SQL = "  SELECT ASS_EMP_ACCEPT(emp_no,'" + year + "', '" + version + "' ) EMP_ACCEPT, EMP_NO,EMP_NAME  , job_no , GET_EMP_title(job_no) as JOB_NAME  , ass_get_emp_app_status(emp_no, '" + year + "' , '" + version + "' ) as Status , case when ass_get_emp_app_status(emp_no, '" + year + "' , '" + version + "' ) = 0 then 'لم يتم التسجيل'  when ass_get_emp_app_status(emp_no, '" + year + "' , '" + version + "' ) = 1 then 'تم التسجيل' when ass_get_emp_app_status(emp_no,'" + year + "', '" + version + "' ) = 2 then 'تمت الموافقة'  end  as  fStatus , level ,  EG_EMP_TYPE(emp_no) as emp_company, ASS_GET_EMP_RESULT(EMP_NO,'" + year + "','" + version + "')  as RSLT , ASS_GET_EMP_GRADE(EMP_NO,'" + year + "','" + version + "')  as GRAD  FROM pay_egic.APP_EMP where  ( EG_EMP_TYPE(EMP_NO) = '" + company + "' OR '" + company + "' = 'ALL'  ) AND NVL(NO_APPRAISAL,0) = 0 AND emp_no <> '" + emp_no + "' AND LEVEL = 2 AND FINSHED = '2' and ( substr(section_no,1,4) = '" + dept + "' or '" + dept + "' = 'ALL' )  START WITH EMP_NO = '" + emp_no + "' CONNECT BY PRIOR EMP_NO = D_EMP_NO ";

            dt = oracleacess.ExecuteDataTable(SQL);

        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Bind_ENTER_EMP_GRID >> Fill employee Data to GridView");
        }
        return dt;
    }
    //***************

    public string Get_Ass_Open_Version()
    {
        string ret_val = "";

        try
        {
            SQL = " select  ver_no from ass_years where status = '1'  ";
            object Pass = oracleacess.ExecuteScalar(SQL);
            ret_val = Pass.ToString();
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Get_new ass Open_Year >> To get page name");
        }

        return ret_val;
    }
    //****************

    public string Get_Employee_Selected_DATA(string Type, string child_emp, string emp_no, string year, string version, string company = "ALL", string dept = "ALL")
    {
        string ret_val = "";
        try
        {
            //Session["ASS_EMP_NAME"] = Emp_Name;
            //Session["ASS_JOB_ID"] = Job_code;
            //Session["ASS_STATUS"] = Status;
            //Session["Emp_Level"] = Level;
            if (Type.Trim() == "Emp_Name")
            {
                SQL = "  SELECT  EMP_NAME   FROM pay_egic.APP_EMP where  ( EG_EMP_TYPE(EMP_NO) = '" + company + "' OR '" + company + "' = 'ALL'  ) AND ( EMP_NO = '" + child_emp + "' ) AND NVL(NO_APPRAISAL,0) = 0 AND emp_no <> '" + emp_no + "' AND LEVEL = 2 AND FINSHED = '2' and ( substr(section_no,1,4) = '" + dept + "' or '" + dept + "' = 'ALL' )  START WITH EMP_NO = '" + emp_no + "' CONNECT BY PRIOR EMP_NO = D_EMP_NO ";
            }
            else if (Type.Trim() == "Job_code")
            {
                SQL = "  SELECT  job_no  FROM pay_egic.APP_EMP where  ( EG_EMP_TYPE(EMP_NO) = '" + company + "' OR '" + company + "' = 'ALL'  ) AND ( EMP_NO = '" + child_emp + "' ) AND NVL(NO_APPRAISAL,0) = 0 AND emp_no <> '" + emp_no + "' AND LEVEL = 2 AND FINSHED = '2' and ( substr(section_no,1,4) = '" + dept + "' or '" + dept + "' = 'ALL' )  START WITH EMP_NO = '" + emp_no + "' CONNECT BY PRIOR EMP_NO = D_EMP_NO ";
            }
            else if (Type.Trim() == "Status")
            {
                SQL = "  SELECT   ass_get_emp_app_status(emp_no, '" + year + "' , '" + version + "' ) as Status   FROM pay_egic.APP_EMP where  ( EG_EMP_TYPE(EMP_NO) = '" + company + "' OR '" + company + "' = 'ALL'  ) AND ( EMP_NO = '" + child_emp + "' ) AND NVL(NO_APPRAISAL,0) = 0 AND emp_no <> '" + emp_no + "' AND LEVEL = 2 AND FINSHED = '2' and ( substr(section_no,1,4) = '" + dept + "' or '" + dept + "' = 'ALL' )  START WITH EMP_NO = '" + emp_no + "' CONNECT BY PRIOR EMP_NO = D_EMP_NO ";
            }
            else if (Type.Trim() == "Level")
            {
                SQL = "  SELECT  level   FROM pay_egic.APP_EMP where  ( EG_EMP_TYPE(EMP_NO) = '" + company + "' OR '" + company + "' = 'ALL'  ) AND ( EMP_NO = '" + child_emp + "' ) AND NVL(NO_APPRAISAL,0) = 0 AND emp_no <> '" + emp_no + "' AND LEVEL = 2 AND FINSHED = '2' and ( substr(section_no,1,4) = '" + dept + "' or '" + dept + "' = 'ALL' )  START WITH EMP_NO = '" + emp_no + "' CONNECT BY PRIOR EMP_NO = D_EMP_NO ";
            }
            object Pass = oracleacess.ExecuteScalar(SQL);
            ret_val = Pass.ToString();
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Get_Employee_Selected_DATA >>");
        }
        return ret_val;
    }
    //*****************

    public string Get_Ass_Open_Year()
    {
        string ret_val = "";

        try
        {
            SQL = " select year_no  from ass_years where status = '1'  ";
            object Pass = oracleacess.ExecuteScalar(SQL);
            ret_val = Pass.ToString();
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Get_Open_Year >> To get page name");
        }

        return ret_val;
    }
    //********************

    public double Get_Ass_Bad_Results(string user_name, string level, string manger_code, string oyear, string oversion, string company, string dept)
    {  //< 60
        double ret_val = 0;
        try
        {
            if (level == "2")
            {
                SQL = " Select count(0) from  ass_emp_header a where year_no = '" + oyear + "' and ver_no = '" + oversion + "' AND ( EG_EMP_TYPE(A.EMP_NO) = '" + company + "' OR '" + company + "' = 'ALL' ) AND result <= 1.5 AND  save_type = 'SAVE' AND pay_egic.get_emp_level('" + manger_code + "' , a.emp_no) = 2  and Save_type = 'SAVE' AND ( substr( EG_EMP_Section(emp_no),1,4) = '" + dept + "' or '" + dept + "' = 'ALL' ) ";

            }
            else
            {
                SQL = " Select count(0) from  ass_emp_header a where year_no = '" + oyear + "' and ver_no = '" + oversion + "' AND ( EG_EMP_TYPE(A.EMP_NO) = '" + company + "' OR '" + company + "' = 'ALL' ) AND result <= 1.5 AND save_type = 'SAVE' AND pay_egic.get_emp_level('" + manger_code + "' , a.emp_no) >= 2 and Save_type = 'SAVE' AND ( substr( EG_EMP_Section(emp_no),1,4) = '" + dept + "' or '" + dept + "' = 'ALL' ) ";
            }
            object Pass = oracleacess.ExecuteScalar(SQL);
            if (!double.TryParse(Pass.ToString(), out ret_val)) ret_val = 0;
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Get_Bad_Results >> To get bad results ");
            ret_val = 0;
        }

        return ret_val;
    }
    //******************

    public double Get_Ass_Midum_Results(string user_name, string level, string manger_code, string oyear, string oversion, string company, string dept)
    {  //between 60 and 70.499
        double ret_val = 0;

        try
        {

            if (level == "2")
            {
                SQL = " Select count(0) from  ass_emp_header a where year_no = '" + oyear + "' and ver_no = '" + oversion + "' AND ( EG_EMP_TYPE(A.EMP_NO) = '" + company + "' OR '" + company + "' = 'ALL' ) AND result between 1.6 and 2.5 AND save_type = 'SAVE' AND pay_egic.get_emp_level('" + manger_code + "' , a.emp_no) = 2 and Save_type = 'SAVE' AND ( substr( EG_EMP_Section(emp_no),1,4) = '" + dept + "' or '" + dept + "' = 'ALL' ) ";
                //AND ( a.user_name = '" + user_name + "' or a.approve_name = '" + user_name + "' )
            }
            else
            {
                SQL = " Select count(0) from  ass_emp_header a where year_no = '" + oyear + "' and ver_no = '" + oversion + "' AND ( EG_EMP_TYPE(A.EMP_NO) = '" + company + "' OR '" + company + "' = 'ALL' ) AND result between 1.6 and 2.5 AND save_type = 'SAVE' AND  pay_egic.get_emp_level('" + manger_code + "' , a.emp_no)  >= 2  and Save_type = 'SAVE' AND ( substr( EG_EMP_Section(emp_no),1,4) = '" + dept + "' or '" + dept + "' = 'ALL' ) ";
                //AND ( a.user_name = '" + user_name + "' OR a.approve_name = '" + user_name + "' )
            }
            object Pass = oracleacess.ExecuteScalar(SQL);
            if (!double.TryParse(Pass.ToString(), out ret_val)) ret_val = 0;
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Get_Midum_Results >> To get miduam results ");
            ret_val = 0;
        }

        return ret_val;
    }
    //******************

    public double Get_Ass_Good_Results(string user_name, string level, string manger_code, string oyear, string oversion, string company, string dept)
    {
        //between 70.5 and 85.499
        double ret_val = 0;

        try
        {

            if (level == "2")
            {
                SQL = " Select count(0) from  ass_emp_header a where year_no = '" + oyear + "' and ver_no = '" + oversion + "' AND ( EG_EMP_TYPE(A.EMP_NO) = '" + company + "' OR '" + company + "' = 'ALL' ) AND result  between 2.6 and 3.5 AND pay_egic.get_emp_level('" + manger_code + "' , a.emp_no) = 2  and Save_type = 'SAVE' AND ( substr( EG_EMP_Section(emp_no),1,4) = '" + dept + "' or '" + dept + "' = 'ALL' ) ";
                //( a.user_name = '" + user_name + "' or a.approve_name = '" + user_name + "' ) and
            }
            else
            {
                SQL = " Select count(0) from  ass_emp_header a where year_no = '" + oyear + "' and ver_no = '" + oversion + "' AND ( EG_EMP_TYPE(A.EMP_NO) = '" + company + "' OR '" + company + "' = 'ALL' ) AND result between 2.6 and 3.5 AND pay_egic.get_emp_level('" + manger_code + "' , a.emp_no) >= 2   and Save_type = 'SAVE' AND ( substr( EG_EMP_Section(emp_no),1,4) = '" + dept + "' or '" + dept + "' = 'ALL' ) ";
                //( a.user_name = '" + user_name + "' or a.approve_name = '" + user_name + "' ) and
            }
            object Pass = oracleacess.ExecuteScalar(SQL);
            if (!double.TryParse(Pass.ToString(), out ret_val)) ret_val = 0;
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Get_Good_Results >> To get good results ");
            ret_val = 0;
        }

        return ret_val;
    }
    //******************

    public double Get_Ass_Very_Good_Results(string user_name, string level, string manger_code, string oyear, string oversion, string company, string dept)
    {  //between 85.5 and 94.999
        double ret_val = 0;

        try
        {

            if (level == "2")
            {
                SQL = " Select count(0) from  ass_emp_header a where year_no = '" + oyear + "' and ver_no = '" + oversion + "' AND ( EG_EMP_TYPE(A.EMP_NO) = '" + company + "' OR '" + company + "' = 'ALL' ) AND result  between 3.6 and 4.5 AND save_type = 'SAVE' AND pay_egic.get_emp_level('" + manger_code + "' , a.emp_no) = 2  and Save_type = 'SAVE' AND ( substr( EG_EMP_Section(emp_no),1,4) = '" + dept + "' or '" + dept + "' = 'ALL' ) ";
                //( a.user_name = '" + user_name + "' or a.approve_name = '" + user_name + "' ) and
            }
            else
            {
                SQL = " Select count(0) from  ass_emp_header a where year_no = '" + oyear + "' and ver_no = '" + oversion + "' AND ( EG_EMP_TYPE(A.EMP_NO) = '" + company + "' OR '" + company + "' = 'ALL' )  AND result between 3.6 and 4.5 AND save_type = 'SAVE' AND pay_egic.get_emp_level('" + manger_code + "' , a.emp_no) >= 2 and Save_type = 'SAVE' AND ( substr( EG_EMP_Section(emp_no),1,4) = '" + dept + "' or '" + dept + "' = 'ALL' )  ";
                //( a.user_name = '" + user_name + "' or a.approve_name = '" + user_name + "' ) and
            }
            object Pass = oracleacess.ExecuteScalar(SQL);
            if (!double.TryParse(Pass.ToString(), out ret_val)) ret_val = 0;
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Get_Very_Good_Results >> To get very good results ");
            ret_val = 0;
        }

        return ret_val;
    }
    //******************

    public double Get_Ass_Exellent_Results(string user_name, string level, string manger_code, string oyear, string oversion, string company, string dept)
    {  // >= 95
        double ret_val = 0;

        try
        {
            if (level == "2")
            {
                SQL = " Select count(0) from  ass_emp_header a where year_no = '" + oyear + "' and ver_no = '" + oversion + "' AND ( EG_EMP_TYPE(A.EMP_NO) = '" + company + "' OR '" + company + "' = 'ALL' ) AND result  >= 4.6 AND save_type = 'SAVE' AND pay_egic.get_emp_level('" + manger_code + "' , a.emp_no) = 2  and Save_type = 'SAVE' AND ( substr( EG_EMP_Section(emp_no),1,4) = '" + dept + "' or '" + dept + "' = 'ALL' )  ";

            }
            else
            {
                SQL = " Select count(0) from  ass_emp_header a where year_no = '" + oyear + "' and ver_no = '" + oversion + "' AND ( EG_EMP_TYPE(A.EMP_NO) = '" + company + "' OR '" + company + "' = 'ALL' ) AND result  >= 4.6 AND save_type = 'SAVE' AND pay_egic.get_emp_level('" + manger_code + "' , a.emp_no)  >= 2  and Save_type = 'SAVE'  AND ( substr( EG_EMP_Section(emp_no),1,4) = '" + dept + "' or '" + dept + "' = 'ALL' )  ";

            }
            object Pass = oracleacess.ExecuteScalar(SQL);
            if (!double.TryParse(Pass.ToString(), out ret_val)) ret_val = 0;
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Get_Exellent_Results >> To get exellentresults ");
            ret_val = 0;
        }

        return ret_val;
    }
    //******************

    #endregion
    //********************************************************************************************************************
    #region " New Appraisal 2017 == Assessment Forms "

    public DataTable Get_Comptences(string job_code, string Year, string Version)
    {
        try
        {
            SQL = "  SELECT distinct  P.CODE PARENT_CODE , P.POS_DESC_AR PARENT_DESC_AR , P.POS_DESC_EN PARENT_DESC_EN  FROM ASS_COMPT D,ASS_COMPT P  WHERE D.PARENT_CODE = P.CODE AND P.CODE IN(SELECT COMP_CODE FROM ASS_JOB_COMPT WHERE JOB_NO =  '" + job_code + "' AND YEAR_NO =  '" + Year + "' AND VER_NO =  '" + Version + "' ) ORDER BY P.CODE  ";

            dt = oracleacess.ExecuteDataTable(SQL);

        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Get_Comptences >> Get Comptences as DataTable");
        }
        return dt;
    }
    //*************

    public DataTable Get_Indicatores(string job_code, string Year, string Version)
    {
        try
        {
            //SQL = "  SELECT P.CODE PARENT_CODE  , D.CODE,D.POS_DESC_AR , D.POS_DESC_EN ,  '0' as COMP_RESULT , D.NEG_DESC_AR , D.NEG_DESC_EN  FROM ASS_COMPT D,ASS_COMPT P  WHERE D.PARENT_CODE = P.CODE AND P.CODE IN(SELECT COMP_CODE FROM ASS_JOB_COMPT WHERE JOB_NO =  '" + job_code + "' AND YEAR_NO =  '" + Year + "' AND VER_NO =  '" + Version + "' ) ORDER BY P.CODE ,D.CODE ";
            SQL = "  SELECT ASS_GET_COMP_FULL_NAME(P.CODE) comp_full_details, P.CODE PARENT_CODE  ,P.POS_DESC_AR DESC_AR, P.POS_DESC_EN DESC_EN, D.CODE ,  D.POS_DESC_AR , D.POS_DESC_EN ,  '0' as COMP_RESULT,'0' as LBL_COMP_RESULT , D.NEG_DESC_AR ||'  '|| D.NEG_DESC_EN as NEG_DESC_AR , D.NEG_DESC_EN,'0' as LBL_COMP_RESULT, '' as DEVELOP_POINTS, '' as TRAIN_PROG    FROM ASS_COMPT D,ASS_COMPT P  WHERE D.PARENT_CODE = P.CODE AND P.CODE IN(SELECT COMP_CODE FROM ASS_JOB_COMPT WHERE JOB_NO =  '" + job_code + "' AND YEAR_NO =  '" + Year + "' AND VER_NO =  '" + Version + "' ) ORDER BY P.CODE ,D.CODE ";

            dt = oracleacess.ExecuteDataTable(SQL);

        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Get_Comptences >> Get Indicatores as DataTable");
        }
        return dt;
    }
    //************

    public DataTable Get_Comptences_Descs(string job_code, string Year, string Version)
    {
        try
        {
            SQL = " select comp_code , AR_DESC , EN_DESC  from ass_compt_desc where comp_code in ( SELECT distinct P.CODE PARENT_CODE  FROM ASS_COMPT D,ASS_COMPT P  WHERE D.PARENT_CODE = P.CODE AND P.CODE IN(SELECT COMP_CODE FROM ASS_JOB_COMPT WHERE JOB_NO = '" + job_code + "' AND YEAR_NO = '" + Year + "' AND VER_NO = '" + Version + "' ) ) ";

            dt = oracleacess.ExecuteDataTable(SQL);

        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Get_Comptences_Descs >> Get Comptences Describtions as DataTable");
        }
        return dt;
    }
    //************

    public AppraisalTypes GetAppaisalType(string year, string version, string jobNo)
    {
        try
        {
            SQL = " select  app_type  from ass_year_options where year_no = '" + year + "' and ver_no = '" + version + "' and job_no = '" + jobNo + "'  ";
            object result = oracleacess.ExecuteScalar(SQL);
            string appType = result.ToString();
            if (appType.Equals("A", StringComparison.CurrentCultureIgnoreCase))
                return AppraisalTypes.ObjectivesAndComptences;
            else if (appType.Equals("C", StringComparison.CurrentCultureIgnoreCase))
                return AppraisalTypes.ComptencesOnly;
            else if (appType.Equals("O", StringComparison.CurrentCultureIgnoreCase))
                return AppraisalTypes.ObjectivesOnly;
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Get_Ass_Job_Appaisal_Type >> To get Get_Ass_Job_Appaisal_Type");
        }
        return AppraisalTypes.None;
    }
    //**********************

    public double Get_Ass_Job_Comp_Weight(string year, string vers, string jobno)
    {
        double ret_val = 0;

        try
        {
            SQL = " select  comp_weight  from ass_year_options where year_no = '" + year + "' and ver_no = '" + vers + "' and job_no = '" + jobno + "'  ";
            object Pass = oracleacess.ExecuteScalar(SQL);
            if (!double.TryParse(Pass.ToString(), out ret_val)) ret_val = 0;
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Get_Ass_Job_Appaisal_Type >> To get Get_Ass_Job_Appaisal_Type");
        }

        return ret_val;
    }
    //***********************

    public double Get_Ass_Job_Obj_Weight(string year, string vers, string jobno)
    {
        double ret_val = 0;

        try
        {
            SQL = " select  obj_weight  from ass_year_options where year_no = '" + year + "' and ver_no = '" + vers + "' and job_no = '" + jobno + "'  ";
            object Pass = oracleacess.ExecuteScalar(SQL);
            if (!double.TryParse(Pass.ToString(), out ret_val)) ret_val = 0;
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Get_Ass_Job_Obj_Weight >> To get Get_Ass_Job_Obj_Weight ");
        }

        return ret_val;
    }
    //**************************

    public int Delete_Empty_Objectives(string emp_no, string year_no, string ver_no)
    {
        int val = 0;
        try
        {
            SQL = " Delete ass_emp_objectives Where ( emp_no = '" + emp_no + "' and year_no = '" + year_no + "' and  ver_no = '" + ver_no + "'  and  obj_desc is null and obj_notes is null )  ";
            val = oracleacess.ExecuteNonQuery(SQL);
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Delete_Empty_Objectives >> ( Delete )  ");
            val = 0;
        }

        return val;
    }
    //*******************

    public int Delete_Empty_Objectives_NY(string emp_no, string year_no, string ver_no)
    {
        int val = 0;
        try
        {
            SQL = " Delete ASS_EMP_NEXT_OBJ Where ( emp_no = '" + emp_no + "' and year_no = '" + year_no + "' and  ver_no = '" + ver_no + "' and  obj_desc is null  )  ";
            oracleacess.ExecuteNonQuery(SQL);
            val = 1;
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Delete_Empty_Objectives_NY >> ( Delete )  ");
            val = 0;
        }

        return val;
    }
    //*******************

    //IsFinalVersion

    public bool IsFinalVersion(string year, string version)
    {
        try
        {
            SQL = "select final from ass_years where year_no = '" + year + "' and ver_no = '" + version + "'";
            var isFinalVersion = oracleacess.ExecuteScalar(SQL).ToString();
            return isFinalVersion == "1";
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "IsFinalVersion >> check is final verion or not");
        }
        return false;
    }


    public DataTable Bind_ASS_EMP_Objectives_GRID(string emp_no, string year, string version)
    {
        try
        {
            Delete_Empty_Objectives(emp_no, year, version);
            SQL = "  select obj_seq as RowNumber , obj_desc , NVL(round(obj_weight,3),'0') as obj_weight , NVL(round(obj_actual,3),'0') as obj_actual, obj_notes, develop_points obj_develop_points, train_prog obj_train_prog from ass_emp_objectives where emp_no = '" + emp_no + "' and year_no = '" + year + "' and ver_no = '" + version + "' Order by obj_seq ";
            dt = oracleacess.ExecuteDataTable(SQL);

        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Bind_ASS_EMP_Objectives_GRID >> Fill emp Objectives_GRID to GridView");
        }
        return dt;
    }


    public string Set_All_Users_Mail_And_Name()
    {
        string message = "";
        try
        {
            string connection = "LDAP://Egic.com.eg";
            DirectorySearcher dssearch = new DirectorySearcher(connection);
            string filter = "(&(objectCategory=person)(objectClass=user)(!userAccountControl:1.2.840.113556.1.4.803:=2))";
            dssearch.Filter = filter;
            var allUsers = dssearch.FindAll();
            int x = 0;
            for (int i = 0; i < allUsers.Count; i++)
            {
                var mails = allUsers[i].Properties["mail"];
                var emp_codes = allUsers[i].Properties["description"];
                if (mails.Count > 0 && emp_codes.Count > 0)
                {
                    var mail = mails[0].ToString();
                    var emp_no = emp_codes[0].ToString();
                    int code;
                    if (int.TryParse(emp_no, out code))
                    {
                        int y = oracleacess.ExecuteNonQuery(string.Format("update pay_egic.app_emp set email = '{0}' where emp_no = {1}", mail, code));
                        x += y;
                    }
                }
            }
            message = x > 0 ? string.Format("SUCCESS ({0})", x) : "SUCCESS but no rows affected";
        }
        catch (Exception ex)
        {
            message = ex.Message;
            LogError(ex.Message, "Set_All_Users_Mail_And_Name >> push data to app_emp table");
        }
        return message;
    }


    public void Update_Emp_Email(string emp_no, string mail)
    {
        if (string.IsNullOrEmpty(emp_no) || string.IsNullOrEmpty(mail))
            return;
        try
        {
            SQL = string.Format("update pay_egic.app_emp set email = '{0}' where emp_no = {1}", mail, emp_no);
            oracleacess.ExecuteNonQuery(SQL);
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Update_Emp_Email ");
        }
    }

    public DataTable Get_All_Users()
    {
        try
        {
            SQL = "select emp_no,emp_name, eemp_name, email from pay_egic.app_emp where Finshed = 2 and (email <> '---' or email is null) order by emp_name";

            dt = oracleacess.ExecuteDataTable(SQL);

        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Get_All_Users_Mail_And_Name >> Fill users ddl");
        }
        return dt;
    }


    public DataTable Mid_Bind_ASS_EMP_Objectives_GRID(string emp_no, string year, string version)
    {
        //ass_create_obj(emp,year,version)
        int empNo = int.Parse(emp_no);
        int yearNo = int.Parse(year);
        int versionNo = int.Parse(version);
        var parameters = new OracleParameter[3];
        parameters[0] = new OracleParameter("emp", empNo);
        parameters[1] = new OracleParameter("yr", yearNo);
        parameters[2] = new OracleParameter("ver", versionNo);
        oracleacess.ExcuteStoredProcdure("ass_create_obj", parameters);
        return Bind_ASS_EMP_Objectives_GRID(emp_no, year, version);
    }
    //***************

    public DataTable Bind_ASS_EMP_NY_Objectives_GRID(string emp_no, string year, string version)
    {

        try
        {
            Delete_Empty_Objectives_NY(emp_no, year, version);
            SQL = "select obj_seq as RowNumber , obj_desc , NVL(obj_weight , '0') as obj_weight,QUART  from ASS_EMP_NEXT_OBJ where emp_no = '" + emp_no + "' and year_no = '" + year + "' and ver_no = '" + version + "'   Order by obj_seq ";

            dt = oracleacess.ExecuteDataTable(SQL);

        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Bind_ASS_EMP_NY_Objectives_GRID >> Fill emp Next Year Objectives_GRID to GridView");
        }
        return dt;
    }
    //***************

    public DataTable Bind_ASS_EMP_NY_Objectives_ByType_GRID(string emp_no, string year, string version, string Type)
    {

        try
        {
            Delete_Empty_Objectives_NY(emp_no, year, version);
            SQL = "  select obj_seq as RowNumber , obj_desc , NVL(obj_weight , '0') as obj_weight, QUART   from ASS_EMP_NEXT_OBJ where emp_no = '" + emp_no + "' and year_no = '" + year + "' and ver_no = '" + version + "' and obj_notes = '" + Type + "'   Order by obj_seq ";

            dt = oracleacess.ExecuteDataTable(SQL);

        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Bind_ASS_EMP_NY_Objectives_ByType_GRID >> Fill emp Next Year Objectives_GRID to GridView");
        }
        return dt;
    }
    //****************

    public DataTable Mid_Bind_ASS_EMP_Indicatores_GRID(string emp_no, string job_no, string year, string version)
    {

        try
        {
            //  D.CODE, D.POS_DESC_AR ||'  '|| D.POS_DESC_EN as POS_DESC_AR
            //SQL = "    SELECT P.CODE PARENT_CODE  , D.CODE,D.POS_DESC_AR , D.POS_DESC_EN , D.NEG_DESC_AR , D.NEG_DESC_EN  , EC.COMP_RESULT FROM ASS_COMPT D,ASS_COMPT P , ASS_EMP_COMPT EC , PAY_EGIC.APP_EMP E WHERE D.PARENT_CODE = P.CODE  AND EC.COMP_CODE = D.CODE AND EC.EMP_NO = E.EMP_NO AND EC.EMP_NO = '" + emp_no + "' AND EC.YEAR_NO = '" + year + "' AND EC.VER_NO = '" + version + "' AND P.CODE IN(SELECT COMP_CODE FROM ASS_JOB_COMPT WHERE JOB_NO =  '" + job_no + "' AND YEAR_NO =  '" + year + "' AND VER_NO =  '" + version + "' ) ORDER BY P.CODE ,D.CODE  ";
            SQL = "    SELECT P.CODE PARENT_CODE  , D.CODE,  D.POS_DESC_AR as POS_DESC_AR, D.POS_DESC_EN as POS_DESC_EN, D.POS_DESC_EN , D.NEG_DESC_AR ||'  '|| D.NEG_DESC_EN as NEG_DESC_AR, D.NEG_DESC_EN  , EC.COMP_RESULT,'0' as LBL_COMP_RESULT, EC.DEVELOP_POINTS, EC.TRAIN_PROG FROM ASS_COMPT D,ASS_COMPT P , ASS_EMP_COMPT EC , PAY_EGIC.APP_EMP E WHERE D.PARENT_CODE = P.CODE  AND EC.COMP_CODE = D.CODE AND EC.EMP_NO = E.EMP_NO AND EC.EMP_NO = '" + emp_no + "' AND EC.YEAR_NO = '" + year + "' AND EC.VER_NO = '" + version + "' AND P.CODE IN(SELECT COMP_CODE FROM ASS_JOB_COMPT WHERE JOB_NO =  '" + job_no + "' AND YEAR_NO =  '" + year + "' AND VER_NO =  '" + version + "' ) ORDER BY P.CODE ,D.CODE  ";

            dt = oracleacess.ExecuteDataTable(SQL);

        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Bind_ASS_EMP_Indicatores_GRID >> Fill emp indicatores to GridView");
        }
        return dt;
    }


    public DataTable Bind_ASS_EMP_Indicatores_GRID(string emp_no, string job_no, string year, string version)
    {

        try
        {
            //  D.CODE, D.POS_DESC_AR ||'  '|| D.POS_DESC_EN as POS_DESC_AR
            //SQL = "    SELECT P.CODE PARENT_CODE  , D.CODE,D.POS_DESC_AR , D.POS_DESC_EN , D.NEG_DESC_AR , D.NEG_DESC_EN  , EC.COMP_RESULT FROM ASS_COMPT D,ASS_COMPT P , ASS_EMP_COMPT EC , PAY_EGIC.APP_EMP E WHERE D.PARENT_CODE = P.CODE  AND EC.COMP_CODE = D.CODE AND EC.EMP_NO = E.EMP_NO AND EC.EMP_NO = '" + emp_no + "' AND EC.YEAR_NO = '" + year + "' AND EC.VER_NO = '" + version + "' AND P.CODE IN(SELECT COMP_CODE FROM ASS_JOB_COMPT WHERE JOB_NO =  '" + job_no + "' AND YEAR_NO =  '" + year + "' AND VER_NO =  '" + version + "' ) ORDER BY P.CODE ,D.CODE  ";
            SQL = "    SELECT P.CODE PARENT_CODE  , D.CODE,  D.POS_DESC_AR , D.POS_DESC_EN , D.NEG_DESC_AR, D.NEG_DESC_EN  , EC.COMP_RESULT,'0' as LBL_COMP_RESULT, EC.DEVELOP_POINTS OBJ_DEVELOP_POINTS, EC.TRAIN_PROG OBJ_TRAIN_PROG FROM ASS_COMPT D,ASS_COMPT P , ASS_EMP_COMPT EC , PAY_EGIC.APP_EMP E WHERE D.PARENT_CODE = P.CODE  AND EC.COMP_CODE = D.CODE AND EC.EMP_NO = E.EMP_NO AND EC.EMP_NO = '" + emp_no + "' AND EC.YEAR_NO = '" + year + "' AND EC.VER_NO = '" + version + "' AND P.CODE IN(SELECT COMP_CODE FROM ASS_JOB_COMPT WHERE JOB_NO =  '" + job_no + "' AND YEAR_NO =  '" + year + "' AND VER_NO =  '" + version + "' ) ORDER BY P.CODE ,D.CODE  ";

            dt = oracleacess.ExecuteDataTable(SQL);

        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Bind_ASS_EMP_Indicatores_GRID >> Fill emp indicatores to GridView");
        }
        return dt;
    }


    public DataTable Get_Emp_Location_Dep(string emp_no)
    {
        try
        {
            SQL = string.Format(@"SELECT EMP_NO, EMP_NAME, S.SECTION_NO , SECTION_NAME, D.VAL LOCATION_ID,
                        D.VAL_ADESC LOCATION_DESC
                        FROM PAY_EGIC.APP_EMP E , PAY_EGIC.APP_SECTION S , PAY_EGIC.PAY_SEG_VALUES D
                        WHERE E.SECTION_NO = S.SECTION_NO
                        AND E.DEPT_STR = D.VAL
                        AND E.EMP_NO = {0}", emp_no);

            dt = oracleacess.ExecuteDataTable(SQL);

        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Bind_FINAL_ASS_EMP_Indicatores_GRID >> Fill emp indicatores to GridView");
        }
        return dt;
    }


    public DataTable Bind_Final_ASS_EMP_Indicatores_GRID(string emp_no, string job_no, string year, string version)
    {
        try
        {
            SQL = " SELECT ASS_GET_COMP_FULL_NAME(P.CODE) comp_full_details, P.CODE PARENT_CODE  ,P.POS_DESC_AR DESC_AR,P.POS_DESC_EN DESC_EN, D.CODE,  D.POS_DESC_AR , D.POS_DESC_EN , D.NEG_DESC_AR, D.NEG_DESC_EN  , round( EC.COMP_RESULT , 1) as COMP_RESULT ,'0' as LBL_COMP_RESULT, EC.DEVELOP_POINTS, EC.TRAIN_PROG FROM ASS_COMPT D,ASS_COMPT P , ASS_EMP_COMPT EC , PAY_EGIC.APP_EMP E WHERE D.PARENT_CODE = P.CODE  AND EC.COMP_CODE = D.CODE AND EC.EMP_NO = E.EMP_NO AND EC.EMP_NO = '" + emp_no + "' AND EC.YEAR_NO = '" + year + "' AND EC.VER_NO = '" + version + "' AND P.CODE IN(SELECT COMP_CODE FROM ASS_JOB_COMPT WHERE JOB_NO =  '" + job_no + "' AND YEAR_NO =  '" + year + "' AND VER_NO =  '" + version + "' ) ORDER BY P.CODE ,D.CODE";

            dt = oracleacess.ExecuteDataTable(SQL);

        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Bind_FINAL_ASS_EMP_Indicatores_GRID >> Fill emp indicatores to GridView");
        }
        return dt;
    }

    //***************

    public DataTable Bind_ASS_EMP_Comptences_GRID(string emp_no, string job_no, string year, string version)
    {

        try
        {
            SQL = " SELECT distinct  P.CODE PARENT_CODE , P.POS_DESC_AR PARENT_DESC_AR , P.POS_DESC_EN PARENT_DESC_EN ,EC.DEVELOP_POINTS, EC.TRAIN_PROG  FROM ASS_COMPT D,ASS_COMPT P  , ASS_EMP_COMPT EC , PAY_EGIC.APP_EMP E WHERE D.PARENT_CODE = P.CODE  AND EC.COMP_CODE = D.CODE AND EC.EMP_NO = E.EMP_NO AND EC.EMP_NO = '" + emp_no + "' AND EC.YEAR_NO = '" + year + "' AND EC.VER_NO = '" + version + "' AND P.CODE IN(SELECT COMP_CODE FROM ASS_JOB_COMPT  WHERE JOB_NO =  '" + job_no + "' AND YEAR_NO =  '" + year + "' AND VER_NO =  '" + version + "' ) ORDER BY P.CODE  ";

            dt = oracleacess.ExecuteDataTable(SQL);

        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Bind_ASS_EMP_Comptences_GRID >> Fill emp comptences to GridView");
        }
        return dt;
    }
    //***************

    public DataTable Bind_ASS_EMP_Comptences_Desc_GRID(string emp_no, string job_no, string year, string version)
    {

        try
        {
            SQL = " SELECT DISTINCT CD.comp_code , AR_DESC , EN_DESC, EC.DEVELOP_POINTS, EC.TRAIN_PROG FROM ASS_COMPT D,ASS_COMPT P  , ASS_EMP_COMPT EC , PAY_EGIC.APP_EMP E , ASS_COMPT_DESC CD WHERE D.PARENT_CODE = P.CODE  AND CD.COMP_CODE = P.CODE AND EC.COMP_CODE = D.CODE AND EC.EMP_NO = E.EMP_NO AND EC.EMP_NO = '" + emp_no + "' AND EC.YEAR_NO = '" + year + "' AND EC.VER_NO = '" + version + "' AND P.CODE IN(SELECT COMP_CODE FROM ASS_JOB_COMPT  WHERE JOB_NO =  '" + job_no + "' AND YEAR_NO =  '" + year + "' AND VER_NO =  '" + version + "' ) ORDER BY COMP_CODE  ";

            dt = oracleacess.ExecuteDataTable(SQL);

        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Bind_ASS_EMP_Comptences_Desc_GRID >> Fill emp comptences describtions to GridView");
        }
        return dt;
    }
    //****************

    public int Check_Saved_Data(string emp_no, string year, string vers, string table_type)
    {
        int ret_val = 0;

        try
        {
            //ass_emp_header 
            SQL = "  select count(1) as totalrec from " + table_type + "  where  emp_no = '" + emp_no + "' and year_no = '" + year + "' and  ver_no = '" + vers + "'   ";
            object Pass = oracleacess.ExecuteScalar(SQL);
            ret_val = int.Parse(Pass.ToString());
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Check_Saved_Data >> To Check Saved Data ");
        }

        return ret_val;
    }

    //****************

    public int Save_ASS_HDR(string emp_no, string year_no, string ver_no, string assessor_name, string result, SaveTypes savetype, string nyo_comment, string COMP_RESULT, string OBJ_RESULT)
    {
        int val = 0;
        try
        {
            int saved = Check_Saved_Data(emp_no, year_no, ver_no, "ass_emp_header");
            if (saved == 0)
            {
                SQL = "INSERT INTO ass_emp_header  ( emp_no, year_no, ver_no, assessor_name, ass_date, result, approved , SAVE_TYPE , NYO_COMMENT , COMP_RESULT , OBJ_RESULT ) ";
                SQL += " VALUES ( '" + emp_no + "', '" + year_no + "', '" + ver_no + "', '" + assessor_name + "',  to_date('" + (System.DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss")) + "','MM/DD/YYYY HH24:MI:SS') , '" + result + "', '0' , '" + savetype + "' , '" + nyo_comment.FixSingleQuotes() + "' , '" + COMP_RESULT + "' , '" + OBJ_RESULT + "'  ) ";
            }
            else
            {
                SQL = " Update ass_emp_header  set assessor_name = '" + assessor_name + "' ,  result = '" + result + "' , SAVE_TYPE = '" + savetype.ToString().ToUpper() + "' , NYO_COMMENT = '" + nyo_comment.FixSingleQuotes() + "' , COMP_RESULT = '" + COMP_RESULT + "' , OBJ_RESULT = '" + OBJ_RESULT + "' ";
                SQL += " Where ( emp_no = '" + emp_no + "' and year_no = '" + year_no + "'  and ver_no = '" + ver_no + "'  ) ";
            }
            oracleacess.ExecuteNonQuery(SQL);
            val = 1;
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Save_ASS_HDR >> ( Header ) Data ");
            val = 0;
        }

        return val;
    }
    //***************

    public int Save_ASS_COMP_DTL(string emp_no, string year_no, string ver_no, string comp_code, string comp_result, string develop_points, string train_prog)
    {
        int val = 0;
        try
        {
            SQL = " INSERT INTO ass_emp_compt  ( emp_no, year_no, ver_no, comp_code, comp_result, develop_points, train_prog   ) ";
            SQL += " VALUES ( '" + emp_no + "', '" + year_no + "', '" + ver_no + "', '" + comp_code + "', '" + comp_result + "','" + develop_points.FixSingleQuotes() + "','" + train_prog.FixSingleQuotes() + "'  ) ";
            oracleacess.ExecuteNonQuery(SQL);
            val = 1;
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Save_ASS_COMP_DTL >> ( Header ) Data ");
            val = 0;
        }

        return val;
    }
    //**************

    public int Save_ASS_OBJ_DTL(string emp_no, string year_no, string ver_no, string obj_seq, string obj_desc, string obj_weight, string obj_actual, string obj_notes, string develop_points, string train_prog)
    {
        int val = 0;
        try
        {
            SQL = "INSERT INTO ass_emp_objectives  ( emp_no, year_no, ver_no, obj_seq, obj_desc, obj_weight, obj_actual, obj_notes, develop_points, train_prog  ) ";
            SQL += " VALUES (  '" + emp_no + "', '" + year_no + "', '" + ver_no + "', '" + obj_seq + "', '" + obj_desc + "', '" + obj_weight + "', '" + obj_actual + "', '" + obj_notes.FixSingleQuotes() + "', '" + develop_points.FixSingleQuotes() + "', '" + train_prog.FixSingleQuotes() + "'  ) ";
            oracleacess.ExecuteNonQuery(SQL);
            val = 1;
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Save_ASS_OBJ_DTL >> ( Detail Objective ) Data ");
            val = 0;
        }

        return val;
    }
    //*******************

    public int Delete_Objectives(string emp_no, string year_no, string ver_no, string table_name, string seq_index)
    {
        int val = 0;
        try
        {
            SQL = " Delete " + table_name + " Where ( emp_no = '" + emp_no + "' and year_no = '" + year_no + "' and  ver_no = '" + ver_no + "' and obj_seq = '" + seq_index + "' )  ";
            oracleacess.ExecuteNonQuery(SQL);
            val = 1;
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Delete_Objectives >> ( Delete ) Data ");
            val = 0;
        }

        return val;
    }
    //*******************

    public int Delete_Objective_ByType(string emp_no, string year_no, string ver_no, string seq_index, string Type)
    {
        int val = 0;
        try
        {
            SQL = " Delete ASS_EMP_NEXT_OBJ Where ( emp_no = '" + emp_no + "' and year_no = '" + year_no + "' and  ver_no = '" + ver_no + "' and obj_seq = '" + seq_index + "' and  obj_notes = '" + Type + "' )  ";
            oracleacess.ExecuteNonQuery(SQL);
            val = 1;
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Delete_Objective_ByType >> ( Delete ) Data ");
            val = 0;
        }

        return val;
    }
    //**************

    public int Delete_Table(string emp_no, string year_no, string ver_no, string table_name)
    {
        int val = 0;
        try
        {
            SQL = " Delete " + table_name + " Where ( emp_no = '" + emp_no + "' and year_no = '" + year_no + "' and  ver_no = '" + ver_no + "' )  ";
            oracleacess.ExecuteNonQuery(SQL);
            val = 1;
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Delete_Table >> ( Delete ) Data ");
            val = 0;
        }

        return val;
    }
    //***************

    public int Save_ASS_COMP_DTTL(string emp_no, string year_no, string ver_no, DataTable COMPDT)
    {
        int saved = Check_Saved_Data(emp_no, year_no, ver_no, "ass_emp_compt");
        int val = 0;
        oracleacess.BeginTransaction();
        try
        {
            if (saved != 0)
            {
                Delete_Table(emp_no, year_no, ver_no, "ass_emp_compt");
            }
            foreach (DataRow dr in COMPDT.Rows)
            {
                string comp_code = dr[0].ToString();
                string comp_result = dr[1].ToString();
                string develop_points = dr[2].ToString();
                string train_prog = dr[3].ToString();

                SQL = " INSERT INTO ass_emp_compt  ( emp_no, year_no, ver_no, comp_code, comp_result, develop_points, train_prog) ";
                SQL += " VALUES ( '" + emp_no + "' , '" + year_no + "', '" + ver_no + "', '" + comp_code + "', '" + comp_result + "', '" + develop_points.FixSingleQuotes() + "', '" + train_prog.FixSingleQuotes() + "'  ) ";
                oracleacess.ExecuteNonQuery(SQL);
                val = 1;
            }
            oracleacess.Commit();
        }
        catch (Exception ex)
        {
            oracleacess.Rollback();
            LogError(ex.Message, "Save_ASS_COMP_DTTL >> ( Header ) Data ");
            val = 0;
        }

        return val;
    }
    //**************

    public int Save_ASS_OBJ_DTTL(string emp_no, string year_no, string ver_no, DataTable OBJDT)
    {
        int val = 0;
        int saved = Check_Saved_Data(emp_no, year_no, ver_no, "ass_emp_objectives");

        // Start a local transaction
        // Assign transaction object for a pending local transaction
        oracleacess.BeginTransaction();
        try
        {
            if (saved != 0)
            {
                Delete_Table(emp_no, year_no, ver_no, "ass_emp_objectives");
            }
            foreach (DataRow dr in OBJDT.Rows)
            {
                string obj_seq = dr[0].ToString();
                string obj_desc = dr[1].ToString();
                string obj_weight = dr[2].ToString();
                string obj_actual = dr[3].ToString();
                string obj_notes = dr[4].ToString();
                string develop_points = dr[5].ToString();
                string train_prog = dr[6].ToString();
                SQL = " INSERT INTO ass_emp_objectives  ( emp_no, year_no, ver_no, obj_seq, obj_desc, obj_weight, obj_actual, obj_notes , develop_points, train_prog ) ";
                SQL += " VALUES (  '" + emp_no + "', '" + year_no + "', '" + ver_no + "', '" + obj_seq + "', '" + obj_desc.FixSingleQuotes() + "', '" + obj_weight + "', '" + obj_actual + "', '" + obj_notes.FixSingleQuotes() + "', '" + develop_points.FixSingleQuotes() + "', '" + train_prog.FixSingleQuotes() + "' ) ";
                oracleacess.ExecuteNonQuery(SQL);
                val = 1;
            }
            oracleacess.Commit();
        }
        catch (Exception ex)
        {
            oracleacess.Rollback();
            LogError(ex.Message, "Save_ASS_OBJ_DTTL >> ( Detail Objective ) Data ");
            val = 0;
        }

        return val;
    }
    //**************

    public int Save_ASS_NY_OBJ_DTTL(string emp_no, string year_no, string ver_no, DataTable OBJDT)
    {
        int val = 0;
        int saved = Check_Saved_Data(emp_no, year_no, ver_no, "ASS_EMP_NEXT_OBJ");
        oracleacess.BeginTransaction();
        try
        {
            if (saved != 0)
            {
                Delete_Table(emp_no, year_no, ver_no, "ASS_EMP_NEXT_OBJ");
            }
            foreach (DataRow dr in OBJDT.Rows)
            {
                string obj_seq = dr[0].ToString();
                string obj_desc = dr[1].ToString();
                string obj_weight = dr[2].ToString();
                string obj_quart = dr[3].ToString();
                string obj_actual = "0";
                string obj_notes = "0";
                SQL = " INSERT INTO ASS_EMP_NEXT_OBJ  ( emp_no, year_no, ver_no, obj_seq, obj_desc, obj_weight, obj_actual, obj_notes,QUART   ) ";
                SQL += " VALUES (  '" + emp_no + "', '" + year_no + "', '" + ver_no + "', '" + obj_seq + "', '" + obj_desc.FixSingleQuotes() + "', '" + obj_weight + "', '" + obj_actual + "', '" + obj_notes.FixSingleQuotes() + "'," + obj_quart + "  ) ";
                oracleacess.ExecuteNonQuery(SQL);
                val = 1;
            }
            oracleacess.Commit();
        }
        catch (Exception ex)
        {
            oracleacess.Rollback();
            LogError(ex.Message, "Save_ASS_Next Year OBJ DTTL >> ( Detail Objective ) Data ");
            val = 0;
        }

        return val;
    }
    //**************

    public int Save_ASS_NY_OBJ_ByType_DTTL(string emp_no, string year_no, string ver_no, string Type, DataTable OBJDT)
    {
        int val = 0;
        //int saved = Check_Saved_Data(emp_no, year_no, ver_no, "ASS_EMP_NEXT_OBJ");
        int saved = Check_Saved_ObjType_Data(emp_no, year_no, ver_no, Type);
        oracleacess.BeginTransaction();
        try
        {
            if (saved != 0)
            {
                //Delete_Table(emp_no, year_no, ver_no, "ASS_EMP_NEXT_OBJ");
                Delete_ObjType(emp_no, year_no, ver_no, Type);
            }
            foreach (DataRow dr in OBJDT.Rows)
            {
                string obj_seq = dr[0].ToString();
                string obj_desc = dr[1].ToString();
                string obj_weight = dr[2].ToString();
                string obj_quart = dr[3].ToString();
                string obj_actual = "0";
                string obj_notes = Type;
                if (string.IsNullOrEmpty(obj_quart))
                    obj_quart = "Null";
                SQL = " INSERT INTO ASS_EMP_NEXT_OBJ  ( emp_no, year_no, ver_no, obj_seq, obj_desc, obj_weight, obj_actual, obj_notes, quart  ) ";
                SQL += " VALUES (  '" + emp_no + "', '" + year_no + "', '" + ver_no + "', '" + obj_seq + "', '" + obj_desc.FixSingleQuotes() + "', '" + obj_weight + "', '" + obj_actual + "', '" + obj_notes.FixSingleQuotes() + "', " + obj_quart + "  ) ";
                oracleacess.ExecuteNonQuery(SQL);
                val = 1;
            }
            oracleacess.Commit();
        }
        catch (Exception ex)
        {
            oracleacess.Rollback();
            LogError(ex.Message, "Save_ASS_Next Year OBJ By Type DTTL >> ( Detail Objective ) Data ");
            val = 0;
        }

        return val;
    }
    //**************

    public int Delete_ObjType(string emp_no, string year_no, string ver_no, string Type)
    {
        int val = 0;
        try
        {
            SQL = " Delete ASS_EMP_NEXT_OBJ Where ( emp_no = '" + emp_no + "' and year_no = '" + year_no + "' and  ver_no = '" + ver_no + "' and obj_notes  = '" + Type + "')  ";
            oracleacess.ExecuteNonQuery(SQL);
            val = 1;
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Delete_ObjType >> ( Delete ) Data ");
            val = 0;
        }

        return val;
    }
    //****************

    public int Check_Saved_ObjType_Data(string emp_no, string year, string vers, string Type)
    {
        int ret_val = 0;

        try
        {
            //ass_emp_header 
            SQL = "  select count(1) as totalrec from ASS_EMP_NEXT_OBJ where  emp_no = '" + emp_no + "' and year_no = '" + year + "' and  ver_no = '" + vers + "' and  obj_notes = '" + Type + "'    ";
            object Pass = oracleacess.ExecuteScalar(SQL);
            ret_val = int.Parse(Pass.ToString());
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Check_Saved_ObjType_Data >> To Check Saved Data ");
        }

        return ret_val;
    }
    //**************

    public DataTable GetEmployeeAppraisalHeader(string year, string version, string emp_no)
    {
        try
        {
            SQL = $@"SELECT 
                        ASSESSOR_NAME, 
                        ASS_DATE, 
                        RESULT, 
                        APPROVED, 
                        SAVE_TYPE, 
                        NYO_COMMENT, 
                        COMP_RESULT, 
                        OBJ_RESULT,
                        EMP_FINAL,
                        DEV_TRN_PLAN FROM ASS_EMP_HEADER WHERE EMP_NO = '{emp_no}' AND  YEAR_NO = '{year}' AND VER_NO = '{version}'";
            var dt = oracleacess.ExecuteDataTable(SQL);
            return dt;
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Get_Ass_Emp_Result >> To get Get_Ass_Emp_Result ");
        }

        return null;
    }
    //***********

    public string Get_Ass_NYO_Comments(string year, string vers, string emp_no)
    {
        string ret_val = "";

        try
        {
            SQL = " select  NYO_COMMENT from ass_emp_header where emp_no = '" + emp_no + "' and  year_no = '" + year + "' and ver_no = '" + vers + "'  ";
            object Pass = oracleacess.ExecuteScalar(SQL);
            ret_val = Pass.ToString();
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Get_Ass_NYO_Comments >> To get Get_Ass_Emp_Result ");
        }

        return ret_val;
    }
    //***********
    #endregion
    //********************************************************************************************************************
    #region " New Appraisal 2017 == Approval Forms "
    //****************

    public DataTable Bind_New_ASS_APPROVE_EMP_GRID(string emp_no, string year, string version, string status, string dept, string company)
    {
        // status 1 = تم التسجيل
        // status 0 = لم يتم التسجيل
        // status 2 = تمت الموافقة

        string ret_val = "";
        try
        {
            SQL = "  SELECT AS_ADMIN_USER(" + emp_no + ") FROM DUAL";
            object admn = oracleacess.ExecuteScalar(SQL);
            ret_val = admn.ToString();

            if (ret_val == "N")
            {
                if (status == "0") // لم يتم التسجيل
                { SQL = " SELECT ASS_EMP_ACCEPT(emp_no,'" + year + "', '" + version + "' ) EMP_ACCEPT, EMP_NO,EMP_NAME , job_no , GET_EMP_title(job_no) as JOB_NAME  , ASS_get_emp_app_status(emp_no,'" + year + "', '" + version + "' ) as Status , case when ASS_get_emp_app_status(emp_no, '" + year + "', '" + version + "') = 0 then 'لم يتم التسجيل'  when ASS_get_emp_app_status(emp_no, '" + year + "', '" + version + "') = 1 then 'تم التسجيل' when ASS_get_emp_app_status(emp_no, '" + year + "' , '" + version + "' ) = 2 then 'تمت الموافقة'  end  as  fStatus , ASS_GET_EMP_RESULT(EMP_NO,'" + year + "','" + version + "')  as RSLT ,  EG_EMP_TYPE(EMP_NO) as emp_company , level  , ASS_GET_EMP_GRADE(EMP_NO,'" + year + "','" + version + "')  as GRAD  FROM pay_egic.APP_EMP  where NVL(NO_APPRAISAL,0) = 0 AND emp_no <> '" + emp_no + "' AND FINSHED = '2' AND ( ASS_get_emp_app_status(emp_no,'" + year + "', '" + version + "' ) = '" + status + "' ) and ( substr(section_no,1,4) = '" + dept + "' or '" + dept + "' = 'ALL' ) and ( EG_EMP_TYPE(EMP_NO) = '" + company + "' OR '" + company + "'  = 'ALL' ) START WITH EMP_NO = '" + emp_no + "' CONNECT BY PRIOR EMP_NO = D_EMP_NO  "; }
                else
                { SQL = "  SELECT ASS_EMP_ACCEPT(emp_no,'" + year + "', '" + version + "' ) EMP_ACCEPT, EMP_NO,EMP_NAME , job_no , GET_EMP_title(job_no) as JOB_NAME  , ASS_get_emp_app_status(emp_no,'" + year + "', '" + version + "' ) as Status , case when ASS_get_emp_app_status(emp_no, '" + year + "', '" + version + "') = 0 then 'لم يتم التسجيل'  when ASS_get_emp_app_status(emp_no, '" + year + "', '" + version + "') = 1 then 'تم التسجيل' when ASS_get_emp_app_status(emp_no, '" + year + "' , '" + version + "' ) = 2 then 'تمت الموافقة'  end  as  fStatus , ASS_GET_EMP_RESULT(EMP_NO,'" + year + "','" + version + "')  as RSLT , EG_EMP_TYPE(EMP_NO) as emp_company , level , ASS_GET_EMP_GRADE(EMP_NO,'" + year + "','" + version + "')  as GRAD   FROM pay_egic.APP_EMP  where NVL(NO_APPRAISAL,0) = 0 AND emp_no <> '" + emp_no + "' AND FINSHED = '2' AND ( ASS_get_emp_app_status(emp_no,'" + year + "', '" + version + "' ) = '" + status + "' OR ASS_get_emp_app_status(emp_no,'" + year + "', '" + version + "' ) = '2' ) and ( substr(section_no,1,4) = '" + dept + "' or '" + dept + "' = 'ALL' ) and ( EG_EMP_TYPE(EMP_NO) = '" + company + "' OR '" + company + "'  = 'ALL' ) START WITH EMP_NO = '" + emp_no + "' CONNECT BY PRIOR EMP_NO = D_EMP_NO  "; }
            }
            else
            {
                if (status == "0")
                { SQL = "  SELECT ASS_EMP_ACCEPT(emp_no,'" + year + "', '" + version + "' ) EMP_ACCEPT, EMP_NO,EMP_NAME , job_no , GET_EMP_title(job_no) as JOB_NAME  , ASS_get_emp_app_status(emp_no,'" + year + "', '" + version + "' ) as Status , case when ASS_get_emp_app_status(emp_no, '" + year + "', '" + version + "') = 0 then 'لم يتم التسجيل'  when ASS_get_emp_app_status(emp_no, '" + year + "', '" + version + "') = 1 then 'تم التسجيل' when ASS_get_emp_app_status(emp_no, '" + year + "' , '" + version + "' ) = 2 then 'تمت الموافقة'  end  as  fStatus , ASS_GET_EMP_RESULT(EMP_NO,'" + year + "','" + version + "')  as RSLT , EG_EMP_TYPE(EMP_NO) as emp_company , level , ASS_GET_EMP_GRADE(EMP_NO,'" + year + "','" + version + "')  as GRAD  FROM pay_egic.APP_EMP  where NVL(NO_APPRAISAL,0) = 0 AND ( emp_no <> '" + emp_no + "' OR as_admin_user(" + emp_no + ") = 'Y' ) AND FINSHED = '2' AND ( ASS_get_emp_app_status(emp_no,'" + year + "', '" + version + "' ) = '" + status + "' ) and ( substr(section_no,1,4) = '" + dept + "' or '" + dept + "' = 'ALL' ) and ( EG_EMP_TYPE(EMP_NO) = '" + company + "' OR '" + company + "'  = 'ALL' )  START WITH EMP_NO = '" + emp_no + "'  CONNECT BY PRIOR EMP_NO = D_EMP_NO "; }
                else
                { SQL = "  SELECT ASS_EMP_ACCEPT(emp_no,'" + year + "', '" + version + "' ) EMP_ACCEPT, EMP_NO,EMP_NAME , job_no , GET_EMP_title(job_no) as JOB_NAME  , ASS_get_emp_app_status(emp_no,'" + year + "', '" + version + "' ) as Status , case when ASS_get_emp_app_status(emp_no, '" + year + "', '" + version + "') = 0 then 'لم يتم التسجيل'  when ASS_get_emp_app_status(emp_no, '" + year + "', '" + version + "') = 1 then 'تم التسجيل' when ASS_get_emp_app_status(emp_no, '" + year + "' , '" + version + "' ) = 2 then 'تمت الموافقة'  end  as  fStatus , ASS_GET_EMP_RESULT(EMP_NO,'" + year + "','" + version + "')  as RSLT , EG_EMP_TYPE(EMP_NO) as emp_company , level , ASS_GET_EMP_GRADE(EMP_NO,'" + year + "','" + version + "')  as GRAD  FROM pay_egic.APP_EMP  where NVL(NO_APPRAISAL,0) = 0 AND ( emp_no <> '" + emp_no + "' OR as_admin_user(" + emp_no + ") = 'Y' ) AND FINSHED = '2' AND ( ASS_get_emp_app_status(emp_no,'" + year + "', '" + version + "' ) = '" + status + "' OR ASS_get_emp_app_status(emp_no,'" + year + "', '" + version + "' ) = '2' ) and ( substr(section_no,1,4) = '" + dept + "' or '" + dept + "' = 'ALL' ) and ( EG_EMP_TYPE(EMP_NO) = '" + company + "' OR '" + company + "'  = 'ALL' )  START WITH EMP_NO = '" + emp_no + "'  CONNECT BY PRIOR EMP_NO = D_EMP_NO "; }
            }


            dt = oracleacess.ExecuteDataTable(SQL);

        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Bind_New_ASS_APPROVE_EMP_GRID >> ");
        }
        return dt;
    }
    //*************

    public DataTable Bind_New_ASS_APPROVE_EMP_FRST_LEVEL_GRID(string emp_no, string year, string version, string status)
    {
        try
        {
            // status 1 = تم التسجيل
            // status 0 = لم يتم التسجيل
            // status 2 = تمت الموافقة
            SQL = "  SELECT  EMP_NO,EMP_NAME  , job_no , GET_EMP_title(job_no) as JOB_NAME  , ass_get_emp_app_status(emp_no, '" + year + "' , '" + version + "' ) as Status , case when ass_get_emp_app_status(emp_no, '" + year + "' , '" + version + "' ) = 0 then 'لم يتم التسجيل'  when ass_get_emp_app_status(emp_no, '" + year + "' , '" + version + "' ) = 1 then 'تم التسجيل' when ass_get_emp_app_status(emp_no,'" + year + "', '" + version + "' ) = 2 then 'تمت الموافقة'  end  as  fStatus , ASS_GET_EMP_RESULT(EMP_NO,'" + year + "','" + version + "')  as RSLT , case when EG_EMP_TYPE(emp_no) IS NULL  then 'Outsource' when EG_EMP_TYPE(emp_no) = 'EGIC' then 'EGIC' end as emp_company, level  FROM pay_egic.APP_EMP where NVL(NO_APPRAISAL,0) = 0 AND emp_no <> '" + emp_no + "' AND LEVEL = 2 AND FINSHED = '2'  AND ASS_get_emp_app_status(emp_no,'" + year + "', '" + version + "' ) = '" + status + "' START WITH EMP_NO = '" + emp_no + "' CONNECT BY PRIOR EMP_NO = D_EMP_NO ";

            dt = oracleacess.ExecuteDataTable(SQL);

        }
        catch (Exception ex)
        {
            LogError(ex.Message, " Bind_New_ASS_APPROVE_EMP_FRST_LEVEL_GRID ");
        }
        return dt;
    }
    //***************

    public int Make_ASS_Approve(string emp_no, string year, string version, string approver)
    {
        int val = 0;
        try
        {
            SQL = " Update ass_emp_header set approved = '1' , approver_name = '" + approver + "'  , approve_date = to_date('" + (System.DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss")) + "','MM/DD/YYYY HH24:MI:SS')   where emp_no = '" + emp_no + "' and year_no = '" + year + "' and ver_no = '" + version + "'  ";
            oracleacess.ExecuteNonQuery(SQL);
            val = 1;
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Make_ASS_Approve >> Make Approve");
            val = 0;
        }

        return val;
    }
    //***************

    #endregion
    //*************************************************( Ramy Updates 12-11-2018 )**************************************************
    #region " New Appraisal 2018 "


    public int Check_Ass_Departments(string emp_no)
    {
        int ret_val = 0;

        try
        {
            SQL = " SELECT count(S.SECTION_NO) as dept FROM PAY_EGIC.EG_DIRECTORS D , PAY_EGIC.EMP E , PAY_EGIC.APP_SECTION S WHERE D.EMP_NO = E.EMP_NO AND D.SECTION_NO = S.SECTION_NO AND E.EMP_NO = '" + emp_no + "' ";
            object Pass = oracleacess.ExecuteScalar(SQL);
            ret_val = int.Parse(Pass.ToString());
        }
        catch (Exception ex)
        {
            LogError(ex.Message, " Check_Ass_Departments >>  ");
        }

        return ret_val;
    }


    public DataTable Bind_ASS_Departments_combo(string emp_no)
    {

        try
        {
            SQL = " SELECT S.SECTION_NO,S.SECTION_NAME FROM PAY_EGIC.EG_DIRECTORS D , PAY_EGIC.EMP E , PAY_EGIC.APP_SECTION S WHERE D.EMP_NO = E.EMP_NO AND D.SECTION_NO = S.SECTION_NO AND E.EMP_NO = '" + emp_no + "' ";

            dt = oracleacess.ExecuteDataTable(SQL);

        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Bind_ASS_Departments_combo");
        }
        return dt;
    }

    #endregion

    //*************************** exception data
    #region " exceptions "


    public DataTable Bind_ASS_EMPLOYE_DDL(string MANG_emp_no, string deptno)
    {

        try
        {
            if (deptno == "NO")
            {
                SQL = " SELECT  EMP_NO,EMP_NAME  FROM pay_egic.APP_EMP  where  ( EG_EMP_TYPE(EMP_NO) = 'ALL' OR 'ALL' = 'ALL'  )   AND NVL(NO_APPRAISAL,0) = 0 AND ( emp_no <> '" + MANG_emp_no + "' ) AND FINSHED = '2'  START WITH EMP_NO = '" + MANG_emp_no + "'   CONNECT BY PRIOR EMP_NO = D_EMP_NO ";
            }
            else
            {
                SQL = " SELECT  EMP_NO,EMP_NAME  FROM pay_egic.APP_EMP  where  ( EG_EMP_TYPE(EMP_NO) = 'ALL' OR 'ALL' = 'ALL'  )   AND NVL(NO_APPRAISAL,0) = 0 AND ( emp_no <> '" + MANG_emp_no + "' ) AND FINSHED = '2' and substr(section_no,1,4) = '" + deptno + "' START WITH EMP_NO = '" + MANG_emp_no + "'   CONNECT BY PRIOR EMP_NO = D_EMP_NO ";
            }


            dt = oracleacess.ExecuteDataTable(SQL);

        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Bind_ASS_EMPLOYE_DDL");
        }
        return dt;
    }
    //**************************************


    public DataTable Bind_ASS_PERF_GRADS_DDL()
    {

        try
        {
            SQL = " select grade_code as code , grade_name as name from ass_performance_grades ";

            dt = oracleacess.ExecuteDataTable(SQL);

        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Bind_ASS_PERF_GRADS_DDL");
        }
        return dt;
    }

    //***************

    public int Save_ASS_Exception(string emp_no, string emp_name, string perf_grade, string rec_user, string year_no, string ver_no)
    {
        int val = 0;
        try
        {
            SQL = "Insert into ass_exceptions (emp_no, emp_name, perf_grade, approved, rec_date, rec_user , year_no , ver_no ) Values ( '" + emp_no + "' ,'" + emp_name + "' ,'" + perf_grade + "' ,'0' ,sysdate ,'" + rec_user + "', '" + year_no + "' , '" + ver_no + "')";
            val = oracleacess.ExecuteNonQuery(SQL);
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Save_ASS_Exception >>");
            val = 0;
        }

        return val;
    }
    //***************

    public int Delete_ASS_Exception(string emp_no)
    {
        int val = 0;
        try
        {
            SQL = " delete from ass_exceptions where  emp_no = '" + emp_no + "'  ";
            val = oracleacess.ExecuteNonQuery(SQL);
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Delete_ASS_Exception >>");
            val = 0;
        }

        return val;
    }
    //*************

    public int Approve_ASS_Exception(string emp_no, string rec_user, string year_no, string ver_no)
    {
        int val = 0;
        try
        {
            SQL = " Update ass_exceptions Set approved = '1' , approve_date = sysdate,  approve_user = '" + rec_user + "'   Where ( emp_no = '" + emp_no + "' ) and ( year_no = '" + year_no + "' )  and (  ver_no = '" + ver_no + "' ) ";
            val = oracleacess.ExecuteNonQuery(SQL);
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Approve_ASS_Exception >>");
            val = 0;
        }

        return val;
    }
    //*************

    public DataTable Bind_ASS_Exceptions_GRID(string emp_no, string dept, string year_no, string ver_no)
    {
        try
        {
            SQL = "  select E.emp_no , E.emp_name , P.GRADE_NAME , case when E.approved = '0' then 'Pending' when  E.approved = '1' then 'Approved' end as approval , E.rec_date , E.rec_user , E.approve_date , E.approve_user   from ass_exceptions E  inner join  ass_performance_grades P on P.GRADE_CODE = E.PERF_GRADE  where E.EMP_NO IN ( SELECT  EMP_NO  FROM pay_egic.APP_EMP   where  ( EG_EMP_TYPE(EMP_NO) = 'ALL' OR 'ALL' = 'ALL'  )    AND NVL(NO_APPRAISAL,0) = 0 AND ( emp_no <> '" + emp_no + "' )   AND FINSHED = '2' and ( substr(section_no,1,4) = '" + dept + "' or '" + dept + "' = 'ALL' ) and ( E.year_no = '" + year_no + "' ) and ( E.ver_no = '" + ver_no + "' )  START WITH EMP_NO = '" + emp_no + "'   CONNECT BY PRIOR EMP_NO = D_EMP_NO ) ";

            dt = oracleacess.ExecuteDataTable(SQL);

        }
        catch (Exception ex)
        {
            LogError(ex.Message, " Bind_ASS_Exceptions_GRID ");
        }
        return dt;
    }
    //***************

    public DataTable Bind_ASS_Approve_Exceptions_GRID(string year_no, string ver_no)
    {
        try
        {
            SQL = "  Select E.emp_no , E.emp_name , S.SECTION_NAME , P.GRADE_NAME , case when E.approved = '0' then 'Pending' when  E.approved = '1' then 'Approved' end as  approval , E.rec_date , E.rec_user , E.approve_date , E.approve_user    from ass_exceptions E   inner join  ass_performance_grades P   on P.GRADE_CODE = E.PERF_GRADE    inner join pay_egic.APP_EMP M   on M.EMP_NO = E.EMP_NO  Inner join pay_egic.section S  on S.section_no = M.section_no where ( E.APPROVED = '0' ) and ( E.year_no = '" + year_no + "' ) and ( E.ver_no = '" + ver_no + "' )  order by e.emp_no ";

            dt = oracleacess.ExecuteDataTable(SQL);

        }
        catch (Exception ex)
        {
            LogError(ex.Message, " Bind_ASS_Approve_Exceptions_GRID ");
        }
        return dt;
    }
    //***************

    public int Check_Ass_Directores(string emp_no)
    {
        int ret_val = 0;

        try
        {
            SQL = "  select count(emp_no) as cont from pay_egic.eg_directors where emp_no = '" + emp_no + "' ";
            object Pass = oracleacess.ExecuteScalar(SQL);
            ret_val = int.Parse(Pass.ToString());
        }
        catch (Exception ex)
        {
            LogError(ex.Message, " Check_Ass_Directores >>  ");
        }

        return ret_val;
    }
    //*****************************

    #endregion

    #region " print assessments form "

    //***************

    public string Check_Ass_approved(string emp_no, string year, string version)
    {
        int ret_val = 0;
        string return_data = "Draft Document";

        try
        {
            SQL = " Select count(emp_no) as empp from Ass_Emp_Header where emp_no = '" + emp_no + "'  and year_no = '" + year + "' and ver_no = '" + version + "'  and approved = '1' and save_type = 'SAVE' ";
            object Pass = oracleacess.ExecuteScalar(SQL);
            ret_val = int.Parse(Pass.ToString());
            if (ret_val > 0)
            {
                return_data = "Final";
            }
            else
            {
                return_data = "Draft";
            }
        }
        catch (Exception ex)
        {
            LogError(ex.Message, " Check_Ass_approved >>  ");
            return_data = "Draft";
        }

        return return_data;
    }
    //*****************************

    #endregion
    //***************************
    #region " Calculate appraisal data "

    //***************************

    public int Get_Ass_Planned_Exll(string emp_no, string company, string dept, string year_no, string ver_no)
    {
        int ret_val = 0;

        try
        {
            SQL = " SELECT round((count(EMP_NO)*0.05),0)  + EXP1.Exl as EXL   FROM  ( select count(emp_no) as Exl from ass_exceptions where perf_grade = 'EXL' and approved = '1' and year_no = '" + year_no + "' and ver_no = '" + ver_no + "' AND PAY_EGIC.GET_EMP_MANG(EMP_NO) = '" + emp_no + "' and  ( substr(eg_emp_section(emp_no),1,4) = '" + dept + "' or '" + dept + "' = 'ALL' )    ) EXP1 , ( select count(emp_no) as vgod from ass_exceptions where perf_grade = 'VGD' and approved = '1' and year_no = '" + year_no + "' and ver_no = '" + ver_no + "' AND PAY_EGIC.GET_EMP_MANG(EMP_NO) = '" + emp_no + "' and  ( substr(eg_emp_section(emp_no),1,4) = '" + dept + "' or '" + dept + "' = 'ALL' ) ) EXP1 , ( select count(emp_no) as good from ass_exceptions where perf_grade = 'GOD' and approved = '1' and year_no = '" + year_no + "' and ver_no = '" + ver_no + "' AND PAY_EGIC.GET_EMP_MANG(EMP_NO) = '" + emp_no + "' and  ( substr(eg_emp_section(emp_no),1,4) = '" + dept + "' or '" + dept + "' = 'ALL' ) ) EXP1 , ( select count(emp_no) as mod from ass_exceptions where perf_grade = 'MOD' and approved = '1' and year_no = '" + year_no + "' and ver_no = '" + ver_no + "' AND PAY_EGIC.GET_EMP_MANG(EMP_NO) = '" + emp_no + "' and  ( substr(eg_emp_section(emp_no),1,4) = '" + dept + "' or '" + dept + "' = 'ALL' ) ) EXP1 ,  pay_egic.APP_EMP  where NVL(NO_APPRAISAL,0) = 0  AND emp_no <> '" + emp_no + "' AND FINSHED = '2' AND ( EG_EMP_TYPE(EMP_NO) = '" + company + "'  OR   '" + company + "'  = 'ALL' ) AND ( substr(section_no,1,4) = '" + dept + "' or '" + dept + "' = 'ALL' ) START WITH EMP_NO = '" + emp_no + "' CONNECT BY PRIOR EMP_NO = D_EMP_NO  group by EXP1.Exl  ";
            object Pass = oracleacess.ExecuteScalar(SQL);
            ret_val = int.Parse(Pass.ToString());
        }
        catch (Exception ex)
        {
            LogError(ex.Message, " Get_Ass_Planned_Exll >>  ");
        }

        return ret_val;
    }
    //***************************

    public int Get_Ass_Planned_VGOD(string emp_no, string company, string dept, string year_no, string ver_no)
    {
        int ret_val = 0;

        try
        {
            SQL = " SELECT  round( (count(EMP_NO)*0.15 ),0)  + EXP1.vgod   as VGD   FROM  ( select count(emp_no) as Exl from ass_exceptions where perf_grade = 'EXL' and approved = '1' and year_no = '" + year_no + "' and ver_no = '" + ver_no + "' AND PAY_EGIC.GET_EMP_MANG(EMP_NO) = '" + emp_no + "' and  ( substr(eg_emp_section(emp_no),1,4) = '" + dept + "' or '" + dept + "' = 'ALL' ) ) EXP1 , ( select count(emp_no) as vgod from ass_exceptions where perf_grade = 'VGD' and approved = '1' and year_no = '" + year_no + "' and ver_no = '" + ver_no + "' AND PAY_EGIC.GET_EMP_MANG(EMP_NO) = '" + emp_no + "' and  ( substr(eg_emp_section(emp_no),1,4) = '" + dept + "' or '" + dept + "' = 'ALL' ) ) EXP1 , ( select count(emp_no) as good from ass_exceptions where perf_grade = 'GOD' and approved = '1' and year_no = '" + year_no + "' and ver_no = '" + ver_no + "' AND PAY_EGIC.GET_EMP_MANG(EMP_NO) = '" + emp_no + "' and  ( substr(eg_emp_section(emp_no),1,4) = '" + dept + "' or '" + dept + "' = 'ALL' ) ) EXP1 , ( select count(emp_no) as mod from ass_exceptions where perf_grade = 'MOD' and approved = '1' and year_no = '" + year_no + "' and ver_no = '" + ver_no + "' AND PAY_EGIC.GET_EMP_MANG(EMP_NO) = '" + emp_no + "' and  ( substr(eg_emp_section(emp_no),1,4) = '" + dept + "' or '" + dept + "' = 'ALL' ) ) EXP1 ,  pay_egic.APP_EMP  where NVL(NO_APPRAISAL,0) = 0  AND emp_no <> '" + emp_no + "' AND FINSHED = '2' AND ( EG_EMP_TYPE(EMP_NO) = '" + company + "'  OR   '" + company + "'  = 'ALL' ) AND ( substr(section_no,1,4) = '" + dept + "' or '" + dept + "' = 'ALL' ) START WITH EMP_NO = '" + emp_no + "' CONNECT BY PRIOR EMP_NO = D_EMP_NO  group by EXP1.vgod ";
            object Pass = oracleacess.ExecuteScalar(SQL);
            ret_val = int.Parse(Pass.ToString());
        }
        catch (Exception ex)
        {
            LogError(ex.Message, " Get_Ass_Planned_VGOD >>  ");
        }

        return ret_val;
    }
    //***************************

    public int Get_Ass_Planned_GOOD(string emp_no, string company, string dept, string year_no, string ver_no)
    {
        int ret_val = 0;

        try
        {
            SQL = " SELECT  round( (count(EMP_NO)*0.70),0)  + EXP1.good  as GOD   FROM  ( select count(emp_no) as Exl from ass_exceptions where perf_grade = 'EXL' and approved = '1' and year_no = '" + year_no + "' and ver_no = '" + ver_no + "' AND PAY_EGIC.GET_EMP_MANG(EMP_NO) = '" + emp_no + "' and  ( substr(eg_emp_section(emp_no),1,4) = '" + dept + "' or '" + dept + "' = 'ALL' ) ) EXP1 , ( select count(emp_no) as vgod from ass_exceptions where perf_grade = 'VGD' and approved = '1' and year_no = '" + year_no + "' and ver_no = '" + ver_no + "' AND PAY_EGIC.GET_EMP_MANG(EMP_NO) = '" + emp_no + "' and  ( substr(eg_emp_section(emp_no),1,4) = '" + dept + "' or '" + dept + "' = 'ALL' ) ) EXP1 , ( select count(emp_no) as good from ass_exceptions where perf_grade = 'GOD' and approved = '1' and year_no = '" + year_no + "' and ver_no = '" + ver_no + "' AND PAY_EGIC.GET_EMP_MANG(EMP_NO) = '" + emp_no + "' and  ( substr(eg_emp_section(emp_no),1,4) = '" + dept + "' or '" + dept + "' = 'ALL' ) ) EXP1 , ( select count(emp_no) as mod from ass_exceptions where perf_grade = 'MOD' and approved = '1' and year_no = '" + year_no + "' and ver_no = '" + ver_no + "' AND PAY_EGIC.GET_EMP_MANG(EMP_NO) = '" + emp_no + "' and  ( substr(eg_emp_section(emp_no),1,4) = '" + dept + "' or '" + dept + "' = 'ALL' ) ) EXP1 ,  pay_egic.APP_EMP  where NVL(NO_APPRAISAL,0) = 0  AND emp_no <> '" + emp_no + "' AND FINSHED = '2' AND ( EG_EMP_TYPE(EMP_NO) = '" + company + "'  OR   '" + company + "'  = 'ALL' ) AND ( substr(section_no,1,4) = '" + dept + "' or '" + dept + "' = 'ALL' ) START WITH EMP_NO = '" + emp_no + "' CONNECT BY PRIOR EMP_NO = D_EMP_NO  group by  EXP1.good  ";
            object Pass = oracleacess.ExecuteScalar(SQL);
            ret_val = int.Parse(Pass.ToString());
        }
        catch (Exception ex)
        {
            LogError(ex.Message, " Get_Ass_Planned_GOOD >>  ");
        }

        return ret_val;
    }
    //***************************

    public int Get_Ass_Planned_MOD(string emp_no, string company, string dept, string year_no, string ver_no)
    {
        int ret_val = 0;

        try
        {
            SQL = " SELECT round( (count(EMP_NO)*0.10),0) + EXP1.mod  as MOD   FROM  ( select count(emp_no) as Exl from ass_exceptions where perf_grade = 'EXL' and approved = '1' and year_no = '" + year_no + "' and ver_no = '" + ver_no + "' AND PAY_EGIC.GET_EMP_MANG(EMP_NO) = '" + emp_no + "' and  ( substr(eg_emp_section(emp_no),1,4) = '" + dept + "' or '" + dept + "' = 'ALL' ) ) EXP1 , ( select count(emp_no) as vgod from ass_exceptions where perf_grade = 'VGD' and approved = '1' and year_no = '" + year_no + "' and ver_no = '" + ver_no + "' AND PAY_EGIC.GET_EMP_MANG(EMP_NO) = '" + emp_no + "' and  ( substr(eg_emp_section(emp_no),1,4) = '" + dept + "' or '" + dept + "' = 'ALL' ) ) EXP1 , ( select count(emp_no) as good from ass_exceptions where perf_grade = 'GOD' and approved = '1' and year_no = '" + year_no + "' and ver_no = '" + ver_no + "' AND PAY_EGIC.GET_EMP_MANG(EMP_NO) = '" + emp_no + "' and  ( substr(eg_emp_section(emp_no),1,4) = '" + dept + "' or '" + dept + "' = 'ALL' ) ) EXP1 , ( select count(emp_no) as mod from ass_exceptions where perf_grade = 'MOD' and approved = '1' and year_no = '" + year_no + "' and ver_no = '" + ver_no + "' AND PAY_EGIC.GET_EMP_MANG(EMP_NO) = '" + emp_no + "' and  ( substr(eg_emp_section(emp_no),1,4) = '" + dept + "' or '" + dept + "' = 'ALL' ) ) EXP1 ,  pay_egic.APP_EMP  where NVL(NO_APPRAISAL,0) = 0  AND emp_no <> '" + emp_no + "' AND FINSHED = '2' AND ( EG_EMP_TYPE(EMP_NO) = '" + company + "'  OR   '" + company + "'  = 'ALL' ) AND ( substr(section_no,1,4) = '" + dept + "' or '" + dept + "' = 'ALL' ) START WITH EMP_NO = '" + emp_no + "' CONNECT BY PRIOR EMP_NO = D_EMP_NO  group by EXP1.mod ";
            object Pass = oracleacess.ExecuteScalar(SQL);
            ret_val = int.Parse(Pass.ToString());
        }
        catch (Exception ex)
        {
            LogError(ex.Message, " Get_Ass_Planned_MOD >>  ");
        }

        return ret_val;
    }
    //***************************

    #endregion
    //***************************


    #region mid year 2019

    public bool IsObjSentOrApproved(string emp_code, string year, string version)
    {
        SQL = string.Format("select emp_final from ass_emp_header where emp_no = {0} and  year_no = {1} and ver_no = {2}", emp_code, year, version);
        object result = oracleacess.ExecuteScalar(SQL);
        return result != null && result.ToString() == "1";
    }

    public DataTable GetEmployeeObjectiveHeader(string emp_code, string year, string version)
    {
        SQL = string.Format(
            @"
            SELECT 
                EMP_NO,
                YEAR_NO,
                VER_NO,
                ASSESSOR_NAME,
                ASS_DATE,
                RESULT,
                APPROVED,
                APPROVER_NAME,
                APPROVE_DATE,
                SAVE_TYPE,
                NYO_COMMENT,
                COMP_RESULT,
                OBJ_RESULT,
                EMP_FINAL,
                EMP_MEET,
                EMP_APPROVE,
                EMP_REASON 
            FROM 
                ASS_EMP_HEADER 
            WHERE 
                EMP_NO = {0} 
                AND YEAR_NO = {1} 
                AND VER_NO = {2}", emp_code, year, version);
        return oracleacess.ExecuteDataTable(SQL);
    }

    public DataTable GetEmployeeObjectives(string emp_code)
    {
        SQL = string.Format(@"
                            SELECT 
                                O.OBJ_DESC, 
                                O.OBJ_WEIGHT, 
                                O.OBJ_NOTES 
                            FROM 
                                ASS_EMP_OBJECTIVES O, 
                                ASS_EMP_HEADER H 
                            WHERE 
                                O.EMP_NO = H.EMP_NO 
                                AND O.YEAR_NO = H.YEAR_NO 
                                AND O.VER_NO = H.VER_NO 
                                AND H.VER_NO IN(SELECT VER_NO FROM ASS_YEARS WHERE STATUS = 1) 
                                AND H.EMP_FINAL = 1 
                                AND H.EMP_NO = {0}", emp_code);

        return oracleacess.ExecuteDataTable(SQL);
    }

    public bool SendObjectiveToEmp(string emp_code, string year, string version)
    {
        try
        {
            SQL = string.Format("UPDATE ASS_EMP_HEADER SET EMP_FINAL = 1, EMP_MEET = '', EMP_APPROVE = '', EMP_REASON = ''  WHERE EMP_NO = {0} AND  YEAR_NO = {1} AND VER_NO = {2}", emp_code, year, version);
            int result = oracleacess.ExecuteNonQuery(SQL);
            return result > 0;
        }
        catch (Exception ex)
        {
            ex.LogError();
        }
        return false;
    }

    public bool SaveEmpMidYearObjectives(string emp_code, string year, string version, bool isFinalSave, DataTable objectivesDT, string assessorName)
    {
        int result = 0;
        string saveType = isFinalSave ? "SAVE" : "DRAFT";
        bool isEmployeeHeaderExists = IsEmployeeHeaderExists(emp_code, year, version);

        oracleacess.BeginTransaction();
        try
        {
            if (!isEmployeeHeaderExists)
            {
                SQL = $@"INSERT INTO ASS_EMP_HEADER (
                        EMP_NO, 
                        YEAR_NO, 
                        VER_NO, 
                        ASSESSOR_NAME, 
                        ASS_DATE, 
                        RESULT, 
                        APPROVED, 
                        SAVE_TYPE, 
                        NYO_COMMENT, 
                        COMP_RESULT, 
                        OBJ_RESULT,
                        EMP_FINAL
                        ) 
                    VALUES ( 
                        {emp_code}, 
                        {year}, 
                        {version}, 
                        '{assessorName}', 
                        sysdate, 
                        '', 
                        '', 
                        '{saveType}', 
                        '', 
                        '', 
                        '',
                        '{(isFinalSave ? "1" : "")}'
                        )";
                result += oracleacess.ExecuteNonQuery(SQL);
            }
            else
            {
                SQL = $@"UPDATE ASS_EMP_HEADER SET 
                        ASSESSOR_NAME = '{assessorName}', 
                        RESULT = '', 
                        SAVE_TYPE = '{saveType}', 
                        NYO_COMMENT = '', 
                        COMP_RESULT = '', 
                        OBJ_RESULT = '', 
                        EMP_FINAL = {(isFinalSave ? 1 : 0)}, 
                        EMP_MEET = '', 
                        EMP_APPROVE = '', 
                        EMP_REASON = ''
                    Where 
                        EMP_NO = {emp_code} 
                        AND YEAR_NO = {year} 
                        AND VER_NO = {version}";
                result += oracleacess.ExecuteNonQuery(SQL);
            }

            SQL = $"DELETE ASS_EMP_OBJECTIVES WHERE EMP_NO = {emp_code} AND YEAR_NO = {year} AND VER_NO = {version}";
            result += oracleacess.ExecuteNonQuery(SQL);

            foreach (DataRow dr in objectivesDT.Rows)
            {
                string obj_seq = dr[0].ToString();
                string obj_desc = dr[1].ToString();
                string obj_weight = dr[2].ToString();
                string obj_actual = dr[3].ToString();
                string obj_notes = dr[4].ToString();
                string develop_points = dr[5].ToString();
                string train_prog = dr[6].ToString();
                SQL = $@"INSERT INTO ASS_EMP_OBJECTIVES (
                            EMP_NO, 
                            YEAR_NO, 
                            VER_NO, 
                            OBJ_SEQ, 
                            OBJ_DESC, 
                            OBJ_WEIGHT, 
                            OBJ_ACTUAL, 
                            OBJ_NOTES, 
                            DEVELOP_POINTS, 
                            TRAIN_PROG
                            ) 
                        VALUES ( 
                            {emp_code}, 
                            {year}, 
                            {version}, 
                            {obj_seq}, 
                            '{obj_desc.FixSingleQuotes()}', 
                            '{obj_weight}', 
                            '{obj_actual}', 
                            '{obj_notes.FixSingleQuotes()}', 
                            '{develop_points.FixSingleQuotes()}', 
                            '{train_prog.FixSingleQuotes()}'
                            )";
                result += oracleacess.ExecuteNonQuery(SQL);
            }
            oracleacess.Commit();
        }
        catch (Exception ex)
        {
            oracleacess.Rollback();
            result = 0;
            ex.LogError();
        }
        return result > 0;
    }

    private bool IsEmployeeHeaderExists(string emp_code, string year, string version)
    {
        int result = 0;
        object res = "";
        try
        {
            SQL = $"SELECT COUNT(1) AS TOTAL_REC FROM ASS_EMP_HEADER WHERE EMP_NO = {emp_code} AND YEAR_NO = {year} AND VER_NO = {version}";
            res = oracleacess.ExecuteScalar(SQL);
            int.TryParse(res.ToString(), out result);
        }
        catch (Exception ex)
        {
            ex.LogError(customeMessage: $"year: {year}, emp_code: {emp_code}, version: {version}, res: {res.ToString()}");
        }
        return result > 0;
    }

    public bool ApproveObjectiveByEmp(string emp_code, string year, string version, bool meetingDone, bool empAgreed, string notes)
    {
        try
        {
            int emp_final = empAgreed ? 1 : 0;
            SQL = string.Format(
                @"
            UPDATE 
                ASS_EMP_HEADER 
            SET 
                EMP_MEET = {0}, 
                EMP_APPROVE = {1}, 
                EMP_REASON = '{2}', 
                EMP_FINAL = {3} 
            WHERE 
                EMP_NO = {4} 
                AND  YEAR_NO = {5} 
                AND VER_NO = {6}",
                    meetingDone ? 1 : 0,
                    empAgreed ? 1 : 0,
                    notes.FixSingleQuotes(),
                    emp_final,
                    emp_code,
                    year,
                    version);

            int result = oracleacess.ExecuteNonQuery(SQL);
            return result > 0;
        }
        catch (Exception ex)
        {
            ex.LogError();
        }
        return false;
    }

    public DataTable GetUserInfo(string emp_no)
    {
        SQL = string.Format(@"
                            SELECT DISTINCT 
                                E.EMP_NO, 
                                E.EMP_NAME, 
                                ED.EMP_MAIL, 
                                E.D_EMP_NO MANAGER_NO, 
                                M.EMP_NAME MANAGER_NAME, 
                                MD.EMP_MAIL MANAGER_MAIL
                            FROM 
                                PAY_EGIC.APP_EMP E, 
                                PAY_EGIC.APP_EMP M, 
                                PAY_EGIC.APP_EMP_DETAIL ED, 
                                PAY_EGIC.APP_EMP_DETAIL MD
                            WHERE 
                                E.D_EMP_NO = M.EMP_NO
                                AND ED.EMP_NO(+) = E.EMP_NO
                                AND MD.EMP_NO(+) = M.EMP_NO
                                AND E.FINSHED = 2 
                                AND E.EMP_NO = {0}", emp_no);

        return oracleacess.ExecuteDataTable(SQL);
    }

    #endregion

    #region final year 2019

    public bool SaveEmplyeeAppraisal(string emp_code, string year, string version, SaveTypes saveType, string assessorName, double comptencesScore, double objectivesScore, double totalPerformanceResult, string notes, string empTrainingPlan, DataTable dtObjectives = null, DataTable dtComptences = null, DataTable dtNextYearOperationalObjectives = null, DataTable dtNextYearDevelopmentalObjectives = null)
    {
        int result = 0;
        Guid guid = Guid.NewGuid();
        bool isEmployeeHeaderExists = IsEmployeeHeaderExists(emp_code, year, version);

        oracleacess.BeginTransaction();
        try
        {
            // Save header data
            if (!isEmployeeHeaderExists)
            {
                SQL = $@"INSERT INTO ASS_EMP_HEADER (
                        EMP_NO, 
                        YEAR_NO, 
                        VER_NO, 
                        ASSESSOR_NAME, 
                        ASS_DATE, 
                        RESULT, 
                        APPROVED, 
                        SAVE_TYPE, 
                        NYO_COMMENT, 
                        COMP_RESULT, 
                        OBJ_RESULT,
                        EMP_FINAL,
                        DEV_TRN_PLAN
                        ) 
                    VALUES ( 
                        {emp_code}, 
                        {year}, 
                        {version}, 
                        '{assessorName}', 
                        sysdate, 
                        '{totalPerformanceResult.FixNumbersFormat()}', 
                        '', 
                        '{saveType.ToString().ToUpper()}', 
                        '{notes.FixSingleQuotes()}', 
                        '{comptencesScore.FixNumbersFormat()}', 
                        '{objectivesScore.FixNumbersFormat()}',
                        '{(saveType == SaveTypes.Save ? "1" : "")}',
                        '{empTrainingPlan.FixSingleQuotes()}'
                        )";
                result += oracleacess.ExecuteNonQuery(SQL);
                Extensions.WriteLog($"INSERT INTO ASS_EMP_HEADER (result: {result}, ID: {guid})");
            }
            else
            {
                SQL = $@"UPDATE ASS_EMP_HEADER SET 
                        ASSESSOR_NAME = '{assessorName}', 
                        RESULT = '{totalPerformanceResult.FixNumbersFormat()}', 
                        SAVE_TYPE = '{saveType.ToString().ToUpper()}', 
                        NYO_COMMENT = '{notes.FixSingleQuotes()}', 
                        COMP_RESULT = '{comptencesScore.FixNumbersFormat()}', 
                        OBJ_RESULT = '{objectivesScore.FixNumbersFormat()}', 
                        EMP_FINAL = {(saveType == SaveTypes.Save ? 1 : 0)}, 
                        EMP_MEET = '', 
                        EMP_APPROVE = '', 
                        EMP_REASON = '',
                        DEV_TRN_PLAN = '{empTrainingPlan.FixSingleQuotes()}'
                    Where 
                        EMP_NO = {emp_code} 
                        AND YEAR_NO = {year} 
                        AND VER_NO = {version}";
                result += oracleacess.ExecuteNonQuery(SQL);
                Extensions.WriteLog($"UPDATE ASS_EMP_HEADER (SQL: {SQL}, result: {result}, ID: {guid})");
            }

            // delete old objectives
            SQL = $"DELETE ASS_EMP_OBJECTIVES WHERE EMP_NO = {emp_code} AND YEAR_NO = {year} AND VER_NO = {version}";
            result += oracleacess.ExecuteNonQuery(SQL);
            Extensions.WriteLog($"DELETE ASS_EMP_OBJECTIVES (result: {result}, ID: {guid})");

            // insert new objectives
            if (dtObjectives != null && dtObjectives.Rows.Count > 0)
                foreach (DataRow dr in dtObjectives.Rows)
                {
                    string obj_seq = dr["obj_seq"].ToString();
                    string obj_desc = dr["obj_desc"].ToString();
                    string obj_weight = dr["obj_weight"].ToString();
                    string obj_actual = dr["obj_actual"].ToString();
                    string obj_notes = dr["obj_notes"].ToString();
                    string develop_points = dr["develop_points"].ToString();
                    string train_prog = dr["train_prog"].ToString();
                    SQL = $@"INSERT INTO ASS_EMP_OBJECTIVES (
                            EMP_NO, 
                            YEAR_NO, 
                            VER_NO, 
                            OBJ_SEQ, 
                            OBJ_DESC, 
                            OBJ_WEIGHT, 
                            OBJ_ACTUAL, 
                            OBJ_NOTES, 
                            DEVELOP_POINTS, 
                            TRAIN_PROG
                            ) 
                        VALUES ( 
                            {emp_code}, 
                            {year}, 
                            {version}, 
                            {obj_seq}, 
                            '{obj_desc.FixSingleQuotes()}', 
                            '{obj_weight}', 
                            '{obj_actual}', 
                            '{obj_notes.FixSingleQuotes()}', 
                            '{develop_points.FixSingleQuotes()}', 
                            '{train_prog.FixSingleQuotes()}'
                            )";
                    result += oracleacess.ExecuteNonQuery(SQL);
                    Extensions.WriteLog($"INSERT INTO ASS_EMP_OBJECTIVES (result: {result}, ID: {guid})");
                }


            // delete old Comptences
            SQL = $"DELETE ASS_EMP_COMPT WHERE EMP_NO = {emp_code} AND YEAR_NO = {year} AND VER_NO = {version}";
            result += oracleacess.ExecuteNonQuery(SQL);
            Extensions.WriteLog($"DELETE ASS_EMP_COMPT (result: {result}, ID: {guid})");

            //save Comptences data
            if (dtComptences != null && dtComptences.Rows.Count > 0)
            {
                foreach (DataRow dr in dtComptences.Rows)
                {
                    string comp_code = dr["comp_code"].ToString();
                    string comp_result = dr["comp_result"].ToString();
                    string develop_points = dr["develop_points"].ToString();
                    string train_prog = dr["train_prog"].ToString();

                    SQL = $@"INSERT INTO ASS_EMP_COMPT (
                            EMP_NO, 
                            YEAR_NO, 
                            VER_NO, 
                            COMP_CODE, 
                            COMP_RESULT, 
                            DEVELOP_POINTS, 
                            TRAIN_PROG
                            ) 
                        VALUES ( 
                            {emp_code}, 
                            {year}, 
                            {version}, 
                            {comp_code}, 
                            '{comp_result}', 
                            '{develop_points.FixSingleQuotes()}', 
                            '{train_prog.FixSingleQuotes()}'
                            )";
                    result += oracleacess.ExecuteNonQuery(SQL);
                    Extensions.WriteLog($"INSERT INTO ASS_EMP_COMPT (result: {result}, ID: {guid})");
                }
            }


            // deleted old next year objectives operational
            SQL = $"Delete ASS_EMP_NEXT_OBJ Where ( emp_no = {emp_code} and year_no = {year} and  ver_no = {version} and obj_notes  = 'O')  ";
            result += oracleacess.ExecuteNonQuery(SQL);
            Extensions.WriteLog($"Delete ASS_EMP_NEXT_OBJ (result: {result}, ID: {guid})");

            //save next year objectives operational
            if (dtNextYearOperationalObjectives != null && dtNextYearOperationalObjectives.Rows.Count > 0)
            {
                foreach (DataRow dr in dtNextYearOperationalObjectives.Rows)
                {
                    string obj_seq = dr["obj_seq"].ToString();
                    string obj_desc = dr["obj_desc"].ToString();
                    string obj_weight = dr["obj_weight"].ToString();
                    string obj_quart = dr["obj_quart"].ToString();
                    string obj_actual = "0";

                    if (string.IsNullOrWhiteSpace(obj_desc) && string.IsNullOrWhiteSpace(obj_weight) && string.IsNullOrWhiteSpace(obj_quart))
                        continue;

                    SQL = $@"INSERT INTO ASS_EMP_NEXT_OBJ  
                            ( 
                                emp_no, 
                                year_no, 
                                ver_no, 
                                obj_seq, 
                                obj_desc, 
                                obj_weight, 
                                obj_actual, 
                                obj_notes, 
                                quart  
                            )  
                            VALUES 
                            (  
                                '{emp_code}', 
                                '{year}', 
                                '{version}',
                                '{obj_seq}',
                                '{obj_desc.FixSingleQuotes()}', 
                                '{obj_weight}', 
                                '{obj_actual}', 
                                'O', 
                                '{obj_quart}'
                            )";
                    result += oracleacess.ExecuteNonQuery(SQL);
                    Extensions.WriteLog($"INSERT INTO ASS_EMP_NEXT_OBJ (result: {result}, ID: {guid})");
                }
            }


            // deleted old next year objectives developmental
            SQL = $"Delete ASS_EMP_NEXT_OBJ Where ( emp_no = {emp_code} and year_no = {year} and  ver_no = {version} and obj_notes  = 'D')  ";
            result += oracleacess.ExecuteNonQuery(SQL);
            Extensions.WriteLog($"Delete ASS_EMP_NEXT_OBJ (result: {result}, ID: {guid})");

            //save next year objectives developmental
            if (dtNextYearDevelopmentalObjectives != null && dtNextYearDevelopmentalObjectives.Rows.Count > 0)
            {
                foreach (DataRow dr in dtNextYearDevelopmentalObjectives.Rows)
                {
                    string obj_seq = dr["obj_seq"].ToString();
                    string obj_desc = dr["obj_desc"].ToString();
                    string obj_weight = dr["obj_weight"].ToString();
                    string obj_quart = dr["obj_quart"].ToString();
                    string obj_actual = "0";

                    if (string.IsNullOrWhiteSpace(obj_desc) && string.IsNullOrWhiteSpace(obj_weight) && string.IsNullOrWhiteSpace(obj_quart))
                        continue;

                    SQL = $@"INSERT INTO ASS_EMP_NEXT_OBJ  
                            ( 
                                emp_no, 
                                year_no, 
                                ver_no, 
                                obj_seq, 
                                obj_desc, 
                                obj_weight, 
                                obj_actual, 
                                obj_notes, 
                                quart  
                            )  
                            VALUES 
                            (  
                                '{emp_code}', 
                                '{year}', 
                                '{version}',
                                '{obj_seq}',
                                '{obj_desc.FixSingleQuotes()}', 
                                '{obj_weight}', 
                                '{obj_actual}', 
                                'D', 
                                '{obj_quart}'
                            )";
                    result += oracleacess.ExecuteNonQuery(SQL);
                    Extensions.WriteLog($"INSERT INTO ASS_EMP_NEXT_OBJ (result: {result}, ID: {guid})");
                }
            }

            oracleacess.Commit();
        }
        catch (Exception ex)
        {
            oracleacess.Rollback();
            result = 0;
            ex.LogError();
        }
        return result > 0;
    }

    #endregion
}