﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;


public partial class Survey_Survey_FormPage : System.Web.UI.Page
{
    AppService ser = new AppService();
    
    DataTable DsT = new DataTable();
    DataTable dt = new DataTable();

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!IsPostBack)
            {
                if (Session["Logged_User"] == null)
                {
                    Response.Redirect("~/Default.aspx");
                }
                lbl_emp_name.Text = Session["SU_EMP_NAME"].ToString();
                lbl_emp_code.Text = Session["SU_EMP_NO"].ToString();
                lbl_job_name.Text = Session["SU_JOB_NAME"].ToString();
                int save_status = ser.Check_Saved_Survey(lbl_emp_code.Text.Trim(), Session["SU_YEAR"].ToString());
                if (save_status == 0)
                {
                    Session["STATUS"] = "SAVE";
                    lbl_rec_date.Text = "";
                    txt_manager.Text = Session["Logged_User"].ToString();
                    txt_year.Text = Session["SU_YEAR"].ToString();
                }
                else
                {
                    Session["STATUS"] = "UPDATE";
                    Get_Data_Survey();
                }

             if( Session["SU_LEVEL"].ToString().Trim() != "2" )
            {
                View_Only();
            }

            }
        }
        catch
        { }
    }
    private void View_Only()
    {
        ddl_q1.Enabled = false;
        ddl_q2.Enabled = false;
        ddl_q3.Enabled = false;
        ddl_q4.Enabled = false;
        ddl_q5.Enabled = false;
        ddl_q6.Enabled = false;
        ddl_q7.Enabled = false;
        ddl_q8.Enabled = false;
        ddl_q9.Enabled = false;
        ddl_q10.Enabled = false;

        btn_save.Visible = false;
    }
    protected void btn_save_Click(object sender, EventArgs e)
    {
        
        try
        {
            if (Session["STATUS"].ToString().Trim() == "SAVE" && Session["SU_LEVEL"].ToString().Trim() == "2")
            {
                Check_And_Calc_Data();
                int save_ = ser.Save_Survey(Session["SU_YEAR"].ToString() , lbl_emp_code.Text, lbl_emp_name.Text, lbl_job_name.Text, ddl_q1.SelectedValue.ToString(), ddl_q2.SelectedValue.ToString(), ddl_q3.SelectedValue.ToString(), ddl_q4.SelectedValue.ToString(), ddl_q5.SelectedValue.ToString(), ddl_q6.SelectedValue.ToString(), ddl_q7.SelectedValue.ToString(), ddl_q8.SelectedValue.ToString(), ddl_q9.SelectedValue.ToString(), ddl_q10.SelectedValue.ToString(), txt_result.Text, Session["Logged_User"].ToString());
                if (save_ == 1)
                { 
                    Alert.Show(" data has been saved successfully "); 
                    return;
                    Session["STATUS"] = "UPDATE";
                }
                else
                { Alert.Show(" Error in saving please check your inputs "); return; }
            }
            else
              if (Session["STATUS"].ToString().Trim() == "UPDATE" && Session["SU_LEVEL"].ToString().Trim() == "2" )
            {
                Check_And_Calc_Data();
                int delete_ = ser.Delete_Survey_Data(lbl_emp_code.Text.Trim(), Session["SU_YEAR"].ToString());
                int save_ = ser.Save_Survey(Session["SU_YEAR"].ToString(), lbl_emp_code.Text, lbl_emp_name.Text, lbl_job_name.Text, ddl_q1.SelectedValue.ToString(), ddl_q2.SelectedValue.ToString(), ddl_q3.SelectedValue.ToString(), ddl_q4.SelectedValue.ToString(), ddl_q5.SelectedValue.ToString(), ddl_q6.SelectedValue.ToString(), ddl_q7.SelectedValue.ToString(), ddl_q8.SelectedValue.ToString(), ddl_q9.SelectedValue.ToString(), ddl_q10.SelectedValue.ToString(), txt_result.Text, Session["Logged_User"].ToString());
                if (save_ == 1)
                { 
                    Alert.Show(" data has been Updated successfully "); return;
                }
                else
                { Alert.Show(" Error in Updating please check your inputs "); return; }
            }
            //else if( Session["SU_LEVEL"].ToString().Trim() != "2" )
            //{
            //    View_Only();
            //}
        }
        catch
        {
 
        }
    }

    private void Get_Data_Survey()
    {
        try
        {

            DsT = ser.Get_Survey_Data(lbl_emp_code.Text.Trim(), Session["SU_YEAR"].ToString().Trim());
            dt = DsT;
            foreach (DataRow var in dt.Rows)
            {
               
                txt_year.Text = var[0].ToString();
                lbl_emp_code.Text = var[1].ToString();
                lbl_emp_name.Text = var[2].ToString();
                lbl_job_name.Text = var[3].ToString();
                lbl_rec_date.Text = var[4].ToString();
                //***********************************************
                ddl_q1.ClearSelection();
                ddl_q1.Items.FindByValue(var[5].ToString()).Selected = true;


                ddl_q2.ClearSelection();
                ddl_q2.Items.FindByValue(var[6].ToString()).Selected = true;


                ddl_q3.ClearSelection();
                ddl_q3.Items.FindByValue(var[7].ToString()).Selected = true;


                ddl_q4.ClearSelection();
                ddl_q4.Items.FindByValue(var[8].ToString()).Selected = true;


                ddl_q5.ClearSelection();
                ddl_q5.Items.FindByValue(var[9].ToString()).Selected = true;

                ddl_q6.ClearSelection();
                ddl_q6.Items.FindByValue(var[10].ToString()).Selected = true;

                ddl_q7.ClearSelection();
                ddl_q7.Items.FindByValue(var[11].ToString()).Selected = true;

                ddl_q8.ClearSelection();
                ddl_q8.Items.FindByValue(var[12].ToString()).Selected = true;

                ddl_q9.ClearSelection();
                ddl_q9.Items.FindByValue(var[13].ToString()).Selected = true;

                ddl_q10.ClearSelection();
                ddl_q10.Items.FindByValue(var[14].ToString()).Selected = true;

                txt_result.Text = " % " + var[15].ToString();
                txt_top_result.Text = " % " + var[15].ToString();

                txt_manager.Text = var[16].ToString();

            }

        }
        catch
        {

        }
    }

    protected void btn_check_Click(object sender, EventArgs e)
    {
        Check_And_Calc_Data();
    }

    private void Check_And_Calc_Data()
    {
        if (ddl_q1.SelectedIndex == 0 || ddl_q2.SelectedIndex == 0 || ddl_q3.SelectedIndex == 0 ||
            ddl_q4.SelectedIndex == 0 || ddl_q5.SelectedIndex == 0 || ddl_q6.SelectedIndex == 0 ||
            ddl_q7.SelectedIndex == 0 || ddl_q8.SelectedIndex == 0 ||
            ddl_q9.SelectedIndex == 0 || ddl_q10.SelectedIndex == 0)
        {
            Alert.Show(" Please Answer all questions .... !! ");
            return;
        }
        else
        {
            double result_sum = double.Parse(ddl_q1.SelectedValue.ToString()) + double.Parse(ddl_q2.SelectedValue.ToString()) + double.Parse(ddl_q3.SelectedValue.ToString()) +
                double.Parse(ddl_q4.SelectedValue.ToString()) + double.Parse(ddl_q5.SelectedValue.ToString()) + double.Parse(ddl_q6.SelectedValue.ToString()) +
                double.Parse(ddl_q7.SelectedValue.ToString()) + double.Parse(ddl_q8.SelectedValue.ToString()) +
                double.Parse(ddl_q9.SelectedValue.ToString()) + double.Parse(ddl_q10.SelectedValue.ToString());
            double result = ( result_sum / 10 ) * 100 ;
            txt_result.Text = Math.Round( result , 2)  + "";
            txt_top_result.Text = Math.Round(result, 2) + "";
        }
    }
}
