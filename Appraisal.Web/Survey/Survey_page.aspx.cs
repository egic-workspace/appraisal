﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;


public partial class Survey_Survey_page : System.Web.UI.Page
{
    AppService ser = new AppService();
    
    DataTable dt = new DataTable();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Session["Logged_User"] == null)
            {
                Response.Redirect("~/Default.aspx");
            }
            //****************************
            TXT_OPEN_YEAR.Text = ser.Get_Survey_Open_Year().ToString();
            //****************************
            try
            {
                Fill_Grid_First_Level();
            }
            catch
            {

            }
        }
    }

    private void Fill_Grid_ALL()
    {
        try
        {
            //Session["EMP_NO"] = "1395";
            Clear_Grids();
            
            string EMP_NO = Session["EMP_NO"].ToString();
            // dt = ser.Bind_ENTER_EMP_GRID(EMP_NO);
            dt = ser.Bind_ENTER_SURVEY_EMP_GRID(EMP_NO, TXT_OPEN_YEAR.Text);
            //GridView1.DataSource = null;
            GridView1.DataSource = dt;
            GridView1.DataBind();
        }
        catch
        {

        }
    }
    private void Fill_Grid_First_Level()
    {
        try
        {
            // Session["EMP_NO"] = "1395";
            Clear_Grids();
            
            string EMP_NO = Session["EMP_NO"].ToString();
            // dt = ser.Bind_ENTER_EMP_GRID(EMP_NO);
            dt = ser.Bind_ENTER_EMP_SURVEY_FRST_LEVEL_GRID(EMP_NO, TXT_OPEN_YEAR.Text);
            //GridView1.DataSource = null;
            GridView1.DataSource = dt;
            GridView1.DataBind();
        }
        catch
        {

        }
    }
    private void Clear_Grids()
    {
        GridView1.DataSource = null;
        GridView1.DataBind();
    }
    protected void btn_all_levels_Click(object sender, EventArgs e)
    {
        try
        {
            Fill_Grid_ALL();
        }
        catch
        {

        }
    }
    protected void btn_frst_level_Click(object sender, EventArgs e)
    {
        try
        {
            Fill_Grid_First_Level();
        }
        catch
        {

        }
    }
    protected void btn_calcAll_Click(object sender, EventArgs e)
    {

    }
    protected void btn_calcOS_Click(object sender, EventArgs e)
    {

    }
    protected void btn_calcOSAll_Click(object sender, EventArgs e)
    {

    }
    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {

    }
    protected void GridView1_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            Session["SU_EMP_NAME"] = ((Label)GridView1.SelectedRow.FindControl("lblemp_name")).Text;
            Session["SU_EMP_NO"] = ((Label)GridView1.SelectedRow.FindControl("lblemp_no")).Text;
            Session["SU_JOB_NAME"] = ((Label)GridView1.SelectedRow.FindControl("lbljob_name")).Text;
            Session["SU_LEVEL"] = ((Label)GridView1.SelectedRow.FindControl("lbllevel")).Text;
            Session["SU_YEAR"] = TXT_OPEN_YEAR.Text.Trim();
            if (TXT_OPEN_YEAR.Text.Trim() == "")
            {
                Alert.Show(" Sorry There are no open years ");
                return;
            }
            else
            {
                Response.Redirect("~/Survey/Survey_FormPage.aspx");
            }
        }
        catch
        {

        }
    }
    protected void GridView1_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
    {

    }
    protected void btn_calc_Click(object sender, EventArgs e)
    {

    }
}
