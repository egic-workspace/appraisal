﻿<%@ Page Language="C#" MasterPageFile="~/Main_Master_Page.master" AutoEventWireup="true" CodeFile="Survey_page.aspx.cs" Inherits="Survey_Survey_page" Title="استقصاء المديرين" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <table style="width: 100%;" align="center">
 <tr>
    <td style="background-color:#e6f1f8; height: 2px; width: 70%;" >
    <span  style="font-size:11pt; font-weight:bold; color:#000080; font-family:Courier New;">
        &nbsp; ادخال الإستقصاء للموظفين </span>
    </td></tr>
 </table>
    <p>
    </p>
    <p>
         <table align="center" dir="rtl" style="width: 98%; height: 98%; float: right">
        <tr>
            <td>
                &nbsp;</td>
            <td colspan="5">
    <span  style="font-size:11pt; font-weight:bold; color:#000000; font-family:Courier New;">
            <asp:Label ID="Label63" runat="server" Font-Bold="True" Text="السنة المفتوحة للإستقصاء" 
                    Font-Size="16pt" Font-Names="Arial" ForeColor="Black" Width="205px"></asp:Label>
                <asp:TextBox ID="TXT_OPEN_YEAR" runat="server" BackColor="#FF9933" 
                    BorderStyle="Solid" Font-Bold="True" Font-Size="15pt" Width="150px" 
                    Enabled="False"></asp:TextBox>
        </span>
            </td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td align="center">
            <asp:Button ID="btn_all_levels" runat="server" BackColor="#3366FF" Font-Bold="True" 
                Font-Size="15pt" ForeColor="White" Text="جميع المستويات " Width="300px" 
                onclick="btn_all_levels_Click" />
            </td>
            <td align="center" colspan="4">
            <asp:Button ID="btn_frst_level" runat="server" BackColor="#3366FF" Font-Bold="True" 
                Font-Size="15pt" ForeColor="White" Text="المستوي الأول" Width="300px" 
                onclick="btn_frst_level_Click" />
            </td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td align="center" colspan="7">
            <asp:GridView ID="GridView1" runat="server" 
                EmptyDataText="ليس لديك بيانات موظفين مسجلة" 
                    Caption="الموظفين المطلوب عمل استقصاء لهم" AutoGenerateColumns="False" 
                CaptionAlign="Top" onselectedindexchanged="GridView1_SelectedIndexChanged" 
                CssClass="mGrid" PagerStyle-CssClass="pgr" 
                AlternatingRowStyle-CssClass="alt" AutoGenerateSelectButton="True" 
                DataKeyNames="EMP_NO" onpageindexchanging="GridView1_PageIndexChanging" Width="800px" 
                        Font-Names="Arial" Font-Size="14pt" 
                    onselectedindexchanging="GridView1_SelectedIndexChanging">
                
                <Columns>
                        
                        <asp:TemplateField HeaderText="كود الموظف" SortExpression="Value">
                            <ItemTemplate>
                                <asp:Label ID="lblemp_no" runat="server" Text='<%# Bind("EMP_NO") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                     
                             <asp:TemplateField HeaderText="اسم الموظف">
                          <ItemTemplate>
                                <asp:Label ID="lblemp_name" runat="server" Text='<%# Bind("EMP_NAME") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        
                        <asp:TemplateField HeaderText="الوظيفة">
                         <ItemTemplate>
                                <asp:Label ID="lbljob_name" runat="server" Text='<%# Bind("JOB_NAME") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        
                        <asp:BoundField DataField="emp_company" HeaderText="الشركة" />
                        
                            <asp:TemplateField HeaderText="المستوي" Visible="false">
                         <ItemTemplate>
                                <asp:Label ID="lbllevel" runat="server" Text='<%# Bind("LEVEL") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                
                <PagerStyle />
                <AlternatingRowStyle />
            </asp:GridView>
                </td>
        </tr>
        <tr>
            <td style="height: 15px">
                &nbsp;</td>
            <td style="height: 15px" align="center" colspan="5">
                &nbsp;</td>
            <td style="height: 15px" align="center">
                &nbsp;</td>
        </tr>
        <tr>
            <td style="height: 15px">
                </td>
            <td style="height: 15px" align="center">
                &nbsp;</td>
            <td style="height: 15px" align="center">
                &nbsp;</td>
            <td style="height: 15px" align="center">
                &nbsp;</td>
            <td style="height: 15px" align="center">
                &nbsp;</td>
            <td style="height: 15px" align="center">
                &nbsp;</td>
            <td style="height: 15px" align="center">
                &nbsp;</td>
        </tr>
        <tr>
            <td style="height: 15px">
                &nbsp;</td>
            <td style="height: 15px" align="center" colspan="5">
                &nbsp;</td>
            <td style="height: 15px" align="center">
                &nbsp;</td>
        </tr>
        <tr>
            <td align="center" colspan="6" rowspan="2">
                
&nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td colspan="4">
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td colspan="4">
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td colspan="4">
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td colspan="4">
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td colspan="4">
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
    </table><br />
    </p>
</asp:Content>

