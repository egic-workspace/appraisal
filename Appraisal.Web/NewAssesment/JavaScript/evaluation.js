﻿function edit_row(no) {
    document.getElementById("edit_button" + no).style.display = "none";
    document.getElementById("save_button" + no).style.display = "inline";

    var goal = document.getElementById("goal_row" + no);
    var weight = document.getElementById("weight_row" + no);
    var grade = document.getElementById("grade_row" + no);
    var reason = document.getElementById("reason_row" + no);

    var goal_data = goal.innerHTML;
    var weight_data = weight.innerHTML;
    var grade_data = grade.innerHTML;
    var reason_data = reason.innerHTML;

    goal.innerHTML = "<input type='text' id='goal_text" + no + "' value='" + goal_data + "'>";
    weight.innerHTML = "<input type='number' id='weight_text" + no + "' value='" + weight_data + "'>";
    grade.innerHTML = "<input type='number' id='grade_text" + no + "' value='" + grade_data + "'>";
    reason.innerHTML = "<input type='text' id='reason_text" + no + "' value='" + reason_data + "'>";
}

function save_row(no) {

    
    var goal_val = document.getElementById("goal_text" + no).value;
    var weight_val = document.getElementById("weight_text" + no).value;
    var grade_val = document.getElementById("grade_text" + no).value;
    var reason_val = document.getElementById("reason_text" + no).value;

    if (!goal_val || !weight_val || !grade_val || !reason_val) {
        bootbox.alert("please fill all fields");
        return;
    }

    document.getElementById("goal_row" + no).innerHTML = goal_val;
    document.getElementById("weight_row" + no).innerHTML = weight_val;
    document.getElementById("grade_row" + no).innerHTML = grade_val;
    document.getElementById("reason_row" + no).innerHTML = reason_val;

    document.getElementById("edit_button" + no).style.display = "inline";
    document.getElementById("save_button" + no).style.display = "none";
}

function delete_row(no) {
    document.getElementById("row" + no + "").outerHTML = "";
}

function add_row() {
    var new_goal = document.getElementById("new_goal").value;
    var new_weight = document.getElementById("new_weight").value;
    var new_grade = document.getElementById("new_grade").value;
    var new_reason = document.getElementById("new_reason").value;

    if (!new_goal || !new_weight || !new_grade || !new_reason) {
        bootbox.alert("please fill all fields");
        return;
    }
    else {
        var table = document.getElementById("data_table");
        var table_len = (table.rows.length) - 1;
        var row = table.insertRow(table_len).outerHTML = "<tr id='row" +
            table_len +
            "'><td id='goal_row"
            + table_len + "'>"
            + new_goal +
            "</td><td id='weight_row"
            + table_len
            + "'>"
            + new_weight
            + "</td><td id='grade_row"
            + table_len + "'>"
            + new_grade
            + "</td><td id='reason_row"
            + table_len
            + "'>"
            + new_reason
            + "</td><td><input type='button' id='edit_button"
            + table_len
            + "' value='Edit' class='btn btn-warning' onclick='edit_row("
            + table_len
            + ")'> <input type='button' id='save_button"
            + table_len
            + "' value='Save' class='btn btn-primary' onclick='save_row("
            + table_len
            + ")'> <input type='button' value='Delete' class='btn btn-danger' onclick='delete_row("
            + table_len
            + ")'></td></tr>";

        document.getElementById("new_goal").value = "";
        document.getElementById("new_weight").value = "";
        document.getElementById("new_grade").value = "";
        document.getElementById("new_reason").value = "";

        document.getElementById("save_button" + table_len).style.display = "none";

        document.getElementById("save_goals").style.display = "inline";
    }
}

    function save_goals()
    {
        var sum_weight = sum_weightd();
        if (sum_weight != 100)
        {
            document.getElementById("divresult").style.display = "inline-block";
            document.getElementById("divresult").classList.remove('alert-success');
            document.getElementById("divresult").classList.add('alert-danger');
            document.getElementById("divresult").innerHTML = "مجموع الاوزان لابد ان يكون 100";
            return;
        }

        var table = document.getElementById("data_table");
        var upperbound = table.rows.length - 1;
        var objectives = [];
        for (var row = 1; row < upperbound; row++) {
            var goal = document.getElementById("goal_row" + row).innerHTML;
            var weight = document.getElementById("weight_row" + row).innerHTML;
            var grade = document.getElementById("grade_row" + row).innerHTML;
            var reason = document.getElementById("reason_row" + row).innerHTML;
            objectives.push({ 'OBJ_DESC': goal, 'OBJ_WEIGHT': weight, 'OBJ_ACTUAL': grade, 'OBJ_NOTES': reason });
          
        }
        $.ajax({
            url: '/EmployeeDetails/addObjective',
            type: "POST",
            async: true,
            dataType: "json",
            data: { objectives: objectives },
            success: function (response) {
                if (response != null && response.success) {

                    document.getElementById("divresult").style.display = "inline-block";
                    document.getElementById("divresult").classList.remove('alert-danger');
                    document.getElementById("divresult").classList.add('alert-success');
                    document.getElementById("divresult").innerHTML = response.responseText;
                    
                   // document.getElementById("save_goals").style.display = "none";
                   // deleterow('data_table');
                    // removeLatestCol();
                    document.getElementById("goals_div").style.display = "none";

                } else {
                    document.getElementById("divresult").style.display = "inline-block";
                    document.getElementById("divresult").classList.remove('alert-success');
                    document.getElementById("divresult").classList.add('alert-danger');
                    document.getElementById("divresult").innerHTML =  response.responseText;
                }
            },
            error: function (response) {
                alert("error!");  // 
            }

        });
      
    }

    function sum_weightd() {
        var table = document.getElementById("data_table");

        // -1 because last row is the 'add' row. 
        // But see my comment above on a potential issue using table.length
        var upperbound = table.rows.length - 1;

        var sum_weight = 0;
        var cur_weight_field = 0;
        // start at 1 because first inserted row is inserted when table length is already 1.
        for (var row = 1; row < upperbound; row++) {
            cur_weight_field = document.getElementById("weight_row" + row);
            // null check as some of the fields may have been deleted.
            if (cur_weight_field != null) {
                //null/empty check as some ages may not have been filled in.
                if (cur_weight_field.innerHTML != null && cur_weight_field.innerHTML != "") {
                    //(+val) to convert text to number.
                    sum_weight = sum_weight + (+cur_weight_field.innerHTML);
                }
            }
        }
        return sum_weight;
    }
    function deleterow(tableID) {
        var table = document.getElementById(tableID);
        var rowCount = table.rows.length;

        table.deleteRow(rowCount - 1);
    }

    function removeLatestCol() {
        var latest_row = $("data_table tr:last");
        var cols = $("td", latest_row);

        if (cols.length == 1) {
            latest_row.remove();
            return;
        }

        var latest_col = cols.filter(":last");
        latest_col.prev().attr("colspan", (latest_col.prev().attr("colspan") || 1) + (latest_col.attr("colspan") || 1));
        latest_col.remove()
    }