﻿<%@ Page Language="C#" MasterPageFile="~/Main_Master_Page.master" AutoEventWireup="true" CodeFile="~/NewAssesment/EmpAssessmentAgreement.aspx.cs" Inherits="NewAssesment_EmpAssessmentAgreement" Title="" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="~/MyControles/Message_Box.ascx" TagName="MessageBox" TagPrefix="mb" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<link type="text/css" rel="stylesheet" href="../CSS/bootstrap.min.css"/>
 <script type="text/javascript" src="../JavaScript/jquery.min.js"></script>
 <script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script type="text/javascript">
        $(function () {
            updateControles()

            $(document).on('change', '[name$="meeting_rb"]', function () {
                updateControles();
            });

            $(document).on('change', '[name$="agree_rb"]', function () {
                updateControles();
                if ($(this).attr('id').indexOf('rbNotAgree') >= 0) {
                    $('[id$="txtNotes"]').focus();
                }
                else {
                    $('[id$="txtNotes"]').val('');
                }
            });

            $(document).on('blur', '[id$="txtNotes"]', function () {
                updateControles();
            });

            $("#btnShow").click(function () {
                showModal();
            });
        });
        function showModal() {
            $("#myModal").modal('show');
        }
        function updateControles() {
            var agree_rb = $('[name$="agree_rb"]:checked'),
                rbMeeting = $('[name$="meeting_rb"]:checked'),
                txtNotes = $('[id$="txtNotes"]'),
                btnSave = $('[name$="btn_save"]'),
                lblReasonRequired = $('#lblReasonRequired');
            
            if (agree_rb.length > 0 && agree_rb.attr('id').indexOf('rbNotAgree') >= 0 ) {
                lblReasonRequired.show();
            }
            else {
                lblReasonRequired.hide();
            }
            
            //if (rbMeeting.length == 0 || agree_rb.length == 0 ) {
            //    btnSave.prop('disabled', true);
            //}
            //else {
            //    btnSave.prop('disabled', false);
            //}
        }
    </script>

        <table style="width: 100%;" align="center">
            <tr>
                <td style="background-color:#71625F; height: 2px; width: 70%;" >
                    <span  style="font-size:11pt; font-weight:bold; color:#FFFFFF; font-family:Arial;">
                        &nbsp; الموافقة علي أهدافي السنوية
                    </span>
                </td>
            </tr>
        </table>
    <table align="center" dir="rtl" style="width: 98%; height: 98%; float: right">
        
        <tr>
            <td align="center" colspan="4">
                <span style="font-size:11pt; font-weight:bold; color:#000000; font-family:Courier New;" 
                    __designer:mapid="86">
                                <span style="font-size:11pt; font-weight:bold; color:#000000; font-family:Courier New; " 
                    __designer:mapid="87">
                                <span style="font-size:11pt; font-weight:bold; color:#000000; font-family:Courier New; " 
                    __designer:mapid="88">
                                <asp:Label ID="lbl_version" runat="server" Font-Bold="True" Font-Names="Arial" 
                                    Font-Size="9pt" ForeColor="Blue" Text="الاصدار" 
                    Visible="False"></asp:Label>
                                </span></span></span>
                            </td>
        </tr>
    </table>
    <table align="center" dir="rtl" style="width: 98%; height: 98%; float: right" runat="server" ID="tblObjectives">
        <tr>
            <td align="center" colspan="4">
                <asp:Panel 
                    ID="Objective_Pnl" 
                    class="panel panel-default" 
                    runat="server" 
                    Width="95%" 
                    GroupingText=" الأهداف&nbsp; &nbsp;Objectives " 
                    Font-Names="Arial" 
                    Font-Size="13pt">
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td align="center">
                &nbsp;</td>
            <td align="center">
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td colspan="4" align="center">
                <asp:Panel 
                    ID="pnlEmpAgreement" 
                    class="panel panel-default" 
                    runat="server" 
                    Width="95%" 
                    GroupingText=" موافقة الموظف&nbsp; &nbsp;Employee Agreement " 
                    Font-Names="Arial" 
                    Font-Size="13pt">
                    <table style="text-align:right; margin-right:30px;" align="right">
                        <tr>
                            <td colspan="2" style="font-size:13pt; font-weight:bold; font-family:Arial;">
                                هل تم إجتماع تحديث الأهداف النصف سنوي مع مديرك؟
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label>
                                    <asp:RadioButton runat="server" ID="rbMeetingDone" GroupName="meeting_rb" />
                                    تم
                                </label>
                            </td>
                            <td >
								<label>
                                    <asp:RadioButton runat="server" ID="rbMeetingNotDone" GroupName="meeting_rb" />
                                    لم يتم
                                </label>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <br />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" style="font-size:13pt; font-weight:bold; font-family:Arial;">
                                هل انت موافق على الأهداف السنوية والتي سيتم تقييمك السنوي بناءً عليها؟
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label>
                                    <asp:RadioButton runat="server" ID="rbAgree" GroupName="agree_rb" />
                                    موافق
                                </label>
                            </td>
                            <td >
								<label>
                                    <asp:RadioButton runat="server" ID="rbNotAgree" GroupName="agree_rb" />
                                    غير موافق
                                </label>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <br />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" style="font-size:13pt; font-weight:bold; font-family:Arial;">
                                تعليق 
                                <small id="lblReasonRequired" style="color:red;display:none;">
                                    (يجب ادخال سبب عدم الموافقة) - برجاء العلم ان هذا السبب سوف يتم ارسالة إلي المدير ليتم مناقشتك فيه
                                </small>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <asp:TextBox runat="server" ID="txtNotes" Columns="55" Rows="3" TextMode="MultiLine" MaxLength="500" style="resize: none;"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <br />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <br />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" align="center">
                                <asp:Button ID="btn_save" runat="server" BackColor="#F79222" Text="حفظ" class="btn btn-primary" Font-Bold="True" Font-Names="Arial" Font-Size="12pt"  onclick="btn_save_Click"  Width="150px" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <br />
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td align="center">
                &nbsp;</td>
            <td align="center">
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td>
                 &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
    </table>
    <table align="center" dir="rtl" style="width: 100%; float: right" runat="server" ID="tblNoObjectives" >
        <tr>
            <td align="center" colspan="4">
                <div class="alert alert-danger">
                    <strong style="font-size:x-large;">
                        <asp:Label runat="server" ID="lblNoObjectivesMessage" Text="لم يتم إرسال الأهداف من المدير بعد..."></asp:Label>
                    </strong>
                </div>
            </td>
        </tr>
    </table>
    <div class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog modal-sm">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h4 class="modal-title">نظام تقييم الموظفين</h4>
            </div>
            <div class="modal-body">
              <asp:Label ID="lblMessage" runat="server" Font-Bold="True" Font-Size="12pt" 
                    Font-Names="Arial" ForeColor="Red"></asp:Label>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
          </div>
        </div>
    </div>

</asp:Content>

