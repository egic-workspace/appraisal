﻿<%@ Page Language="C#" MasterPageFile="~/Main_Master_Page.master" AutoEventWireup="true" CodeFile="~/NewAssesment/dynamic_assesment.aspx.cs" Inherits="NewAssesment_dynamic_assesment" Title="" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="~/MyControles/Message_Box.ascx" TagName="MessageBox" TagPrefix="mb" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

  <script type="text/javascript" src="../JavaScript/jquery.min.js"></script>
    <script type="text/javascript" >
         function AutoExpand(txtbox) 
           {
           txtbox.style.height = "1px";
             txtbox.style.width = "350px";
            txtbox.style.height = (50 + txtbox.scrollHeight) + "px";
            var textLength = txtbox.length ;
            if(textLength > 30) 
            {  txtbox.style.fontSize =  "5px";  }
            else if(textLength > 20) 
            {  txtbox.style.fontSize =  "10px"; }
            else if(textLength > 10) 
            { txtbox.style.fontSize =  "15px"; }
             
           //txtbox.style.width = "400px";
           //  txtbox.style.width = (350 + txtbox.scrollWidth) + "px";
            //txtbox.style.width = 500 + "px";
            //txtbox.style.width = 100 + "%";
          //  txtbox.style.width = (100 + txtbox.scrollHeight) + "px";
//            elem = document.getElementById(txtbox); 
//          if (txtbox.clientHeight < txtbox.scrollHeight) 
//              alert("The element has a vertical scrollbar!"); 
//             else 
//              alert("The element doesn't have a vertical scrollbar."); 
             }
         function AutoExpand_comments(txtbox) 
           {
           if(txtbox!=null){
           txtbox.style.height = "1px";
           txtbox.style.height = (25 + txtbox.scrollHeight) + "px";
             }
             }
         function showModal() {
            $("#myModal").modal('show');
           }

        $(function () {
            $("#btnShow").click(function () {
                showModal();
            });
        });
    </script>
 
 <script type="text/javascript">
       function PrintPanel() {
     
           $('*[class^="btn btn-primary"]').hide(); 
           var type = document.getElementById("<%=lbltype.ClientID %>").innerHTML;
           var panel = document.getElementById("<%=Panel1.ClientID %>");
           var panel2 = document.getElementById("<%=Objective_Pnl.ClientID %>");
           var panel5 = document.getElementById("<%=Footerpnl.ClientID %>");

        var printWindow = window.open('', '', 'scrollbars=yes,  resizable=yes, width=800, height=700');
        printWindow.document.write('<html><head>');
        printWindow.document.write('<style>legend{font-size:18px!important;}</style>');
        printWindow.document.write('\<script type="text/javascript"> <%=_blah%> ><\/script>');
        printWindow.document.write('\<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"><\/script>');
        printWindow.document.write('\<script>$(function(){$("#print-page").find("fieldset legend").hide();})<\/script>');
        printWindow.document.write('<link rel="stylesheet" href="../CSS/bootstrap.min.css"/>');
        printWindow.document.write('</head><body dir="rtl"  id="print-page">');
        printWindow.document.write('<h1>نموذج تحديث الأهداف</h1>');
        printWindow.document.write('');
        
           if (type == "ObjectivesAndComptences" )
         {
        printWindow.document.write(panel.innerHTML);
            printWindow.document.write(panel2.innerHTML);
            printWindow.document.write(panel5.innerHTML); 
         }
           else if (type == "ObjectivesOnly" )
        {
            printWindow.document.write(panel.innerHTML);
            printWindow.document.write(panel2.innerHTML);
            printWindow.document.write(panel5.innerHTML); 
        }
           else if (type == "ComptencesOnly" )
        {
            printWindow.document.write(panel.innerHTML);
            printWindow.document.write(panel5.innerHTML); 
        }
        
        printWindow.document.write('</body></html>');
        printWindow.document.close();
        $('*[class^="btn btn-primary"]').show();
        
        setTimeout(function () {
        printWindow.print();
                 }, 500);
          return false;
       
    }
</script>
 <link type="text/css" rel="stylesheet" href="../CSS/bootstrap.min.css"/>
 <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
 <script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
 
    <table style="width: 100%;" align="center">
 <tr>
    <td style="background-color:#71625F; height: 2px; width: 70%;" >
    <span  style="font-size:11pt; font-weight:bold; color:#FFFFFF; font-family:Arial;">
        &nbsp; ادخال التقييم للموظفين </span>
    </td></tr>
 </table>
    <p>
         <table align="center" dir="rtl" style="width: 98%; height: 98%; float: right">
        <tr>
            <td align="center"colspan="4">
                <asp:Panel ID="Panel1" runat="server" class="panel panel-default" BorderStyle="Ridge" 
                    Width="90%" ScrollBars="Vertical">
                    <table align="center" style="width: 100%; float: right">
                        <tr>
                            <td align="right" style="padding-right: 20px;">
                                <span style="font-size:11pt; font-weight:bold; color:#000000; font-family:Courier New; ">
                                <asp:Label ID="Label18" runat="server" Font-Bold="True" Font-Names="Arial" 
                                    Font-Size="14pt" ForeColor="Black" Text="كود الموظف" Width="90px"></asp:Label>
                                <span style="font-size:11pt; font-weight:bold; color:#000000; font-family:Courier New; ">
                                <span style="font-size:11pt; font-weight:bold; color:#000000; font-family:Courier New;">
                                <span style="font-size:11pt; font-weight:bold; color:#000000; font-family:Courier New;">
                                <asp:Label ID="lbl_emp_no" runat="server" Font-Bold="True" Font-Names="Arial" 
                                    Font-Size="14pt" ForeColor="Blue" Text="1001"></asp:Label>
                                </span></span></span></span>
                            </td>
                            <td align="right" dir="rtl" style="padding-right: 33pt;">
                                <span style="font-size:11pt; font-weight:bold; color:#000000; font-family:Courier New; ">
                                <span style="font-size:11pt; font-weight:bold; color:#000000; font-family:Courier New;">
                                <span style="font-size:11pt; font-weight:bold; color:#000000; font-family:Courier New;">
                                <asp:Label ID="Label8" runat="server" Font-Bold="True" Font-Names="Arial" 
                                    Font-Size="14pt" ForeColor="Black" Text="اسم الموظف" Width="90px"></asp:Label>
                                <span style="font-size:11pt; font-weight:bold; color:#000000; font-family:Courier New; ">
                                <span style="font-size:11pt; font-weight:bold; color:#000000; font-family:Courier New;">
                                <span style="font-size:11pt; font-weight:bold; color:#000000; font-family:Courier New;">
                                <span style="font-size:11pt; font-weight:bold; color:#000000; font-family:Courier New;">
                                <span style="font-size:11pt; font-weight:bold; color:#000000; font-family:Courier New; ">
                                <asp:Label ID="lbl_emp_name" runat="server" Font-Bold="True" Font-Names="Arial" 
                                    Font-Size="14pt" ForeColor="Blue" Text="اسم الموظف"></asp:Label>
                                </span></span></span></span></span></span></span></span>
                            </td>
                            <td align="right">
                                <span style="font-size:11pt; font-weight:bold; color:#000000; font-family:Courier New; ">
                                <span style="font-size:11pt; font-weight:bold; color:#000000; font-family:Courier New;">
                                <span style="font-size:11pt; font-weight:bold; color:#000000; font-family:Courier New;">
                                <span style="font-size:11pt; font-weight:bold; color:#000000; font-family:Courier New;">
                                <span style="font-size:11pt; font-weight:bold; color:#000000; font-family:Courier New; ">
                                <asp:Label ID="Label10" runat="server" Font-Bold="True" Font-Names="Arial" 
                                    Font-Size="14pt" ForeColor="Black" Text="سنة التقييم" Width="80px"></asp:Label>
                                <span style="font-size:11pt; font-weight:bold; color:#000000; font-family:Courier New; ">
                                <span style="font-size:11pt; font-weight:bold; color:#000000; font-family:Courier New;">
                                <span style="font-size:11pt; font-weight:bold; color:#000000; font-family:Courier New;">
                                <span style="font-size:11pt; font-weight:bold; color:#000000; font-family:Courier New;">
                                <span style="font-size:11pt; font-weight:bold; color:#000000; font-family:Courier New; ">
                                <span style="font-size:11pt; font-weight:bold; color:#000000; font-family:Courier New;">
                                <span style="font-size:11pt; font-weight:bold; color:#000000; font-family:Courier New; ">
                                <asp:Label ID="lbl_year" runat="server" Font-Bold="True" Font-Names="Arial" 
                                    Font-Size="14pt" ForeColor="Blue" Text="1999"></asp:Label>
                                <span style="font-size:11pt; font-weight:bold; color:#000000; font-family:Courier New; ">
                                <span style="font-size:11pt; font-weight:bold; color:#000000; font-family:Courier New;">
                                <span style="font-size:11pt; font-weight:bold; color:#000000; font-family:Courier New;">
                                <span style="font-size:11pt; font-weight:bold; color:#000000; font-family:Courier New;">
                                <span style="font-size:11pt; font-weight:bold; color:#000000; font-family:Courier New; ">
                                <span style="font-size:11pt; font-weight:bold; color:#000000; font-family:Courier New;">
                                <span style="font-size:11pt; font-weight:bold; color:#000000; font-family:Courier New; ">
                                <span style="font-size:11pt; font-weight:bold; color:#000000; font-family:Courier New;">
                                <span style="font-size:11pt; font-weight:bold; color:#000000; font-family:Courier New; ">
                                <span style="font-size:11pt; font-weight:bold; color:#000000; font-family:Courier New; ">
                                <asp:Label ID="lbltype" runat="server" Font-Bold="True" Font-Names="Arial" 
                                    Font-Size="9pt" ForeColor="White"></asp:Label>
                                </span></span></span></span></span></span></span></span></span></span></span>
                                </span></span></span></span></span></span></span></span></span></span></span>
                            </td>
                        </tr>
                        
                    </table>
                    <br />
                    <br />
                    <br />
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td align="center" colspan="4">
                <span style="font-size:11pt; font-weight:bold; color:#000000; font-family:Courier New;" 
                    __designer:mapid="86">
                                <span style="font-size:11pt; font-weight:bold; color:#000000; font-family:Courier New; " 
                    __designer:mapid="87">
                                <span style="font-size:11pt; font-weight:bold; color:#000000; font-family:Courier New; " 
                    __designer:mapid="88">
                                <asp:Label ID="lbl_version" runat="server" Font-Bold="True" Font-Names="Arial" 
                                    Font-Size="9pt" ForeColor="Blue" Text="الاصدار" 
                    Visible="False"></asp:Label>
                                </span></span></span>
                            </td>
        </tr>
        <tr>
            <td align="center" colspan="4">
                <asp:Panel ID="Panel2" runat="server" class="panel panel-default" ScrollBars="Vertical" BorderStyle="Ridge" Width="90%">
                    <table align="center" style="width: 100%; float: right">
                        <tr>
                            <td align="center" style="width: 100%; float: right;">
                                <span style="font-size:11pt; font-weight:bold; color:#000000; font-family:Courier New; ">
                                <asp:Button ID="btn_save" runat="server" BackColor="#F79222" Text="حفظ مؤقت"
                                    class="btn btn-primary" Font-Bold="True" Font-Names="Arial" Font-Size="12pt" 
                                    onclick="btn_save_Click"  Width="150px" />
                                <span style="font-size:11pt; font-weight:bold; color:#000000; font-family:Courier New;">
                                <asp:Button ID="btn_send" runat="server" BackColor="#F79222" 
                                    class="btn btn-primary" Font-Bold="True" Font-Names="Arial" Font-Size="12pt" 
                                    onclick="btn_send_Click" Text="حفظ وارسال" Width="150px" />
                                <span dir="rtl" 
                                    style="font-size:11pt; font-weight:bold; color:#F79222; font-family:Courier New;">
                                <button runat="server" ID="btnPrint" Visible="false" class="btn btn-primary" onclick="PrintPanel()" style="font-weight: bold;  background-color: #F79222; font-family: Arial; font-size: 15px;" type="button" >
                                    طــباعة
                                </button>
                                
                                </span></span></span>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td align="center" colspan="4">
                <asp:Panel ID="Objective_Pnl" class="panel panel-default" 
                    runat="server" Width="95%" 
                    GroupingText=" الأهداف&nbsp; &nbsp;Objectives "  Font-Names="Arial" Font-Size="13pt">
                </asp:Panel>
            </td>
        </tr>
        
        <tr>
            <td>
                &nbsp;</td>
            <td align="center">
                &nbsp;</td>
            <td align="center">
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td align="center" colspan="4" dir="rtl">
                <asp:Panel ID="Footerpnl" runat="server" class="panel panel-default" BorderStyle="Ridge" 
                    Width="90%" Height="60px">
                    <table align="center" style="width: 100%; float: right">
                        <tr>
                            <td align="right">
                                <span 
                                    
                                    
                                    style="font-size:11pt; font-weight:bold; color:#000000; font-family:Courier New;" 
                                    dir="ltr">
                                <asp:Label ID="Label15" runat="server" Font-Bold="True" Font-Names="Arial" 
                                    Font-Size="16pt" ForeColor="Black" Text="توقيع الموظف"></asp:Label>
                                </span></td>
                            <td>
                                &nbsp;</td>
                            <td align="left" dir="ltr">
                                <span style="font-size:11pt; font-weight:bold; color:#000000; font-family:Courier New;">
                                <span style="font-size:11pt; font-weight:bold; color:#000000; font-family:Courier New;">
                                <span style="font-size:11pt; font-weight:bold; color:#000000; font-family:Courier New;" 
                                    dir="ltr">
                                <asp:Label ID="Label17" runat="server" Font-Bold="True" Font-Names="Arial" 
                                    Font-Size="16pt" ForeColor="Black" Text="توقيع المدير"></asp:Label>
                                </span></span></span></td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;</td>
                            <td>
                                &nbsp;</td>
                            <td>
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;</td>
                            <td>
                                &nbsp;</td>
                            <td>
                                &nbsp;</td>
                        </tr>
                    </table>
                    <br />
                    <br />
                    <br />
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td>
                 &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
    </table><br />
    </p>
    
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog modal-sm">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">نظام تقييم الموظفين</h4>
        </div>
        <div class="modal-body">
          <asp:Label ID="lblMessage" runat="server" Font-Bold="True" Font-Size="12pt" 
                Font-Names="Arial" ForeColor="Red"></asp:Label>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>
  
</asp:Content>

