﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;



public partial class NewAssesment_Ass_approve : System.Web.UI.Page
{
    AppService ser = new AppService();
    
    DataTable Dst = new DataTable();
    DataTable dt = new DataTable();
    // string Job_Code = "";
     string Year = "";
     string Version = "";

     protected void Page_Init(object sender, EventArgs e)
     {
         //Session["ASS_APPAISAL_TYPE"] 
         //Session["ASS_Comp_Weight"] 
         //Session["ASS_Obj_Weight"] 
         //Session["ASS_EMP_ID"] 
         //Session["ASS_EMP_NAME"]
         //Session["ASS_JOB_ID"]
         //Session["OPEN_YEAR"] 
         //Session["OPEN_VERSION"]
         //****************************************************************
         if (Session["Logged_User"] == null)
         {
             Response.Redirect("~/Default.aspx");
         }
         int APPROVER = ser.Check_Approver(Session["EMP_NO"].ToString());
         if (APPROVER == 0)
         { Response.Redirect("~/Setting/Unauthorized_Pg.aspx"); }
         Session["OPEN_YEAR"] = ser.Get_Ass_Open_Year().ToString();
         Session["OPEN_VERSION"] = ser.Get_Ass_Open_Version().ToString();

         // status 1 = تم التسجيل
         // status 0 = لم يتم التسجيل
         // status 2 = تمت الموافقة
     }
    protected void Page_Load(object sender, EventArgs e)
    {

        string ass_options = Request.QueryString["option"];

        if (!IsPostBack)
        {
            if (Session["Logged_User"] == null)
            {
                Response.Redirect("~/Default.aspx");
            }
            //****************************
            Year = Session["OPEN_YEAR"].ToString();
            Version = Session["OPEN_VERSION"].ToString();
            //lbl_open_year.Text = Year;
            //lbl_open_version.Text = Version;
            //*****************************
            Fill_Grid_ALL();
            Fill_Grid_NOT_APRV_ALL();
            //*****************************
        }

        Select_Options(ass_options);
    }
    private void Select_Options(string options)
    {
        if (options != null)
        {
            if (options == "0")
            {
                Fill_Grid_ALL();
                Fill_Grid_NOT_APRV_ALL();
            }
            else if (options == "1")
            {
                Fill_Grid_First_Level();
                Fill_Grid_NOT_APRV_First_Level();
            }
        }
    }

    private void Fill_Grid_ALL()
    {
        try
        {
            Clear_Grids();
            
            string EMP_NO = Session["EMP_NO"].ToString();
            dt = ser.Bind_New_ASS_APPROVE_EMP_GRID(EMP_NO , Session["OPEN_YEAR"].ToString(), Session["OPEN_VERSION"].ToString(), "1","ALL","ALL");
            GridView1.DataSource = dt;
            GridView1.DataBind();
        }
        catch
        {

        }
    }

    private void Fill_Grid_First_Level()
    {
        try
        {
            Clear_Grids();
            
            string EMP_NO = Session["EMP_NO"].ToString();
            dt = ser.Bind_New_ASS_APPROVE_EMP_FRST_LEVEL_GRID(EMP_NO , Session["OPEN_YEAR"].ToString(), Session["OPEN_VERSION"].ToString(), "1");
            GridView1.DataSource = dt;
            GridView1.DataBind();
        }
        catch
        {

        }
    }

    private void Fill_Grid_NOT_APRV_ALL()
    {
        try
        {
            Clear_Grids_Two();
            
            string EMP_NO = Session["EMP_NO"].ToString();
            Dst = ser.Bind_New_ASS_APPROVE_EMP_GRID(EMP_NO, Session["OPEN_YEAR"].ToString(), Session["OPEN_VERSION"].ToString(), "0","ALL","ALL");
            GridView2.DataSource = Dst;
            GridView2.DataBind();
        }
        catch
        {

        }
    }

    private void Fill_Grid_NOT_APRV_First_Level()
    {
        try
        {
            Clear_Grids_Two();
            
            string EMP_NO = Session["EMP_NO"].ToString();
            Dst = ser.Bind_New_ASS_APPROVE_EMP_FRST_LEVEL_GRID(EMP_NO, Session["OPEN_YEAR"].ToString(), Session["OPEN_VERSION"].ToString(), "0");
            GridView2.DataSource = Dst;
            GridView2.DataBind();
        }
        catch
        {

        }
    }

    private void Clear_Grids()
    {
        GridView1.DataSource = null;
        GridView1.DataBind();
    }
    private void Clear_Grids_Two()
    {
        GridView2.DataSource = null;
        GridView2.DataBind();
    }

    protected void btn_approve_Click(object sender, EventArgs e)
    {
        int APRV = 0;
        int APRV_STS = 0;
        int APRV_STS_LVL = 0;
        string confirmValue = Request.Form["confirm_value"];
        if (confirmValue == "No")
        {

        }
        else
        {
            foreach (GridViewRow row in GridView1.Rows)
            {
                CheckBox chk = row.Cells[2].FindControl("chk_data") as CheckBox;

                if (chk != null && chk.Checked)
                {
                    string emp_no = (row.Cells[2].FindControl("lblemp_no") as Label).Text;
                    string AprvLevel = (row.Cells[10].FindControl("lbllevel") as Label).Text;
                    if (AprvLevel.Trim() == "2")
                    {
                        APRV = ser.Make_ASS_Approve(emp_no, Session["OPEN_YEAR"].ToString(), Session["OPEN_VERSION"].ToString(), Session["Logged_User"].ToString());
                        if (APRV == 0) { APRV_STS++; }
                    }
                    else
                    {
                        APRV_STS_LVL++;
                    }
                  
                }
            }

            if (APRV == 1 && APRV_STS == 0)
            {

                Show_Modal_Message("تمت الموافقة بنجاح ");
                Fill_Grid_ALL();
                Fill_Grid_NOT_APRV_ALL();
            }
            else
                if (APRV_STS_LVL != 0)
                {
                    Show_Modal_Message(" لا يمكنك الموافقة علي التقييم ,, المدير المباشر فقط ");
                    return;
                }
            else
            {
                //Alert.Show(" لم يتم إختيار موظف للتقييم   ");
                Show_Modal_Message(" لم يتم إختيار موظف للتقييم ");
                return;
            }
        }
    }

    protected void GridView1_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
    protected void GridView1_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
    {

    }
    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {

    }
    protected void GridView1_DataBound(object sender, EventArgs e)
    {

    }
    protected void GridView1_DataBinding(object sender, EventArgs e)
    {

    }
    private void Show_Modal_Message(string msg)
    {
        try
        {
            lblMessage.Text = msg;//showConfModal() 
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "showModal();", true);
        }
        catch
        {

        }

    }
    private void Show_Modal_COnfirm(string msg)
    {
        try
        {
            lblConf_Message.Text = msg;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "showConfModal();", true);
        }
        catch
        {

        }

    }
    protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        //Determine the RowIndex of the Row whose Button was clicked.
        int rowIndex = Convert.ToInt32(e.CommandArgument);
        //Reference the GridView Row.
        GridViewRow row = GridView1.Rows[rowIndex];

        //Fetch value of Name.
        string Emp_Code = (row.FindControl("lblemp_no") as Label).Text;
        string Emp_Name = (row.FindControl("lblemp_name") as Label).Text;
        string Job_code = (row.FindControl("lbljob_code") as Label).Text;
        string Status = (row.FindControl("lblstatus") as Label).Text;
        string Level = (row.FindControl("lbllevel") as Label).Text;
        Year = Session["OPEN_YEAR"].ToString();
        Version = Session["OPEN_VERSION"].ToString();
        //*********************************************************************
        Session["ASS_APPAISAL_TYPE"] = ser.GetAppaisalType(Session["OPEN_YEAR"].ToString(), Session["OPEN_VERSION"].ToString() , Job_code).ToString();
        Session["ASS_Comp_Weight"] = ser.Get_Ass_Job_Comp_Weight(Session["OPEN_YEAR"].ToString(), Session["OPEN_VERSION"].ToString() , Job_code);
        Session["ASS_Obj_Weight"] = ser.Get_Ass_Job_Obj_Weight(Session["OPEN_YEAR"].ToString(), Session["OPEN_VERSION"].ToString() , Job_code);
        Session["ASS_EMP_ID"] = Emp_Code;
        Session["ASS_APRV_LVL"] = Level;
        //*********************************************************************
        if (e.CommandName == "Display")
        {
            Session["ASS_EMP_ID"] = Emp_Code;
            Session["ASS_EMP_NAME"] = Emp_Name;
            Session["ASS_JOB_ID"] = Job_code;
            Session["ASS_STATUS"] = Status;
            Session["Emp_Level"] = Level;
            Session["ASS_DISPLAY_ONLY"] = "Y";

            Response.Redirect("~/NewAssesment/dynamic_assesment.aspx");
        }
        else
        if (e.CommandName == "APPROVE")
         {
             if (Session["ASS_APRV_LVL"].ToString().Trim() == "2")
             {
                 Show_Modal_COnfirm("هل تريد الموافقة علي هذا التقييم ؟");
             }
             else
             {
                 Show_Modal_Message(" لا يمكنك الموافقة علي التقييم ,, المدير المباشر فقط ");
                 return;
             }
         }

    }
    protected void Button1_Click(object sender, EventArgs e)
    {
    //    Show_Modal_COnfirm("هل تريد الموافقة علي هذا التقييم ؟"); 
    }

    protected void btnapp_Click(object sender, EventArgs e)
    {


        int APRV = ser.Make_ASS_Approve(Session["ASS_EMP_ID"].ToString(), Session["OPEN_YEAR"].ToString(), Session["OPEN_VERSION"].ToString(), Session["Logged_User"].ToString());
        if (APRV == 1)
        {
            //data-toggle="modal" data-target="#confirm-delete"
            Show_Modal_Message("تمت الموافقة بنجاح ");
            Fill_Grid_ALL();
            Fill_Grid_NOT_APRV_ALL();
        }
        else
        {
            //Alert.Show(" هناك خطأ في عملية الموافقة برجاء الاتصال بادارة النظم ");
            Show_Modal_Message("هناك خطأ في عملية الموافقة برجاء الاتصال بادارة النظم ");
            return;
        }
      
    }
    protected void GridView2_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (MidYearVersionHelper.IsMidYearVersion(Session["OPEN_YEAR"].ToString(), Session["OPEN_VERSION"].ToString()))
        {
            this.GridView2.Columns[3].Visible = false;
            this.GridView2.Columns[4].Visible = false;
            this.GridView2.Columns[5].Visible = false;
        }
    }
    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (MidYearVersionHelper.IsMidYearVersion(Session["OPEN_YEAR"].ToString(), Session["OPEN_VERSION"].ToString()))
        {
            this.GridView1.Columns[7].Visible = false;
            this.GridView1.Columns[8].Visible = false;
            this.GridView1.Columns[9].Visible = false;
        }
    }
}
