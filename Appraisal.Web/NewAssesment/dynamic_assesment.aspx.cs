﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class NewAssesment_dynamic_assesment : System.Web.UI.Page
{

    #region feildes

    private readonly AppService _appService = new AppService();
    private string _emp_no = "";
    private string _year = "";
    private string _version = "";
    private string _appraisalType = "";
    protected string _blah = "";

    #endregion

    #region Page Events

    protected void Page_Init(object sender, EventArgs e)
    {
        if (Session["Logged_User"] == null || Session["ASS_EMP_ID"] == null || string.IsNullOrWhiteSpace(Session["ASS_EMP_ID"].ToString()))
            Response.Redirect("~/Default.aspx");

        _emp_no = Session["ASS_EMP_ID"].ToString().Trim();
        _appraisalType = Session["ASS_APPAISAL_TYPE"].ToString().Trim();
        _year = Session["OPEN_YEAR"].ToString().Trim();
        _version = Session["OPEN_VERSION"].ToString().Trim();

        lbl_emp_name.Text = Session["ASS_EMP_NAME"].ToString();
        lbl_emp_no.Text = _emp_no;
        lbl_year.Text = _year;
        lbl_version.Text = _version;
        lbltype.Text = _appraisalType;

        if (Session["ASS_STATUS"].ToString().Trim() == "2" || Session["ASS_DISPLAY_ONLY"].ToString().Trim() == "Y")
            btn_save.Visible = false;

        Objective_Pnl.Visible = false;
        if (_appraisalType == AppraisalTypes.ObjectivesAndComptences.ToString() || _appraisalType == AppraisalTypes.ObjectivesOnly.ToString())
        {
            Objective_Pnl.Visible = true;
            DisplayObjectivesData();
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if ((Session["Emp_Level"].ToString().Trim() != "2") && (Session["ASS_STATUS"].ToString().Trim() == "0" || Session["ASS_STATUS"].ToString().Trim() == "1"))
            {
                ShowModalMessage(" لا يمكنك إدخال التقييم لهذا الموظف , المستوي المباشر فقط ");
                return;
            }

            if (string.IsNullOrWhiteSpace(_year) || string.IsNullOrWhiteSpace(_version))
            {
                ShowModalMessage(" لا توجد سنه أو اصدار مفتوح للتقييم  ");
                Objective_Pnl.Visible = false;
                return;
            }
        }

    }

    protected void Page_LoadComplete(object sender, EventArgs e)
    {
       // if (_appraisalType == "A" || _appraisalType == "O")
            if (_appraisalType == AppraisalTypes.ObjectivesAndComptences.ToString() || _appraisalType == AppraisalTypes.ObjectivesOnly.ToString())
                Expand_GridTextBoxs();
    }

    protected void btn_save_Click(object sender, EventArgs e)
    {
        string errorMessage = ObjectivesValidationResult();
        if (!string.IsNullOrWhiteSpace(errorMessage))
        {
            ShowModalMessage(errorMessage);
            return;
        }
        else
        {
            bool result = SaveData();
            if (result)
                ShowModalMessage("تم حفظ البيانات بنجاح ");
            else
                ShowModalMessage("توجد مشكلة في عمليه حفظ البيانات");
        }
    }

    protected void btn_send_Click(object sender, EventArgs e)
    {
        string errorMessage = ObjectivesValidationResult();
        if (!string.IsNullOrWhiteSpace(errorMessage))
        {
            ShowModalMessage(errorMessage);
            return;
        }
        else
        {
            var result = SaveData(isFinalSave: true);
            if (result)
            {
                ShowModalMessage("تم فتح مشاهدة الاهداف للموظف، برجاء ابلاغه بذلك والطلب منه الدخول والموافقة على الاهداف.");
                DisableEditing();
            }
            else
                ShowModalMessage("توجد مشكلة في عمليه حفظ البيانات");
        }
    }

    #endregion

    #region Utilities

    private void DisplayObjectivesData()
    {
        try
        {
            DataTable odt = new DataTable();
            MyControles_Objective_Control OBJ = (MyControles_Objective_Control)Page.LoadControl("~/MyControles/Objective_Control.ascx");
            OBJ.ID = "OBJ_CONTROL_DATA";

            odt = _appService.Bind_ASS_EMP_Objectives_GRID(_emp_no, _year, _version);
            if (odt.Rows.Count == 0)
                odt = _appService.Mid_Bind_ASS_EMP_Objectives_GRID(_emp_no, _year, _version);

            string static_Target = _appService.Emp_Static_Target(_emp_no).Trim();

            if (static_Target == "1" || Session["ASS_STATUS"].ToString().Trim() == "2" || Session["ASS_DISPLAY_ONLY"].ToString().Trim() == "Y")
                OBJ.Static_Mode = "Y";

            if (odt.Rows.Count != 0)
            {
                OBJ.Display_Mode = "Y"; // display mode
                OBJ.dtCurrentTable = odt;
            }

            Objective_Pnl.Controls.Add(OBJ);

            bool isObjSentOrApproved = _appService.IsObjSentOrApproved(_emp_no, _year, _version);
            if (odt.Rows.Count > 0 && isObjSentOrApproved)
                DisableEditing();
        }
        catch (Exception ex)
        {
            string error = ex.Message;
        }
    }

    private void DisableEditing()
    {
        Objective_Pnl.Enabled = false;
        btn_save.Enabled = false;
        btn_send.Enabled = false;
        btnPrint.Visible = true;
    }

    private void Expand_GridTextBoxs()
    {
        MyControles_Objective_Control OBJC = (MyControles_Objective_Control)Objective_Pnl.FindControl("OBJ_CONTROL_DATA");
        if (OBJC != null && OBJC.GV_Objectes != null && OBJC.GV_Objectes.Rows.Count > 0)
        {
            string script = "window.onload = function() { ";
            foreach (GridViewRow row in OBJC.GV_Objectes.Rows)
            {
                string txt_obj_descID = ((TextBox)row.FindControl("txt_obj_desc")).ClientID;
                script += "AutoExpand(" + txt_obj_descID + ");";

                string txt_obj_notesID = ((TextBox)row.FindControl("txt_obj_notes")).ClientID;
                script += "AutoExpand(" + txt_obj_notesID + ");";
            }
            script += "};";
            Page.ClientScript.RegisterStartupScript(this.GetType(), "AutoExpand", script, true);
        }
    }

    private string ObjectivesValidationResult()
    {
        string obj_status1 = ValidateObjectives("LINE").ToString().Trim();
        string obj_status5 = ValidateObjectives("OBJ").ToString().Trim();
        string obj_status3 = ValidateObjectives("NOTE").ToString().Trim();

        //if (_appraisalType.Trim() == "A" || _appraisalType.Trim() == "O")
            if (_appraisalType == AppraisalTypes.ObjectivesAndComptences.ToString() || _appraisalType == AppraisalTypes.ObjectivesOnly.ToString())
            {
            if (obj_status1 == "0")
                return "مجموع الاوزان لابد ان يكون 100";

            if ((obj_status1 == "-1" || obj_status5 == "2" || obj_status1 == "2"))
                return "برجاء ادخال نص الهدف ";

            if (obj_status3 == "3")
                return ("برجاء ادخال الحقول الفارغة بالأهداف ");

            MyControles_Objective_Control OBJC = (MyControles_Objective_Control)Objective_Pnl.FindControl("OBJ_CONTROL_DATA");
            List<Tuple<string, string, string>> dbObjectivesList = new List<Tuple<string, string, string>>();

            // get all objectives from database for this emp
            var odt = _appService.Bind_ASS_EMP_Objectives_GRID(_emp_no, _year, _version);
            for (int i = 0; i < odt.Rows.Count; i++)
            {
                string dbObjective = odt.Rows[i]["obj_desc"].ToString().Trim();
                string dbWeight = odt.Rows[i]["obj_weight"].ToString().Trim();
                string dbNotes = odt.Rows[i]["obj_notes"].ToString().Trim();
                dbObjectivesList.Add(Tuple.New<string, string, string>(dbObjective, dbWeight, dbNotes));
            }

            var dbAllObjectives = dbObjectivesList.Select(x => x.Item1).ToList();
            foreach (GridViewRow row in OBJC.GV_Objectes.Rows)
            {
                string currentObjective = ((TextBox)row.FindControl("txt_obj_desc")).Text.Trim();
                string currentWeight = ((TextBox)row.FindControl("txt_obj_weight")).Text.Trim();
                string currentNote = ((TextBox)row.FindControl("txt_obj_notes")).Text.Trim();

                double _currentWeight;
                if (string.IsNullOrWhiteSpace(currentWeight) || !double.TryParse(currentWeight, out _currentWeight) || _currentWeight < 1)
                    return "يجب ادخال وزن الهدف برقم اكبر من صفر";

                
                int index = dbAllObjectives.IndexOf(currentObjective);
                bool isNewOrModified = index < 0;

                if (
                    (isNewOrModified && string.IsNullOrEmpty(currentNote)) ||
                    (!isNewOrModified && dbObjectivesList[index].Item2 != currentWeight && string.IsNullOrEmpty(currentNote))
                    )
                {
                    return " برجاء كتابة سبب عند إضافة أو تعديل هدف";
                }
                else if (!isNewOrModified && !string.IsNullOrEmpty(dbObjectivesList[index].Item3) && string.IsNullOrEmpty(currentNote))
                {
                    return "يجب الإحتفاظ بالسبب وعدم حذفه";

                }
            }
        }
        return string.Empty;
    }

    private string ValidateObjectives(string type)
    {
        string Result = "0";
        string Line_Result = "0";
        string Obj_Result = "0";
        string Degree_Result = "0";
        string Degree_MAX_Result = "0";
        string Note_Result = "0";

        int total = 0;
        double line_weight = 0;

        int Obj_line = 0;
        int note_line = 0;
        int degre_line = 0;
        int degre_max_line = 0;
        //  DataTable dt = new DataTable();
        //dt = (DataTable)ViewState["CurrentTable"];
        MyControles_Objective_Control OBJC = (MyControles_Objective_Control)Objective_Pnl.FindControl("OBJ_CONTROL_DATA");
        if (OBJC != null && OBJC.GV_Objectes != null && OBJC.GV_Objectes.Rows.Count > 0)
        {

            total = OBJC.GV_Objectes.Rows.Count;
            foreach (GridViewRow row in OBJC.GV_Objectes.Rows)
            {
                string obj_desc = ((TextBox)row.FindControl("txt_obj_desc")).Text.Trim();
                string obj_notes = ((TextBox)row.FindControl("txt_obj_notes")).Text.Trim();

                string obj_weightStr = ((TextBox)row.FindControl("txt_obj_weight")).Text.Trim();
                double obj_weight;
                if (!double.TryParse(obj_weightStr, out obj_weight))
                    obj_weight = 0;

                string obj_degreeStr = string.Empty;
                double obj_degree;
                if (!double.TryParse(obj_degreeStr, out obj_degree))
                    obj_degree = 0;

                line_weight += obj_weight;

                if (string.IsNullOrWhiteSpace(obj_desc))
                    Obj_line++;

                if (obj_degree == 0)
                    degre_line++;

                if (string.IsNullOrWhiteSpace(obj_notes))
                    note_line++;

                if (obj_degree > obj_weight) // الدرجة يجب الا تتعدي الوزن
                    degre_max_line++;
            }
        }
        if (total > 0)
        {
            if (line_weight != 100)
                Line_Result = "0";  // case of (  "مجموع الاوزان لابد ان يكون 100"

            if (line_weight == 100)
                Line_Result = "1";  // case of true way 

            if (Obj_line != 0)
            {
                Obj_Result = "2";
                Line_Result = "2";  // ahdaf gheer dakhla
            }

            if (degre_line != 0)
                Degree_Result = "3";  // dargat gheer dakhla

            if (note_line != 0)
                Note_Result = "4";   // asbab gheer dakhla

            if (degre_max_line != 0)
                Degree_MAX_Result = "5";   // الدرجة اكبر من 100

        }
        else
            Line_Result = "-1";  // case of no lines inserted

        type = type.Trim();
        if (type == "LINE")
            Result = Line_Result;

        if (type == "OBJ")
            Result = Obj_Result;

        if (type == "DEGREE")
            Result = Degree_Result;

        if (type == "NOTE")
            Result = Note_Result;

        if (type == "MAXDEGREE")
            Result = Degree_MAX_Result;

        return Result;
    }

    private bool SaveData(bool isFinalSave = false)
    {
        //if (_appraisalType == "A" || _appraisalType == "O")
            if (_appraisalType == AppraisalTypes.ObjectivesAndComptences.ToString() || _appraisalType == AppraisalTypes.ObjectivesOnly.ToString())
            {
            DataTable empObjectivesDT = new DataTable("OBJDT");
            empObjectivesDT.Columns.Add("SEQ");
            empObjectivesDT.Columns.Add("OBJ");
            empObjectivesDT.Columns.Add("WGT");
            empObjectivesDT.Columns.Add("DGR");
            empObjectivesDT.Columns.Add("NOT");
            empObjectivesDT.Columns.Add("DP");
            empObjectivesDT.Columns.Add("TP");
            empObjectivesDT.Rows.Clear();

            int line = 0;
            MyControles_Objective_Control OBJC = (MyControles_Objective_Control)Objective_Pnl.FindControl("OBJ_CONTROL_DATA");
            foreach (GridViewRow row in OBJC.GV_Objectes.Rows)
            {
                line++;
                string obj_desc = ((TextBox)row.FindControl("txt_obj_desc")).Text;
                string obj_weight = ((TextBox)row.FindControl("txt_obj_weight")).Text;
                string obj_notes = ((TextBox)row.FindControl("txt_obj_notes")).Text;

                empObjectivesDT.NewRow();
                empObjectivesDT.Rows.Add(line, obj_desc, obj_weight, string.Empty, obj_notes, string.Empty, string.Empty);
            }

            string assessor_name = Session["Logged_User"].ToString();
            bool dataSavedsuccessfully = _appService.SaveEmpMidYearObjectives(_emp_no, _year, _version, isFinalSave, empObjectivesDT, assessor_name);
            return dataSavedsuccessfully;
        }
        return false;
    }

    private void ShowModalMessage(string message)
    {
        if (string.IsNullOrWhiteSpace(message))
            return;
        lblMessage.Text = message;
        ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "showModal();", true);
    }

    #endregion

}