﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

using System.Drawing;
using System.Globalization;
using System.Web.Script.Serialization;

public partial class NewAssesment_Nass_Enter : System.Web.UI.Page
{
    AppService ser = new AppService();
    
    DataTable dt = new DataTable();
    public string[,] Ranksarray = new string[4, 2];
    public JavaScriptSerializer javaSerial = new JavaScriptSerializer();

    protected void Page_Load(object sender, EventArgs e)
    {
        string shlevel = Request.QueryString["lvl"];

        if (!IsPostBack)
        {
            if (Session["Logged_User"] == null)
            {
                Response.Redirect("~/Default.aspx");
            }
            //****************************
           // lbl_open_year.Text = ser.Get_Ass_Open_Year().ToString();
          //  lbl_open_version.Text = ser.Get_Ass_Open_Version().ToString();
            Session["OPEN_YEAR"] = ser.Get_Ass_Open_Year().ToString();
            Session["OPEN_VERSION"] = ser.Get_Ass_Open_Version().ToString();

            //****************************
            try
            {
                //Fill_Grid_First_Level("EGIC");
                //First_Level_EGIC();

                Fill_Grid_First_Level("ALL");
                First_Level_All_Companies();
            }
            catch
            {
                lbl_bad.Text = "0" + "%";
                lbl_good.Text = "0" + "%";
                lbl_very_good.Text = "0" + "%";
                lbl_exelent.Text = "0" + "%";
            }
         
        }

        Select_Options(shlevel);

        //string[,] Ranksarray = new string[4, 2]{
        //        { "ممتاز", lbl_exelent.Text.Replace("%","").ToString() }, { "جيد جدا", lbl_very_good.Text.Replace("%","").ToString() },
        //        { "جيد", lbl_good.Text.Replace("%","").ToString() } , { "متوسط - غير مرضي",  lbl_bad.Text.Replace("%","").ToString()  } };
    }

    private void Select_Options(string shlevel)
    {
        if (shlevel != null)
        {
            if (shlevel == "1") // مستوي اول
            {
                Fill_Grid_First_Level("ALL");
                First_Level_All_Companies();
            }
            else if (shlevel == "2")  // نتائج مستوي اول ايجيك
            {
                First_Level_EGIC();
                Fill_Grid_First_Level("EGIC");
            }
            else if (shlevel == "3")   // جميع مستويات ايجيك
            {
                Fill_Grid_ALL("EGIC");
                All_Levels_EGIC();
            }
            else if (shlevel == "4") // مستوي أول اوت سورس
            {
                Fill_Grid_First_Level("OutSource");
                First_Level_OutSource();
            }
            else if (shlevel == "5")  // جميع مستويات اوت سورس
            {
                Fill_Grid_ALL("OutSource");
                All_Levels_OutSource();
            }
            else if (shlevel == "0")  // جميع المستويات
            {
                Fill_Grid_ALL("ALL");
                All_Levels_ALL_Companies();
                //All_Levels_EGIC();
            }

        }
    }

    private void Fill_Grid_ALL(string company)
    {
        try
        {
            Clear_Grids();
            
            string EMP_NO = Session["EMP_NO"].ToString();
            dt = ser.Bind_New_ASS_ENTER_EMP_GRID(EMP_NO, Session["OPEN_YEAR"].ToString(), Session["OPEN_VERSION"].ToString(), company,"ALL");
            GridView1.DataSource = dt;
            GridView1.DataBind();
        }
        catch
        {

        }
    }

    private void Fill_Grid_First_Level(string company)
    {
        try
        {
            Clear_Grids();
            
            string EMP_NO = Session["EMP_NO"].ToString();
            dt = ser.Bind_New_ASS_ENTER_EMP_FRST_LEVEL_GRID(EMP_NO, Session["OPEN_YEAR"].ToString(), Session["OPEN_VERSION"].ToString(), company,"ALL");
            GridView1.DataSource = dt;
            GridView1.DataBind();
        }
        catch
        {

        }
    }

    private void Clear_Grids()
    {
        GridView1.DataSource = null;
        GridView1.DataBind();
    }
    //*****************************************************
    private void All_Levels_ALL_Companies()
    {
        try
        {
            int Emp_count = ser.Get_All_Emp_Level_Count(Session["EMP_NO"].ToString(), "ALL", "ALL");
            double bad = ser.Get_Ass_Bad_Results(Session["Logged_User"].ToString(), "00", Session["EMP_NO"].ToString(), Session["OPEN_YEAR"].ToString(), Session["OPEN_VERSION"].ToString(), "ALL", "ALL");
            double mid = ser.Get_Ass_Midum_Results(Session["Logged_User"].ToString(), "00", Session["EMP_NO"].ToString(), Session["OPEN_YEAR"].ToString(), Session["OPEN_VERSION"].ToString(), "ALL", "ALL");
            double god = ser.Get_Ass_Good_Results(Session["Logged_User"].ToString(), "00", Session["EMP_NO"].ToString(), Session["OPEN_YEAR"].ToString(), Session["OPEN_VERSION"].ToString(), "ALL", "ALL");
            double vgd = ser.Get_Ass_Very_Good_Results(Session["Logged_User"].ToString(), "00", Session["EMP_NO"].ToString(), Session["OPEN_YEAR"].ToString(), Session["OPEN_VERSION"].ToString(), "ALL", "ALL");
            double ext = ser.Get_Ass_Exellent_Results(Session["Logged_User"].ToString(), "00", Session["EMP_NO"].ToString(), Session["OPEN_YEAR"].ToString(), Session["OPEN_VERSION"].ToString(), "ALL", "ALL");
           // lblResultComp.Text = "نتائج موظفي جميع المستويات";
            if (Emp_count != 0)
            {
                bad = bad + mid;
                lbl_bad.Text = Math.Round((bad / Emp_count) * 100, 2) + "%";
                //lbl_midum.Text = Math.Round((mid / Emp_count) * 100, 2) + "%";
                lbl_good.Text = Math.Round((god / Emp_count) * 100, 2) + "%";
                lbl_very_good.Text = Math.Round((vgd / Emp_count) * 100, 2) + "%";
                lbl_exelent.Text = Math.Round((ext / Emp_count) * 100, 2) + "%";
            }
            else
            {
                lbl_bad.Text = "0" + "%";
                //lbl_midum.Text = "0" + "%";
                lbl_good.Text = "0" + "%";
                lbl_very_good.Text = "0" + "%";
                lbl_exelent.Text = "0" + "%";
            }
        }
        catch
        {
            lbl_bad.Text = "0" + "%";
            //lbl_midum.Text = "0" + "%";
            lbl_good.Text = "0" + "%";
            lbl_very_good.Text = "0" + "%";
            lbl_exelent.Text = "0" + "%";
        }
    }

    private void First_Level_All_Companies()
    {
        try
        {
            int Emp_count = ser.Get_First_Emp_Level_Count(Session["EMP_NO"].ToString(), "ALL", "ALL");
            double bad = ser.Get_Ass_Bad_Results(Session["Logged_User"].ToString(), "2", Session["EMP_NO"].ToString(), Session["OPEN_YEAR"].ToString(), Session["OPEN_VERSION"].ToString(), "ALL", "ALL");
            double mid = ser.Get_Ass_Midum_Results(Session["Logged_User"].ToString(), "2", Session["EMP_NO"].ToString(), Session["OPEN_YEAR"].ToString(), Session["OPEN_VERSION"].ToString(), "ALL", "ALL");
            double god = ser.Get_Ass_Good_Results(Session["Logged_User"].ToString(), "2", Session["EMP_NO"].ToString(), Session["OPEN_YEAR"].ToString(), Session["OPEN_VERSION"].ToString(), "ALL", "ALL");
            double vgd = ser.Get_Ass_Very_Good_Results(Session["Logged_User"].ToString(), "2", Session["EMP_NO"].ToString(), Session["OPEN_YEAR"].ToString(), Session["OPEN_VERSION"].ToString(), "ALL", "ALL");
            double ext = ser.Get_Ass_Exellent_Results(Session["Logged_User"].ToString(), "2", Session["EMP_NO"].ToString(), Session["OPEN_YEAR"].ToString(), Session["OPEN_VERSION"].ToString(), "ALL", "ALL");
           // lblResultComp.Text = "نتائج موظفي المستوي الأول جميع الشركات";
            if (Emp_count != 0)
            {
                bad = bad + mid;
                lbl_bad.Text = Math.Round((bad / Emp_count) * 100, 2) + "%";
                //lbl_midum.Text = Math.Round((mid / Emp_count) * 100, 2) + "%";
                lbl_good.Text = Math.Round((god / Emp_count) * 100, 2) + "%";
                lbl_very_good.Text = Math.Round((vgd / Emp_count) * 100, 2) + "%";
                lbl_exelent.Text = Math.Round((ext / Emp_count) * 100, 2) + "%";
                string[,] Ranksarray =  {
                { "إستثنائي", lbl_exelent.Text.Replace("%","").ToString() }, { "يفوق التوقعات", lbl_very_good.Text.Replace("%","").ToString() },
                { "يحقق التوقعات", lbl_good.Text.Replace("%","").ToString() } , { "متوسط – غير مرضي",  lbl_bad.Text.Replace("%","").ToString()  } };
            }
            else
            {
                lbl_bad.Text = "0" + "%";
                //lbl_midum.Text = "0" + "%";
                lbl_good.Text = "0" + "%";
                lbl_very_good.Text = "0" + "%";
                lbl_exelent.Text = "0" + "%";
            }
        }
        catch
        {
            lbl_bad.Text = "0" + "%";
            //lbl_midum.Text = "0" + "%";
            lbl_good.Text = "0" + "%";
            lbl_very_good.Text = "0" + "%";
            lbl_exelent.Text = "0" + "%";
        }
    }

    private void First_Level_EGIC()
    {
        try
        {
            int Emp_count = ser.Get_First_Emp_Level_Count(Session["EMP_NO"].ToString(), "EGIC", "ALL");
            double bad = ser.Get_Ass_Bad_Results(Session["Logged_User"].ToString(), "2", Session["EMP_NO"].ToString(), Session["OPEN_YEAR"].ToString(), Session["OPEN_VERSION"].ToString(), "EGIC", "ALL");
            double mid = ser.Get_Ass_Midum_Results(Session["Logged_User"].ToString(), "2", Session["EMP_NO"].ToString(), Session["OPEN_YEAR"].ToString(), Session["OPEN_VERSION"].ToString(), "EGIC", "ALL");
            double god = ser.Get_Ass_Good_Results(Session["Logged_User"].ToString(), "2", Session["EMP_NO"].ToString(), Session["OPEN_YEAR"].ToString(), Session["OPEN_VERSION"].ToString(), "EGIC", "ALL");
            double vgd = ser.Get_Ass_Very_Good_Results(Session["Logged_User"].ToString(), "2", Session["EMP_NO"].ToString(), Session["OPEN_YEAR"].ToString(), Session["OPEN_VERSION"].ToString(), "EGIC", "ALL");
            double ext = ser.Get_Ass_Exellent_Results(Session["Logged_User"].ToString(), "2", Session["EMP_NO"].ToString(), Session["OPEN_YEAR"].ToString(), Session["OPEN_VERSION"].ToString(), "EGIC", "ALL");
           // lblResultComp.Text = "نتائج موظفي EGIC مستوى اول";
            if (Emp_count != 0)
            {
                bad = bad + mid;
                lbl_bad.Text = Math.Round((bad / Emp_count) * 100, 2) + "%";
                //lbl_midum.Text = Math.Round((mid / Emp_count) * 100, 2) + "%";
                lbl_good.Text = Math.Round((god / Emp_count) * 100, 2) + "%";
                lbl_very_good.Text = Math.Round((vgd / Emp_count) * 100, 2) + "%";
                lbl_exelent.Text = Math.Round((ext / Emp_count) * 100, 2) + "%";

                string[,] Ranksarray =  {
                { "إستثنائي", lbl_exelent.Text.Replace("%","").ToString() }, { "يفوق التوقعات", lbl_very_good.Text.Replace("%","").ToString() },
                { "يحقق التوقعات", lbl_good.Text.Replace("%","").ToString() } , { "متوسط – غير مرضي",  lbl_bad.Text.Replace("%","").ToString()  } };
            }
            else
            {
                lbl_bad.Text = "0" + "%";
                //lbl_midum.Text = "0" + "%";
                lbl_good.Text = "0" + "%";
                lbl_very_good.Text = "0" + "%";
                lbl_exelent.Text = "0" + "%";
            }
        }
        catch
        {
            lbl_bad.Text = "0" + "%";
            //lbl_midum.Text = "0" + "%";
            lbl_good.Text = "0" + "%";
            lbl_very_good.Text = "0" + "%";
            lbl_exelent.Text = "0" + "%";
        }
    }


    private void All_Levels_EGIC()
    {
        try
        {
            int Emp_count = ser.Get_All_Emp_Level_Count(Session["EMP_NO"].ToString(), "EGIC", "ALL");
            double bad = ser.Get_Ass_Bad_Results(Session["Logged_User"].ToString(), "00", Session["EMP_NO"].ToString(), Session["OPEN_YEAR"].ToString(), Session["OPEN_VERSION"].ToString(), "EGIC", "ALL");
            double mid = ser.Get_Ass_Midum_Results(Session["Logged_User"].ToString(), "00", Session["EMP_NO"].ToString(), Session["OPEN_YEAR"].ToString(), Session["OPEN_VERSION"].ToString(), "EGIC", "ALL");
            double god = ser.Get_Ass_Good_Results(Session["Logged_User"].ToString(), "00", Session["EMP_NO"].ToString(), Session["OPEN_YEAR"].ToString(), Session["OPEN_VERSION"].ToString(), "EGIC", "ALL");
            double vgd = ser.Get_Ass_Very_Good_Results(Session["Logged_User"].ToString(), "00", Session["EMP_NO"].ToString(), Session["OPEN_YEAR"].ToString(), Session["OPEN_VERSION"].ToString(), "EGIC", "ALL");
            double ext = ser.Get_Ass_Exellent_Results(Session["Logged_User"].ToString(), "00", Session["EMP_NO"].ToString(), Session["OPEN_YEAR"].ToString(), Session["OPEN_VERSION"].ToString(), "EGIC", "ALL");
          //  lblResultComp.Text = "نتائج موظفي EGIC جميع المستويات";
            if (Emp_count != 0)
            {
                bad = bad + mid;
                lbl_bad.Text = Math.Round((bad / Emp_count) * 100, 2) + "%";
                //lbl_midum.Text = Math.Round((mid / Emp_count) * 100, 2) + "%";
                lbl_good.Text = Math.Round((god / Emp_count) * 100, 2) + "%";
                lbl_very_good.Text = Math.Round((vgd / Emp_count) * 100, 2) + "%";
                lbl_exelent.Text = Math.Round((ext / Emp_count) * 100, 2) + "%";
            }
            else
            {
                lbl_bad.Text = "0" + "%";
                //lbl_midum.Text = "0" + "%";
                lbl_good.Text = "0" + "%";
                lbl_very_good.Text = "0" + "%";
                lbl_exelent.Text = "0" + "%";
            }
        }
        catch
        {
            lbl_bad.Text = "0" + "%";
            //lbl_midum.Text = "0" + "%";
            lbl_good.Text = "0" + "%";
            lbl_very_good.Text = "0" + "%";
            lbl_exelent.Text = "0" + "%";
        }
    }

    private void First_Level_OutSource()
    {
        try
        {
            int Emp_count = ser.Get_First_Emp_Level_Count_OS(Session["EMP_NO"].ToString(), "ALL");
            double bad = ser.Get_Ass_Bad_Results(Session["Logged_User"].ToString(), "2", Session["EMP_NO"].ToString(), Session["OPEN_YEAR"].ToString(), Session["OPEN_VERSION"].ToString(), "OutSource", "ALL");
            double mid = ser.Get_Ass_Midum_Results(Session["Logged_User"].ToString(), "2", Session["EMP_NO"].ToString(), Session["OPEN_YEAR"].ToString(), Session["OPEN_VERSION"].ToString(), "OutSource", "ALL");
            double god = ser.Get_Ass_Good_Results(Session["Logged_User"].ToString(), "2", Session["EMP_NO"].ToString(), Session["OPEN_YEAR"].ToString(), Session["OPEN_VERSION"].ToString(), "OutSource", "ALL");
            double vgd = ser.Get_Ass_Very_Good_Results(Session["Logged_User"].ToString(), "2", Session["EMP_NO"].ToString(), Session["OPEN_YEAR"].ToString(), Session["OPEN_VERSION"].ToString(), "OutSource", "ALL");
            double ext = ser.Get_Ass_Exellent_Results(Session["Logged_User"].ToString(), "2", Session["EMP_NO"].ToString(), Session["OPEN_YEAR"].ToString(), Session["OPEN_VERSION"].ToString(), "OutSource", "ALL");
           // lblResultComp.Text = "نتائج موظفي OutSource مستوى اول";
            if (Emp_count != 0)
            {
                bad = bad + mid;
                lbl_bad.Text = Math.Round((bad / Emp_count) * 100, 2, MidpointRounding.AwayFromZero) + "%";
                //lbl_midum.Text = Math.Round((mid / Emp_count) * 100, 2) + "%";
                lbl_good.Text = Math.Round((god / Emp_count) * 100, 2, MidpointRounding.AwayFromZero) + "%";
                lbl_very_good.Text = Math.Round((vgd / Emp_count) * 100, 2, MidpointRounding.AwayFromZero) + "%";
                lbl_exelent.Text = Math.Round((ext / Emp_count) * 100, 2, MidpointRounding.AwayFromZero) + "%";

                string[,] Ranksarray =  {
                { "إستثنائي", lbl_exelent.Text.Replace("%","").ToString() }, { "يفوق التوقعات", lbl_very_good.Text.Replace("%","").ToString() },
                { "يحقق التوقعات", lbl_good.Text.Replace("%","").ToString() } , { "متوسط – غير مرضي",  lbl_bad.Text.Replace("%","").ToString()  } };
            }
            else
            {
                lbl_bad.Text = "0" + "%";
                lbl_good.Text = "0" + "%";
                lbl_very_good.Text = "0" + "%";
                lbl_exelent.Text = "0" + "%";
            }
        }
        catch
        {
            lbl_bad.Text = "0" + "%";
            //lbl_midum.Text = "0" + "%";
            lbl_good.Text = "0" + "%";
            lbl_very_good.Text = "0" + "%";
            lbl_exelent.Text = "0" + "%";
        }
    }

    private void All_Levels_OutSource()
    {
        try
        {
            int Emp_count = ser.Get_All_Emp_Level_Count_OS(Session["EMP_NO"].ToString(), "ALL");
            double bad = ser.Get_Ass_Bad_Results(Session["Logged_User"].ToString(), "00", Session["EMP_NO"].ToString(), Session["OPEN_YEAR"].ToString(), Session["OPEN_VERSION"].ToString(), "OutSource", "ALL");
            double mid = ser.Get_Ass_Midum_Results(Session["Logged_User"].ToString(), "00", Session["EMP_NO"].ToString(), Session["OPEN_YEAR"].ToString(), Session["OPEN_VERSION"].ToString(), "OutSource", "ALL");
            double god = ser.Get_Ass_Good_Results(Session["Logged_User"].ToString(), "00", Session["EMP_NO"].ToString(), Session["OPEN_YEAR"].ToString(), Session["OPEN_VERSION"].ToString(), "OutSource", "ALL");
            double vgd = ser.Get_Ass_Very_Good_Results(Session["Logged_User"].ToString(), "00", Session["EMP_NO"].ToString(), Session["OPEN_YEAR"].ToString(), Session["OPEN_VERSION"].ToString(), "OutSource", "ALL");
            double ext = ser.Get_Ass_Exellent_Results(Session["Logged_User"].ToString(), "00", Session["EMP_NO"].ToString(), Session["OPEN_YEAR"].ToString(), Session["OPEN_VERSION"].ToString(), "OutSource", "ALL");
            //lblResultComp.Text = "نتائج موظفي OutSource جميع المستويات";
            bad = bad + mid;
            lbl_bad.Text = Math.Round((bad / Emp_count) * 100, 2) + "%";
            //lbl_midum.Text = Math.Round((mid / Emp_count) * 100, 2) + "%";
            lbl_good.Text = Math.Round((god / Emp_count) * 100, 2) + "%";
            lbl_very_good.Text = Math.Round((vgd / Emp_count) * 100, 2) + "%";
            lbl_exelent.Text = Math.Round((ext / Emp_count) * 100, 2) + "%";
        }
        catch
        {
            lbl_bad.Text = "0" + "%";
            //lbl_midum.Text = "0" + "%";
            lbl_good.Text = "0" + "%";
            lbl_very_good.Text = "0" + "%";
            lbl_exelent.Text = "0" + "%";
        }
    }
    //*****************************************************
    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {

    }
    protected void GridView1_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
    protected void GridView1_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
    {

    }
    protected void CloseLinkClicked(Object sender, EventArgs e)
    {
        // onclick="EMP_no_Click"
        var closeLink = (Control)sender;
        GridViewRow row = (GridViewRow)closeLink.NamingContainer;
        string firstCellText = row.Cells[0].Text; // here we are
    }
    protected void EMP_no_Click(object sender, EventArgs e)
    {
        try
        {

            LinkButton btn = (LinkButton)sender;
            GridViewRow gvr = (GridViewRow)btn.NamingContainer;

            string Emp_Code = btn.Text;
            string Emp_Name = ((Label)gvr.FindControl("lblemp_name")).Text;
            string Job_Name = ((Label)gvr.FindControl("lbljob_name")).Text;
            string Job_code = ((Label)gvr.FindControl("lbljob_code")).Text;
            string Status = ((Label)gvr.FindControl("lblstatus")).Text;
            string FStatus = ((Label)gvr.FindControl("lblfstatus")).Text;

            // 0 = لم يتم التسجيل
            // 1 = تم التسجيل
            // 2 = تمت الموافقة

            string Level = ((Label)gvr.FindControl("lbllevel")).Text;

            //******************************************

            //LinkButton button = (LinkButton)sender;
            //string emp_code = button.Text ;
            //string emp_name = button.ToolTip;
            //string job_code = button.CommandName;

            Session["ASS_APPAISAL_TYPE"] = ser.GetAppaisalType(Session["OPEN_YEAR"].ToString(), Session["OPEN_VERSION"].ToString(), Job_code);
            Session["ASS_Comp_Weight"] = ser.Get_Ass_Job_Comp_Weight(Session["OPEN_YEAR"].ToString(), Session["OPEN_VERSION"].ToString(), Job_code);
            Session["ASS_Obj_Weight"] = ser.Get_Ass_Job_Obj_Weight(Session["OPEN_YEAR"].ToString(), Session["OPEN_VERSION"].ToString(), Job_code);

            Session["ASS_EMP_ID"] = Emp_Code;
            Session["ASS_EMP_NAME"] = Emp_Name;
            Session["ASS_JOB_ID"] = Job_code;
            Session["ASS_STATUS"] = Status;
            Session["Emp_Level"] = Level;
            if (Level.Trim() == "2")
            {
                Session["ASS_DISPLAY_ONLY"] = "N";
            }
            else
            {
                Session["ASS_DISPLAY_ONLY"] = "Y";
            }
            Response.Redirect("~/NewAssesment/dynamic_assesment.aspx");

        }
        catch 
        {

           
        }

    }
    protected void GridView1_DataBinding(object sender, EventArgs e)
    {
        
    }
    protected void GridView1_DataBound(object sender, EventArgs e)
    {

    }
    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
       
    }
    

    protected void GridView1_RowDataBound1(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            string company = e.Row.Cells[4].Text;

            foreach (TableCell cell in e.Row.Cells)
            {
                if (company.Trim() == "OutSource")
                {
                    
                    string colorcode = "#5DBE77";
                    int argb = Int32.Parse(colorcode.Replace("#", ""), NumberStyles.HexNumber);
                    Color clr = Color.FromArgb(argb);
                    cell.BackColor = clr;
                    cell.ForeColor = Color.White;
                }
            }
        }
        if (MidYearVersionHelper.IsMidYearVersion(Session["OPEN_YEAR"].ToString(), Session["OPEN_VERSION"].ToString()))
        {
            this.GridView1.Columns[8].Visible = false;
            this.GridView1.Columns[9].Visible = false;
        }
    }
    protected void btn_all_reslt_Click(object sender, EventArgs e)
    {
        First_Level_All_Companies();
    }
    protected void btn_egic_reslt_Click(object sender, EventArgs e)
    {
        First_Level_EGIC();
    }
    protected void btn_os_reslt_Click(object sender, EventArgs e)
    {
        First_Level_OutSource();
    }
}
