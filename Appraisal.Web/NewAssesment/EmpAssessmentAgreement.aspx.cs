﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Text;
using System.Web.Mail;
using System.Web.UI;
using System.Web.UI.WebControls;


public partial class NewAssesment_EmpAssessmentAgreement : System.Web.UI.Page
{
    AppService _appService = new AppService();
    string year = "";
    string version = "";
    string emp_no = "";
    DataTable empObjHeader = new DataTable();
    bool isObjSentOrApproved = false;

    #region Utilities

    private void UpdatePageContent()
    {
        
        if (empObjHeader.Rows.Count > 0 && empObjHeader.Rows[0]["EMP_FINAL"].ToString() == "1")
        {
            tblObjectives.Visible = true;
            tblNoObjectives.Visible = false;
            if(empObjHeader.Rows[0]["EMP_APPROVE"].ToString() == "1")
                pnlEmpAgreement.Enabled = false;
        }
        else
        {
            tblObjectives.Visible = false;
            tblNoObjectives.Visible = true;
            lblNoObjectivesMessage.Text =
                empObjHeader.Rows.Count > 0 && empObjHeader.Rows[0]["EMP_APPROVE"].ToString() == "0" && !string.IsNullOrWhiteSpace(empObjHeader.Rows[0]["EMP_REASON"].ToString()) ? 
                "يقوم المدير بمراجعة الأهداف، فضلاً انتظر حتي إرسال الأهداف من المدير" :
                "لم يتم إرسال الأهداف من المدير بعد...";
        }
    }

    private void DisplayObjectivesData()
    {
        try
        {
            MyControles_EmpObjectivesControl OBJ = (MyControles_EmpObjectivesControl)Page.LoadControl("~/MyControles/EmpObjectivesControl.ascx");
            OBJ.ID = "OBJ_CONTROL_DATA";

            DataTable odt = new DataTable();
            odt = _appService.GetEmployeeObjectives(emp_no);

            if (odt.Rows.Count > 0)
                OBJ.dtCurrentTable = odt;

            Objective_Pnl.Controls.Add(OBJ);
            Objective_Pnl.Enabled = false;
        }
        catch (Exception ex)
        {
            string error = ex.Message;
            ShowModalMessage(error);
        }
    }

    private void ShowModalMessage(string msg)
    {
        try
        {
            lblMessage.Text = msg;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "showModal();", true);
        }
        catch { }
    }

    #endregion

    protected void Page_Init(object sender, EventArgs e)
    {
        if (Session["EMP_NO"] == null)
        {
            ShowModalMessage("تأكد من تسجيل الدخول أولاً");
            return;
        }

        emp_no = Session["EMP_NO"].ToString().Trim();
        year = Session["OPEN_YEAR"].ToString().Trim();
        version = Session["OPEN_VERSION"].ToString().Trim();
        //lbl_emp_name.Text = Session["NAME"].ToString();
        //lbl_emp_no.Text = emp_no;
        //lbl_year.Text = Year;
        lbl_version.Text = version;

        empObjHeader = _appService.GetEmployeeObjectiveHeader(emp_no, year, version);
        isObjSentOrApproved = empObjHeader.Rows.Count > 0 && empObjHeader.Rows[0]["EMP_FINAL"].ToString() == "1";
    }



    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack && isObjSentOrApproved)
        {
            txtNotes.Text = empObjHeader.Rows[0]["EMP_REASON"].ToString();
            string isMeetingDoneStr = empObjHeader.Rows[0]["EMP_MEET"].ToString();
            if (isMeetingDoneStr == "1")
                rbMeetingDone.Checked = true;
            else if (isMeetingDoneStr == "0")
                rbMeetingNotDone.Checked = true;

            string isEmployeeAgreedStr = empObjHeader.Rows[0]["EMP_APPROVE"].ToString();
            if (isEmployeeAgreedStr == "1")
                rbAgree.Checked = true;
            else if (isEmployeeAgreedStr == "0")
                rbNotAgree.Checked = true;
        }
        if (string.IsNullOrWhiteSpace(year) || string.IsNullOrWhiteSpace(version))
        {
            ShowModalMessage(" لا توجد سنه أو اصدار مفتوح للتقييم  ");
            Objective_Pnl.Visible = false;
            return;
        }
        else
        {
            DisplayObjectivesData();
            UpdatePageContent();
        }

    }

    protected void btn_save_Click(object sender, EventArgs e)
    {
        if (!rbMeetingDone.Checked && !rbMeetingNotDone.Checked)
        {
            ShowModalMessage(" يجب تحديد هل تم الإجتماع ام لم يتم");
            return;
        }
        if (!rbAgree.Checked && !rbNotAgree.Checked)
        {
            ShowModalMessage(" يجب الرد بالموافقة او عدم الموافقة");
            return;
        }
        string note = txtNotes.Text.Trim();
        if (rbNotAgree.Checked && string.IsNullOrWhiteSpace(note))
        {
            ShowModalMessage(" يجب ادخال سبب عدم الموافقة على الأهداف ");
            return;
        }
        bool success = _appService.ApproveObjectiveByEmp(emp_no, year, version, rbMeetingDone.Checked, rbAgree.Checked, note);
        if (success)
        {
            if (rbNotAgree.Checked)
            {
                empObjHeader = _appService.GetEmployeeObjectiveHeader(emp_no, year, version);
                UpdatePageContent();
                SendMailToDirectManager();
            }
            pnlEmpAgreement.Enabled = false;
            ShowModalMessage(" تم الحفظ بنجاح");
        }
        else
            ShowModalMessage("فشل في حفظ البيانات");
    }

    private void SendMailToDirectManager()
    {
        try
        {
            var userInfo = _appService.GetUserInfo(emp_no);
            string empMail = ConfigurationManager.AppSettings["Appraisal_Mail"];//Session["MAIL"].ToString();

            bool isUserHasData = userInfo.Rows.Count > 0;

            string managerMail = isUserHasData ? userInfo.Rows[0]["MANAGER_MAIL"].ToString() : string.Empty;
            if (string.IsNullOrWhiteSpace(managerMail))
                managerMail = ConfigurationManager.AppSettings["HR_Appraisal_Mail"];

            if (string.IsNullOrWhiteSpace(empMail) || string.IsNullOrWhiteSpace(managerMail))
                return;

            string empName = isUserHasData ? userInfo.Rows[0]["EMP_NAME"].ToString() : string.Empty;
            if (string.IsNullOrWhiteSpace(empName))
                empName = emp_no;

            string subject = "أهداف التقييم النصف سنوي";
            string body = string.Format(@"<pre dir='rtl'>الرجاء العلم ان الموظف ({0}) غير موافق على الأهداف السنوية بسبب: {1}</pre>", empName, txtNotes.Text.Trim());
            MailMessage mm = new MailMessage();
            mm.To = managerMail;
            mm.From = empMail;
            mm.Bcc = ConfigurationManager.AppSettings["HR_Appraisal_Mail"];
            mm.Subject = subject;
            mm.Body = body;
            mm.BodyEncoding = Encoding.UTF8;
            mm.BodyFormat = MailFormat.Html;

            System.Web.Mail.SmtpMail.SmtpServer = "Mail2.egic.com.eg";
            System.Web.Mail.SmtpMail.Send(mm);
        }
        catch { }
    }
}